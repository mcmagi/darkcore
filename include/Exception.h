/* Exception.h */

#ifndef DARKCORE_EXCEPTION_H_
#define DARKCORE_EXCEPTION_H_

#include <exception>        // std::exception
#include <ios>              // std::ios_base
#include <sstream>          // std::ostringstream
#include <string>           // std::string

namespace darkcore
{
    class Exception : public std::exception
    {
      public:
        Exception();
        Exception(const std::string &message);
        Exception(const std::string &message, std::exception &cause);
        Exception(const Exception &ex);
        ~Exception() throw ();

        Exception & operator<<(const bool &msg);
        Exception & operator<<(const char &msg);
        Exception & operator<<(const char *msg);
        Exception & operator<<(const int &msg);
        Exception & operator<<(const unsigned int &msg);
        Exception & operator<<(const double &msg);
        Exception & operator<<(const std::string &msg);
        Exception & operator<<(std::ios_base & (*pf)(std::ios_base &));

        virtual const char * what() const throw ();
        std::exception * getCause() const throw ();
        virtual std::string getMessage() const throw ();

      private:
        std::ostringstream message;
        std::exception *cause;
    };

    // inline functions
    inline std::exception * Exception::getCause() const throw () { return cause; }

    class IndexException : public Exception
    {
      public:
        IndexException(const std::string &errmsg, int lower, int upper, int index);
        IndexException(const IndexException &ex);
        ~IndexException() throw ();

        int getLowerBound() const;
        int getUpperBound() const;
        int getPassedIndex() const;

        virtual std::string getMessage() const throw ();

      private:
        int lower_bound;
        int upper_bound;
        int passed_index;
    };

    // inline functions
    inline int IndexException::getLowerBound() const { return lower_bound; }
    inline int IndexException::getUpperBound() const { return upper_bound; }
    inline int IndexException::getPassedIndex() const { return passed_index; }

    class FileException : public Exception
    {
      public:
        FileException(const std::string &errmsg, const std::string &filename);
        FileException(const FileException &ex);
        ~FileException() throw ();

        std::string getFileName() const;

        virtual std::string getMessage() const throw ();

      private:
        // name of file that failed
        std::string filename;
    };

    // inline functions
    inline std::string FileException::getFileName() const { return filename; }

    class UnimplementedException : public Exception
    {
      public:
        UnimplementedException(const std::string &errmsg);
        UnimplementedException(const UnimplementedException &ex);
        ~UnimplementedException() throw ();

      private:
    };
}

#endif
