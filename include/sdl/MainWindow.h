/* MainWindow.h */

#ifndef DARKCORE_SDL_MAINWINDOW_H_
#define DARKCORE_SDL_MAINWINDOW_H_

#include "SDLWindow.h"

namespace darkcore
{
    namespace game
    {
        class GameData;
    }

    namespace sdl
    {
        class MapWindow;

        /**
         * Main SDL-based window.
         */
        class MainWindow : public SDLWindow
        {
          public:
            /**
             * Main window constructor with image dimensions.
             */
            MainWindow(int w, int h, const darkcore::game::GameData &data);

            /**
             * Copy-constructor.
             */
            MainWindow(const MainWindow &win);

            /**
             * Virtual destructor.
             */
            virtual ~MainWindow();

            /**
             * Virtual assignment operator.
             * @param win Other window
             * @return This window
             */
            virtual SDLWindow & operator=(const SDLWindow &win);
            virtual MainWindow & operator=(const MainWindow &win);

            virtual void display();

          private:
            MapWindow *mapWindow;
        };
    }
}

#endif
