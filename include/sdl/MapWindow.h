/* MapWindow.h */

#ifndef DARKCORE_SDL_MAPWINDOW_H_
#define DARKCORE_SDL_MAPWINDOW_H_

#include <memory>                   // std::unique_ptr
#include "SDLWindow.h"
#include "image/ImageFormat.h"      // RGBImageFormat

namespace darkcore
{
    namespace game
    {
        class GameData;
    }

    namespace image
    {
        class Image;
    }

    namespace sdl
    {
        /**
         * Map SDL-based window.
         */
        class MapWindow : public SDLWindow
        {
          public:
            /**
             * Default constructor.
             */
            MapWindow(const darkcore::game::GameData &data, SDLWindow &parent, int w, int h);

            /**
             * Copy-constructor.
             */
            MapWindow(const MapWindow &win);

            /**
             * Virtual destructor.
             */
            virtual ~MapWindow();

            /**
             * Virtual assignment operator.
             * @param win Other window
             * @return This window
             */
            virtual SDLWindow & operator=(const SDLWindow &win);
            virtual MapWindow & operator=(const MapWindow &win);

            virtual void display();

          private:
            void displayRGBImage();

            const darkcore::game::GameData *data;
            darkcore::image::RGBImageFormat rgbImgFmt;  // TODO: TO BE MOVED!
            std::unique_ptr<darkcore::image::Image> img;  // TODO: for rendering only.  fixme later
        };
    }
}

#endif
