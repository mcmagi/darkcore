/* SDLWindow.h */

#ifndef DARKCORE_SDL_SDLWINDOW_H_
#define DARKCORE_SDL_SDLWINDOW_H_

#include "SDL/SDL.h"

namespace darkcore
{
    namespace sdl
    {
        /**
         * Abstract base class of an SDL-based window.
         */
        class SDLWindow
        {
          public:
            /**
             * Virtual destructor.
             */
            virtual ~SDLWindow();

            /**
             * Virtual assignment operator.
             * @param win Other window
             * @return This window
             */
            virtual SDLWindow & operator=(const SDLWindow &win);

            virtual void display() = 0;

            const SDLWindow & getParent() const;
            SDLWindow & getParent();
            int getWidth() const;
            int getHeight() const;
            int getX() const;
            int getY() const;
            SDL_Surface * getSurface() const;

          protected:
            /**
             * Constructor for root window with dimensions.
             * @param parent Parent SDL window
             * @param w Width
             * @param h Height
             * @param x X position (default 0)
             * @param y Y position (default 0)
             */
            SDLWindow(int w, int h, int x = 0, int y = 0);

            /**
             * Constructor with sub-window with dimensions.
             * @param parent Parent SDL window
             * @param w Width
             * @param h Height
             * @param x X position (default 0)
             * @param y Y position (default 0)
             */
            SDLWindow(SDLWindow &parent, int w, int h, int x = 0, int y = 0);

            /**
             * Copy-constructor.
             */
            SDLWindow(const SDLWindow &win);

            void setSurface(SDL_Surface *surface);

            void freeSurface();

          private:
            SDLWindow *parent;
            int width;
            int height;
            int x;
            int y;
            SDL_Surface *surface;
        };

        inline const SDLWindow & SDLWindow::getParent() const { return *parent; }
        inline SDLWindow & SDLWindow::getParent() { return *parent; }
        inline int SDLWindow::getWidth() const { return width; }
        inline int SDLWindow::getHeight() const { return height; }
        inline int SDLWindow::getX() const { return x; }
        inline int SDLWindow::getY() const { return y; }
        inline SDL_Surface * SDLWindow::getSurface() const { return surface; }
    }
}

#endif
