/* SDLException.h */

#ifndef DARKCORE_SDL_SDLEXCEPTION_H_
#define DARKCORE_SDL_SDLEXCEPTION_H_

#include <string>			// std::string
#include "Exception.h"		// darkcore::Exception

namespace darkcore
{
    namespace sdl
    {
        /**
         * Abstract base class of an SDL-based window.
         */
        class SDLException : public darkcore::Exception
        {
          public:
            SDLException();
            SDLException(const SDLException &ex);
            ~SDLException() throw ();

          private:
        };
    }
}

#endif
