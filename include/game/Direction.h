/* Direction.h */

#ifndef DARKCORE_GAME_DIRECTION_H_
#define DARKCORE_GAME_DIRECTION_H_

#include <string>
#include "Coordinates.h"
//#include "Vector.h"

namespace darkcore
{
    namespace game
    {
        class Direction
        {
          public:
            static const Direction NONE;
            static const Direction NORTH;
            static const Direction SOUTH;
            static const Direction EAST;
            static const Direction WEST;
            static const Direction NORTHWEST;
            static const Direction NORTHEAST;
            static const Direction SOUTHWEST;
            static const Direction SOUTHEAST;
            static const Direction UP;
            static const Direction DOWN;

            /**
             * Copy constructs this direction from another.
             * @param dir Other direction
             */
            Direction(const Direction &dir);

            /**
             * Destructor.
             */
            ~Direction();

            /**
             * Assigns this direction with another.
             * @param dir Other direction
             * @return This direction
             */
            Direction & operator=(const Direction &dir);

            /**
             * Returns the direction as cartesian coordinates.
             * @return Coordinates
             */
            Coordinates getOffset() const;

            /**
             * Returns the direction as a unit vector.
             * @return Unit vector
             */
            //Vector asVector() const;

            /**
             * Returns true if the directions are equivalent.
             * @param dir Other direction
             * @return True if equivalent
             */
            bool operator==(const Direction &dir) const;

            /**
             * Returns true if the directions are not equivalent.
             * @param dir Other direction
             * @return True if not equivalent
             */
            bool operator!=(const Direction &dir) const;

            /**
             * Returns the Coordinates as a character array.
             * @return string
             */
            operator const char * () const;

            /**
             * Returns the Coordinates as a string.
             * @return string
             */
            std::string toString() const;

          private:
            /**
             * Constructs direction given a cartesian coordinate offset.
             * Private constructor ensures local instantiation only.
             * @param coords Coordinates
             */
            Direction(const Coordinates &coords);

            Coordinates coords;
        };

        inline Coordinates Direction::getOffset() const { return coords; }
        //inline Vector Direction::asVector() const { return Vector(*this); }
    }
}

#endif
