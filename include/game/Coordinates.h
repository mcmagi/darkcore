/* Coordinates.h */

#ifndef DARKCORE_GAME_COORDINATES_H_
#define DARKCORE_GAME_COORDINATES_H_

#include <string>

namespace darkcore
{
    namespace game
    {
        class Direction;
        //class Vector;

        /**
         * Represents the cartesian coordinates of a position in the game world.
         */
        class Coordinates
        {
          public:
            /**
             * Constructs coordinates.
             * @param x X coordinate
             * @param y Y coordinate
             * @param z Z coordinate
             */
            Coordinates(int x = 0, int y = 0, int z = 0);

            /**
             * Copy constructs these coordinates from another.
             * @param coords Other Coordinates
             */
            Coordinates(const Coordinates &coords);

            /**
             * Destructor.
             */
            ~Coordinates();

            /**
             * Assigns these coordinates with another.
             * @param coords Other Coordinates
             */
            Coordinates & operator=(const Coordinates &coords);

            /**
             * Returns the X coordinate.
             * @return X coordinate
             */
            int getX() const;

            /**
             * Returns the Y coordinate.
             * @return Y coordinate
             */
            int getY() const;

            /**
             * Returns the Z coordinate.
             * @return Z coordinate
             */
            int getZ() const;

            /**
             * Adds these coordinates with another.
             * @param coords Other Coordinates
             * @return New Coordinates
             */
            Coordinates operator+(const Coordinates &coords) const;

            /**
             * Adds these coordinates with a unit vector in the specified direction.
             * @param dir Direction
             * @return New Coordinates
             */
            Coordinates operator+(const Direction &dir) const;

            /**
             * Adds these coordinates with a vector
             * @param vect Vector
             * @return New Coordinates
             */
            //Coordinates operator+(const Vector &v) const;

            /**
             * Adds these coordinates with another.
             * @param coords Other Coordinates
             * @return These Coordinates
             */
            Coordinates & operator+=(const Coordinates &coords);

            /**
             * Adds these coordinates with a unit vector in the specified direction.
             * @param dir Direction
             * @return These Coordinates
             */
            Coordinates & operator+=(const Direction &dir);

            /**
             * Adds these coordinates with a vector
             * @param vect Vector
             * @return These Coordinates
             */
            //Coordinates & operator+=(const Vector &v);

            /**
             * Returns whether two coordinates are equivalent.
             * @return True if equivalent
             */
            bool operator==(const Coordinates &coords) const;

            /**
             * Returns whether two coordinates are not equivalent.
             * @return True if not equivalent
             */
            bool operator!=(const Coordinates &coords) const;

            /**
             * Returns the Coordinates as a character array.
             * @return string
             */
            operator const char * () const;

            /**
             * Returns the Coordinates as a string.
             * @return string
             */
            std::string toString() const;

          private:
            int x;
            int y;
            int z;
        };

        inline int Coordinates::getX() const { return x; }
        inline int Coordinates::getY() const { return y; }
        inline int Coordinates::getZ() const { return z; }
    }
}

#endif
