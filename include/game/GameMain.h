/* GameMain.h */

#ifndef DARKCORE_GAME_GAMEMAIN_H_
#define DARKCORE_GAME_GAMEMAIN_H_

#include <memory>               // std::unique_ptr
#include <string>               // std::string
#include "GameData.h"           // GameData

// prototypes
namespace darkcore
{
    namespace config
    {
        class UltimaGameConfig;
        class UltimaMapConfig;
    }

    namespace image
    {
        class ImageSet;
    }

    namespace map
    {
        class Map;
        class TextIndexMapper;
    }

    namespace sdl
    {
        class MainWindow;
    }

    namespace util
    {
        class File;
    }

    namespace game
    {
        class Coordinates;
    }
}

namespace darkcore
{
    namespace game
    {
        class GameMain
        {
          public:
            GameMain(const darkcore::config::UltimaGameConfig &cfg, const std::string &mapFilename,
                    const std::unique_ptr<darkcore::util::File> &textFile);
            ~GameMain();

            void start();

          private:
            GameMain(const GameMain &gm);
            GameMain & operator=(const GameMain &gm);

            bool validateCoordinates(Coordinates coords) const;
            void exportImage() const;

            static std::unique_ptr<darkcore::image::ImageSet> loadTileSet(
                const darkcore::config::UltimaMapConfig &mapCfg,
                int bpp = 0);
            static std::unique_ptr<darkcore::map::Map> loadMap(
                const darkcore::config::UltimaMapConfig &mapCfg,
                const std::unique_ptr<darkcore::util::File> &textFile);

            const darkcore::config::UltimaGameConfig &cfg;
            const darkcore::config::UltimaMapConfig &mapCfg;
            std::string mapFilename;
            std::unique_ptr<darkcore::image::ImageSet> tileSet;
            std::unique_ptr<darkcore::map::Map> map;
            GameData gameData;
            std::unique_ptr<darkcore::sdl::MainWindow> window;
            const std::unique_ptr<darkcore::util::File> &textFile;
        };
    }
}

#endif
