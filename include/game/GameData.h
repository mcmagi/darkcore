/* GameData.h */

#ifndef DARKCORE_GAME_GAMEDATA_H_
#define DARKCORE_GAME_GAMEDATA_H_

#include "Coordinates.h"        // Coordinates

namespace darkcore
{
    namespace map
    {
        class Map;
    }

    namespace image
    {
        class ImageSet;
    }

    namespace game
    {
        class GameData
        {
          public:
            GameData(const darkcore::map::Map &map, const darkcore::image::ImageSet &tileSet, const Coordinates &coords);
            GameData(const GameData &data);
            ~GameData();

            GameData & operator=(const GameData &data);

            const darkcore::map::Map & getMap() const;
            void setMap(const darkcore::map::Map &map);
            const darkcore::image::ImageSet & getTileSet() const;
            void setTileSet(const darkcore::image::ImageSet &tileSet);
            Coordinates & getCoordinates();
            const Coordinates & getCoordinates() const;
            bool isCGAComposite() const;
            void setCGAComposite(bool b);

          private:
            const darkcore::map::Map *map;
            const darkcore::image::ImageSet *tileSet;
            Coordinates coords;
            bool cgaComposite;
        };

        inline const darkcore::map::Map & GameData::getMap() const
        {
            return *map;
        }

        inline void GameData::setMap(const darkcore::map::Map &map)
        {
            this->map = &map;
        }

        inline const darkcore::image::ImageSet & GameData::getTileSet() const
        {
            return *tileSet;
        }

        inline void GameData::setTileSet(const darkcore::image::ImageSet &tileSet)
        {
            this->tileSet = &tileSet;
        }

        inline Coordinates & GameData::getCoordinates()
        {
            return coords;
        }

        inline const Coordinates & GameData::getCoordinates() const
        {
            return coords;
        }

        inline bool GameData::isCGAComposite() const
        {
            return cgaComposite;
        }

        inline void GameData::setCGAComposite(bool b)
        {
            cgaComposite = b;
        }
    }
}

#endif
