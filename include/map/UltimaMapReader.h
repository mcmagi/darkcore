/* UltimaMapReader.h */

#ifndef DARKCORE_MAP_ULTIMAMAPREADER_H_
#define DARKCORE_MAP_ULTIMAMAPREADER_H_

#include <memory>                   // std::unique_ptr
#include <string>                   // std::string
#include <vector>                   // std::vector
#include "MapReader.h"              // MapReader
#include "config/IndexEntry.h"      // IndexEntry

// prototypes
namespace darkcore
{
    namespace config
    {
        class UltimaMapConfig;
    }
    namespace map
    {
        class Map;
    };
}

namespace darkcore
{
    namespace map
    {
        class UltimaMapReader : public MapReader
        {
          public:
            static const int U5_WATER_TILE;
            static const int U5_WATER_INDEX;

            UltimaMapReader(const darkcore::config::UltimaMapConfig &cfg);
            ~UltimaMapReader();

            std::unique_ptr<Map> read();

            bool end() const;

          private:
            std::unique_ptr<Map> readMap();
            std::unique_ptr<Map> readChunkedMap();
            void loadFileChunks();

            const darkcore::config::UltimaMapConfig &cfg;
            std::unique_ptr<darkcore::util::Reader<darkcore::config::IndexEntry> > indexReader;
            std::vector<Map *> fileChunks;
        };
    }
}

#endif
