/* TextIndexMapper.h */

#ifndef DARKCORE_MAP_TEXTINDEXMAPPER_H_
#define DARKCORE_MAP_TEXTINDEXMAPPER_H_

#include <map>                      // std::map


// prototypes
namespace darkcore
{
    namespace config
    {
        class UltimaMapConfig;
    }
}

namespace darkcore
{
    namespace map
    {
        class TextIndexMapper
        {
          public:
            /**
             * For mapping U2 overworld maps.
             */
            static const TextIndexMapper U2_MAPPER;

            /**
             * For mapping U2 town/village/castle maps.
             */
            static const TextIndexMapper U2_TOWN_MAPPER;

            /**
             * For mapping U3 indoor maps.
             */
            static const TextIndexMapper U3_MAPPER;

            /**
             * Returns the text index mapper associated with the specified map config.
             * @param mapCfg Map config
             * @return Text index mapper
             */
            static const TextIndexMapper & forConfig(const darkcore::config::UltimaMapConfig &mapCfg);

            /**
             * Constructs a new mapper.
             */
            TextIndexMapper();

            /**
             * Destructs the mapper.
             */
            ~TextIndexMapper();

            /**
             * Adds a text/index mapping.
             * @param textValue The text value
             * @param intValue The index value
             * @return This instance of the mapper
             */
            TextIndexMapper & add(char textValue, int intValue);

            /**
             * Returns the tile index for the corresponding text value.
             * @param textValue The text value
             * @return The index value
             */
            int getTileIndex(char textValue) const;

            /**
             * Returns the text value for the corresponding index value.
             * @param intValue The index value
             * @return The text value
             */
            char getTextValue(int intValue) const;

          private:
            std::map<char,int> textToIndexMap;
            std::map<int,char> indexToTextMap;
        };
    }
}

#endif
