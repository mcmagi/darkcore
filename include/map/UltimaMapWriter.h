/* UltimaMapWriter.h */

#ifndef DARKCORE_MAP_ULTIMAMAPWRITER_H_
#define DARKCORE_MAP_ULTIMAMAPWRITER_H_

#include <memory>                   // std::unique_ptr
#include "MapWriter.h"              // MapWriter
#include "config/IndexEntry.h"      // IndexEntry

// prototypes
namespace darkcore
{
    namespace config
    {
        class UltimaMapConfig;
    }
    namespace map
    {
        class Map;
    }
}

namespace darkcore
{
    namespace map
    {
        class UltimaMapWriter : public MapWriter
        {
          public:
            UltimaMapWriter(const darkcore::config::UltimaMapConfig &cfg);
            ~UltimaMapWriter();

            void write(const Map &map);

          private:
            const darkcore::config::UltimaMapConfig &cfg;
            std::unique_ptr<darkcore::util::Writer<darkcore::config::IndexEntry> > indexWriter;
        };
    }
}

#endif
