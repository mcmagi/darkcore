/* Map.h */

#ifndef DARKCORE_MAP_MAP_H_
#define DARKCORE_MAP_MAP_H_

#include <vector>           // std::vector

namespace darkcore
{
    namespace image
    {
        class ImageSet;
    }

    namespace map
    {
        class MapImageBuilder;

        class Map
        {
          public:
            /**
             * Creates an empty map with the specified dimensions.
             * @param w Width
             * @param h Height
             */
            Map(int w, int h);

            /**
             * Creates a map with the specified dimensions and content.
             * @param w Width
             * @param h Height
             * @param data Map data
             */
            Map(int w, int h, int *data);

            /**
             * Creates a map consisting of "chunks" of map content.
             * Chunks will be tiled into the new map, arranged in the
             * specified number of columns.
             * @param chunks Map chunks
             * @param columns # of columns of chunks
             */
            Map(const std::vector<Map *> &chunks, int columns = 1);

            /**
             * Copy-constructs this map from another.
             * @param map Other map
             */
            Map(const Map &map);

            /**
             * Destructor.
             */
            ~Map();

            /**
             * Assigns another map to this one.
             * @param map Other map
             * @return This map
             */
            Map & operator=(const Map &map);

            /**
             * Returns the number of columns in the map.
             * @return Width
             */
            int getWidth() const;

            /**
             * Returns the number of rows in the map.
             * @return Height
             */
            int getHeight() const;

            /**
             * Returns the size of the map (necessarily width * height).
             * @return Size of map
             */
            int getSize() const;

            /**
             * Returns the image id at the specified coordinates.
             * @param x Column
             * @param y Row
             * @return Image id
             */
            int getImageId(int x, int y) const;

            /**
             * Sets the image id at the specified coordinates.
             * @param x Column
             * @param y Row
             * @param imageId Image id
             */
            void setImageId(int x, int y, int imageId);

            /**
             * Returns a map image builder for this map and the specified tileset.
             * @param tileSet An ImageSet represetnting the map's tileset
             * @return Map image builder
             */
            MapImageBuilder getMapImageBuilder(const darkcore::image::ImageSet &tileSet) const;

            void dump() const;

          private:
            /**
             * Calculats the index into the map data given cartesian coordinates.
             * @param x Column position
             * @param y Row position
             */
            int calculateIndex(int x, int y) const;

            int width;
            int height;
            int size;
            int *mapData;
        };

        inline int Map::getWidth() const { return width; }
        inline int Map::getHeight() const { return height; }
        inline int Map::getSize() const { return size; }
        inline int Map::getImageId(int x, int y) const { return mapData[calculateIndex(x,y)]; }
        inline void Map::setImageId(int x, int y, int imageId) { mapData[calculateIndex(x,y)] = imageId; }
    }
}

#endif
