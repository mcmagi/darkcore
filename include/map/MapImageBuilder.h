/* MapImageBuilder.h */

#ifndef DARKCORE_MAP_MAPIMAGEBUILDER_H_
#define DARKCORE_MAP_MAPIMAGEBUILDER_H_

#include <memory>                   // std::unique_ptr
#include "image/ImageSet.h"

namespace darkcore
{
    namespace image
    {
        class Image;
    }

    namespace map
    {
        class Map;

        class MapImageBuilder
        {
          public:
            /**
             * Creates a map image builder for a given map and tileset.
             * @param map Map
             * @param tileSet Tile set
             */
            MapImageBuilder(const Map &map, const darkcore::image::ImageSet &tileSet);

            /**
             * Copy-constructs this builder from another.
             * @param mib Other builder
             */
            MapImageBuilder(const MapImageBuilder &mib);

            /**
             * Destructor.
             */
            ~MapImageBuilder();

            /**
             * Returns the map used to construct the image.
             * @return Map
             */
            const Map & getMap() const;

            /**
             * Returns the tileset used to construct the image.
             * @return ImageSet
             */
            const darkcore::image::ImageSet & getImageSet() const;

            /**
             * Returns the Image at the specified map coordinates.
             * @param x Map column
             * @param y Map row
             * @return Image
             */
            const darkcore::image::Image & getImage(int x, int y) const;

            /**
             * Returns the entire map as an Image.  The returned image must
             * be explicitly deleted.
             * @return An image of the entire map
             */
            std::unique_ptr<darkcore::image::Image> createMapImage() const;

            /**
             * Returns a section of the map as an Image.  The returned image must
             * be explicitly deleted.
             * @return An image of the map section
             */
            std::unique_ptr<darkcore::image::Image> createMapImage(int x, int y, int w, int h) const;

          private:
            /**
             * Not implemented.
             * @param mib Other builder
             * @return This builder
             */
            MapImageBuilder & operator=(const MapImageBuilder &mib);

            const Map &map;
            const darkcore::image::ImageSet &tileSet;
        };

        inline const Map & MapImageBuilder::getMap() const { return map; }
        inline const darkcore::image::ImageSet & MapImageBuilder::getImageSet() const { return tileSet; }
        inline const darkcore::image::Image & MapImageBuilder::getImage(int x, int y) const { return tileSet.getImage(map.getImageId(x,y)); }
    }
}

#endif
