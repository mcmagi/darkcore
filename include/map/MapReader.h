/* MapReader.h */

#ifndef DARKCORE_MAP_MAPREADER_H_
#define DARKCORE_MAP_MAPREADER_H_

#include <memory>                   // std::unique_ptr
#include "util/Reader.h"            // Reader

// prototypes
namespace darkcore
{
    namespace map
    {
        class Map;
    }
}

namespace darkcore
{
    namespace map
    {
        class MapReader : public darkcore::util::Reader<std::unique_ptr<Map> >
        {
          public:
            MapReader();
            virtual ~MapReader();

            virtual std::unique_ptr<Map> read() = 0;

            virtual bool end() const = 0;
        };

        inline MapReader::MapReader()
            : Reader<std::unique_ptr<Map> >()
        {
        }

        inline MapReader::~MapReader()
        {
        }
    }
}

#endif
