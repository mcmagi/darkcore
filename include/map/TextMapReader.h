/* TextMapReader.h */

#ifndef DARKCORE_MAP_TEXTMAPREADER_H_
#define DARKCORE_MAP_TEXTMAPREADER_H_

#include <istream>                  // std::ifstream
#include <memory>                   // std::unique_ptr
#include "MapReader.h"              // MapReader
#include "util/File.h"              // File

// prototypes
namespace darkcore
{
    namespace map
    {
        class Map;
        class TextIndexMapper;
    }
}

namespace darkcore
{
    namespace map
    {
        class TextMapReader : public MapReader
        {
          public:
            TextMapReader(const darkcore::util::File &file, const TextIndexMapper &mapper, int width, int height);
            ~TextMapReader();

            std::unique_ptr<Map> read();

            bool end() const;

            static bool isWhitespace(char c);

          private:
            std::unique_ptr<std::ifstream> inputStream;
            const TextIndexMapper &mapper;
            int width;
            int height;
            bool isDone;
        };
    }
}

#endif
