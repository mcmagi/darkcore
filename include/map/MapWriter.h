/* MapWriter.h */

#ifndef DARKCORE_MAP_MAPWRITER_H_
#define DARKCORE_MAP_MAPWRITER_H_

#include <memory>                   // std::unique_ptr
#include "util/Writer.h"            // Writer

// prototypes
namespace darkcore
{
    namespace map
    {
        class Map;
    }
}

namespace darkcore
{
    namespace map
    {
        class MapWriter : public darkcore::util::Writer<Map>
        {
          public:
            MapWriter();
            virtual ~MapWriter();

            /**
             * Writes a map object to the output stream.
             * @param map Map
             */
            virtual void write(const Map &map) = 0;
        };

        inline MapWriter::MapWriter()
            : Writer<Map>()
        {
        }

        inline MapWriter::~MapWriter()
        {
        }
    }
}

#endif
