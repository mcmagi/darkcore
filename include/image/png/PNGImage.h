/* PNGImage.h */

#ifndef DARKCORE_IMAGE_PNG_PNGIMAGE_H_
#define DARKCORE_IMAGE_PNG_PNGIMAGE_H_

#include <png.h>
#include <vector>               // std::vector
#include "Exception.h"          // UnimplementedException

// prototypes
namespace darkcore
{
    namespace image
    {
        namespace png
        {
            class PNGInfo;
        }
    }
}

namespace darkcore
{
    namespace image
    {
        namespace png
        {
            class PNGImage
            {
              public:
                PNGImage(const PNGInfo &info);
                ~PNGImage();

                /**
                 * Returns the image data as a double-pointer to the byte array.
                 * @return
                 */
                operator png_bytep *();

                /**
                 * Returns the image data as a two-dimensional vector.
                 */
                std::vector<std::vector<png_byte> > & getData();

              private:
                /**
                 * Copy constructor.
                 * @throws UnimplementedException
                 */
                PNGImage(const PNGImage &img);

                /**
                 * Assignment operator.
                 * @throws UnimplementedException
                 */
                PNGImage & operator=(const PNGImage &img);

                std::vector<std::vector<png_byte> > data;
                std::vector<png_bytep> rowptrs;
            };

            inline PNGImage::PNGImage(const PNGImage &img)
            {
                throw darkcore::UnimplementedException(__FUNCTION__);
            }

            inline PNGImage & PNGImage::operator=(const PNGImage &img)
            {
                throw darkcore::UnimplementedException(__FUNCTION__);
            }

            inline PNGImage::operator png_bytep *()
            {
                return &rowptrs[0];
            }

            inline std::vector<std::vector<png_byte> > & PNGImage::getData()
            {
                return data;
            }
        }
    }
}

#endif
