/* PNGReader.h */

#ifndef DARKCORE_IMAGE_PNG_PNGREADER_H_
#define DARKCORE_IMAGE_PNG_PNGREADER_H_

#include <png.h>
#include <istream>                  // std::istream
#include <memory>                   // std::unique_ptr
#include <vector>                   // std::vector
#include "PNGContext.h"             // PNGContext
#include "PNGInfo.h"                // PNGInfo
#include "image/Color.h"            // Color, Pixel
#include "image/ImageIO.h"          // ImageReader

// prototypes
namespace darkcore
{
    namespace image
    {
        class Color;
        class Image;
    }
}

namespace darkcore
{
    namespace image
    {
        namespace png
        {
            class PNGReader : public ImageReader
            {
              public:
                static const int PNG_HDR_SZ;

                PNGReader(std::istream &is);

                /**
                 * Destructor.
                 */
                ~PNGReader();

                /**
                 * Reads an image from the input stream.
                 * @return Image
                 */
                std::unique_ptr<Image> read();

                /**
                 * Returns true if the image has been read.
                 * @return True if read
                 */
                bool end() const;

                const PNGInfo & getPNGInfo() const;

                PNGInfo & getPNGInfo();

              private:
                static void userReadData(png_structp png_ptr, png_bytep data,
                        png_size_t length);

                static Color mapColor(png_color color);

                void buildImageData(const std::vector<std::vector<png_byte> > &pngdata, std::vector<Pixel> &imgdata) const;
            
                PNGContext pngCtx;
                PNGInfo pngInfo;
                bool imgRead;
            };

            inline const PNGInfo & PNGReader::getPNGInfo() const
            {
                return pngInfo;
            }

            inline PNGInfo & PNGReader::getPNGInfo()
            {
                return pngInfo;
            }

            inline bool PNGReader::end() const
            {
                return imgRead;
            }
        }
    }
}

#endif