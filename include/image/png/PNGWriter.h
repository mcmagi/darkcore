/* PNGWriter.h */

#ifndef DARKCORE_IMAGE_PNG_PNGWRITER_H_
#define DARKCORE_IMAGE_PNG_PNGWRITER_H_

#include <png.h>
#include <ostream>                  // std::ostream
#include <vector>                   // std::vector
#include "PNGContext.h"             // PNGContext
#include "PNGInfo.h"                // PNGInfo
#include "image/Color.h"            // Color, Pixel
#include "image/ImageIO.h"          // ImageWriter

// prototypes
namespace darkcore
{
    namespace image
    {
        class Color;
        class Image;
    }
}

namespace darkcore
{
    namespace image
    {
        namespace png
        {
            class PNGWriter : public ImageWriter
            {
              public:
                PNGWriter(std::ostream &is);

                /**
                 * Destructor.
                 */
                ~PNGWriter();

                void write(const Image &pngImg);

                const PNGInfo & getPNGInfo() const;

                PNGInfo & getPNGInfo();

              private:
                static void userWriteData(png_structp png_ptr, png_bytep data,
                        png_size_t length);
                static void userFlushData(png_structp png_ptr);

                void buildImageData(const std::vector<Pixel> &imgdata, std::vector<std::vector<png_byte> > &pngdata) const;
                static png_color mapColor(Color color);

                PNGContext pngCtx;
                PNGInfo pngInfo;
                bool imgWritten;
            };

            inline const PNGInfo & PNGWriter::getPNGInfo() const
            {
                return pngInfo;
            }

            inline PNGInfo & PNGWriter::getPNGInfo()
            {
                return pngInfo;
            }
        }
    }
}

#endif
