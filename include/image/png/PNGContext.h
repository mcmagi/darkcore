/* PNGContext.h */

#ifndef DARKCORE_IMAGE_PNG_PNGCONTEXT_H_
#define DARKCORE_IMAGE_PNG_PNGCONTEXT_H_

#include <png.h>
#include <memory>           // std:unique_ptr
#include "Exception.h"      // UnimplementedException

namespace darkcore
{
    namespace image
    {
        namespace png
        {
            class PNGContext
            {
              public:
                enum AccessType { READ, WRITE };

                /**
                 * Constructs a PNG Context with the specified access type.
                 * @param type
                 */
                PNGContext(AccessType type);

                /**
                 * Destructor.
                 */
                ~PNGContext();

                /**
                 * png_structp cast operator.
                 * @return PNG struct as png_structp struct pointer
                 */
                operator png_structp() const;

                /**
                 * Returns the access type (read or write).
                 * @return Access type
                 */
                AccessType getAccessType() const;

              private:
                /**
                 * Copy constructor.
                 * @throws UnimplmentedException
                 */
                PNGContext(const PNGContext &info);

                /**
                 * Assignment operator.
                 * @throws UnimplmentedException
                 */
                PNGContext & operator=(const PNGContext &info);

                /**
                 * Creates the png struct pointer.
                 * @param type Access type
                 * @return png ptr
                 */
                static png_structp createPNGStruct(AccessType type);

                AccessType type;
                png_structp png_ptr;
            };

            inline PNGContext::PNGContext(const PNGContext &ctx) :
                type(ctx.type), png_ptr(NULL)
            {
                throw darkcore::UnimplementedException(__FUNCTION__);
            }

            inline PNGContext & PNGContext::operator=(const PNGContext &ctx)
            {
                throw darkcore::UnimplementedException(__FUNCTION__);
            }

            inline PNGContext::AccessType PNGContext::getAccessType() const
            {
                return type;
            }

            inline PNGContext::operator png_structp() const
            {
                return png_ptr;
            }
        }
    }
}

#endif