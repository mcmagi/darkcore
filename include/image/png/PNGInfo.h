/* PNGInfo.h */

#ifndef DARKCORE_IMAGE_PNG_PNGINFO_H_
#define DARKCORE_IMAGE_PNG_PNGINFO_H_

#include <png.h>
#include <memory>                   // std::unique_ptr
#include "Exception.h"              // UnimplementedException

// prototypes
namespace darkcore
{
    namespace image
    {
        class ImageFormat;

        namespace png
        {
            class PNGContext;
            class PNGPalette;
        }
    }
}

namespace darkcore
{
    namespace image
    {
        namespace png
        {
            class PNGInfo
            {
              public:
                PNGInfo(const PNGContext &ctx);

                /**
                 * Destructor.
                 */
                ~PNGInfo();

                /**
                 * png_infop cast operator.
                 * @return PNG info as png_infop struct pointer
                 */
                operator png_infop() const;

                png_uint_32 getWidth() const;
                png_uint_32 getHeight() const;
                png_byte getColorType() const;
                png_byte getBitDepth() const;
                png_byte getChannels() const;
                png_byte getFilterType() const;
                png_byte getCompressionType() const;
                png_byte getInterlaceType() const;
                png_uint_32 getRowBytes() const;

                void configure(png_uint_32 width, png_uint_32 height, png_byte depth, png_byte colorType);

                // palette operations
                std::unique_ptr<PNGPalette> getPalette() const;
                void setPalette(const PNGPalette &pal);

              private:
                /**
                 * Copy constructor.
                 * @throws UnimplmentedException
                 */
                PNGInfo(const PNGInfo &info);

                /**
                 * Assignment operator.
                 * @throws UnimplmentedException
                 */
                PNGInfo & operator=(const PNGInfo &info);

                /**
                 * Creates the PNG info struct pointer.
                 * @param png_ptr PNG struct pointer
                 * @param PNG info pointer
                 */
                static png_infop createInfoStruct(png_structp png_ptr);

                const PNGContext &pngCtx;
                png_infop info_ptr;
                std::shared_ptr<darkcore::image::ImageFormat> format;
            };

            inline PNGInfo::PNGInfo(const PNGInfo &info) :
                pngCtx(info.pngCtx), info_ptr(NULL)
            {
                throw darkcore::UnimplementedException(__FUNCTION__);
            }

            inline PNGInfo & PNGInfo::operator=(const PNGInfo &info)
            {
                throw darkcore::UnimplementedException(__FUNCTION__);
            }

            inline PNGInfo::operator png_infop() const
            {
                return info_ptr;
            }
        }
    }
}

#endif