/* PNGException.h */

#ifndef DARKCORE_IMAGE_PNG_PNGEXCEPTION_H_
#define DARKCORE_IMAGE_PNG_PNGEXCEPTION_H_

#include <string>           // std::string
#include "Exception.h"      // Exception

namespace darkcore
{
    namespace image
    {
        namespace png
        {
            class PNGException : public Exception
            {
              public:
                PNGException(const std::string &message);
                PNGException(const PNGException &ex);
                ~PNGException() throw ();

              private:
            };
        }
    }
}

#endif