/* PNGPalette.h */

#ifndef DARKCORE_IMAGE_PNG_PNGPALETTE_H_
#define DARKCORE_IMAGE_PNG_PNGPALETTE_H_

#include <png.h>
#include <memory>           // std:unique_ptr
#include "Exception.h"      // UnimplementedException

// prototypes
namespace darkcore
{
    namespace image
    {
        class ImageFormat;

        namespace png
        {
            class PNGContext;
            class PNGInfo;
        }
    }
}

namespace darkcore
{
    namespace image
    {
        namespace png
        {
            class PNGPalette
            {
              public:
                /**
                 * Constructs a PNG Palette in read mode.
                 * @param pngCtx
                 * @param pngInfo
                 */
                PNGPalette(const PNGContext &pngCtx, const PNGInfo &pngInfo);

                /**
                 * Constructs a PNG Palette in write mode.
                 * @param pngCtx
                 * @param size
                 */
                PNGPalette(const PNGContext &pngCtx, int size);

                /**
                 * Destructor.
                 */
                ~PNGPalette();

                /**
                 * Returns the color at the specified index.
                 * @param index Palette index
                 * @return Color
                 */
                png_color getColor(int index) const;

                /**
                 * png_colorp cast operator.
                 * @return PNG palette as png_color array pointer
                 */
                operator png_colorp() const;

                /**
                 * Sets the color at the specified index.
                 * @param index Palette index
                 * @param color Color
                 */
                void setColor(int index, png_color color);

                /**
                 * Returns the number of colors in the palette.
                 * @return Number of colors
                 */
                int getNumColors() const;

              private:
                /**
                 * Copy constructor.
                 * @throws UnimplmentedException
                 */
                PNGPalette(const PNGPalette &info);

                /**
                 * Assignment operator.
                 * @throws UnimplmentedException
                 */
                PNGPalette & operator=(const PNGPalette &info);

                const PNGContext &pngCtx;
                png_color *palette;
                int num_colors;
            };

            inline PNGPalette::PNGPalette(const PNGPalette &pal) :
                pngCtx(pal.pngCtx), palette(NULL), num_colors(0)
            {
                throw darkcore::UnimplementedException(__FUNCTION__);
            }

            inline PNGPalette & PNGPalette::operator=(const PNGPalette &ctx)
            {
                throw darkcore::UnimplementedException(__FUNCTION__);
            }

            inline PNGPalette::operator png_colorp() const
            {
                return palette;
            }

            inline int PNGPalette::getNumColors() const
            {
                return num_colors;
            }
        }
    }
}

#endif