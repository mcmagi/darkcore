/* ImageSet.h */

#ifndef DARKCORE_IMAGE_IMAGESET_H_
#define DARKCORE_IMAGE_IMAGESET_H_

#include <memory>                   // std::unique_ptr, std::shared_ptr
#include <vector>                   // std::vector

namespace darkcore
{
    namespace image
    {
        class Image;
        class ImageFormat;

        /**
         * Represents a collection of images with the same dimensions and format.
         * Images that are contained within an ImageSet are 'owned' by that ImageSet
         * and will be deleted when the ImageSet gets destructed.
         */
        class ImageSet
        {
          public:
            /**
             * Constructs an ImageSet of the specified size, dimensions, and format.
             * @param width Width of Images
             * @param heigth Height of Images
             * @param format Format of Images
             * @param size Size of ImageSet
             */
            ImageSet(int width, int height, const ImageFormat &format, int size = 0);

            /**
             * Constructs an ImageSet of the specified size, dimensions, and format.
             * @param width Width of Images
             * @param heigth Height of Images
             * @param format Format of Images
             * @param size Size of ImageSet
             */
            ImageSet(int width, int height, const std::shared_ptr<ImageFormat> format, int size = 0);

            /**
             * Copy constructs an ImageSet from the specified ImageSet.
             * @param imgset ImageSet to copy from
             */
            ImageSet(const ImageSet &imgset);

            /**
             * Destructs this ImageSet.
             */
            virtual ~ImageSet();

            int getNumImages() const;
            int getWidth() const;
            int getHeight() const;
            const ImageFormat & getImageFormat() const;

            /**
             * Returns a const Image at the specified index.
             * @param num Index
             * @return Image
             */
            const Image & getImage(int num) const;

            /**
             * Returns the Image at the specified index.
             * @param num Index
             * @return Image
             */
            Image & getImage(int num);

            /**
             * Replaces the image at the specified index.
             * Ownership of the image is transferred to the
             * ImageSet.
             * @param num Index
             * @param img Image to set
             */
            void setImage(int num, std::unique_ptr<Image> img);

            /**
             * Replaces the image at the specified index.
             * A copy is made of the Image before adding it
             * to the ImageSet.
             * @param num Index
             * @param img Image to set
             */
            void setImage(int num, const Image &img);

            /**
             * Appends an image to the end of the ImageSet.
             * Ownership of the image is transferred to the
             * ImageSet.
             * @param img Image to add
             */
            void addImage(std::unique_ptr<Image> img);

            /**
             * Appends an image to the end of the ImageSet.
             * A copy is made of the Image before adding it
             * to the ImageSet.
             * @param img Image to add
             */
            void addImage(const Image &img);

            /**
             * Resizes the ImageSet to the specified size.  If downsizing the
             * ImageSet, any images in indices greater than the specified size
             * will be deleted.
             * @param size New size
             */
            void resize(int size);

          private:
            /**
             * Replaces the image at the specified index.
             * A copy is made of the Image before adding it
             * to the ImageSet.
             * @param num Index
             * @param img Image to set
             */
            void setImage(int num, Image *img);

            /**
             * Appends an image to the end of the ImageSet.
             * A copy is made of the Image before adding it
             * to the ImageSet.
             * @param img Image to add
             */
            void addImage(Image *img);

            /**
             * Validates that the specified size is at least 1.  If not, an
             * exception is thrown.
             * @param size Requested size
             */
            void validateSize(int size) const;

            /**
             * Validates that the specified index is within the bounds of this
             * ImageSet.  Throws an IndexException if it fails.
             * @param num ImageNum
             */
            void validateImageNum(int num) const;

            /**
             * Validates the dimensions and format of the Image meet the
             * requirements for this ImageSet.  Throws an Exception if it
             * fails.
             * @param img Image to validate
             */
            void validateImage(const Image &img) const;

            std::vector<Image *> images;
            int width;
            int height;
            std::shared_ptr<ImageFormat> format;
        };

        inline int ImageSet::getNumImages() const
        {
            return images.size();
        }

        inline int ImageSet::getWidth() const
        {
            return width;
        }

        inline int ImageSet::getHeight() const
        {
            return height;
        }

        inline const ImageFormat & ImageSet::getImageFormat() const
        {
            return *format;
        }
    }
}


#endif
