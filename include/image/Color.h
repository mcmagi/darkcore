/* Color.h */

#ifndef DARKCORE_IMAGE_COLOR_H_
#define DARKCORE_IMAGE_COLOR_H_

#include <cstdint>                  // uint8_t, uint32_t (c++0x)
#include <string>                   // std::string

namespace darkcore
{
    namespace image
    {
        /**
         * Represents a 32-bit colored pixel.
         */
        typedef uint32_t Pixel;

        /**
         * Represents an R, G, or B component of a color.
         */
        typedef uint8_t Component;

        class Color
        {
          public:
            // the following colors are the standard CGA/EGA 16 colors
            static const Color BLACK;
            static const Color BLUE;
            static const Color GREEN;
            static const Color CYAN;
            static const Color RED;
            static const Color MAGENTA;
            static const Color BROWN;
            static const Color GRAY;
            static const Color DK_GRAY;
            static const Color LT_BLUE;
            static const Color LT_GREEN;
            static const Color LT_CYAN;
            static const Color LT_RED;
            static const Color LT_MAGENTA;
            static const Color YELLOW;
            static const Color WHITE;

            Color(Component red, Component green, Component blue);

            /**
             * Copy-constructs this color from another.
             * @param c Color
             */
            Color(const Color &c);

            /**
             * Destructor.
             */
            ~Color();

            Component getRed() const;
            Component getGreen() const;
            Component getBlue() const;

            /**
             * Assigns another color to this color.
             * @param color Another color
             * @return This color
             */
            Color & operator=(const Color &);

            // comparison
            bool operator==(const Color &c) const;
            bool operator!=(const Color &c) const;
            bool operator<(const Color &c) const;

            // string conversion
            operator const char *() const;
            std::string toString() const;

          private:
            Component red;
            Component green;
            Component blue;
        };

        inline Component Color::getRed() const { return red; }
        inline Component Color::getGreen() const { return green; }
        inline Component Color::getBlue() const { return blue; }
    }
}

#endif
