/* ImageOperation.h */

#ifndef DARKCORE_IMAGE_OPERATION_IMAGEOPERATION_H_
#define DARKCORE_IMAGE_OPERATION_IMAGEOPERATION_H_

#include <vector>                   // std::vector
#include "Exception.h"              // UnimplementedException
#include "image/Color.h"            // Pixel

namespace darkcore
{
    namespace image
    {
        class Image; // declare Image
    }
}

namespace darkcore
{
    namespace image
    {
        namespace operation
        {
            /**
             * The ImageOperation class provides an extensible means to
             * perform direct access and manipulations to image data.
             */
            class ImageOperation
            {
              public:
                /**
                 * Virtual destructor.
                 */
                virtual ~ImageOperation() { };

                /**
                 * Operates on the image data in the manner specified
                 * by this ImageOperation.  Pure virtual function.
                 */
                virtual void operate(Image &img, std::vector<Pixel> &data) const = 0;

              protected:
                /**
                 * Protected constructor for abstract class.
                 */
                ImageOperation() { };

                /**
                 * Protected copy constructor for abstract class.
                 * @param op
                 */
                ImageOperation(const ImageOperation &op) { };

              private:
                /**
                 * Assignment operator.  Not implemented.
                 * @param op
                 * @return
                 */
                ImageOperation & operator=(const ImageOperation &op);
            };

            inline ImageOperation & ImageOperation::operator=(const ImageOperation &op)
            {
                throw UnimplementedException(__FUNCTION__);
            }
        }
    }
}

#endif