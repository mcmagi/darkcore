/* DumpOperation.h */

#ifndef DARKCORE_IMAGE_OPERATION_DUMPOPERATION_H_
#define DARKCORE_IMAGE_OPERATION_DUMPOPERATION_H_

#include <vector>                   // std::vector
#include "image/Color.h"            // Pixel
#include "ImageOperation.h"         // ImageOperation

namespace darkcore
{
    namespace image
    {
        namespace operation
        {
            /**
             * Performs a hexadecimal dump operation on the image data.
             */
            class DumpOperation : public ImageOperation
            {
              public:
                DumpOperation();
                DumpOperation(const DumpOperation &op);
                virtual ~DumpOperation();

                virtual void operate(Image &img, std::vector<Pixel> &data) const;
            };
        }
    }
}

#endif