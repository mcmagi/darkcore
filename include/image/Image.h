/* Image.h */

#ifndef DARKCORE_IMAGE_IMAGE_H_
#define DARKCORE_IMAGE_IMAGE_H_

#include <memory>                   // std::unique_ptr, std::shared_ptr
#include <vector>                   // std::vector
#include "Color.h"                  // Pixel

// prototypes
namespace darkcore
{
    namespace image
    {
        class ImageFormat;
        class ImageSet;

        namespace operation
        {
            class ImageOperation;
        }

        namespace converter
        {
            class ImageConverter;
        }
    }
}

namespace darkcore
{
    namespace image
    {
        /*
         * Image base class (abstract)
         *
         * This is the base class for Image types.  It cannot be instantiated.
         * Image dimensions remain constant throughout the life of the object.
         */
        class Image
        {
          public:
            /**
             * Constructs an image object with the specified dimensions and format.
             * @param w Width
             * @param h Height
             * @param format Image format
             */
            Image(int w, int h, const ImageFormat &format);

            /**
             * Constructs an image object with the specified dimensions and format.
             * @param w Width
             * @param h Height
             * @param format Image format
             */
            Image(int w, int h, std::shared_ptr<ImageFormat> format);

            /**
             * Constructs an image object with the specified dimensions, format,
             * and image data.  The image data must be an array of length x*y.
             * @param w Width
             * @param h Height
             * @param format Image format
             * @param imgData Image data (must be a vector of length x*y)
             */
            Image(int w, int h, const ImageFormat &format, const std::vector<Pixel> &imgData);

            /**
             * Constructs an image object with the specified dimensions, format,
             * and image data.  The image data must be an array of length x*y.
             * @param w Width
             * @param h Height
             * @param format Image format
             * @param imgData Image data (must be a vector of length x*y)
             */
            Image(int w, int h, std::shared_ptr<ImageFormat> format, const std::vector<Pixel> &imgData);

            /**
             * Constructs an Image object from the ImageSet.  The Images in
             * the ImageSet will be arranged first horizontally (specified by the
             * number of columns, which defaults to 1) then vertically.  This is
             * useful to construct a "superimage" from a set of tiles.
             * @param imgSet The ImageSet
             * @param columns Number of tile columns (defaults to 1)
             */
            Image(const ImageSet &imgSet, int columns = 1);

            /**
             * Constructs an Image object from the vector of Images.  The Images in
             * the vector will be arranged first horizontally (specified by the
             * number of columns, which defaults to 1) then vertically.  This is
             * useful to construct a "superimage" from a set of tiles.
             * @param images The images
             * @param columns Number of tile columns (defaults to 1)
             */
            Image(const std::vector<const Image *> &images, int columns = 1);

            /**
             * Copy contructor copies the image data in the specified
             * Image to this newly constructed Image.
             * @param img Image to copy from
             */
            Image(const Image &img);

            /**
             * Destructor.
             */
            virtual ~Image();

            /**
             * Assignment operator copies the image data in the specified
             * Image to this Image.
             * @param Image to assign from
             * @return This image
             */
            virtual Image & operator=(const Image &img);

            // get methods
            const ImageFormat & getImageFormat() const;
            int getNumPixels() const;
            int getWidth() const;
            int getHeight() const;
            int getPitch() const;

            /**
             * Returns the value of a single pixel.
             * @param x Column position
             * @param y Row position
             * @return Pixel value
             */
            Pixel getPixel(int x, int y) const;

            /**
             * Sets the value of a single pixel.
             * @param x Column position
             * @param y Row position
             * @param c Pixel value
             */
            void setPixel(int x, int y, Pixel c);

            /**
             * Returns the current image data as an array of Pixels.  The
             * array is of size getNumPixels().  The array is returned
             * const to prevent uncontrolled modification of the image
             * data.  To modify the image data, use an ImageOperator or
             * ImageConverter.
             * @return Image data
             */
            const std::vector<Pixel> & getImageData() const;

            /**
             * Returns the current image data as an array of Pixels.  The
             * array is of size getNumPixels().  It is recommended that one
             * use an ImageOperator or ImageConverter to perform modifications
             * to the image data.
             * @return Image data
             */
            std::vector<Pixel> & getImageData();

            // conversion functions
            std::unique_ptr<Image> convert(const ImageFormat &format) const;
            std::unique_ptr<Image> convert(const darkcore::image::converter::ImageConverter &converter) const;
            std::unique_ptr<Image> resize(int w, int h) const;

            void operate(const darkcore::image::operation::ImageOperation &op);

            /**
             * Copies data from this Image and returns it as another Image.
             * The data copied is determined by the rectangle {(x,y),(w-1,h-1)}.
             * The returned image will have width 'w' and height 'h'.   The
             * returned Image must be explicitly deleted.
             */
            std::unique_ptr<Image> copy(int x, int y, int w, int h) const;

            /**
             * Pastes data from another Image object into this Image.
             * @param img Image to copy from
             * @param x Starting x coordinate to paste into
             * @param y Starting y coordinate to paste into
             */
            void paste(const Image &img, int x = 0, int y = 0);

            /**
             * Breaks the image into a grid of tiles and returns the resulting ImageSet.
             * The data returned is a copy of the image data.  Changing the tiled images
             * will not change the parent image.  The returned ImageSet must be
             * explicitly deleted.
             */
            std::unique_ptr<ImageSet> tileImage(int tileX, int tileY) const;

            /**
             * For debugging.
             */
            void dump();

          private:
            /**
             * Overwrites the current Image data with the supplied data.
             * The supplied data MUST be the same size as this Image's data.
             * @param imgdata New Image data
             * @deprecated
             */
            void setImageData(const Pixel *imgdata);

            /**
             * Copies data from one image object to another image object.  It does
             * this by copying data from the source image starting at coordinate
             * position (srcX,srcY) to the target image at coordinate (tgtX,tgtY).
             * The amount copied is the minumum of both images widths and heights,
             * constrained by the edges of the image.  For example, the following
             * algorithm finds the width and height to be copied:
             *   copyWidth = min(tgt.width - tgtX, src.width - srcX)
             *   copyHeight = min(tgt.height - tgtY, src.height - srcY)
             * By itself, the signature of this function is not entirely intuitive,
             * and is therefore private.  But it acts is the workhorse behind the copy()
             * and paste() methods.
             */
            static void copyImageData(const Image &src, int srcX, int srcY,
                    Image &tgt, int tgtX, int tgtY);

            /**
             * Returns an index into the Image data array given a cartesian position.
             * @param x Column position
             * @param y Row position
             * @return Index into image data array
             */
            int calculatePosition(int x, int y) const;

            /**
             * Validates that the image array is within bounds of the image.  Throws
             * an IndexException if it is out of bounds.
             * @param x Column position
             * @param y Row position
             * @return Index into image data array
             */
            void validatePosition(int x, int y) const;

            /**
             * Used in Image construction from ImageSets and vectors.
             * @param images Vector of images
             * @param columns Number of columns
             */
            void constructFromVector(const std::vector<const Image *> &images, int columns);

            /**
             * Validates that the vector size matches the image dimensions.  Used
             * on image creation.
             */
            void validateSize();

            // image data
            std::vector<Pixel> data;

            // image dimensions
            int width;
            int height;
            std::shared_ptr<ImageFormat> format;
        };

        inline const ImageFormat & Image::getImageFormat() const
        {
            return *format;
        }

        inline int Image::getNumPixels() const
        {
            return data.size();
        }

        inline int Image::getWidth() const
        {
            return width;
        }

        inline int Image::getHeight() const
        {
            return height;
        }

        inline int Image::getPitch() const
        {
            return width * sizeof(Pixel);
        }
    }
}


#endif
