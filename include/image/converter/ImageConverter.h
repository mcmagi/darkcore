/* ImageConverter.h */

#ifndef DARKCORE_IMAGE_CONVERTER_IMAGECONVERTER_H_
#define DARKCORE_IMAGE_CONVERTER_IMAGECONVERTER_H_

#include <memory>                   // std::unique_ptr
#include <vector>                   // std::vector
#include "Exception.h"              // UnimplementedException
#include "image/Color.h"            // Pixel

// prototypes
namespace darkcore
{
    namespace image
    {
        class Image;
    }
}

namespace darkcore
{
    namespace image
    {
        namespace converter
        {
            class ImageConverter
            {
              public:
                /**
                 * Virtual destructor.
                 */
                virtual ~ImageConverter() { }

                /**
                 * Converts the pixel data in a manner specified
                 * by this ImageConverter.  Pure virtual function.
                 */
                virtual std::unique_ptr<Image> convert(const Image &img, const std::vector<Pixel> &data) const = 0;

              protected:
                /**
                 * Protected constructor for abstract class.
                 */
                ImageConverter() { }

                /**
                 * Protected copy constructor for abstract class.
                 * @param conv
                 */
                ImageConverter(const ImageConverter &conv) { }

              private:
                /**
                 * Assignment operator.  Not implemented.
                 * @param conv
                 * @return
                 */
                ImageConverter & operator=(const ImageConverter &conv);
            };

            inline ImageConverter & ImageConverter::operator=(const ImageConverter &conv)
            {
                throw UnimplementedException(__FUNCTION__);
            }
        }
    }
}

#endif
