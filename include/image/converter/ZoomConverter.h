/* ZoomConverter.h */

#ifndef DARKCORE_IMAGE_CONVERTER_ZOOMCONVERTER_H_
#define DARKCORE_IMAGE_CONVERTER_ZOOMCONVERTER_H_

#include <memory>                   // std::unique_ptr
#include <vector>                   // std::vector
#include "image/Color.h"            // Pixel
#include "ImageConverter.h"         // ImageConverter

// prototypes
namespace darkcore
{
    namespace image
    {
        class Image;
    }
}

namespace darkcore
{
    namespace image
    {
        namespace converter
        {
            /**
             * An ImageConverter that returned a magnified image with pixels
             * multiplied by the specified zoomLevel.
             */
            class ZoomConverter : public ImageConverter
            {
              public:
                ZoomConverter(int zoomLevel);
                ZoomConverter(const ZoomConverter &conv);

                /**
                 * Destructor.
                 */
                virtual ~ZoomConverter();

                void setZoomLevel(int zoomLevel);
                int getZoomLevel() const;

                /**
                 * Performs the conversion.
                 * @param img Image to convert
                 * @param data Image data
                 * @return Converted image.
                 */
                virtual std::unique_ptr<Image> convert(const Image &img, const std::vector<Pixel> &data) const;

              private:
                int zoomLevel;
            };

            inline void ZoomConverter::setZoomLevel(int zoomLevel)
            {
                this->zoomLevel = zoomLevel;
            }

            inline int ZoomConverter::getZoomLevel() const
            {
                return zoomLevel;
            }
        }
    }
}

#endif
