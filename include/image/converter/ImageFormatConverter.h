/* ImageFormatConverter.h */

#ifndef DARKCORE_IMAGE_CONVERTER_IMAGEFORMATCONVERTER_H_
#define DARKCORE_IMAGE_CONVERTER_IMAGEFORMATCONVERTER_H_

#include <memory>                   // std::unique_ptr
#include <vector>                   // std::vector
#include "image/Color.h"            // Pixel
#include "ImageConverter.h"         // ImageConverter

// prototypes
namespace darkcore
{
    namespace image
    {
        class Image;
        class ImageFormat;
    }
}

namespace darkcore
{
    namespace image
    {
        namespace converter
        {
            /**
             * An ImageConverter that converts an image from a source format
             * to a target format.
             */
            class ImageFormatConverter : public ImageConverter
            {
              public:
                /**
                 * Destructor.
                 */
                virtual ~ImageFormatConverter();

                /**
                 * Returns the source format of the image.
                 * @return Source format
                 */
                const ImageFormat & getSourceImageFormat() const;

                /**
                 * Returns the target format of the image.
                 * @return Target format
                 */
                const ImageFormat & getTargetImageFormat() const;

                /**
                 * Returns the default converter matching the specified source
                 * and target formats.  Note there is no default converter for
                 * RGB to Indexed image formats.
                 * @param srcFormat Source format
                 * @param tgtFormat Target format
                 * @return Default converter
                 */
                static std::unique_ptr<ImageFormatConverter> getDefaultConverter(
                        const ImageFormat &srcFormat, const ImageFormat &tgtFormat);

                /**
                 * Performs the conversion.
                 * @param img Image to convert
                 * @param data Image data
                 * @return Converted image.
                 */
                virtual std::unique_ptr<Image> convert(const Image &img, const std::vector<Pixel> &data) const = 0;

              protected:
                /**
                 * Protected constructor.
                 * @param srcFormat Source format
                 * @param tgtFormat Target format
                 */
                ImageFormatConverter(const ImageFormat &srcFormat, const ImageFormat &tgtFormat);

                /**
                 * Copy constructor.
                 * @param conv Image format converter
                 */
                ImageFormatConverter(const ImageFormatConverter &conv);

              private:
                const ImageFormat *srcFormat;
                const ImageFormat *tgtFormat;
            };

            inline const ImageFormat & ImageFormatConverter::getSourceImageFormat() const
            {
                return *srcFormat;
            }

            inline const ImageFormat & ImageFormatConverter::getTargetImageFormat() const
            {
                return *tgtFormat;
            }


            class IndexedToRGBConverter : public ImageFormatConverter
            {
              public:
                IndexedToRGBConverter(const IndexedImageFormat &srcFormat, const RGBImageFormat &tgtFormat);
                IndexedToRGBConverter(const IndexedToRGBConverter &conv);

                /**
                 * Destructor.
                 */
                virtual ~IndexedToRGBConverter();

                /**
                 * Performs the conversion.
                 * @param img Image to convert
                 * @param data Image data
                 * @return Converted image.
                 */
                virtual std::unique_ptr<Image> convert(const Image &img, const std::vector<Pixel> &data) const;
            };


            class IndexedToIndexedConverter : public ImageFormatConverter
            {
              public:
                IndexedToIndexedConverter(const IndexedImageFormat &srcFormat, const IndexedImageFormat &tgtFormat);
                IndexedToIndexedConverter(const IndexedToIndexedConverter &conv);

                /**
                 * Destructor.
                 */
                virtual ~IndexedToIndexedConverter();

                /**
                 * Performs the conversion.
                 * @param img Image to convert
                 * @param data Image data
                 * @return Converted image.
                 */
                virtual std::unique_ptr<Image> convert(const Image &img, const std::vector<Pixel> &data) const;
            };


            class RGBToRGBConverter : public ImageFormatConverter
            {
              public:
                RGBToRGBConverter(const RGBImageFormat &srcFormat, const RGBImageFormat &tgtFormat);
                RGBToRGBConverter(const RGBToRGBConverter &conv);

                /**
                 * Destructor.
                 */
                virtual ~RGBToRGBConverter();

                /**
                 * Performs the conversion.
                 * @param img Image to convert
                 * @param data Image data
                 * @return Converted image.
                 */
                virtual std::unique_ptr<Image> convert(const Image &img, const std::vector<Pixel> &data) const;
            };
        }
    }
}

/* General rules for image conversion:
 *
 * Mono->CGA (convert 0 -> 0(00), 1 -> 3(11))
 * CGA->EGA (convert 0(00) -> 0(0000), 1 -> 3(0011), 2 -> 5(0101), 3 -> 7(0111)
 * EGA->VGA (convert 0-16 -> 0-16 / no conversion)
 * VGA->TruColor (convert 0-255 to 24-bit RGB value by palette lookup)
 */

#endif
