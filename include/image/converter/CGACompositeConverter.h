/* CGACompositeConverter.h */

#ifndef DARKCORE_IMAGE_CONVERTER_CGACOMPOSITECONVERTER_H_
#define DARKCORE_IMAGE_CONVERTER_CGACOMPOSITECONVERTER_H_

#include <memory>                   // std::unique_ptr
#include <vector>                   // std::vector
#include "image/Color.h"            // Color, Pixel
#include "ImageConverter.h"         // ImageConverter

// prototypes
namespace darkcore
{
    namespace image
    {
        class Image;
        class RGBImageFormat;
    }
}

namespace darkcore
{
    namespace image
    {
        namespace converter
        {
            /**
             * An ImageConverter that expects a CGA image and returns a
             * CGA-composite image.
             */
            class CGACompositeConverter : public ImageConverter
            {
              public:
                CGACompositeConverter(const RGBImageFormat &imgFmt);
                CGACompositeConverter(const CGACompositeConverter &conv);

                /**
                 * Destructor.
                 */
                virtual ~CGACompositeConverter();

                /**
                 * Performs the conversion.
                 * @param img Image to convert
                 * @param data Image data
                 * @return Converted image.
                 */
                virtual std::unique_ptr<Image> convert(const Image &img, const std::vector<Pixel> &data) const;

              private:
                const RGBImageFormat &imageFormat;
                std::vector<Color> compColors;
            };
        }
    }
}

#endif
