/* ColorMask.h */

#ifndef DARKCORE_IMAGE_COLORMASK_H_
#define DARKCORE_IMAGE_COLORMASK_H_

#include <stdint.h>                 // uint32_t
#include <string>                   // std::string
#include "Color.h"                  // Color

namespace darkcore
{
    namespace image
    {
        class Color;

        class ColorMask
        {
          public:
            // full 32-bit color
            static const ColorMask FULL32;

            /**
             * Constructs a color mask with the specified properties.
             * @param bpp Color depth
             * @param rmask Red mask
             * @param gmask Green mask
             * @param bmask Blue mask
             */
            ColorMask(int bpp, uint32_t rmask, uint32_t gmask, uint32_t bmask);

            /**
             * Copy-constructs a color mask.
             * @param mask Color mask
             */
            ColorMask(const ColorMask &mask);

            /**
             * Destructor.
             */
            ~ColorMask();

            int getColorDepth() const;
            uint32_t getRedMask() const;
            uint32_t getGreenMask() const;
            uint32_t getBlueMask() const;
            int getRedShift() const;
            int getGreenShift() const;
            int getBlueShift() const;

            uint32_t getRGBValue(const Color &c) const;
            Color getColor(const uint32_t &p) const;
        
            /**
             * Assigns another color mask to this mask.
             * @param mask Another color mask
             * @return This color mask
             */
            ColorMask & operator=(const ColorMask &mask);

            /**
             * Returns true if the specified ColorMask equals this ColorMask.
             * @param mask ColorMask to compare
             */
            bool operator==(const ColorMask &mask) const;

            /**
             * Returns true if the specified ColorMask does not equal this ColorMask.
             * This method is non-virtual and is simply the negation of operator==.  Override
             * operator== instead.
             * @param mask ColorMask to compare
             */
            bool operator!=(const ColorMask &mask) const;

            /**
             * Returns the ColorMask attributes formatted as a string.
             * @return Formatted string
             */
            std::string toString() const;

          private:
            int bpp;
            uint32_t rmask;
            uint32_t gmask;
            uint32_t bmask;
            int rshift;
            int gshift;
            int bshift;

            /**
             * Calculates the shift for a given mask.
             * @param mask Mask
             * @return shift
             */
            static int calculateShift(uint32_t mask);
        };

        inline int ColorMask::getColorDepth() const { return bpp; }
        inline uint32_t ColorMask::getRedMask() const { return rmask; }
        inline uint32_t ColorMask::getGreenMask() const { return gmask; }
        inline uint32_t ColorMask::getBlueMask() const { return bmask; }
        inline int ColorMask::getRedShift() const { return rshift; }
        inline int ColorMask::getGreenShift() const { return gshift; }
        inline int ColorMask::getBlueShift() const { return bshift; }
    }
}

#endif
