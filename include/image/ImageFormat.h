/* ImageFormat.h */

#ifndef DARKCORE_IMAGE_IMAGEFORMAT_H_
#define DARKCORE_IMAGE_IMAGEFORMAT_H_

#include <cmath>                    // std::pow
#include <memory>                   // std::unique_ptr
#include <string>                   // std::string
#include "ColorMask.h"              // ColorMask
#include "ColorPalette.h"           // ColorPalette, StandardPaletteType

namespace darkcore
{
    namespace image
    {
        class ColorMask;
        class ColorPalette;

        /**
         * An abstract base class representing the format of an Image.  Formats
         * can be one of two types: indexed (where each pixel is an index to an
         * RGB color palette) or RGB (where each pixel contains the RGB color).
         * These are represented by the sub-classes IndexedImageFormat and
         * RGBImageFormat.
         */
        class ImageFormat
        {
          public:
            /**
             * The image format types available represented as an enum.
             */
            enum FormatType { RGB, INDEXED };

            /**
             * Virtual destructor.
             */
            virtual ~ImageFormat();

            /**
             * Returns the image format type.
             * @return FormatType enum
             */
            FormatType getFormatType() const;

            /**
             * Returns the color depth (BPP) of the image format.
             * @return Color Depth / BPP
             */
            int getColorDepth() const;

            /**
             * Returns the number of colors available at this color depth.
             * @return Number of colors
             */
            double getNumColors() const;

            /**
             * Returns true if the specified ImageFormat equals this ImageFormat.
             * @param format ImageFormat to compare
             */
            virtual bool operator==(const ImageFormat &format) const;

            /**
             * Returns true if the specified ImageFormat does not equal this ImageFormat.
             * This method is non-virtual and is simply the negation of operator==.  Override
             * operator== instead.
             * @param format ImageFormat to compare
             */
            bool operator!=(const ImageFormat &format) const;

            /**
             * Makes an exact copy of this object.  This method is intended
             * to be polymorhpic and must be overriden by all subclasses to
             * ensure it always returns the correct type.
             * @return Copied ImageFormat
             */
            virtual std::unique_ptr<ImageFormat> clone() const = 0;

            /**
             * Returns the ImageFormat attributes formatted as a string.
             * @return Formatted string
             */
            virtual std::string toString() const;

          protected:
            /**
             * Protected constructor for an abstract class.
             * @param type Format type enum
             * @param bpp Color depth / BPP
             */
            ImageFormat(FormatType type, int bpp);

            /**
             * Copy-constructs an image format.
             * @param format Indexed image format
             */
            ImageFormat(const ImageFormat &format);

            /**
             * Assigns another image format to this image format.
             * @param format Another image format
             * @return This image format
             */
            virtual ImageFormat & operator=(const ImageFormat &format);

          private:
            FormatType type;
            int depth;
        };

        inline ImageFormat::FormatType ImageFormat::getFormatType() const
        {
            return type;
        }

        inline int ImageFormat::getColorDepth() const
        {
            return depth;
        }

        inline double ImageFormat::getNumColors() const
        {
            return std::pow((float) 2, depth);
        }


        /**
         * Represents an image format where each pixel in the image is an
         * index into a color palette.
         */
        class IndexedImageFormat : public ImageFormat
        {
          public:
            /**
             * Constructs an indexed image format for the specified standard palette type.
             * @param palType Standard palette type
             */
            IndexedImageFormat(StandardPaletteType palType);

            /**
             * Constructs an indexed image format with the specified depth and color palette.
             * @param bpp Color Depth / BPP
             * @param pal Color Palette
             */
            IndexedImageFormat(int bpp, const ColorPalette &pal);

            /**
             * Constructs an indexed image format with the specified depth and color palette.
             * @param bpp Color Depth / BPP
             * @param pal Color Palette
             */
            IndexedImageFormat(int bpp, std::unique_ptr<ColorPalette> pal);

            /**
             * Copy-constructs an indexed image format.
             * @param format Indexed image format
             */
            IndexedImageFormat(const IndexedImageFormat &format);

            /**
             * Destructor.
             */
            virtual ~IndexedImageFormat();

            /**
             * Returns the color palette used in pixel indices.
             * @return Color Palette
             */
            const ColorPalette & getColorPalette() const;

            /**
             * Returns true if the specified ImageFormat equals this ImageFormat.
             * @param format ImageFormat to compare
             */
            virtual bool operator==(const ImageFormat &format) const;

            /**
             * Assigns another image format to this image format.
             * @param format Another image format
             * @return This image format
             */
            virtual ImageFormat & operator=(const ImageFormat &format);

            /**
             * Assigns another image format to this image format.
             * @param format Another image format
             * @return This image format
             */
            virtual IndexedImageFormat & operator=(const IndexedImageFormat &format);

            /**
             * Returns an exact copy of this object.
             * @return Copied ImageFormat
             */
            virtual std::unique_ptr<ImageFormat> clone() const;

            /**
             * Returns the ImageFormat attributes formatted as a string.
             * @return Formatted string
             */
            virtual std::string toString() const;

          private:
            std::unique_ptr<ColorPalette> palette;
        };

        inline const ColorPalette & IndexedImageFormat::getColorPalette() const
        {
            return *palette;
        }


        /**
         * Represents an image format where each pixel in the Image contains an
         * RGB color value.
         */
        class RGBImageFormat : public ImageFormat
        {
          public:
            /**
             * Constructs an RGB image format with the specified color mask.
             * @param mask Color Mask
             */
            RGBImageFormat(const ColorMask &mask);

            /**
             * Copy-constructs an RGB image format.
             * @param format RGB image format
             */
            RGBImageFormat(const RGBImageFormat &format);

            /**
             * Destructor.
             */
            virtual ~RGBImageFormat();

            /**
             * Returns the color mask used to mask RGB pixel values.
             * @return Color Mask
             */
            const ColorMask & getColorMask() const;

            /**
             * Returns true if the specified ImageFormat equals this ImageFormat.
             * @param format ImageFormat to compare
             */
            virtual bool operator==(const ImageFormat &format) const;

            /**
             * Assigns another image format to this image format.
             * @param format Another image format
             * @return This image format
             */
            virtual ImageFormat & operator=(const ImageFormat &format);

            /**
             * Assigns another image format to this image format.
             * @param format Another image format
             * @return This image format
             */
            virtual RGBImageFormat & operator=(const RGBImageFormat &format);

            /**
             * Returns an exact copy of this object.
             * @return Copied ImageFormat
             */
            virtual std::unique_ptr<ImageFormat> clone() const;

            /**
             * Returns the ImageFormat attributes formatted as a string.
             * @return Formatted string
             */
            virtual std::string toString() const;

          private:
            ColorMask mask;
        };

        inline const ColorMask & RGBImageFormat::getColorMask() const
        {
            return mask;
        }
    }
}

#endif
