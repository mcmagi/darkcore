/* UltimaImageReader.h */

#ifndef DARKCORE_IMAGE_ULTIMAIMAGEREADER_H_
#define DARKCORE_IMAGE_ULTIMAIMAGEREADER_H_

#include <istream>              // std::istream
#include <memory>               // std::unique_ptr
#include <string>               // std::string
#include <vector>               // std::vector
#include "Color.h"              // Pixel
#include "ImageIO.h"            // ImageReader, ImageWriter
#include "util/Reader.h"        // Reader

// prototypes
namespace darkcore
{
    namespace config
    {
        class IndexEntry;
        class UltimaImageConfig;
    }

    namespace image
    {
        class Image;
        class ImageSet;
    }
}

namespace darkcore
{
    namespace image
    {
        /**
         * Used to read an Ultima image file and return it as a set of Image objects.
         */
        class UltimaImageReader : public ImageReader
        {
          public:
            /**
             * Constructs a reader of Ultima image content from the file in the specified configuration.
             * @param imgCfg Image configuration
             */
            UltimaImageReader(const darkcore::config::UltimaImageConfig &imgCfg);

            /**
             * Destructor.
             */
            virtual ~UltimaImageReader();

            const darkcore::config::UltimaImageConfig & getImageConfig() const;

            /**
             * Reads the next Image from the input stream.  Images returned from this
             * method must be explicitly deleted by the caller.
             * @return Image
             */
            virtual std::unique_ptr<Image> read();

          private:
            void buildImageData(std::vector<Pixel> &imgdata, const std::vector<unsigned char> &rawdata) const;
            void buildCGAPixelBlockImageData(std::vector<Pixel> &imgdata, const std::vector<unsigned char> &rawdata) const;
            void buildPixelBlockImageData(std::vector<Pixel> &imgdata, const std::vector<unsigned char> &rawdata) const;
            void unpackRow(Pixel *row, const unsigned char *rawdata, int width) const;
            void unpackSequentialRow(Pixel *row, const unsigned char *rawdata, int width) const;
            void unpackBitMaskedRow(Pixel *row, const unsigned char *rawdata, int width) const;
            int getPixelFromRow(int pix_idx, int row_idx) const;

            const darkcore::config::UltimaImageConfig &imgCfg;
            std::unique_ptr<darkcore::util::Reader<darkcore::config::IndexEntry> > indexReader;
        };

        inline const darkcore::config::UltimaImageConfig & UltimaImageReader::getImageConfig() const
        {
            return imgCfg;
        }
    }
}

#endif
