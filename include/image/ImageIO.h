/* ImageIO.h */

#ifndef DARKCORE_IMAGE_IMAGEIO_H_
#define DARKCORE_IMAGE_IMAGEIO_H_

#include <istream>              // std::istream
#include <memory>               // std::unique_ptr, std::shared_ptr
#include <string>               // std::string
#include <ostream>              // std::ostream
#include "util/FileReader.h"    // FileReader
#include "util/FileWriter.h"    // FileWriter

// prototypes
namespace darkcore
{
    namespace image
    {
        class Image;
        class ImageSet;
        class ImageFormat;
    }
}

namespace darkcore
{
    namespace image
    {
        /**
         * Base class for ImageReader and ImageWriter classes.  Provides
         * common properties and operations needed for both classes.
         */
        class ImageIO
        {
          public:
            virtual ~ImageIO();

            int getWidth() const;
            int getHeight() const;
            const ImageFormat & getImageFormat() const;
            int getNumPixels() const;

            /**
             * Returns the total number of images available to read from the ImageReader
             * or the maximum that can be written to the ImageWriter.
             * @return
             */
            int getNumImages() const;

          protected:
            ImageIO();
            ImageIO(int width, int height, int numImages, std::shared_ptr<ImageFormat> format);
            ImageIO(const ImageIO &io);

            void setWidth(int width);
            void setHeight(int height);
            void setImageFormat(const ImageFormat &format);

            std::shared_ptr<ImageFormat> getImageFormatPtr();
            void setImageFormatPtr(std::shared_ptr<ImageFormat> format);

            /**
             * Sets the number of images to be read.
             * @param numImages
             */
            void setNumImages(int numImages);

          private:
            int width;
            int height;
            int num_images;
            std::shared_ptr<ImageFormat> format;
        };

        inline int ImageIO::getWidth() const
        {
            return width;
        }

        inline int ImageIO::getHeight() const
        {
            return height;
        }

        inline int ImageIO::getNumPixels() const
        {
            return width * height;
        }

        inline const ImageFormat & ImageIO::getImageFormat() const
        {
            return *format;
        }

        inline std::shared_ptr<ImageFormat> ImageIO::getImageFormatPtr()
        {
            return format;
        }

        inline void ImageIO::setWidth(int width)
        {
            this->width = width;
        }

        inline void ImageIO::setHeight(int height)
        {
            this->height = height;
        }

        inline int ImageIO::getNumImages() const
        {
            return num_images;
        }

        inline void ImageIO::setNumImages(int numImages)
        {
            this->num_images = numImages;
        }


        /**
         * Used to read an image stream and return it as a set of Image objects.
         * Subclasses must provide an implementation for the pure virtual method read().
         */
        class ImageReader : public darkcore::util::FileReader<std::unique_ptr<Image> >, public ImageIO
        {
          public:
            virtual ~ImageReader();

            /**
             * Reads the next Image from the input stream.
             * @return Image
             */
            virtual std::unique_ptr<Image> read() = 0;

            /**
             * Reads all images from the input stream to an ImageSet.
             * @return ImageSet
             */
            virtual std::unique_ptr<ImageSet> readAll();

          protected:
            /**
             * Constructor given an filename to read image content from.  Defaults are
             * given in the event dimensions are not available during initialization.
             * In this case, we expect the subclass's constructor to set up all the
             * properties (width, height, format) before returning.
             * @param is name of file to read images from
             * @param width Width of images
             * @param height Height of images
             * @param numImages Number of images
             * @param format Image format
             */
            ImageReader(const std::string &filename, int width = 0, int height = 0, int numImages = 0,
                std::shared_ptr<ImageFormat> format = std::shared_ptr<ImageFormat>());

            /**
             * Constructor given an stream to read image content from.  Defaults are
             * given in the event dimensions are not available during initialization.
             * In this case, we expect the subclass's constructor to set up all the
             * properties (width, height, format) before returning.
             * @param is Input stream to read images from
             * @param width Width of images
             * @param height Height of images
             * @param numImages Number of images
             * @param format Image format
             */
            ImageReader(std::istream &is, int width = 0, int height = 0, int numImages = 0,
                std::shared_ptr<ImageFormat> format = std::shared_ptr<ImageFormat>());

            /**
             * Constructor given an stream to read image content from.  Defaults are
             * given in the event dimensions are not available during initialization.
             * In this case, we expect the subclass's constructor to set up all the
             * properties (width, height, format) before returning.
             * Ownership of the stream is transferred to the reader.
             * @param is Input stream to read images from
             * @param width Width of images
             * @param height Height of images
             * @param numImages Number of images
             * @param format Image format
             */
            ImageReader(std::unique_ptr<std::istream> is, int width = 0, int height = 0, int numImages = 0,
                std::shared_ptr<ImageFormat> format = std::shared_ptr<ImageFormat>());
        };


        /**
         * Used to write an image stream from a set of Image objects.
         * Subclasses must provide an implementation for the pure virtual method write().
         */
        class ImageWriter : public darkcore::util::FileWriter<Image>, public ImageIO
        {
          public:
            virtual ~ImageWriter();

            /**
             * Writes an Image to the output stream.
             * @param img Image
             */
            virtual void write(const Image &img) = 0;

            /**
             * Writes all images ImageSet to the output stream.
             * @param set ImageSet
             */
            virtual void writeAll(const ImageSet &set);

          protected:
            /**
             * Constructor given an filename to write image content to.  Defaults are
             * given in the event dimensions are not available during initialization.
             * In this case, we expect the subclass's constructor to set up all the
             * properties (width, height, format) before returning.
             * @param filename Name of file to write images to
             * @param width Width of images
             * @param height Height of images
             * @param numImages Number of images
             * @param format Image format
             */
            ImageWriter(const std::string &filename, int width = 0, int height = 0, int numImages = 0,
                        std::shared_ptr<ImageFormat> format = std::shared_ptr<ImageFormat>());

            /**
             * Constructor given an stream to write image content to.  Defaults are
             * given in the event dimensions are not available during initialization.
             * In this case, we expect the subclass's constructor to set up all the
             * properties (width, height, format) before returning.
             * @param os Output stream to write images to
             * @param width Width of images
             * @param height Height of images
             * @param numImages Number of images
             * @param format Image format
             */
            ImageWriter(std::ostream &os, int width = 0, int height = 0, int numImages = 0,
                        std::shared_ptr<ImageFormat> format = std::shared_ptr<ImageFormat>());

            /**
             * Constructor given an stream to write image content to.  Defaults are
             * given in the event dimensions are not available during initialization.
             * In this case, we expect the subclass's constructor to set up all the
             * properties (width, height, format) before returning.
             * Ownership of the stream is transferred to the writer.
             * @param os Output stream to write images to
             * @param width Width of images
             * @param height Height of images
             * @param numImages Number of images
             * @param format Image format
             */
            ImageWriter(std::unique_ptr<std::ostream> os, int width = 0, int height = 0, int numImages = 0,
                        std::shared_ptr<ImageFormat> format = std::shared_ptr<ImageFormat>());
        };
    }
}

#endif
