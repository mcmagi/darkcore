/* UltimaImageWriter.h */

#ifndef DARKCORE_IMAGE_ULTIMAIMAGEWRITER_H_
#define DARKCORE_IMAGE_ULTIMAIMAGEWRITER_H_

#include <string>               // std::string
#include <memory>               // std::memory
#include <ostream>              // std::ostream
#include "Color.h"              // Pixel
#include "ImageIO.h"            // ImageReader, ImageWriter
#include "util/Writer.h"        // Writer

// prototypes
namespace darkcore
{
    namespace config
    {
        class IndexEntry;
        class UltimaImageConfig;
    }

    namespace image
    {
        class Image;
        class ImageSet;
    }
}

namespace darkcore
{
    namespace image
    {
        /**
         * Used to write an Ultima image file from a set of Image objects.
         */
        class UltimaImageWriter : public ImageWriter
        {
          public:
            /**
             * Constructs a writer of Ultima image content to the file in the specified configuration.
             * @param imgCfg Image configuration
             */
            UltimaImageWriter(const darkcore::config::UltimaImageConfig &imgCfg);

            /**
             * Destructor.
             */
            virtual ~UltimaImageWriter();

            const darkcore::config::UltimaImageConfig & getImageConfig() const;

            /**
             * Writes an Image to the output stream.
             * @param img Image
             */
            virtual void write(const Image &img);

          private:
            void buildRawData(const Pixel *imgdata, unsigned char *rawdata, int rawdata_size) const;
            void packRow(const Pixel *row, unsigned char *rawdata) const;
            void packSequentialRow(const Pixel *row, unsigned char *rawdata, int width) const;
            void packBitMaskedRow(const Pixel *row, unsigned char *rawdata, int width) const;
            int getPixelFromRow(int pix_idx, int row_idx) const;

            const darkcore::config::UltimaImageConfig &imgCfg;
            std::unique_ptr<darkcore::util::Writer<darkcore::config::IndexEntry> > indexWriter;
        };

        inline const darkcore::config::UltimaImageConfig & UltimaImageWriter::getImageConfig() const
        {
            return imgCfg;
        }
    }
}

#endif
