/* ColorPalette.h */

#ifndef DARKCORE_IMAGE_COLORPALETTE_H_
#define DARKCORE_IMAGE_COLORPALETTE_H_

#include <stdint.h>         // uint8_t
#include <cmath>            // log2
#include <map>              // std::map
#include <memory>           // std::unique_ptr
#include <string>           // std::string
#include <vector>           // std::vector
#include "Color.h"          // Color

namespace darkcore
{
    namespace image
    {
        class Color;

        /**
         * The standard types of indexed color palettes.
         */
        enum StandardPaletteType { MONO = 1, CGA = 2, EGA = 4, VGA = 8 };

        /**
         * A ColorPalette used for indexed color schemes.
         */
        class ColorPalette
        {
          public:
            /**
             * Constructor.
             * @param bpp Bits per pixel (max of 8)
             * @param transparent Index of transparent color (none by default)
             */
            ColorPalette(int bpp = 0, int transparent = -1);

            /**
             * Copy-constructs a color palette.
             * @param pal Color palette
             */
            ColorPalette(const ColorPalette &pal);

            /**
             * Destructor.
             */
            ~ColorPalette();

            /**
             * Returns the number of Color entries in this palette.
             * @return Number of colors
             */
            int getNumColors() const;

            /**
             * Returns the color depth of this palette.
             * @return Color Depth / BPP
             */
            int getColorDepth() const;

            /**
             * Returns the Color at the specified palette index.
             * @param index Palette index
             * @return Color
             */
            Color getColor(int index) const;

            /**
             * Sets the Color at the specified palette index.
             * @param index Palette index
             * @param c Palette Color
             */
            void setColor(int index, const Color &c);

            /**
             * Returns the index of the transparent color, or -1 if none.
             * @return Transparent color index or -1
             */
            int getTransparentColor() const;

            /**
             * Sets the index of the transparent color.
             * @param index Transparent color index or -1
             */
            void setTransparentColor(int index);

            /**
             * Loads a VGA palette from a VGA PAL file.
             * @param filename VGA PAL file
             * @return Palette
             */
            static ColorPalette loadVGAPalette(const std::string &filename);

            /**
             * Returns a color palette singleton that matches
             * the specified standard type.
             * @param type Standard type
             * @return Palette
             */
            static ColorPalette &getStandardPalette(StandardPaletteType type);

            /**
             * Assigns another color palette to this palette.
             * @param pal Another color palette
             * @return This color palette
             */
            ColorPalette & operator=(const ColorPalette &pal);

            /**
             * Returns true if the specified ColorPalette equals this ColorPalette.
             * @param pal ColorPalette to compare
             */
            bool operator==(const ColorPalette &pal) const;

            /**
             * Returns true if the specified ColorPalette does not equal this ColorPalette.
             * This method is non-virtual and is simply the negation of operator==.  Override
             * operator== instead.
             * @param pal ColorPalette to compare
             */
            bool operator!=(const ColorPalette &pal) const;

            /**
             * Returns the ColorPalette attributes formatted as a string.
             * @return Formatted string
             */
            std::string toString() const;

            virtual std::unique_ptr<ColorPalette> clone() const;

          private:
            static std::map<StandardPaletteType,ColorPalette> STD_PALETTE_MAP;

            std::vector<Color> colorData;
            int transparent;

            static uint8_t convertVgaPalColor(char color);

            /**
             * Private construction of the STD_PALETTE_MAP.
             */
            static void initStdPaletteMap();

            /**
             * Used to build the standard VGA palette map.
             */
            static void buildVGAColorWheel(ColorPalette &pal, int &idx, int start, int end, int interval);
        };

        inline int ColorPalette::getNumColors() const { return colorData.size(); }
        inline int ColorPalette::getColorDepth() const { return (int) log2(colorData.size()); }
        inline Color ColorPalette::getColor(int index) const { return colorData[index]; }
        inline void ColorPalette::setColor(int index, const Color &c) { colorData[index] = c; }
        inline int ColorPalette::getTransparentColor() const { return transparent; }
        inline void ColorPalette::setTransparentColor(int index) { transparent = index; }
    }
}

#endif
