/* LZW.h */

#ifndef DARKCORE_LZW_LZW_H_
#define DARKCORE_LZW_LZW_H_

#include <string.h>                 // memcpy
#include <stack>                    // std::stack
#include "Exception.h"
#include "Dict.h"


namespace darkcore
{
    namespace lzw
    {
        /* Decompress
         *
         * This utility class is used to decompress source data using the LZW
         * Decompression Algorithm
         */

        class Decompress
        {
          public:
            // constructor & destructor
            Decompress(const unsigned char *data, long length);
            ~Decompress();
            
            // get functions
            long getUncompressedSize() const;
            void getUncompressedData(unsigned char *data) const;

          private:
            // static data decompression funtions
            static long readUncompressedSize(const unsigned char *data);
            static bool isValidLZWData(const unsigned char *data, int length);

            // main decompression routine
            void decompress();

            // decompression supporting functions
            int getNextCodeword(long &bits_read, int codeword_size) const;
            void outputRoot(unsigned char root, long &position);
            void getString(int codeword);
            int readSource(int index) const;

            // object reinitialization functions
            void initStack();
            void initDictionary();

            unsigned char *source;          // source data
            unsigned char *destination;     // destination data
            long source_length;             // size of source data
            long destination_length;        // size of destination data
            std::stack<unsigned char> stack;// a stack
            Dict *dict;                     // a dictionary
        };

        // Decompress inline functions
        inline long Decompress::getUncompressedSize() const
            { return destination_length; }
        inline void Decompress::getUncompressedData(unsigned char *data) const
            { memcpy(data, destination, destination_length); }


        /* Compress
         *
         * This utility class is used to encode source data in a form that is
         * readable by an LZW decompressor.  Note that this is not an
         * implementation of the LZW compression algorithm.
         */

        class Compress
        {
          public:
            // constructor & destructor
            Compress(const unsigned char *data, long length);
            ~Compress();

            // get functions
            long getCompressedSize() const;
            void getCompressedData(unsigned char *data) const;

          private:
            // supporting functions
            void write9(int nbyte);
            void write4(long outval);

            // main compression routine
            void compress();

            // main data members
            unsigned char *source;          // source data
            unsigned char *destination;     // destination data
            long source_length;             // size of source data
            long destination_length;        // size of destination data
            long source_pos;                // position in source data
            long destination_pos;           // position in destination data
            long bits_written;              // number of bits written

            // constants
            static int BLOCK_SIZE;
        };

        // Compress inline functions
        inline long Compress::getCompressedSize() const
            { return destination_length; }
        inline void Compress::getCompressedData(unsigned char *data) const
            { memcpy(data, destination, destination_length); }


        /* CompressionException
         * 
         * This class is thrown if any error occurs during compression
         * or decompression of the source data.
         */

        class CompressionException : public Exception
        {
          public:
            CompressionException(const std::string &errmsg);
            CompressionException(const CompressionException &ex);
            ~CompressionException() throw ();
        };
    }
}


#endif
