/* LZWIOStream.h */

#ifndef DARKCORE_LZW_LZWIOSTREAM_H_
#define DARKCORE_LZW_LZWIOSTREAM_H_

#include <istream>                  // std::istream
#include <memory>                   // std::unique_ptr
#include <ostream>                  // std::ostream
#include <streambuf>                // std::streamsize
#include "util/ByteArrayIOStream.h" // ByteArrayInputStream, ByteArrayOutputStream
#include "util/FilterIOStream.h"    // FilterStreamBuffer, FilterInputStream, FilterOutputStream

namespace darkcore
{
    namespace lzw
    {
        /**
         * The stream buffer containing the byte array to be read from
         * or written to.
         */
        class LZWStreamBuffer : public darkcore::util::FilterStreamBuffer
        {
          public:
            /**
             * Constructs a LZW stream buffer that wraps an compressed input stream.
             * @param is Wrapped input stream
             */
            LZWStreamBuffer(std::istream &is);

            /**
             * Constructs a LZW stream buffer that wraps an compressed input stream.
             * Ownership is transferred to this stream buffer.
             * @param is Wrapped input stream
             */
            LZWStreamBuffer(std::unique_ptr<std::istream> is);

            /**
             * Constructs a LZW stream buffer that wraps an compressed output stream.
             * @param os Wrapped output stream
             */
            LZWStreamBuffer(std::ostream &os);

            /**
             * Constructs a LZW stream buffer that wraps an compressed output stream.
             * Ownership is transferred to this stream buffer.
             * @param os Wrapped output stream
             */
            LZWStreamBuffer(std::unique_ptr<std::ostream> os);

            /**
             * Destructs the stream buffer.
             */
            virtual ~LZWStreamBuffer();

            /**
             * Returns the size of the decompressed file.
             * @return Size
             */
            int getDecompressedSize();

          protected:
            virtual std::streamsize showmanyc();
            virtual int pbackfail(int c = traits_type::eof());
            virtual int uflow();
            virtual int underflow();
            virtual int overflow(int c = traits_type::eof());
            virtual int sync();
            virtual std::streamsize xsgetn(char *s, std::streamsize n);
            virtual std::streamsize xsputn(const char *s, std::streamsize n);
            virtual std::streampos seekoff(std::streamoff off, std::ios_base::seekdir way,
                    std::ios_base::openmode which = std::ios_base::in | std::ios_base::out);

          private:
            void compressData();
            void decompressData();

            darkcore::util::ByteArrayInputStream *bais;
            darkcore::util::ByteArrayOutputStream *baos;
            unsigned char *inDecompData;
            int outStart;
        };

        inline int LZWStreamBuffer::getDecompressedSize()
        {
            return showmanyc();
        }


        /**
         * An input stream that reads LZW-compressed data from another stream,
         * returning the decompressed data.
         */
        class LZWInputStream : public darkcore::util::FilterInputStream
        {
          public:
            /**
             * Constructs an LZW input stream around the specified input stream.
             * @param innerStream Wrapped input stream
             */
            LZWInputStream(std::istream &innerStream);

            /**
             * Constructs an LZW input stream around the specified input stream.
             * Ownership is transferred to this stream.
             * @param innerStream Wrapped input stream
             */
            LZWInputStream(std::unique_ptr<std::istream> innerStream);

            /**
             * Destructs the input stream.
             */
            virtual ~LZWInputStream();

            /**
             * Returns the size of the decompressed file.
             * @return Size
             */
            int getDecompressedSize();
        };

        inline int LZWInputStream::getDecompressedSize()
        {
            LZWStreamBuffer *lzwbuf = dynamic_cast<LZWStreamBuffer *>(rdbuf());
            return lzwbuf->getDecompressedSize();
        }


        /**
         * An output stream that flushes LZW-compressed data to another stream.
         */
        class LZWOutputStream : public darkcore::util::FilterOutputStream
        {
          public:
            /**
             * Constructs an LZW output stream around the specified output stream.
             * @param innerStream Wrapped output stream
             */
            LZWOutputStream(std::ostream &innerStream);

            /**
             * Constructs an LZW output stream around the specified output stream.
             * Ownership is transferred to this stream.
             * @param innerStream Wrapped output stream
             */
            LZWOutputStream(std::unique_ptr<std::ostream> innerStream);

            /**
             * Destructs the output stream.
             */
            virtual ~LZWOutputStream();
        };
    }
}

#endif
