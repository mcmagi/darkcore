/* Dict.h */

#ifndef DARKCORE_LZW_DICT_H_
#define DARKCORE_LZW_DICT_H_


namespace darkcore
{
    namespace lzw
    {
        /* Dict
         *
         * A dictionary
         */

        class Dict
        {
          public:
            Dict(int size);
            ~Dict();

            void add(unsigned char root, int codeword);
            unsigned char getRoot(int codeword) const;
            int getCodeword(int codeword) const;

          private:
            struct dict_entry
            {
                unsigned char root;
                int codeword;
            };

            struct dict_entry *dict;
            int contains;
            int size;
        };

        // inline methods
        inline unsigned char Dict::getRoot(int codeword) const
            { return (dict[codeword].root); }
        inline int Dict::getCodeword(int codeword) const
            { return (dict[codeword].codeword); }
    }
}


#endif
