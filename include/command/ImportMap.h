#ifndef DARKCORE_COMMAND_IMPORTMAP_H
#define	DARKCORE_COMMAND_IMPORTMAP_H

#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include "command/Command.h"            // Command, CommandFactory
#include "config/UltimaGameConfig.h"    // UltimaGame
#include "util/File.h"                  // File

// prototypes
namespace darkcore
{
    namespace command
    {
        class Arguments;
    }
}

namespace darkcore
{
    namespace command
    {
        /**
         * Represents a "darkcore map view" command.
         */
        class ImportMap : public darkcore::command::Command
        {
        public:
            class Factory : public darkcore::command::CommandFactory<ImportMap>
            {
            public:
                std::unique_ptr<ImportMap> create(const darkcore::command::Arguments &args) const;
                std::string help() const;
            };
        
            ImportMap(darkcore::config::UltimaGame game, const darkcore::util::File file, const darkcore::util::File textFile,
                std::unique_ptr<darkcore::util::File> destFile);
            void run() const;
        private:
            darkcore::config::UltimaGame game;
            darkcore::util::File file;
            darkcore::util::File textFile;
            std::unique_ptr<darkcore::util::File> destFile;
        };

        inline ImportMap::ImportMap(darkcore::config::UltimaGame game, const darkcore::util::File file,
                const darkcore::util::File textFile, std::unique_ptr<darkcore::util::File> destFile) :
            game(game), file(file), textFile(textFile), destFile(std::move(destFile))
        {
        }
    }
}

#endif