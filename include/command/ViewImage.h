#ifndef DARKCORE_COMMAND_VIEWIMAGE_H
#define	DARKCORE_COMMAND_VIEWIMAGE_H

#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include "command/Command.h"            // Command, CommandFactory
#include "config/UltimaGameConfig.h"    // UltimaGame
#include "util/File.h"                  // File

// prototypes
namespace darkcore
{
    namespace command
    {
        class Arguments;
    }
}

namespace darkcore
{
    namespace command
    {
        /**
         * Represents a "darkcore image view" command.
         */
        class ViewImage : public darkcore::command::Command
        {
        public:
            class Factory : public darkcore::command::CommandFactory<ViewImage>
            {
            public:
                std::unique_ptr<ViewImage> create(const darkcore::command::Arguments &args) const;
                std::string help() const;
            };
        
            ViewImage(darkcore::config::UltimaGame game, const darkcore::util::File file, bool showall);
            void run() const;
        private:
            darkcore::config::UltimaGame game;
            darkcore::util::File file;
            bool showall;
        };
    }
}

#endif