/* Arguments.h */

#ifndef DARKCORE_COMMAND_ARGUMENTS_H
#define	DARKCORE_COMMAND_ARGUMENTS_H

#include <string>               // std:string
#include <unordered_set>        // std:unordered_set
#include <vector>               // std:vector

namespace darkcore
{
    namespace command
    {
        /**
         * Contains command arguments passed to the 'darkcore' tool.
         */
        class Arguments
        {
        public:
            Arguments(const std::string program, const std::string command,
                    const std::string subcommand, const std::vector<std::string> arguments);
            Arguments(const Arguments &args);

            const std::string & getProgram() const;
            const std::string & getCommand() const;
            const std::string & getSubCommand() const;
            const std::vector<std::string> & getArguments() const;
        private:
            const std::string program;
            const std::string command;
            const std::string subcommand;
            const std::vector<std::string> arguments;
        };

        // inline method implementations

        inline const std::string & Arguments::getProgram() const
        {
            return program;
        }

        inline const std::string & Arguments::getCommand() const
        {
            return command;
        }

        inline const std::string & Arguments::getSubCommand() const
        {
            return subcommand;
        }

        inline const std::vector<std::string> & Arguments::getArguments() const
        {
            return arguments;
        }

        /**
         * Constructs Arguments objects.  Subcommands can be registered with
         * the factory using {@link registerNestedCommand(const std::string)}.
         */
        class ArgumentsFactory
        {
        public:
            /**
             * Parses command arguments and returns an Arguments instance.
             * @param argc
             * @param argv
             * @return Command arguments instance
             */
            Arguments create(int argc, char *argv[]) const;

            /**
             * Registers a command that contains a sub command.
             * @param command
             */
            void registerNestedCommand(const std::string command);
        private:
            // vector utility methods (should be in a ContainerUtils or something)
            static std::vector<std::string> subset(std::vector<std::string> orig, int start);
            static std::vector<std::string> subset(std::vector<std::string> orig, int start, int end);

            /**
             * Returns true if this command expects sub-commands.
             * @param command
             * @return 
             */
            bool hasSubCommand(const std::string &command) const;

            std::unordered_set<std::string> nestedCommands;
        };
    }
}

#endif