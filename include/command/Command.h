/* Command.h */

#ifndef DARKCORE_COMMAND_COMMAND_H
#define	DARKCORE_COMMAND_COMMAND_H

#include <memory>               // std:unique_ptr
#include <string>               // std:string
#include <vector>               // std:vector
#include "Exception.h"          // Exception

// prototype
namespace darkcore
{
    namespace command
    {
        class Arguments;
    }
}

namespace darkcore
{
    namespace command
    {
        /**
         * An abstraction of a 'darkcore' tool command.  This is used as the
         * base class for all commands invoked by the 'darkcore' tool.
         */
        class Command
        {
        public:
            /**
             * Runs the command.
             */
            virtual void run() const = 0;
        };
        
        /**
         * A Factory class that creates desired Command objects by parsing
         * the arguments array.
         * @param T command
         */
        template <class T> // where T should be a Command
        class CommandFactory
        {
        public:
            /**
             * Creates a Command object by parsing the arguments array.  The
             * array should be a subset of the commands provided to the tool;
             * only those that pertain to this command.
             * @param args
             * @return Command object
             */
            virtual std::unique_ptr<T> create(const darkcore::command::Arguments &args) const = 0;

            /**
             * Returns a help message.
             * @return Help message.
             */
            virtual std::string help() const = 0;
        };
        
        /**
         * An exception thrown by the CommandFactory.  The error message will
         * contain the help message to be printed to stdout/stderr.
         */
        class UsageException : public darkcore::Exception
        {
        public:
            UsageException(const std::string helpMessage);
            UsageException(const darkcore::command::Arguments &args, const std::string helpMessage);
            UsageException(const UsageException &ex);
            ~UsageException() throw ();

            std::string getProgram() const;
            std::string getCommand() const;
            std::string getSubCommand() const;
        private:
            const std::string program;
            const std::string command;
            const std::string subcommand;
        };

        inline std::string UsageException::getProgram() const
        {
            return program;
        }

        inline std::string UsageException::getCommand() const
        {
            return command;
        }

        inline std::string UsageException::getSubCommand() const
        {
            return subcommand;
        }
    }
}

#endif