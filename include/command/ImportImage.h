#ifndef DARKCORE_COMMAND_IMPORTIMAGE_H
#define	DARKCORE_COMMAND_IMPORTIMAGE_H

#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include "command/Command.h"            // Command, CommandFactory
#include "config/UltimaGameConfig.h"    // UltimaGame
#include "util/File.h"                  // File

// prototypes
namespace darkcore
{
    namespace command
    {
        class Arguments;
    }
}

namespace darkcore
{
    namespace command
    {
        /**
         * Represents a "darkcore image view" command.
         */
        class ImportImage : public darkcore::command::Command
        {
        public:
            class Factory : public darkcore::command::CommandFactory<ImportImage>
            {
            public:
                std::unique_ptr<ImportImage> create(const darkcore::command::Arguments &args) const;
                std::string help() const;
            };
        
            ImportImage(darkcore::config::UltimaGame game, const darkcore::util::File file, const darkcore::util::File pngFile);
            void run() const;
        private:
            darkcore::config::UltimaGame game;
            darkcore::util::File file;
            darkcore::util::File pngFile;
        };
    }
}

#endif