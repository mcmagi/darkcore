#ifndef DARKCORE_COMMAND_VIEWMAP_H
#define	DARKCORE_COMMAND_VIEWMAP_H

#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include "command/Command.h"            // Command, CommandFactory
#include "config/UltimaGameConfig.h"    // UltimaGame
#include "util/File.h"                  // File

// prototypes
namespace darkcore
{
    namespace command
    {
        class Arguments;
    }
}

namespace darkcore
{
    namespace command
    {
        /**
         * Represents a "darkcore map view" command.
         */
        class ViewMap : public darkcore::command::Command
        {
        public:
            class Factory : public darkcore::command::CommandFactory<ViewMap>
            {
            public:
                std::unique_ptr<ViewMap> create(const darkcore::command::Arguments &args) const;
                std::string help() const;
            };
        
            ViewMap(darkcore::config::UltimaGame game, const darkcore::util::File file, std::unique_ptr<darkcore::util::File> textFile);
            void run() const;
        private:
            darkcore::config::UltimaGame game;
            darkcore::util::File file;
            std::unique_ptr<darkcore::util::File> textFile;
        };
    }
}

#endif