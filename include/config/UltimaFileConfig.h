/* UltimaFileConfig.h */

#ifndef DARKCORE_CONFIG_ULTIMAFILECONFIG_H_
#define DARKCORE_CONFIG_ULTIMAFILECONFIG_H_

#include <iostream>             // std::istream, std::ostream
#include <memory>               // std::unique_ptr
#include <string>               // std::string
#include "Flags.h"              // Flags
#include "UltimaGameConfig.h"   // UltimaGameConfig
#include "util/File.h"          // File

namespace darkcore
{
    namespace config
    {
        class UltimaFileConfig
        {
          public:
            static const Flags COMPRESSED;

            /**
             * Constructs an Ultima file configuration for the specified filename.
             * @param filename Name of file
             * @param flags Any file flags (default none)
             * @param offset Offset into file
             */
            UltimaFileConfig(const std::string &filename,
                    const Flags &flags = Flags::NONE,
                    int offset = 0);

            /**
             * Copy-constructs from another Ultima file configuration.
             * @param cfg Other configuration
             */
            UltimaFileConfig(const UltimaFileConfig &cfg);

            /**
             * Destructor.
             */
            ~UltimaFileConfig();

            /**
             * Assigns from another Ultima file configuration.
             * @param cfg Other configuration
             * @return This configuration
             */
            UltimaFileConfig & operator=(const UltimaFileConfig &cfg);

            void setGameConfig(const UltimaGameConfig &gameCfg);
            const UltimaGameConfig & getGameConfig() const;
            const std::string & getFilename() const;
            bool isCompressed() const;
            int getOffset() const;

            /**
             * Returns the File object for this file.
             * @return File
             */
            darkcore::util::File getFile() const;

            /**
             * Constructs the input stream as configured for this file.
             * The returned object is owned by the caller and must be
             * explicitly deleted.
             * @return Input stream
             */
            std::unique_ptr<std::istream> newInputStream() const;

            /**
             * Constructs the output stream as configured for this file.
             * The returned object is owned by the caller and must be
             * explicitly deleted.
             * @return Output stream
             */
            std::unique_ptr<std::ostream> newOutputStream() const;

            /**
             * Returns a new instance of the config, but referencing the
             * specified filename.
             * @param newFilename Replacement filename
             * @return New config
             */
            virtual std::unique_ptr<UltimaFileConfig> forFile(const std::string &newFilename) const;

          private:
            const UltimaGameConfig *gameCfg;
            std::string filename;
            Flags flags;
            int offset;
        };

        inline void UltimaFileConfig::setGameConfig(const UltimaGameConfig &gameCfg)
        {
            this->gameCfg = &gameCfg;
        }

        inline const UltimaGameConfig & UltimaFileConfig::getGameConfig() const
        {
            return *gameCfg;
        }

        inline const std::string & UltimaFileConfig::getFilename() const
        {
            return filename;
        }

        inline bool UltimaFileConfig::isCompressed() const
        {
            return flags & COMPRESSED;
        }

        inline int UltimaFileConfig::getOffset() const
        {
            return offset;
        }

        inline darkcore::util::File UltimaFileConfig::getFile() const
        {
            return darkcore::util::File(gameCfg->getPath(), filename);
        }
    }
}

#endif
