/* U2CGAIndexConfig.h */

#ifndef DARKCORE_CONFIG_U2CGAINDEXCONFIG_H_
#define DARKCORE_CONFIG_U2CGAINDEXCONFIG_H_

#include <memory>               // std::unique_ptr
#include "IndexConfig.h"        // IndexConfig
#include "util/Reader.h"        // Reader
#include "util/Writer.h"        // Writer

// prototypes
namespace darkcore
{
    namespace config
    {
        class IndexEntry;
    }
}

namespace darkcore
{
    namespace config
    {
        /**
         * Represents the configuration for Ultima 2's image index.
         */
        class U2CGAIndexConfig : public IndexConfig
        {
          public:
            /**
             * Creates the index configuration with the specified padding
             * (or # of bytes between images).  The image size is hard-coded
             * for now (possible item to address in the future).
             * @param size Number of entries in the map index
             * @param padding Bytes between images
             */
            U2CGAIndexConfig(int size, int padding = 0);

            /**
             * Copy constructs this configuration from another configuration.
             * @param index Other configuration
             */
            U2CGAIndexConfig(const U2CGAIndexConfig &idx);

            /**
             * Destructor.
             */
            ~U2CGAIndexConfig();

            /**
             * Assigns this configuration from another configuration.
             * @param idx Other configuration
             * @return This configuration
             */
            U2CGAIndexConfig & operator=(const U2CGAIndexConfig &idx);

            /**
             * Assigns this configuration from another configuration.
             * @param idx Other configuration
             * @return This configuration
             */
            IndexConfig & operator=(const IndexConfig &idx);

            /**
             * Returns a copy of the IndexConfig.  The returned object is owned
             * by the caller and must be explicitly deleted.
             * @return IndexConfig copy
             */
            std::unique_ptr<IndexConfig> clone() const;

            /**
             * Returns a Reader for the Ultima 2 image index.  The returned
             * object is owned by the caller and must be explicitly deleted.
             * @return Index Reader
             */
            std::unique_ptr<darkcore::util::Reader<IndexEntry> > newEntryReader() const;

            /**
             * Always returns NULL.  There are no index Writers for Ultima 2's
             * images.
             * @return NULL
             */
            std::unique_ptr<darkcore::util::Writer<IndexEntry> > newEntryWriter() const;

            /**
             * Returns the padding (in bytes) between images in the tile set.
             * @return Padding
             */
            int getPadding() const;

            /**
             * Returns the size of the images in the tile set.
             * @return Image size
             */
            int getImageSize() const;

          private:
            /**
             * A reader which returns U2 index entries in sequence.
             */
            class IndexEntryReader : public darkcore::util::Reader<IndexEntry>
            {
              public:
                /**
                 * Constructs a Reader for the specified config.
                 * @param cfg U2 Index Config
                 */
                IndexEntryReader(const U2CGAIndexConfig &cfg);

                /**
                 * Destructor.
                 */
                ~IndexEntryReader();

                /**
                 * Returns the next index entry.
                 * @return Index entry
                 */
                IndexEntry read();

                /**
                 * Virtual index, so always returns false.
                 * @return False
                 */
                bool end() const;
                
              private:
                const U2CGAIndexConfig &cfg;
                int offset; // first 64 tiles
                int extraOffset; // extra tile - attack 'circle'
                int counter;
            };

            int padding;
            int imgSize;
        };

        inline int U2CGAIndexConfig::getPadding() const
        {
            return padding;
        }

        inline int U2CGAIndexConfig::getImageSize() const
        {
            return imgSize;
        }

        inline U2CGAIndexConfig::IndexEntryReader::IndexEntryReader(
                const U2CGAIndexConfig &cfg) :
            darkcore::util::Reader<IndexEntry>(),
            cfg(cfg), offset(0x7c42), extraOffset(0x5488), counter(0)
        {
        }

        inline U2CGAIndexConfig::IndexEntryReader::~IndexEntryReader()
        {
        }

        inline bool U2CGAIndexConfig::IndexEntryReader::end() const
        {
            return cfg.getSize() >= 0 && counter >= cfg.getSize();
        }
    }
}

#endif
