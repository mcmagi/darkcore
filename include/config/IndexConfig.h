/* IndexConfig.h */

#ifndef DARKCORE_CONFIG_INDEXCONFIG_H_
#define DARKCORE_CONFIG_INDEXCONFIG_H_

#include <memory>               // std::unique_ptr
#include "util/Reader.h"        // Reader
#include "util/Writer.h"        // Writer

// prototypes
namespace darkcore
{
    namespace config
    {
        class IndexEntry;
        class UltimaGameConfig;
    }
}

namespace darkcore
{
    namespace config
    {
        /**
         * Represents the configuration for a resource index.
         */
        class IndexConfig
        {
          public:
            /**
             * Destructor.
             */
            virtual ~IndexConfig();

            /**
             * Protected assignment operator.
             * @param idx Other index configuration
             * @return This index configuration
             */
            virtual IndexConfig & operator=(const IndexConfig &idx);

            /**
             * Returns a copy of the IndexConfig.  The returned object is owned
             * by the caller and must be explicitly deleted.
             * @return IndexConfig copy
             */
            virtual std::unique_ptr<IndexConfig> clone() const = 0;

            /**
             * Returns a Reader for the resource index.  The returned object is
             * owned by the caller and must be explicitly deleted.
             * @return Index Reader
             */
            virtual std::unique_ptr<darkcore::util::Reader<IndexEntry> > newEntryReader() const = 0;

            /**
             * Returns a Writer to the resource index.  The returned object is
             * owned by the caller and must be explicitly deleted.
             * @return Index Writer
             */
            virtual std::unique_ptr<darkcore::util::Writer<IndexEntry> > newEntryWriter() const = 0;

            /**
             * Constraints the number of entries in the index.
             * @return Number of entries
             */
            virtual void setSize(int size);

            /**
             * Returns the number of entries in the index.
             * @return Number of entries
             */
            int getSize() const;

            virtual void setGameConfig(const UltimaGameConfig &gameCfg);

            const UltimaGameConfig & getGameConfig() const;

          protected:
            /**
             * Protected constructor.
             * @param size Number of entries
             */
            IndexConfig(int size = -1);

            /**
             * Protected copy-constructor.
             * @param idx Other index configuration
             */
            IndexConfig(const IndexConfig &idx);

          private:
            const UltimaGameConfig *gameCfg;
            int size;
        };

        inline IndexConfig::IndexConfig(int size) :
            gameCfg(NULL), size(size)
        {
        }

        inline IndexConfig::IndexConfig(const IndexConfig &idx) :
            gameCfg(idx.gameCfg), size(idx.size)
        {
        }

        inline IndexConfig::~IndexConfig()
        {
        }

        inline IndexConfig & IndexConfig::operator=(const IndexConfig &idx)
        {
            if (this != &idx)
            {
                gameCfg = idx.gameCfg;
                size = idx.size;
            }
            return *this;
        }

        inline void IndexConfig::setSize(int size)
        {
            this->size = size;
        }

        inline int IndexConfig::getSize() const
        {
            return size;
        }

        inline void IndexConfig::setGameConfig(const UltimaGameConfig &gameCfg)
        {
            this->gameCfg = &gameCfg;
        }

        inline const UltimaGameConfig & IndexConfig::getGameConfig() const
        {
            return *gameCfg;
        }
    }
}

#endif
