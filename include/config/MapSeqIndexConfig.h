/* MapSeqIndexConfig.h */

#ifndef DARKCORE_CONFIG_MAPSEQINDEXCONFIG_H_
#define DARKCORE_CONFIG_MAPSEQINDEXCONFIG_H_

#include <memory>               // std::unique_ptr
#include "IndexConfig.h"        // IndexConfig
#include "util/Reader.h"        // Reader
#include "util/Writer.h"        // Writer

namespace darkcore
{
    namespace config
    {
        /**
         * Represents the configuration for a sequential map index.
         */
        class MapSeqIndexConfig : public IndexConfig
        {
          public:
            /**
             * Creates the index configuration.
             * @param size Number of entries in the map index
             */
            MapSeqIndexConfig(int size = -1);

            /**
             * Copy constructs this configuration from another.
             * @param index Other configuration
             */
            MapSeqIndexConfig(const MapSeqIndexConfig &idx);

            /**
             * Destructor.
             */
            ~MapSeqIndexConfig();

            /**
             * Assigns this configuration from another.
             * @param idx Other configuration
             * @return This configuration
             */
            MapSeqIndexConfig & operator=(const MapSeqIndexConfig &idx);

            /**
             * Assigns this configuration from another configuration.
             * @param idx Other configuration
             * @return This configuration
             */
            IndexConfig & operator=(const IndexConfig &idx);

            /**
             * Returns a copy of the IndexConfig.  The returned object is owned
             * by the caller and must be explicitly deleted.
             * @return IndexConfig copy
             */
            std::unique_ptr<IndexConfig> clone() const;

            /**
             * Returns a Reader for the Ultima 6 image index.  The returned
             * object is owned by the caller and must be explicitly deleted.
             * @return Index Reader
             */
            std::unique_ptr<darkcore::util::Reader<IndexEntry> > newEntryReader() const;

            /**
             * Returns a Writer for the Ultima 6 image index.  The returned
             * object is owned by the caller and must be explicitly deleted.
             * @return Index Writer
             */
            std::unique_ptr<darkcore::util::Writer<IndexEntry> > newEntryWriter() const;

          private:
            /**
             * A reader which returns U4-style map chunk index entries.
             */
            class IndexEntryReader : public darkcore::util::Reader<IndexEntry>
            {
              public:
                /**
                 * Constructs a Reader for the specified config.
                 * @param cfg U6 Index Config
                 */
                IndexEntryReader(const MapSeqIndexConfig &cfg);

                /**
                 * Destructor.
                 */
                ~IndexEntryReader();

                /**
                 * Returns the next index entry.
                 * @return Index entry
                 */
                IndexEntry read();

                /**
                 * Returns whether we are at the end of the index.
                 * @return True if at end
                 */
                bool end() const;

              private:
                const MapSeqIndexConfig &cfg;
                int counter;
            };
        };
    }
}

#endif
