/* UltimaMapSetConfig.h */

#ifndef DARKCORE_CONFIG_ULTIMAMAPSETCONFIG_H_
#define DARKCORE_CONFIG_ULTIMAMAPSETCONFIG_H_

#include <map>                  // std::map
#include <string>               // std::string
#include "UltimaMapConfig.h"    // UltimaMapConfig

// prototypes
namespace darkcore
{
    namespace config
    {
        class UltimaFileConfig;
        class UltimaGameConfig;
        class UltimaImageConfig;
    }
}

namespace darkcore
{
    namespace config
    {
        /**
         * Represents a configuration for a collection of related maps.  For
         * example some maps are spread across multiple files with chunk data
         * and chunk indices, and others have multiple heterogenous maps within
         * the same file.  U6's MAP file is a prime example of both.  The
         * filename used here is largely symbolic.  The actual method used to
         * load the map indices is within each UltimaMapConfig.
         */
        class UltimaMapSetConfig
        {
          public:
            UltimaMapSetConfig(const std::string &filename, const std::vector<UltimaMapConfig> &mapCfg,
                    const std::map<int,const UltimaImageConfig *> &tileSetCfg);

            UltimaMapSetConfig(const std::string &filename, const UltimaMapConfig &mapCfg,
                    const std::map<int,const UltimaImageConfig *> &tileSetCfg);

            UltimaMapSetConfig(const std::string &filename, std::string &description,
                    const std::map<int,const UltimaImageConfig *> &tileSetCfg, int width, int height,
                    int numMaps = 1, const Flags &flags = Flags::NONE, const UltimaMapConfig *chunkCfg = NULL);

            UltimaMapSetConfig(const UltimaFileConfig &fileCfg, const std::string &description,
                    const std::map<int,const UltimaImageConfig *> &tileSetCfg, int width, int height,
                    int numMaps = 1, const Flags &flags = Flags::NONE, const UltimaMapConfig *chunkCfg = NULL);

            /**
             * Copy-constructs from another Ultima map set configuration.
             * @param cfg Other configuration
             */
            UltimaMapSetConfig(const UltimaMapSetConfig &cfg);

            /**
             * Destructor.
             */
            ~UltimaMapSetConfig();

            /**
             * Assigns from another Ultima map set configuration.
             * @param cfg Other configuration
             * @return This configuration
             */
            UltimaMapSetConfig & operator=(const UltimaMapSetConfig &cfg);

            void setGameConfig(const UltimaGameConfig &gameCfg);

            const UltimaGameConfig & getGameConfig() const;
            const std::filename & getFilename() const;
            bool hasTileSetConfig(int bpp) const;
            const UltimaImageConfig & getTileSetConfig(int bpp = 0) const;
            const UltimaImageConfig & getBestTileSetConfig() const;
            const std::vector<UltimaMapConfig> & getMapConfigs() const;

          private:
            typedef std::map<int,const UltimaImageConfig *> TileSetMap;

            const UltimaGameConfig *gameCfg;
            std::string filename;
            TileSetMap tileSetCfgs;
            std::vector<UltimaMapConfig> mapCfgs;
        };

        inline void UltimaMapConfig::setGameConfig(const UltimaGameConfig &gameCfg)
        {
            this->gameCfg = &gameCfg;
            for (int i = 0; i < mapCfgs.size(); i++)
                mapCfgs[i].setGameConfig(gameCfg);
        }

        inline const UltimaGameConfig & UltimaMapConfig::getGameConfig() const
        {
            return *gameCfg;
        }

        inline const std::string & UltimaMapConfig::getFilename() const
        {
            return filename;
        }

        inline const UltimaMapConfig & getMapConfigs() const
        {
            return mapCfgs;
        }
    }
}

#endif
