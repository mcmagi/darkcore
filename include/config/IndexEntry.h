/* IndexEntry.h */

#ifndef DARKCORE_CONFIG_INDEXENTRY_H_
#define DARKCORE_CONFIG_INDEXENTRY_H_

namespace darkcore
{
    namespace config
    {
        /**
         * Represents an entry within an ImageIndex.
         */
        class IndexEntry
        {
          public:
            enum MaskType
            {
                NORMAL,

                // image types
                TRANSPARENT,
                U6CGA_PIXEL_BLOCK,
                U6VGA_PIXEL_BLOCK,

                // map types
                ALL_WATER_CHUNK
            };

            IndexEntry(int offset, MaskType maskType = NORMAL);
            IndexEntry(const IndexEntry &idx);
            ~IndexEntry();

            IndexEntry & operator=(const IndexEntry &idx);

            int getOffset() const;
            MaskType getMaskType() const;

          private:
            int offset;
            MaskType maskType;
        };

        inline IndexEntry::IndexEntry(int offset, MaskType maskType) :
            offset(offset), maskType(maskType)
        {
        }

        inline IndexEntry::IndexEntry(const IndexEntry &idx) :
            offset(idx.offset), maskType(idx.maskType)
        {
        }

        inline IndexEntry::~IndexEntry()
        {
        }

        inline IndexEntry & IndexEntry::operator=(const IndexEntry &idx)
        {
            if (this != &idx)
            {
                offset = idx.offset;
                maskType = idx.maskType;
            }
            return *this;
        }

        inline int IndexEntry::getOffset() const
        {
            return offset;
        }

        inline IndexEntry::MaskType IndexEntry::getMaskType() const
        {
            return maskType;
        }
    }
}

#endif
