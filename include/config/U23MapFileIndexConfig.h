/* U23MapFileIndexConfig.h */

#ifndef DARKCORE_CONFIG_U23MAPFILEINDEXCONFIG_H_
#define DARKCORE_CONFIG_U23MAPFILEINDEXCONFIG_H_

#include <memory>               // std::unique_ptr
#include "IndexConfig.h"        // IndexConfig
#include "MapFileIndexConfig.h" // MapFileIndexConfig
#include "util/Reader.h"        // Reader
#include "util/Writer.h"        // Writer

// prototypes
namespace darkcore
{
    namespace config
    {
        class UltimaFileConfig;
    }
}

namespace darkcore
{
    namespace config
    {
        /**
         * Represents the configuration for a U2/U3 file-based map index.
         */
        class U23MapFileIndexConfig : public MapFileIndexConfig
        {
          public:
            /**
             * Creates the index configuration.
             * @param size Number of entries in the map index
             * @param filename Filename
             */
            U23MapFileIndexConfig(const std::string &filename, int size = -1);

            /**
             * Creates the index configuration.
             * @param size Number of entries in the map index
             * @param fileCfg File configuration
             */
            U23MapFileIndexConfig(const UltimaFileConfig &fileCfg, int size = -1);

            /**
             * Copy constructs this configuration from another.
             * @param index Other configuration
             */
            U23MapFileIndexConfig(const U23MapFileIndexConfig &idx);

            /**
             * Destructor.
             */
            ~U23MapFileIndexConfig();

            /**
             * Assigns this configuration from another.
             * @param idx Other configuration
             * @return This configuration
             */
            U23MapFileIndexConfig & operator=(const U23MapFileIndexConfig &idx);

            /**
             * Assigns this configuration from another.
             * @param idx Other configuration
             * @return This configuration
             */
            MapFileIndexConfig & operator=(const MapFileIndexConfig &idx);

            /**
             * Assigns this configuration from another configuration.
             * @param idx Other configuration
             * @return This configuration
             */
            IndexConfig & operator=(const IndexConfig &idx);

            /**
             * Returns a copy of the IndexConfig.  The returned object is owned
             * by the caller and must be explicitly deleted.
             * @return IndexConfig copy
             */
            std::unique_ptr<IndexConfig> clone() const;

            /**
             * Returns a Reader for the Ultima 6 image index.  The returned
             * object is owned by the caller and must be explicitly deleted.
             * @return Index Reader
             */
            std::unique_ptr<darkcore::util::Reader<IndexEntry> > newEntryReader() const;

            /**
             * Returns a Writer for the Ultima 6 image index.  The returned
             * object is owned by the caller and must be explicitly deleted.
             * @return Index Writer
             */
            std::unique_ptr<darkcore::util::Writer<IndexEntry> > newEntryWriter() const;

            /**
             * Returns the multiplier used in the index data.
             * @return Multiplier
             */
            int getMultiplier() const;

          private:
            /**
             * A reader which returns map chunk index entries from a file.
             */
            class IndexEntryReader : public darkcore::util::Reader<IndexEntry>
            {
              public:
                /**
                 * Constructs a Reader for the specified config.
                 * @param cfg U6 Index Config
                 */
                IndexEntryReader(const U23MapFileIndexConfig &cfg, std::unique_ptr<darkcore::util::Reader<IndexEntry> > reader);

                /**
                 * Destructor.
                 */
                ~IndexEntryReader();

                /**
                 * Returns the next index entry.
                 * @return Index entry
                 */
                IndexEntry read();

                /**
                 * Returns whether we are at the end of the index.
                 * @return True if at end
                 */
                bool end() const;

                /**
                 * Maps the specified index value to a tile index.  The raw indices
                 * differ from the tile indices by a multiplier of 4.
                 * @param value Raw index
                 * @return Tile index
                 */
                int mapIndex(int value) const;

              private:
                const U23MapFileIndexConfig &cfg;
                std::unique_ptr<darkcore::util::Reader<IndexEntry> > reader;
            };

            /**
             * A writer which sends map chunk index entries to a file.
             */
            class IndexEntryWriter : public darkcore::util::Writer<IndexEntry>
            {
              public:
                /**
                 * Constructs a Writer for the specified config.
                 * @param cfg U2/3 Index Config
                 */
                IndexEntryWriter(const U23MapFileIndexConfig &cfg, std::unique_ptr<darkcore::util::Writer<IndexEntry> > writer);

                /**
                 * Destructor.
                 */
                ~IndexEntryWriter();

                /**
                 * Writes the next index entry.
                 * @param entry Index entry
                 */
                void write(const IndexEntry &entry);

                /**
                 * Maps the specified tile index value to a raw index.  The raw indices
                 * differ from the tile indices by a multiplier of 4.
                 * @param value Raw index
                 * @return Tile index
                 */
                int mapIndex(int value) const;

              private:
                const U23MapFileIndexConfig &cfg;
                std::unique_ptr<darkcore::util::Writer<IndexEntry> > writer;
            };

            int multiplier;
        };

        inline int U23MapFileIndexConfig::getMultiplier() const
        {
            return multiplier;
        }

        inline bool U23MapFileIndexConfig::IndexEntryReader::end() const
        {
            return reader->end();
        }
    }
}

#endif
