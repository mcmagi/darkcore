/* UltimaMapConfig.h */

#ifndef DARKCORE_CONFIG_ULTIMAMAPCONFIG_H_
#define DARKCORE_CONFIG_ULTIMAMAPCONFIG_H_

#include <cmath>                // std::pow
#include <map>                  // std::map
#include <memory>               // std::unique_ptr
#include <string>               // std::string
#include "Flags.h"              // Flags
#include "IndexConfig.h"        // IndexConfig
#include "UltimaFileConfig.h"   // UltimaFileConfig

// prototypes
namespace darkcore
{
    namespace config
    {
        class MapFileIndexConfig;
        class UltimaGameConfig;
        class UltimaImageConfig;
    }
}

namespace darkcore
{
    namespace config
    {
        class UltimaMapConfig
        {
          public:
            static const Flags CHUNK;
            static const Flags COMPRESSED_WATER_CHUNK;

            UltimaMapConfig(const std::string &filename, const std::string &description,
                    const std::map<int,const UltimaImageConfig *> &tileSetCfg, int width, int height,
                    int numMaps = 1, const UltimaMapConfig *chunkCfg = NULL, const Flags &flags = Flags::NONE);

            UltimaMapConfig(const UltimaFileConfig &fileCfg, const std::string &description,
                    const std::map<int,const UltimaImageConfig *> &tileSetCfg, int width, int height,
                    int numMaps = 1, const UltimaMapConfig *chunkCfg = NULL, const Flags &flags = Flags::NONE);

            UltimaMapConfig(const MapFileIndexConfig &idxCfg, const std::string &description,
                    const std::map<int,const UltimaImageConfig *> &tileSetCfg, int width, int height,
                    int numMaps = 1, const UltimaMapConfig *chunkCfg = NULL, const Flags &flags = Flags::NONE);

            UltimaMapConfig(const std::string &filename, const IndexConfig &idxCfg, const std::string &description,
                    const std::map<int,const UltimaImageConfig *> &tileSetCfg, int width, int height,
                    int numMaps = 1, const UltimaMapConfig *chunkCfg = NULL, const Flags &flags = Flags::NONE);

            /**
             * Copy-constructs from another Ultima map configuration.
             * @param cfg Other configuration
             */
            UltimaMapConfig(const UltimaMapConfig &cfg);

            /**
             * Destructor.
             */
            ~UltimaMapConfig();

            /**
             * Assigns from another Ultima map configuration.
             * @param cfg Other configuration
             * @return This configuration
             */
            UltimaMapConfig & operator=(const UltimaMapConfig &cfg);

            void setGameConfig(const UltimaGameConfig &gameCfg);

            const UltimaGameConfig & getGameConfig() const;
            const IndexConfig & getIndexConfig() const;
            const std::string & getDescription() const;
            int getHeight() const;
            int getWidth() const;
            int getSize() const;
            int getNumMaps() const;
            bool hasTileSetConfig(int bpp) const;
            const UltimaImageConfig & getTileSetConfig(int bpp = 0) const;
            const UltimaImageConfig & getBestTileSetConfig() const;

            /**
             * Returns true if this map is divided into chunks.  A map is
             * chunked if and only if it has a chunk config.
             * @return True if chunked
             */
            bool isChunked() const;

            /**
             * Returns true if this map represents a chunk.  A chunk is a mapped
             * region of a larger map.
             * @return True if chunk
             */
            bool isChunk() const;

            /**
             * Returns true if this map represents a superchunk.  A superchunk
             * is a map chunk that is also itself chunked.  (i.e., isChunked()
             * and isChunk() both return true)
             * @return True if superchunk
             */
            bool isSuperChunk() const;

            /**
             * Returns true if the 0xFF map index represents a water chunk that
             * is not in the chunk data.  Unique to U5.
             * @return True if has a compressed water chunk
             */
            bool isCompressedWaterChunk() const;

            /**
             * Returns the map configuration for the chunks of this map.
             * @return Map config for chunk
             */
            const UltimaMapConfig * getChunkConfig() const;

            /**
             * Returns the "best guess" filename for this file.
             */
            const std::string & getFilename() const;

            /**
             * Returns a new instance of the config, but referencing the
             * specified filename.
             * @param newFilename Replacement filename
             * @return New config
             */
            virtual UltimaMapConfig forFile(const std::string &newFilename) const;

          private:
            typedef std::map<int,const UltimaImageConfig *> TileSetMap;

            /**
             * Calculates the default expected size of the index based on map
             * properties.
             * @return Default index size
             */
            int getDefaultIndexSize() const;

            /**
             * Derives a default filename for this map based on map properties.
             * @return Default filename
             */
            std::string getDefaultFilename() const;

            const UltimaGameConfig *gameCfg;
            std::unique_ptr<IndexConfig> idxCfg;
            std::string description;
            TileSetMap tileSetCfgs;
            int width;
            int height;
            int numMaps;
            Flags flags;
            std::unique_ptr<UltimaMapConfig> chunkCfg;
            std::string filename;
        };

        inline void UltimaMapConfig::setGameConfig(const UltimaGameConfig &gameCfg)
        {
            this->gameCfg = &gameCfg;
            idxCfg->setGameConfig(gameCfg);
            if (chunkCfg.get() != NULL)
                chunkCfg->setGameConfig(gameCfg);
        }

        inline const UltimaGameConfig & UltimaMapConfig::getGameConfig() const
        {
            return *gameCfg;
        }

        inline const IndexConfig & UltimaMapConfig::getIndexConfig() const
        {
            return *idxCfg;
        }

        inline const std::string & UltimaMapConfig::getDescription() const
        {
            return description;
        }

        inline int UltimaMapConfig::getHeight() const
        {
            return height;
        }

        inline int UltimaMapConfig::getWidth() const
        {
            return width;
        }

        inline int UltimaMapConfig::getSize() const
        {
            return width * height;
        }

        inline int UltimaMapConfig::getNumMaps() const
        {
            return numMaps;
        }

        inline bool UltimaMapConfig::isChunked() const
        {
            return chunkCfg.get() != NULL;
        }

        inline bool UltimaMapConfig::isChunk() const
        {
            return flags & CHUNK;
        }

        inline bool UltimaMapConfig::isSuperChunk() const
        {
            return isChunk() && isChunked();
        }

        inline bool UltimaMapConfig::isCompressedWaterChunk() const
        {
            return flags & COMPRESSED_WATER_CHUNK;
        }

        inline const UltimaMapConfig * UltimaMapConfig::getChunkConfig() const
        {
            return chunkCfg.get();
        }

        inline const std::string & UltimaMapConfig::getFilename() const
        {
            return filename;
        }
    }
}

#endif
