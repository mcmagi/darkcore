/* UltimaGameConfig.h */

#ifndef DARKCORE_CONFIG_ULTIMAGAMECONFIG_H_
#define DARKCORE_CONFIG_ULTIMAGAMECONFIG_H_

#include <map>              // std::map
#include <string>           // std::string
#include <vector>           // std::vector

namespace darkcore
{
    namespace config
    {
        class UltimaImageConfig;
        class UltimaMapConfig;

        enum UltimaGame
        {
            ULTIMA1 = 1, ULTIMA2 = 2, ULTIMA3 = 3, ULTIMA4 = 4, ULTIMA5 = 5, ULTIMA6 = 6
        };

        class UltimaGameConfig
        {
          public:
            /**
             * Copy constructs the Ultima game configuration.
             * @param cfg UltimaGameConfig
             */
            UltimaGameConfig(const UltimaGameConfig &cfg);

            /**
             * Destructor.
             */
            ~UltimaGameConfig();

            /**
             * Assigns from another Ultima game configuration.
             * @param cfg Other configuration
             * @return This configuration
             */
            UltimaGameConfig & operator=(const UltimaGameConfig &cfg);

            /**
             * Returns the game enum this config is associated with.
             */
            UltimaGame getGame() const;

            /**
             * Returns the path the game is located in.
             */
            const std::string & getPath() const;

            /**
             * Returns the image configuration for the given image filename.
             * @param filename Image filename
             * @return Image configuration
             */
            const UltimaImageConfig & getImageConfig(const std::string &filename) const;

            /**
             * Returns the list of image configurations in this game config.
             * @return Image configurations
             */
            std::vector<UltimaImageConfig> getImageConfigs() const;

            /**
             * Returns the map configuration for the given map filename.
             * @param filename Map filename
             * @return Map configuration
             */
            const UltimaMapConfig & getMapConfig(const std::string &filename) const;

            /**
             * Returns the list of map configurations in this game config.
             * @return Map configurations
             */
            std::vector<UltimaMapConfig> getMapConfigs() const;

            /**
             * Factory method returns the game configuration for the specified game and path.
             * @param game Ultima game
             * @param path Path to game
             * @return Game configuration
             */
            static const UltimaGameConfig getGameConfig(UltimaGame game, const std::string &path);

          private:
            /**
             * Construts a game configuration for the specified game and located
             * in the specified path.
             * @param game Ultima game
             * @param path Path to game
             */
            UltimaGameConfig(UltimaGame game, const std::string &path);

            /**
             * Adds an image configuration to this game configuration.
             * @param imgCfg Image configuration
             */
            void addImageConfig(const UltimaImageConfig &imgCfg);

            /**
             * Adds a map configuration to this game configuration.
             * @param mapCfg Map configuration
             */
            void addMapConfig(const UltimaMapConfig &mapCfg);

            typedef std::map<std::string,UltimaImageConfig> ImageConfigMap;
            typedef std::map<std::string,UltimaMapConfig> MapConfigMap;

            UltimaGame game;
            std::string path;
            ImageConfigMap images;
            MapConfigMap maps;
        };

        inline UltimaGame UltimaGameConfig::getGame() const { return game; }
        inline const std::string & UltimaGameConfig::getPath() const { return path; }
    }
}

#endif
