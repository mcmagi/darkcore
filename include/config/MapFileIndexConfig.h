/* MapFileIndexConfig.h */

#ifndef DARKCORE_CONFIG_MAPFILEINDEXCONFIG_H_
#define DARKCORE_CONFIG_MAPFILEINDEXCONFIG_H_

#include <memory>               // std::unique_ptr
#include "IndexConfig.h"        // IndexConfig
#include "UltimaFileConfig.h"   // UltimaFileConfig
#include "util/Reader.h"        // Reader
#include "util/StreamReader.h"  // StreamReader
#include "util/StreamWriter.h"  // StreamWriter
#include "util/Writer.h"        // Writer

// prototypes
namespace darkcore
{
    namespace config
    {
        class IndexEntry;
        class UltimaGameConfig;
    }
}

namespace darkcore
{
    namespace config
    {
        /**
         * Represents the configuration for a file-based map index.
         */
        class MapFileIndexConfig : public IndexConfig
        {
          public:
            /**
             * Creates the index configuration.
             * @param size Number of entries in the map index
             * @param filename Filename
             */
            MapFileIndexConfig(const std::string &filename, int size = -1);

            /**
             * Creates the index configuration.
             * @param size Number of entries in the map index
             * @param fileCfg File configuration
             */
            MapFileIndexConfig(const UltimaFileConfig &fileCfg, int size = -1);

            /**
             * Copy constructs this configuration from another.
             * @param index Other configuration
             */
            MapFileIndexConfig(const MapFileIndexConfig &idx);

            /**
             * Destructor.
             */
            virtual ~MapFileIndexConfig();

            /**
             * Assigns this configuration from another.
             * @param idx Other configuration
             * @return This configuration
             */
            virtual MapFileIndexConfig & operator=(const MapFileIndexConfig &idx);

            /**
             * Assigns this configuration from another configuration.
             * @param idx Other configuration
             * @return This configuration
             */
            virtual IndexConfig & operator=(const IndexConfig &idx);

            /**
             * Returns a copy of the IndexConfig.  The returned object is owned
             * by the caller and must be explicitly deleted.
             * @return IndexConfig copy
             */
            virtual std::unique_ptr<IndexConfig> clone() const;

            /**
             * Returns a Reader for the Ultima 6 image index.  The returned
             * object is owned by the caller and must be explicitly deleted.
             * @return Index Reader
             */
            virtual std::unique_ptr<darkcore::util::Reader<IndexEntry> > newEntryReader() const;

            /**
             * Returns a Writer for the Ultima 6 image index.  The returned
             * object is owned by the caller and must be explicitly deleted.
             * @return Index Writer
             */
            virtual std::unique_ptr<darkcore::util::Writer<IndexEntry> > newEntryWriter() const;

            /**
             * Returns a new instance of the config, but referencing the
             * specified filename.
             * @param newFilename Replacement filename
             * @return New config
             */
            virtual std::unique_ptr<IndexConfig> forFile(const std::string &newFilename) const;

            /**
             * Returns the config for the index file.
             * @param UltimaFileConfig
             */
            virtual const UltimaFileConfig & getFileConfig() const;

            virtual void setGameConfig(const UltimaGameConfig &gameCfg);

          private:
            /**
             * A reader which returns map chunk index entries from a file.
             */
            class IndexEntryReader : public darkcore::util::StreamReader<IndexEntry>
            {
              public:
                /**
                 * Constructs a Reader for the specified config.
                 * @param cfg U6 Index Config
                 */
                IndexEntryReader(const MapFileIndexConfig &cfg);

                /**
                 * Destructor.
                 */
                ~IndexEntryReader();

                /**
                 * Returns the next index entry.
                 * @return Index entry
                 */
                IndexEntry read();

                /**
                 * Returns whether we are at the end of the index.
                 * @return True if at end
                 */
                bool end() const;

              private:
                const MapFileIndexConfig &cfg;
                int counter;
            };

            /**
             * A writer which sends map chunk index entries to a file.
             */
            class IndexEntryWriter : public darkcore::util::StreamWriter<IndexEntry>
            {
              public:
                /**
                 * Constructs a Writer for the specified config.
                 * @param cfg U6 Index Config
                 */
                IndexEntryWriter(const MapFileIndexConfig &cfg);

                /**
                 * Destructor.
                 */
                ~IndexEntryWriter();

                /**
                 * Writes the next index entry.
                 * @param entry Index entry
                 */
                void write(const IndexEntry &entry);

              private:
                const MapFileIndexConfig &cfg;
            };

            UltimaFileConfig fileCfg;
        };

        inline const UltimaFileConfig & MapFileIndexConfig::getFileConfig() const
        {
            return fileCfg;
        }

        inline void MapFileIndexConfig::setGameConfig(const UltimaGameConfig &gameCfg)
        {
            IndexConfig::setGameConfig(gameCfg);
            fileCfg.setGameConfig(gameCfg);
        }
    }
}

#endif
