/* U1MapFileIndexConfig.h */

#ifndef DARKCORE_CONFIG_U1MAPFILEINDEXCONFIG_H_
#define DARKCORE_CONFIG_U1MAPFILEINDEXCONFIG_H_

#include <memory>               // std::unique_ptr
#include "IndexConfig.h"        // IndexConfig
#include "MapFileIndexConfig.h" // MapFileIndexConfig
#include "util/Reader.h"        // Reader
#include "util/Writer.h"        // Writer

// prototypes
namespace darkcore
{
    namespace config
    {
        class UltimaFileConfig;
    }
}

namespace darkcore
{
    namespace config
    {
        /**
         * Represents the configuration for a U1 file-based map index.
         */
        class U1MapFileIndexConfig : public MapFileIndexConfig
        {
          public:
            /**
             * Creates the index configuration.
             * @param size Number of entries in the map index
             * @param filename Filename
             */
            U1MapFileIndexConfig(const std::string &filename, int size = -1);

            /**
             * Creates the index configuration.
             * @param size Number of entries in the map index
             * @param fileCfg File configuration
             */
            U1MapFileIndexConfig(const UltimaFileConfig &fileCfg, int size = -1);

            /**
             * Copy constructs this configuration from another.
             * @param index Other configuration
             */
            U1MapFileIndexConfig(const U1MapFileIndexConfig &idx);

            /**
             * Destructor.
             */
            ~U1MapFileIndexConfig();

            /**
             * Assigns this configuration from another.
             * @param idx Other configuration
             * @return This configuration
             */
            U1MapFileIndexConfig & operator=(const U1MapFileIndexConfig &idx);

            /**
             * Assigns this configuration from another.
             * @param idx Other configuration
             * @return This configuration
             */
            MapFileIndexConfig & operator=(const MapFileIndexConfig &idx);

            /**
             * Assigns this configuration from another configuration.
             * @param idx Other configuration
             * @return This configuration
             */
            IndexConfig & operator=(const IndexConfig &idx);

            /**
             * Returns a copy of the IndexConfig.  The returned object is owned
             * by the caller and must be explicitly deleted.
             * @return IndexConfig copy
             */
            std::unique_ptr<IndexConfig> clone() const;

            /**
             * Returns a Reader for the Ultima 6 image index.  The returned
             * object is owned by the caller and must be explicitly deleted.
             * @return Index Reader
             */
            std::unique_ptr<darkcore::util::Reader<IndexEntry> > newEntryReader() const;

            /**
             * Returns a Writer for the Ultima 6 image index.  The returned
             * object is owned by the caller and must be explicitly deleted.
             * @return Index Writer
             */
            std::unique_ptr<darkcore::util::Writer<IndexEntry> > newEntryWriter() const;

            /**
             * Returns the size (in bits) of the index.
             * @return Bit size
             */
            int getBitSize() const;

            /**
             * Returns the bit mask used to extract the index.
             * @return Bit mask
             */
            int getBitMask() const;

            /**
             * Returns the number of indices per byte.
             * @return Indices per byte
             */
            int getIndexPerByte() const;

          private:
            /**
             * A reader which returns map chunk index entries from a file.
             */
            class IndexEntryReader : public darkcore::util::Reader<IndexEntry>
            {
              public:
                /**
                 * Constructs a Reader for the specified config.
                 * @param cfg U6 Index Config
                 */
                IndexEntryReader(const U1MapFileIndexConfig &cfg, std::unique_ptr<darkcore::util::Reader<IndexEntry> > reader);

                /**
                 * Destructor.
                 */
                ~IndexEntryReader();

                /**
                 * Returns the next index entry.
                 * @return Index entry
                 */
                IndexEntry read();

                /**
                 * Returns whether we are at the end of the index.
                 * @return True if at end
                 */
                bool end() const;

                /**
                 * Maps the specified index value to a tile index.  The raw indices
                 * used are 0-7, while the tile numbers are 0-9.  This is because there
                 * are two castle and towne tiles due to a tile-swap animation.  This
                 * function adjusts the raw index to the appropriate tile index in the
                 * imageset.
                 * @param value Raw index
                 * @return Tile index
                 */
                int mapIndex(int value) const;

              private:
                const U1MapFileIndexConfig &cfg;
                std::unique_ptr<darkcore::util::Reader<IndexEntry> > reader;
                int counter;
                int idxCtr;
                int data;
            };

            int bitSize;
            int bitMask;
            int indexPerByte;
        };

        inline int U1MapFileIndexConfig::getBitSize() const
        {
            return bitSize;
        }

        inline int U1MapFileIndexConfig::getBitMask() const
        {
            return bitMask;
        }

        inline int U1MapFileIndexConfig::getIndexPerByte() const
        {
            return indexPerByte;
        }

        inline bool U1MapFileIndexConfig::IndexEntryReader::end() const
        {
            return cfg.getSize() >= 0 && counter >= cfg.getSize();
        }
    }
}

#endif
