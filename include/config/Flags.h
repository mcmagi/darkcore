/* Flags.h */

#ifndef DARKCORE_CONFIG_FLAGS_H_
#define DARKCORE_CONFIG_FLAGS_H_

namespace darkcore
{
    namespace config
    {
        class Flags
        {
          public:
            static const Flags NONE;

            Flags(int value);
            Flags(const Flags &flags);
            ~Flags();

            Flags & operator=(const Flags &flags);

            Flags operator|(const Flags &flags) const;
            Flags operator&(const Flags &flags) const;
            operator bool () const;

          private:
            int value;
        };

        inline Flags Flags::operator|(const Flags &flags) const
        {
            return Flags(value | flags.value);
        }

        inline Flags Flags::operator&(const Flags &flags) const
        {
            return Flags(value & flags.value);
        }

        inline Flags::operator bool () const
        {
            return value > 0;
        }
    }
}

#endif
