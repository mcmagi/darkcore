/* U6TileIndexConfig.h */

#ifndef DARKCORE_CONFIG_U6TILEINDEXCONFIG_H_
#define DARKCORE_CONFIG_U6TILEINDEXCONFIG_H_

#include <memory>               // std::unique_ptr
#include <iostream>             // ifstream, ofstream
#include "IndexConfig.h"        // IndexConfig
#include "UltimaFileConfig.h"   // UltimaFileConfig
#include "util/Reader.h"        // Reader
#include "util/Writer.h"        // Writer

// prototypes
namespace darkcore
{
    namespace config
    {
        class UltimaGameConfig;
    }
}

namespace darkcore
{
    namespace config
    {
        /**
         * Represents the configuration for Ultima 6's (or U6 clones') image
         * index.
         */
        class U6TileIndexConfig : public IndexConfig
        {
          public:
            /**
             * Creates the index configuration with the specified index and
             * mask type files.
             * @param size Number of entries in the map index
             * @param depth The color depth of the image
             * @param indexFileCfg Index file config
             * @param maskTypeFileCfg Mask type file config
             */
            U6TileIndexConfig(int size, int depth,
                    const UltimaFileConfig &indexFileCfg,
                    const UltimaFileConfig &maskTypeFileCfg);

            /**
             * Copy constructs this configuration from another configuration.
             * @param index Other configuration
             */
            U6TileIndexConfig(const U6TileIndexConfig &idx);

            /**
             * Destructor.
             */
            ~U6TileIndexConfig();

            /**
             * Assigns this configuration from another configuration.
             * @param idx Other configuration
             * @return This configuration
             */
            U6TileIndexConfig & operator=(const U6TileIndexConfig &idx);

            /**
             * Assigns this configuration from another configuration.
             * @param idx Other configuration
             * @return This configuration
             */
            IndexConfig & operator=(const IndexConfig &idx);

            /**
             * Returns a copy of the IndexConfig.  The returned object is owned
             * by the caller and must be explicitly deleted.
             * @return IndexConfig copy
             */
            std::unique_ptr<IndexConfig> clone() const;

            /**
             * Returns a Reader for the Ultima 6 image index.  The returned
             * object is owned by the caller and must be explicitly deleted.
             * @return Index Reader
             */
            std::unique_ptr<darkcore::util::Reader<IndexEntry> > newEntryReader() const;

            /**
             * Returns a Writer for the Ultima 6 image index.  The returned
             * object is owned by the caller and must be explicitly deleted.
             * @return Index Writer
             */
            std::unique_ptr<darkcore::util::Writer<IndexEntry> > newEntryWriter() const;

            /**
             * Returns the color depth of the images in the index.
             * @return Color depth
             */
            int getDepth() const;

            /**
             * Returns the index file configuration.
             * @return File config
             */
            const UltimaFileConfig & getIndexFileConfig() const;

            /**
             * Returns the mask type file configuration.
             * @return File config
             */
            const UltimaFileConfig & getMaskTypeFileConfig() const;

            void setGameConfig(const UltimaGameConfig &gameCfg);

          private:
            enum VGAMaskType {
                VGA_NORMAL = 0x00,
                VGA_TRANSPARENT = 0x05,
                VGA_PIXEL_BLOCK = 0x0A
            };
            enum EGAMaskType {
                // reference: http://nuvie.sourceforge.net/phorum/read.php?1,113,page=2
                EGA_NORMAL = 0x00,
                EGA_RIGHT = 0x01,
                EGA_LEFT = 0x02,
                EGA_WIDE = 0x03,
                EGA_NORMAL_CYCLE = 0x04,
                EGA_RIGHT_CYCLE = 0x05,
                EGA_LEFT_CYCLE = 0x06,
                EGA_WIDE_CYCLE = 0x07
            };
            enum CGAMaskType {
                CGA_NORMAL = 0x00,
                CGA_PIXEL_BLOCK = 0x05
            };

            /**
             * A reader which returns U6-style index entries.
             */
            class IndexEntryReader : public darkcore::util::Reader<IndexEntry>
            {
              public:
                /**
                 * Constructs a Reader for the specified config.
                 * @param cfg U6 Index Config
                 */
                IndexEntryReader(const U6TileIndexConfig &cfg);

                /**
                 * Destructor.
                 */
                ~IndexEntryReader();

                /**
                 * Returns the next index entry.
                 * @return Index entry
                 */
                IndexEntry read();

                /**
                 * Returns whether we are at the end of the index.
                 * @return True if at end
                 */
                bool end() const;

              private:
                const U6TileIndexConfig &cfg;
                std::unique_ptr<std::istream> indexStream;
                std::unique_ptr<std::istream> maskTypeStream;
            };

            /**
             * A writer which returns U6-style index entries.
             */
            class IndexEntryWriter : public darkcore::util::Writer<IndexEntry>
            {
              public:
                /**
                 * Constructs a Writer for the specified config.
                 * @param cfg U6 Index Config
                 */
                IndexEntryWriter(const U6TileIndexConfig &cfg);

                /**
                 * Destructor.
                 */
                ~IndexEntryWriter();

                /**
                 * Writes the next index entry.
                 * @param entry Index entry
                 */
                void write(const IndexEntry &entry);

              private:
                const U6TileIndexConfig &cfg;
                std::unique_ptr<std::ostream> indexStream;
                std::unique_ptr<std::ostream> maskTypeStream;
            };

            int depth;
            UltimaFileConfig indexFileCfg;
            UltimaFileConfig maskTypeFileCfg;
        };

        inline void U6TileIndexConfig::setGameConfig(const UltimaGameConfig &gameCfg)
        {
            IndexConfig::setGameConfig(gameCfg);
            indexFileCfg.setGameConfig(gameCfg);
            maskTypeFileCfg.setGameConfig(gameCfg);
        }

        inline int U6TileIndexConfig::getDepth() const
        {
            return depth;
        }

        inline const UltimaFileConfig & U6TileIndexConfig::getIndexFileConfig() const
        {
            return indexFileCfg;
        }

        inline const UltimaFileConfig & U6TileIndexConfig::getMaskTypeFileConfig() const
        {
            return maskTypeFileCfg;
        }

    }
}

#endif
