/* U6ChunkIndexConfig.h */

#ifndef DARKCORE_CONFIG_U6CHUNKINDEXCONFIG_H_
#define DARKCORE_CONFIG_U6CHUNKINDEXCONFIG_H_

#include <memory>               // std::unique_ptr
#include "IndexConfig.h"        // IndexConfig
#include "MapFileIndexConfig.h" // MapFileIndexConfig
#include "util/Reader.h"        // Reader
#include "util/StreamReader.h"  // StreamReader
#include "util/Writer.h"        // Writer

// prototypes
namespace darkcore
{
    namespace config
    {
        class UltimaFileConfig;
    }
}

namespace darkcore
{
    namespace config
    {
        /**
         * Represents the configuration for a U6 file-based chunk index.
         */
        class U6ChunkIndexConfig : public MapFileIndexConfig
        {
          public:
            /**
             * Creates the index configuration.
             * @param size Number of entries in the map index
             * @param filename Filename
             */
            U6ChunkIndexConfig(const std::string &filename, int size = -1);

            /**
             * Creates the index configuration.
             * @param size Number of entries in the map index
             * @param fileCfg File configuration
             */
            U6ChunkIndexConfig(const UltimaFileConfig &fileCfg, int size = -1);

            /**
             * Copy constructs this configuration from another.
             * @param index Other configuration
             */
            U6ChunkIndexConfig(const U6ChunkIndexConfig &idx);

            /**
             * Destructor.
             */
            ~U6ChunkIndexConfig();

            /**
             * Assigns this configuration from another.
             * @param idx Other configuration
             * @return This configuration
             */
            U6ChunkIndexConfig & operator=(const U6ChunkIndexConfig &idx);

            /**
             * Assigns this configuration from another.
             * @param idx Other configuration
             * @return This configuration
             */
            MapFileIndexConfig & operator=(const MapFileIndexConfig &idx);

            /**
             * Assigns this configuration from another configuration.
             * @param idx Other configuration
             * @return This configuration
             */
            IndexConfig & operator=(const IndexConfig &idx);

            /**
             * Returns a copy of the IndexConfig.  The returned object is owned
             * by the caller and must be explicitly deleted.
             * @return IndexConfig copy
             */
            std::unique_ptr<IndexConfig> clone() const;

            /**
             * Returns a Reader for the Ultima 6 image index.  The returned
             * object is owned by the caller and must be explicitly deleted.
             * @return Index Reader
             */
            std::unique_ptr<darkcore::util::Reader<IndexEntry> > newEntryReader() const;

            /**
             * Returns a Writer for the Ultima 6 image index.  The returned
             * object is owned by the caller and must be explicitly deleted.
             * @return Index Writer
             */
            std::unique_ptr<darkcore::util::Writer<IndexEntry> > newEntryWriter() const;

          private:
            /**
             * A reader which returns map chunk index entries from a file.
             */
            class IndexEntryReader : public darkcore::util::StreamReader<IndexEntry>
            {
              public:
                /**
                 * Constructs a Reader for the specified config.
                 * @param cfg U6 Index Config
                 */
                IndexEntryReader(const U6ChunkIndexConfig &cfg);

                /**
                 * Destructor.
                 */
                ~IndexEntryReader();

                /**
                 * Returns the next index entry.
                 * @return Index entry
                 */
                IndexEntry read();

                /**
                 * Returns whether we are at the end of the index.
                 * @return True if at end
                 */
                bool end() const;

              private:
                const U6ChunkIndexConfig &cfg;
                int nextData;
                int counter;
            };
        };

        inline bool U6ChunkIndexConfig::IndexEntryReader::end() const
        {
            return cfg.getSize() >= 0 && counter >= cfg.getSize();
        }
    }
}

#endif
