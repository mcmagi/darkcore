/* UltimaImageConfig.h */

#ifndef DARKCORE_CONFIG_ULTIMAIMAGECONFIG_H_
#define DARKCORE_CONFIG_ULTIMAIMAGECONFIG_H_

#include <memory>               // std::unique_ptr
#include <string>               // std::string
#include "Flags.h"              // Flags
#include "IndexConfig.h"        // IndexConfig
#include "UltimaFileConfig.h"   // UltimaFileConfig

// prototypes
namespace darkcore
{
    namespace image
    {
        class ImageFormat;
    }

    namespace config
    {
        class UltimaGameConfig;
    }
}

namespace darkcore
{
    namespace config
    {
        class UltimaImageConfig
        {
          public:
            static const Flags INTERLACED;
            static const Flags BITMASKED;
            static const Flags IBMCGA;
            static const Flags TYPEMASKED;

            UltimaImageConfig(const std::string &filename, const std::string &description,
                    int numImages, int width, int height, const darkcore::image::ImageFormat &format,
                    const Flags &flags = Flags::NONE, const IndexConfig *idx = NULL);
            UltimaImageConfig(const UltimaFileConfig &fileCfg, const std::string &description,
                    int numImages, int width, int height, const darkcore::image::ImageFormat &format,
                    const Flags &flags = Flags::NONE, const IndexConfig *idx = NULL);

            /**
             * Copy-constructs from another Ultima image configuration.
             * @param cfg Other configuration
             */
            UltimaImageConfig(const UltimaImageConfig &cfg);

            /**
             * Destructor.
             */
            ~UltimaImageConfig();

            /**
             * Assigns from another Ultima image configuration.
             * @param cfg Other configuration
             * @return This configuration
             */
            UltimaImageConfig & operator=(const UltimaImageConfig &cfg);

            void setGameConfig(const UltimaGameConfig &gameCfg);
            const UltimaGameConfig & getGameConfig() const;
            const UltimaFileConfig & getFileConfig() const;
            const std::string & getDescription() const;
            int getHeight() const;
            int getWidth() const;
            int getSize() const;
            const darkcore::image::ImageFormat & getImageFormat() const;
            int getNumImages() const;
            bool isInterlaced() const;
            bool isBitMasked() const;
            bool isIBMCGA() const;
            bool isTypeMasked() const;
            int getOffset() const;
            int getPadding() const;

            /**
             * Returns a Index config for this Image config, if one was
             * configured for it.  May also be NULL.
             * @return IndexConfig or NULL
             */
            const IndexConfig * getIndexConfig() const;

          private:
            /**
             * Makes a null-safe clone of the index config.  Internal use only.
             * @param idxCfg Index config or NULL
             * @return Clone
             */
            static std::unique_ptr<IndexConfig> clone(const IndexConfig *idxCfg);

            const UltimaGameConfig *gameCfg;
            UltimaFileConfig fileCfg;
            std::string description;
            int numImages;
            int width;
            int height;
            std::unique_ptr<darkcore::image::ImageFormat> format; // shared_ptr ???
            Flags flags;
            int offset;
            std::unique_ptr<IndexConfig> idxCfg;
        };

        inline void UltimaImageConfig::setGameConfig(const UltimaGameConfig &gameCfg)
        {
            this->gameCfg = &gameCfg;
            fileCfg.setGameConfig(gameCfg);
            if (idxCfg.get() != NULL)
                idxCfg->setGameConfig(gameCfg);
        }

        inline const UltimaGameConfig & UltimaImageConfig::getGameConfig() const
        {
            return *const_cast<UltimaGameConfig *>(gameCfg);
        }

        inline const UltimaFileConfig & UltimaImageConfig::getFileConfig() const
        {
            return fileCfg;
        }

        inline const std::string & UltimaImageConfig::getDescription() const
        {
            return description;
        }

        inline int UltimaImageConfig::getHeight() const
        {
            return height;
        }

        inline int UltimaImageConfig::getWidth() const
        {
            return width;
        }

        inline int UltimaImageConfig::getSize() const
        {
            return width * height;
        }

        inline const darkcore::image::ImageFormat & UltimaImageConfig::getImageFormat() const
        {
            return *format;
        }

        inline int UltimaImageConfig::getNumImages() const
        {
            return numImages;
        }

        inline bool UltimaImageConfig::isInterlaced() const
        {
            return flags & INTERLACED;
        }

        inline bool UltimaImageConfig::isBitMasked() const
        {
            return flags & BITMASKED;
        }

        inline bool UltimaImageConfig::isIBMCGA() const
        {
            return flags & IBMCGA;
        }

        inline bool UltimaImageConfig::isTypeMasked() const
        {
            return flags & TYPEMASKED;
        }

        inline const IndexConfig * UltimaImageConfig::getIndexConfig() const
        {
            return idxCfg.get();
        }
    }
}

#endif
