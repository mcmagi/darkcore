/* ImageDisplaySDL.h */

#ifndef DARKCORE_BROWSER_IMAGEDISPLAYSDL_H_
#define DARKCORE_BROWSER_IMAGEDISPLAYSDL_H_

#include "SDL/SDL.h"                // SDL_Surface
#include "ImageDisplay.h"           // ImageDisplay
#include "image/Color.h"            // Pixel
#include "image/ImageFormat.h"      // RGBImageFormat
#include "image/converter/ZoomConverter.h" // ZoomConverter


namespace darkcore
{
    namespace image
    {
        class Image;
    }

    namespace browser
    {
        class ImageDisplaySDL : public ImageDisplay
        {
          public:
            static const int MIN_ZOOM;
            static const int MAX_ZOOM;

            ImageDisplaySDL(int width, int height, int zoom = 2);
            ~ImageDisplaySDL();

            void display(const darkcore::image::Image &img) const;

            void setZoom(int zoom);
            int getZoom() const;

			void setCgaComposite(bool composite);
			bool isCgaComposite() const;

          private:
            void displayRGBImage(const darkcore::image::Image &img) const;

            SDL_Surface *pDisplaySurface;
            darkcore::image::RGBImageFormat displayImageFormat;
            darkcore::image::converter::ZoomConverter zoomConv;
			bool cga_composite;
        };

        inline void ImageDisplaySDL::setZoom(int zoom)
        {
            zoomConv.setZoomLevel(zoom);
        }

        inline int ImageDisplaySDL::getZoom() const
        {
            return zoomConv.getZoomLevel();
        }

        inline void ImageDisplaySDL::setCgaComposite(bool composite)
        {
            this->cga_composite = composite;
        }

        inline bool ImageDisplaySDL::isCgaComposite() const
        {
            return cga_composite;
        }
    }
}


#endif
