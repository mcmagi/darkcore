/* AsciiBrowser.h */

#ifndef DARKCORE_BROWSER_ASCIIBROWSER_H_
#define DARKCORE_BROWSER_ASCIIBROWSER_H_

#include "TileBrowser.h"
#include "ImageDisplayAscii.h"


namespace darkcore
{
    namespace config
    {
        class UltimaImageConfig;
    }

    namespace image
    {
        class ImageSet;
    }

    namespace browser
    {
        class AsciiBrowser
        {
          public:
	        AsciiBrowser(darkcore::image::ImageSet &ts, const darkcore::config::UltimaImageConfig &imgCfg);

	        void browserMain();

          private:
	        void printHelp();

	        ImageDisplayAscii ida;
	        TileBrowser tb;
	        bool display_tile;
        };
    }
}


#endif
