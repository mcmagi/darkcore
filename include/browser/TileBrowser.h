/* TileBrowser.h */

#ifndef DARKCORE_BROWSER_TILEBROWSER_H_
#define DARKCORE_BROWSER_TILEBROWSER_H_

#include <string>                           // std::string
#include "AbstractTileBrowser.h"            // AbstractTileBrowser
#include "image/Color.h"                    // Pixel
#include "image/ImageFormat.h"              // ImageFormat
#include "image/ImageSet.h"                 // ImageSet

namespace darkcore
{
    namespace config
    {
        class UltimaImageConfig;
    }

    namespace browser
    {
        class ImageDisplay;

        class TileBrowser : public AbstractTileBrowser
        {
          public:
        	TileBrowser(darkcore::image::ImageSet &ts, const ImageDisplay &id, const darkcore::config::UltimaImageConfig &imgCfg);

        	// navigation
        	bool nextTile();
        	bool prevTile();
        	bool firstTile();
        	bool lastTile();
        	bool gotoTile(int i);

        	// set colors
        	bool setColor(int x, int y, int c);
        	bool changeColor(darkcore::image::Pixel old_c, darkcore::image::Pixel new_c);

        	// display
        	void display() const;

        	// save
        	void saveTileSet(const std::string &filename);
            void exportImage() const;

        	// get fcns
        	int getTileNum() const;
        	int getColorDepth() const;

            /**
             * Returns a recommended width (in tiles) of an ImageSet with the specified config.
             * Useful when constructing an Image from an ImageSet.
             * @param imgCfg UltimaImageConfig
             * @return Recommended with
             */
            static int calculateMergedImageWidth(const darkcore::config::UltimaImageConfig &imgCfg);

          private:
            darkcore::image::ImageSet &ts;
            const darkcore::config::UltimaImageConfig &imgCfg;
        	int tile;
        };

        inline int TileBrowser::getTileNum() const { return tile; }
        inline int TileBrowser::getColorDepth() const { return ts.getImageFormat().getColorDepth(); }
    }
}


#endif
