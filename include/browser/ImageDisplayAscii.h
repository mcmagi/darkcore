/* ImageDisplayAscii.h */

#ifndef DARKCORE_BROWSER_IMAGEDISPLAYASCII_H_
#define DARKCORE_BROWSER_IMAGEDISPLAYASCII_H_

#include <string>               // std::string
#include "ImageDisplay.h"       // ImageDisplay

namespace darkcore
{
    namespace image
    {
        class Color;
        class Image;
    }

    namespace browser
    {
        class ImageDisplayAscii : public ImageDisplay
        {
          public:
            ImageDisplayAscii();
            ~ImageDisplayAscii();

            void display(const darkcore::image::Image &img) const;

            static const char PIXEL_CHAR = '#';

          private:
            void printAscii(const std::string &colorCode, char chr) const;
            std::string getAsciiColorCode(const darkcore::image::Color &c) const;
        };
    }
}


#endif
