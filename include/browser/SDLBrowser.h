/* SDLBrowser.h */

#ifndef DARKCORE_BROWSER_SDLBROWSER_H_
#define DARKCORE_BROWSER_SDLBROWSER_H_

#include <memory>                       // std::unique_ptr
#include "AbstractTileBrowser.h"        // AbstractTileBrowser
#include "ImageDisplaySDL.h"            // ImageDisplaySDL


namespace darkcore
{
    namespace config
    {
        class UltimaImageConfig;
    }

    namespace image
    {
        class Image;
        class ImageSet;
    }

    namespace browser
    {
        class SDLBrowser
        {
          public:
	        SDLBrowser(darkcore::image::ImageSet &ts, const darkcore::config::UltimaImageConfig &imgCfg);
	        SDLBrowser(darkcore::image::Image &img);

	        void browserMain();

          private:
	        ImageDisplaySDL ids;
	        std::unique_ptr<AbstractTileBrowser> tb;
	        bool display_tile;
        };
    }
}


#endif
