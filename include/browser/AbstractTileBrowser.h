/* AbstractTileBrowser.h */

#ifndef DARKCORE_BROWSER_ABSTRACTTILEBROWSER_H_
#define DARKCORE_BROWSER_ABSTRACTTILEBROWSER_H_

#include <string>
#include "ImageDisplay.h"                   // ImageDisplay
#include "image/Color.h"                    // Pixel

namespace darkcore
{
    namespace browser
    {
        class AbstractTileBrowser
        {
          public:

        	// navigation
        	virtual bool nextTile() = 0;
        	virtual bool prevTile() = 0;
        	virtual bool firstTile() = 0;
        	virtual bool lastTile() = 0;
        	virtual bool gotoTile(int i) = 0;

        	// set colors
        	virtual bool setColor(int x, int y, int c) = 0;
        	virtual bool changeColor(darkcore::image::Pixel old_c, darkcore::image::Pixel new_c) = 0;

        	// display
        	virtual void display() const = 0;

        	// save
        	virtual void saveTileSet(const std::string &filename) = 0;
            virtual void exportImage() const = 0;

        	// get fcns
        	virtual int getTileNum() const = 0;
        	virtual int getColorDepth() const = 0;

          protected:
        	AbstractTileBrowser(const ImageDisplay &id);

        	const ImageDisplay &id;
        };

        inline AbstractTileBrowser::AbstractTileBrowser(const ImageDisplay& id) : id(id) { }
    }
}


#endif
