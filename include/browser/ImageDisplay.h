/* ImageDisplay.h */

#ifndef DARKCORE_BROWSER_IMAGEDISPLAY_H_
#define DARKCORE_BROWSER_IMAGEDISPLAY_H_


namespace darkcore
{
    namespace image
    {
        class Image;
    }

    namespace browser
    {
        /*
         * Image Display base class (abstract)
         *
         * This base class (or interface) establishes a framework for setting up a
         * way to display image data.  It cannot be instantiated.
         */

        class ImageDisplay
        {
          public:
            virtual ~ImageDisplay();

        	virtual void display(const darkcore::image::Image &img) const = 0;

          protected:
            /**
             * Protected constructor enforces abstract base class behavior.
             */
            ImageDisplay();
        };
    }
}


#endif
