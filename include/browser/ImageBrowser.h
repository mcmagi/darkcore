/* TileBrowser.h */

#ifndef DARKCORE_BROWSER_IMAGEBROWSER_H_
#define DARKCORE_BROWSER_IMAGEBROWSER_H_

#include <string>                           // std::string
#include "AbstractTileBrowser.h"            // AbstractTileBrowser
#include "image/Color.h"                    // Pixel
#include "image/Image.h"                    // Image
#include "image/ImageFormat.h"              // ImageFormat

namespace darkcore
{
    namespace browser
    {
        class ImageDisplay;

        class ImageBrowser : public AbstractTileBrowser
        {
          public:
        	ImageBrowser(darkcore::image::Image &img, const ImageDisplay &id);

        	// navigation
        	bool nextTile();
        	bool prevTile();
        	bool firstTile();
        	bool lastTile();
        	bool gotoTile(int i);

        	// set colors
        	bool setColor(int x, int y, int c);
        	bool changeColor(darkcore::image::Pixel old_c, darkcore::image::Pixel new_c);

        	// display
        	void display() const;

        	// save
        	void saveTileSet(const std::string &filename);
            void exportImage() const;

        	// get fcns
        	int getTileNum() const;
        	int getColorDepth() const;

          private:
            darkcore::image::Image &img;
        };

        inline int ImageBrowser::getTileNum() const { return 0; }
        inline int ImageBrowser::getColorDepth() const { return img.getImageFormat().getColorDepth(); }

        inline bool ImageBrowser::nextTile() { return false; }
        inline bool ImageBrowser::prevTile() { return false; }
        inline bool ImageBrowser::firstTile() { return false; }
        inline bool ImageBrowser::lastTile() { return false; }
        inline bool ImageBrowser::gotoTile(int i) { return false; }

        inline bool ImageBrowser::setColor(int x, int y, int c) { return false; }
        inline bool ImageBrowser::changeColor(darkcore::image::Pixel old_c, darkcore::image::Pixel new_c) { return false; }
    }
}


#endif
