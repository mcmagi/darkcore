/* File.h */

#ifndef DARKCORE_UTIL_FILE_H_
#define DARKCORE_UTIL_FILE_H_

#include <fstream>      // std::ifstream, std::ofstream, std::ios_base
#include <memory>       // std::unique_ptr
#include <string>       // std::string

namespace darkcore
{
    namespace util
    {
        class File
        {
          public:
            /**
             * Constructs a File object for the specified filename.
             * @param filename Name of file
             */
            File(const std::string &filename);

            /**
             * Constructs a File object for the specified path and filename.
             * @param path Path to file
             * @param filename Name of file
             */
            File(const std::string &path, const std::string &filename);

            /**
             * Copy constructs this File from another File.
             * @param file Other File
             */
            File(const File &file);

            /**
             * Destructor.
             */
            ~File();

            /**
             * Assigns another File to this File.
             * @param file Other File
             * @return This File
             */
            File & operator=(const File &file);

            /**
             * Returns the full filename represented by this File.
             * @return Name of file
             */
            std::string getFullName() const;

            /**
             * Returns the filename represented by this File.
             * @return Name of file
             */
            std::string getName() const;

            /**
             * Returns the path name of the parent directory.
             * @return Name of path
             */
            std::string getParentName() const;

            /**
             * Returns a File object for the parent directory.
             * @return File
             */
            File getParent() const;

            /**
             * Returns whether or not the file exists.
             * @return True if exists
             */
            bool exists() const;

            /**
             * Opens the specified file, returning an ifstream.  This
             * is used internally to construct a Reader for a filename.
             * The returned stream must be explicitly deleted.
             * @param mode Open mode
             * @return Input file stream
             */
            std::unique_ptr<std::ifstream> newInputStream(
                    std::ios_base::openmode mode = std::ios_base::in) const;

            /**
             * Opens the specified file, returning an ofstream.  This
             * is used internally to construct a Writer for a filename.
             * The returned stream must be explicitly deleted.
             * @param mode Open mode
             * @return Output file stream
             */
            std::unique_ptr<std::ofstream> newOutputStream(
                    std::ios_base::openmode mode = std::ios_base::out) const;

          private:
            std::string filename;
        };

        inline std::string File::getFullName() const
        {
            return filename;
        }
    }
}

#endif
