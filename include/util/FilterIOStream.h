/* FilterIOStream.h */

#ifndef DARKCORE_UTIL_FILTERIOSTREAM_H_
#define DARKCORE_UTIL_FILTERIOSTREAM_H_

#include <istream>              // std::istream
#include <memory>               // std::unique_ptr
#include <ostream>              // std::ostream
#include <streambuf>            // std::streambuf, std::streamsize

namespace darkcore
{
    namespace util
    {
        /**
         * An unbuffered stream buffer that wraps another input
         * or output stream.
         */
        class FilterStreamBuffer : public std::streambuf
        {
          public:
            /**
             * Constructs a filter stream buffer that wraps an input stream.
             * @param is Wrapped input stream
             */

            FilterStreamBuffer(std::istream &is);
            /**
             * Constructs a filter stream buffer that wraps an input stream.
             * Ownership of the stream is transferred to this stream buffer.
             * @param is Wrapped input stream
             */
            FilterStreamBuffer(std::unique_ptr<std::istream> is);

            /**
             * Constructs a filter stream buffer that wraps an output stream.
             * @param os Wrapped output stream
             */
            FilterStreamBuffer(std::ostream &os);

            /**
             * Constructs a filter stream buffer that wraps an output stream.
             * Ownership of the stream is transferred to this stream buffer.
             * @param os Wrapped output stream
             */
            FilterStreamBuffer(std::unique_ptr<std::ostream> os);

            /**
             * Destructs the stream buffer.
             */
            virtual ~FilterStreamBuffer();

            /**
             * Returns the wrapped input stream.
             * @return Wrapped input stream
             */
            std::istream * getInputStream();

            /**
             * Returns the wrapped output stream.
             * @return Wrapped output stream
             */
            std::ostream * getOutputStream();

          protected:
            virtual std::streamsize showmanyc();
            virtual int pbackfail(int c = traits_type::eof());
            virtual int uflow();
            virtual int underflow();
            virtual int overflow(int c = traits_type::eof());
            virtual std::streamsize xsgetn(char *s, std::streamsize n);
            virtual std::streamsize xsputn(const char *s, std::streamsize n);
            virtual std::streampos seekoff(std::streamoff off, std::ios_base::seekdir way,
                    std::ios_base::openmode which = std::ios_base::in | std::ios_base::out);
            virtual std::streampos seekpos(std::streampos sp,
                    std::ios_base::openmode which = std::ios_base::in | std::ios_base::out);

          private:
            std::istream *is;
            std::ostream *os;
            bool ownStream;
        };

        inline std::istream * FilterStreamBuffer::getInputStream()
        {
            return is;
        }

        inline std::ostream * FilterStreamBuffer::getOutputStream()
        {
            return os;
        }


        /**
         * An input stream that wraps another input stream.
         */
        class FilterInputStream : public std::istream
        {
          public:
            /**
             * Constructs a filter input stream around the specified input stream.
             * @param innerStream Wrapped input stream
             */
            FilterInputStream(std::istream &is);

            /**
             * Constructs a filter input stream around the specified input stream.
             * Ownership of the stream is transferred to this stream.
             * @param innerStream Ptr to wrapped input stream
             */
            FilterInputStream(std::unique_ptr<std::istream> is);

            /**
             * Destructs the input stream.
             */
            virtual ~FilterInputStream();

            /**
             * Returns the wrapped input stream.
             * @return Wrapped input stream
             */
            std::istream & getInnerStream();

          protected:
            /**
             * Constructs a filter input stream around the specified input stream
             * and uses a particular stream buffer for buffering.  Intended to be used
             * by subclasses.
             * @param buf Stream buffer
             */
            FilterInputStream(FilterStreamBuffer *buf);

            /**
             * Casts and returns the stream buffer as a FilterStreamBuffer pointer.
             * @return FilterStreamBuffer
             */
            FilterStreamBuffer * getFilterStreamBuffer();

          private:
            bool ownStreamBuf;
        };

        inline FilterStreamBuffer * FilterInputStream::getFilterStreamBuffer()
        {
            return (FilterStreamBuffer *) rdbuf();
        }

        inline std::istream & FilterInputStream::getInnerStream()
        {
            return * getFilterStreamBuffer()->getInputStream();
        }


        /**
         * An output stream that wraps another output stream.
         */
        class FilterOutputStream : public std::ostream
        {
          public:
            /**
             * Constructs a filter output stream around the specified output stream.
             * @param innerStream Wrapped output stream
             */
            FilterOutputStream(std::ostream &os);

            /**
             * Constructs a filter input stream around the specified output stream.
             * Ownership of the stream is transferred to this stream.
             * @param innerStream Ptr to wrapped output stream
             */
            FilterOutputStream(std::unique_ptr<std::ostream> os);

            /**
             * Destructs the input stream.
             */
            virtual ~FilterOutputStream();

            /**
             * Returns the wrapped output stream.
             * @return Wrapped output stream
             */
            std::ostream & getInnerStream();

          protected:
            /**
             * Constructs a filter output stream around the specified output stream
             * and uses a particular stream buffer for buffering.  Intended to be used
             * by subclasses.
             * @param buf Stream buffer
             */
            FilterOutputStream(FilterStreamBuffer *buf);

            /**
             * Casts and returns the stream buffer as a FilterStreamBuffer pointer.
             * @return FilterStreamBuffer
             */
            FilterStreamBuffer * getFilterStreamBuffer();

          private:
            bool ownStreamBuf;
        };

        inline FilterStreamBuffer * FilterOutputStream::getFilterStreamBuffer()
        {
            return (FilterStreamBuffer *) rdbuf();
        }

        inline std::ostream & FilterOutputStream::getInnerStream()
        {
            return * getFilterStreamBuffer()->getOutputStream();
        }
    }
}

#endif
