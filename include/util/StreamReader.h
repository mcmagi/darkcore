/* StreamReader.h */

#ifndef DARKCORE_UTIL_STREAMREADER_H_
#define DARKCORE_UTIL_STREAMREADER_H_

#include <istream>              // std::istream
#include <memory>               // std::unique_ptr
#include "Reader.h"             // Reader

namespace darkcore
{
    namespace util
    {
        /**
         * Used to read content from a stream and return it as an object.
         * Subclasses must provide an implementation for the pure virtual method read().
         */
        template <class T>
        class StreamReader : public Reader<T>
        {
          public:
            /**
             * Virtual destructor.
             */
            virtual ~StreamReader();

            /**
             * Reads the next Image from the input stream.
             * @return Image
             */
            virtual T read() = 0;

            /**
             * Returns true if the reader has reached the end of the stream.
             * @return True if at end of input
             */
            virtual bool end() const;

          protected:
            /**
             * Constructor prepared with the Input Stream.
             * @param is Input stream to read data from
             */
            StreamReader(std::istream &is);

            /**
             * Constructor prepared with the Input Stream.
             * Ownership is transferred to this Reader.
             * @param is Input stream to read data from
             */
            StreamReader(std::unique_ptr<std::istream> is);

            /**
             * Returns the input stream.
             * @return Input straem
             */
            std::istream & getInputStream();

            /**
             * Sets the input stream to be used.  This is a convencience
             * for subclasses to provide wrapped or alternate streams.
             * @param is Input stream
             */
            void setInputStream(std::istream &is);

            /**
             * Sets the input stream to be used.  This is a convencience
             * for subclasses to provide wrapped or alternate streams.
             * Ownership is transferred to this Reader.
             * @param is Input stream
             */
            void setInputStream(std::unique_ptr<std::istream> is);

          private:
            std::istream *is;
            bool ownStream;
        };

        template <class T>
        inline StreamReader<T>::StreamReader(std::istream &is) :
            Reader<T>(), is(&is), ownStream(false)
        {
        }

        template <class T>
        inline StreamReader<T>::StreamReader(std::unique_ptr<std::istream> is) :
            Reader<T>(), is(is.release()), ownStream(true)
        {
        }

        template <class T>
        inline StreamReader<T>::~StreamReader()
        {
            if (ownStream)
                delete is;
        }

        template <class T>
        inline std::istream & StreamReader<T>::getInputStream()
        {
            return *is;
        }

        template <class T>
        inline void StreamReader<T>::setInputStream(std::istream &is)
        {
            if (ownStream) // clean up old stream if need be
                delete this->is;

            this->is = &is;
            ownStream = false;
        }

        template <class T>
        inline void StreamReader<T>::setInputStream(std::unique_ptr<std::istream> is)
        {
            if (ownStream) // clean up old stream if need be
                delete this->is;

            this->is = is.release();
            ownStream = true;
        }

        template <class T>
        inline bool StreamReader<T>::end() const
        {
            return is->eof();
        }
    }
}

#endif
