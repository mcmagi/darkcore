/* StreamWriter.h */

#ifndef DARKCORE_UTIL_STREAMWRITER_H_
#define DARKCORE_UTIL_STREAMWRITER_H_

#include <memory>               // std::unique_ptr
#include <ostream>              // std::ostream
#include "Writer.h"             // Writer

namespace darkcore
{
    namespace util
    {
        /**
         * Used to write content to a stream from an object.
         * Subclasses must provide an implementation for the pure virtual method write().
         */
        template <class T>
        class StreamWriter : public Writer<T>
        {
          public:
            /**
             * Virtual destructor.
             */
            virtual ~StreamWriter();

            /**
             * Writes an Image to the output stream.
             * @param img Image
             */
            virtual void write(const T &img) = 0;

          protected:
            /**
             * Constructor prepared with the Output Stream.
             * @param os Output stream to write images to
             */
            StreamWriter(std::ostream &os);

            /**
             * Constructor prepared with the Output Stream.
             * Ownership is transferred to this Writer.
             * @param os Output stream to write images to
             */
            StreamWriter(std::unique_ptr<std::ostream> os);

            /**
             * Returns the output stream.
             * @return Output straem
             */
            std::ostream & getOutputStream();

            /**
             * Sets the output stream to be used.  This is a convencience
             * for subclasses to provide wrapped or alternate streams.
             * @param os Output stream
             */
            void setOutputStream(std::ostream &os);

            /**
             * Sets the output stream to be used.  This is a convencience
             * for subclasses to provide wrapped or alternate streams.
             * Ownership is transferred to this Writer.
             * @param os Output stream
             */
            void setOutputStream(std::unique_ptr<std::ostream> os);

          private:
            std::ostream *os;
            bool ownStream;
        };

        template <class T>
        inline StreamWriter<T>::StreamWriter(std::ostream &os) :
            Writer<T>(), os(&os), ownStream(false)
        {
        }

        template <class T>
        inline StreamWriter<T>::StreamWriter(std::unique_ptr<std::ostream> os) :
            Writer<T>(), os(os.release()), ownStream(true)
        {
        }

        template <class T>
        inline StreamWriter<T>::~StreamWriter()
        {
            if (ownStream)
                delete os;
        }

        template <class T>
        inline std::ostream & StreamWriter<T>::getOutputStream()
        {
            return *os;
        }

        template <class T>
        inline void StreamWriter<T>::setOutputStream(std::ostream &os)
        {
            if (ownStream) // clean up old stream if need be
                delete this->os;

            this->os = &os;
            ownStream = false;
        }

        template <class T>
        inline void StreamWriter<T>::setOutputStream(std::unique_ptr<std::ostream> os)
        {
            if (ownStream) // clean up old stream if need be
                delete this->os;

            this->os = os.release();
            ownStream = true;
        }
    }
}

#endif
