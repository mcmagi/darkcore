/* FileReader.h */

#ifndef DARKCORE_UTIL_FILEREADER_H_
#define DARKCORE_UTIL_FILEREADER_H_

#include <fstream>              // std::ifstream, std::ios
#include <istream>              // std::istream
#include <memory>               // std::unique_ptr, std::move
#include <string>               // std::string
#include "File.h"               // File
#include "StreamReader.h"       // StreamReader

namespace darkcore
{
    namespace util
    {
        /**
         * Used to read content from a file or stream and return it as an object.
         * Subclasses must provide an implementation for the pure virtual method read().
         */
        template <class T>
        class FileReader : public StreamReader<T>
        {
          public:
            /**
             * Virtual destructor.
             */
            virtual ~FileReader();

            /**
             * Reads the next Image from the input stream.
             * @return Image
             */
            virtual T read() = 0;

          protected:
            /**
             * Constructor prepared with the name of a file to read from.
             * @param filename Name of file for reading
             */
            FileReader(const std::string &filename);

            /**
             * Constructor prepared with an Input Stream.
             * @param is Input stream to read data from
             */
            FileReader(std::istream &is);

            /**
             * Constructor prepared with an Input Stream.
             * Ownership is transferred to this Reader.
             * @param is Input stream to read data from
             */
            FileReader(std::unique_ptr<std::istream> is);
        };

        template <class T>
        inline FileReader<T>::FileReader(const std::string &filename) :
            StreamReader<T>((std::unique_ptr<std::istream>) File(filename).newInputStream(std::ios::in | std::ios::binary))
        {
        }

        template <class T>
        inline FileReader<T>::FileReader(std::istream &is) :
            StreamReader<T>(is)
        {
        }

        template <class T>
        inline FileReader<T>::FileReader(std::unique_ptr<std::istream> is) :
            StreamReader<T>(std::move(is))
        {
        }

        template <class T>
        inline FileReader<T>::~FileReader()
        {
        }
    }
}

#endif
