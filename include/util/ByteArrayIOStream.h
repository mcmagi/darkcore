/* ByteArrayIOStream.h */

#ifndef DARKCORE_UTIL_BYTEARRAYIOSTREAM_H_
#define DARKCORE_UTIL_BYTEARRAYIOSTREAM_H_

#include <istream>              // std::istream
#include <ostream>              // std::ostream
#include <streambuf>            // std::streambuf
#include <vector>               // std::vector

namespace darkcore
{
    namespace util
    {
        /**
         * The stream buffer containing the byte array to be read from
         * or written to.
         */
        class ByteArrayStreamBuffer : public std::streambuf
        {
          public:
            // constructors & destructors
            ByteArrayStreamBuffer();
            ByteArrayStreamBuffer(const unsigned char *data, int size);
            ByteArrayStreamBuffer(const std::vector<unsigned char> &data);
            virtual ~ByteArrayStreamBuffer();

            /**
             * Returns the internal array as a vector.
             * @return Vector
             */
            const std::vector<unsigned char> & toVector() const;

            /**
             * Returns the current size of the internal array.
             * @return Size
             */
            int getSize() const;

          protected:
            virtual std::streamsize showmanyc();
            virtual int pbackfail(int c = traits_type::eof());
            virtual int uflow();
            virtual int underflow();
            virtual int overflow(int c = traits_type::eof());
            virtual std::streamsize xsgetn(char *s, std::streamsize n);
            virtual std::streamsize xsputn(const char *s, std::streamsize n);
            virtual std::streampos seekoff(std::streamoff off, std::ios_base::seekdir way,
                    std::ios_base::openmode which = std::ios_base::in | std::ios_base::out);
            virtual std::streampos seekpos(std::streampos sp,
                    std::ios_base::openmode which = std::ios_base::in | std::ios_base::out);

          private:
            std::vector<unsigned char> data;
            unsigned int inpos;
            unsigned int outpos;
        };

        inline const std::vector<unsigned char> & ByteArrayStreamBuffer::toVector() const { return data; }
        inline int ByteArrayStreamBuffer::getSize() const { return data.size(); }


        /**
         * An input stream backed by a character (or byte) array.
         */
        class ByteArrayInputStream : public std::istream
        {
          public:
            // constructors & destructors
            ByteArrayInputStream(const unsigned char data[], int size);
            ByteArrayInputStream(const std::vector<unsigned char> &data);
            virtual ~ByteArrayInputStream();

            /**
             * Returns the size of the passed-in byte array.
             * @return Size
             */
            int getSize() const;

          private:
            ByteArrayStreamBuffer buf;
        };

        inline int ByteArrayInputStream::getSize() const { return buf.getSize(); }


        /**
         * An output stream backed by a character (or byte) array.
         * The array is extended as new characters are added.
         */
        class ByteArrayOutputStream : public std::ostream
        {
          public:
            // constructors & destructors
            ByteArrayOutputStream();
            virtual ~ByteArrayOutputStream();

            /**
             * Returns the output array as a vector.
             * @return Vector
             */
            const std::vector<unsigned char> & toVector() const;

            /**
             * Returns the output array as an array.
             * @return Array
             */
            const unsigned char * toArray() const;

            /**
             * Returns the current size of the output array.
             * @return Size
             */
            int getSize() const;

            /**
             * Writes the byte array to the specified output stream.
             * @param stream Stream to write to
             */
            void writeTo(std::ostream &stream);

          private:
            ByteArrayStreamBuffer buf;
        };

        inline const std::vector<unsigned char> & ByteArrayOutputStream::toVector() const { return buf.toVector(); }
        inline const unsigned char * ByteArrayOutputStream::toArray() const { return &buf.toVector()[0]; }
        inline int ByteArrayOutputStream::getSize() const { return buf.getSize(); }
    }
}

#endif
