/* FileWriter.h */

#ifndef DARKCORE_UTIL_FILEWRITER_H_
#define DARKCORE_UTIL_FILEWRITER_H_

#include <fstream>              // std::ofstream, std::ios
#include <memory>               // std::unique_ptr, std::move
#include <string>               // std::string
#include <ostream>              // std::ostream
#include "File.h"               // File
#include "StreamWriter.h"       // StreamWriter

namespace darkcore
{
    namespace util
    {
        /**
         * Used to write content to a file or stream from an object.
         * Subclasses must provide an implementation for the pure virtual method write().
         */
        template <class T>
        class FileWriter : public StreamWriter<T>
        {
          public:
            /**
             * Virtual destructor.
             */
            virtual ~FileWriter();

            /**
             * Writes an Image to the output stream.
             * @param img Image
             */
            virtual void write(const T &img) = 0;

          protected:
            /**
             * Constructor prepared with the name of a file to write to.
             * @param filename Name of file for writing
             */
            FileWriter(const std::string &filename);

            /**
             * Constructor prepared with the Output Stream.
             * @param os Output stream to write data to
             */
            FileWriter(std::ostream &os);

            /**
             * Constructor prepared with the Output Stream.
             * Ownership is transferred to this Writer.
             * @param os Output stream to write data to
             */
            FileWriter(std::unique_ptr<std::ostream> os);
        };

        template <class T>
        inline FileWriter<T>::FileWriter(const std::string &filename) :
            StreamWriter<T>((std::unique_ptr<std::ostream>) File(filename).newOutputStream(std::ios::out | std::ios::binary))
        {
        }

        template <class T>
        inline FileWriter<T>::FileWriter(std::ostream &os) :
            StreamWriter<T>(os)
        {
        }

        template <class T>
        inline FileWriter<T>::FileWriter(std::unique_ptr<std::ostream> os) :
            StreamWriter<T>(std::move(os))
        {
        }

        template <class T>
        inline FileWriter<T>::~FileWriter()
        {
        }
    }
}

#endif
