/* Writer.h */

#ifndef DARKCORE_UTIL_WRITER_H_
#define DARKCORE_UTIL_WRITER_H_

#include "Exception.h"          // UnimplementedException

namespace darkcore
{
    namespace util
    {
        /**
         * Used to write content from an object.
         * Subclasses must provide an implementation for the pure virtual method write().
         */
        template <class T>
        class Writer
        {
          public:
            /**
             * Virtual destructor.
             */
            virtual ~Writer();

            /**
             * Writes an object to the output stream.
             * @param img Image
             */
            virtual void write(const T &img) = 0;

          protected:
            /**
             * Protected default constructor.
             */
            Writer();

          private:
            /**
             * Copy constructor.  Not implemented.
             * @param w Writer to copy from
             */
            Writer(const Writer<T> &w);

            /**
             * Assignment operator.  Not implemented.
             * @param w Writer to assign from
             * @return This Writer
             */
            virtual Writer<T> & operator=(const Writer<T> &w);
        };

        template <class T>
        inline Writer<T>::Writer()
        {
        }

        template <class T>
        inline Writer<T>::Writer(const Writer<T> &w)
        {
            throw UnimplementedException(__FUNCTION__);
        }

        template <class T>
        inline Writer<T>::~Writer()
        {
        }

        template <class T>
        inline Writer<T> & Writer<T>::operator=(const Writer<T> &w)
        {
            throw UnimplementedException(__FUNCTION__);
        }
    }
}

#endif
