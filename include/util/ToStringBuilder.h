/* ToStringBuilder.h */

#ifndef DARKCORE_UTIL_TOSTRINGBUILDER_H_
#define DARKCORE_UTIL_TOSTRINGBUILDER_H_

#include <sstream>      // std::ostringstream
#include <string>       // std::string
#include <vector>       // std::vector

namespace darkcore
{
    namespace util
    {
        class ToStringBuilder
        {
          public:
            ToStringBuilder(const std::string &classname);
            ~ToStringBuilder();

            ToStringBuilder & add(const std::string &param, const int &value);
            ToStringBuilder & add(const std::string &param, const std::string &value);
            template <class T>
            ToStringBuilder & add(const std::string &param, const std::vector<T> &value);

            std::string toString() const;

          private:
            ToStringBuilder(const ToStringBuilder &builder);
            ToStringBuilder & operator=(const ToStringBuilder &builder);

            void commaCheck();

            std::ostringstream ss;
            bool firstTime;
        };

        template <class T>
        inline ToStringBuilder & ToStringBuilder::add(const std::string &param, const std::vector<T> &value)
        {
            std::ostringstream valueStr("[");
            for (int i = 0; i < (int) value.size(); i++)
                valueStr << (i > 0 ? "," : "") << value[i];
            valueStr << "]";
            return this->add(param, valueStr.str());
        }
    }
}

#endif
