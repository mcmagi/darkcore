/* Reader.h */

#ifndef DARKCORE_UTIL_READER_H_
#define DARKCORE_UTIL_READER_H_

#include "Exception.h"          // UnimplementedException

namespace darkcore
{
    namespace util
    {
        /**
         * Used to read content and return it as an object.
         * Subclasses must provide an implementation for the pure virtual method read().
         */
        template <class T>
        class Reader
        {
          public:
            /**
             * Virtual destructor.
             */
            virtual ~Reader();

            /**
             * Reads the next object from the input stream.
             * @return Image
             */
            virtual T read() = 0;

            /**
             * Returns true if the reader has reached the end of input.
             * @return True if at end of input
             */
            virtual bool end() const = 0;

          protected:
            /**
             * Protected default constructor.
             */
            Reader();

          private:
            /**
             * Copy constructor.  Not implemented.
             * @param r Reader to copy from
             */
            Reader(const Reader<T> &r);

            /**
             * Assignment operator.  Not implemented.
             * @param r Reader to assign from
             * @return This Reader
             */
            Reader<T> & operator=(const Reader<T> &r);
        };

        template <class T>
        inline Reader<T>::Reader()
        {
        }

        template <class T>
        inline Reader<T>::Reader(const Reader<T> &r)
        {
            throw UnimplementedException(__FUNCTION__);
        }

        template <class T>
        inline Reader<T>::~Reader()
        {
        }

        template <class T>
        inline Reader<T> & Reader<T>::operator=(const Reader<T> &r)
        {
            throw UnimplementedException(__FUNCTION__);
        }
    }
}

#endif
