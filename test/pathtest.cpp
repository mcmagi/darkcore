#define BOOST_FILESYSTEM_VERSION 3

#include <boost/filesystem/path.hpp>    // boost:filesystem::path

using namespace std;
using boost::filesystem::path;

int main(int argc, char *argv[])
{
    // check arguments
    if (argc < 2)
    {
        // print error
        cout << "usage:" << endl;
        cout << " " << argv[0] << " <filename>" << endl;
        exit(1);
    }

    path file = string(argv[1]);
    cout << "file: " << file << endl;
    cout << "file.string: " << file.string() << endl;
    cout << "file.parent_path: " << file.parent_path() << endl;
    cout << "file.relative_path: " << file.relative_path() << endl;
    cout << "file.filename: " << file.filename() << endl;
    cout << "file.stem: " << file.stem() << endl;
    cout << "file.extension: " << file.extension() << endl;
    cout << "file.root_path: " << file.root_path() << endl;
    cout << "file.root_name: " << file.root_name() << endl;
    cout << "file.root_directory: " << file.root_directory() << endl;

    return 0;
}
