# directories
SRC = src
LIB = lib
INC = include
BIN = bin

all:
	make -C ${SRC}

clean:
	make -C ${SRC} clean
