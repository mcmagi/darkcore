/* MainWindow.cpp */

#include <algorithm>            // min
#include <cassert>              // assert
#include <iostream>             // std::cout
#include <typeinfo>             // typeid
#include "game/GameData.h"      // GameData
#include "sdl/MainWindow.h"     // MainWindow
#include "sdl/MapWindow.h"      // MapWindow
#include "sdl/SDLException.h"   // SDLException
#include "sdl/SDLWindow.h"      // SDLWindow

using namespace std;
using darkcore::game::GameData;
using darkcore::sdl::MainWindow;
using darkcore::sdl::MapWindow;
using darkcore::sdl::SDLException;
using darkcore::sdl::SDLWindow;

MainWindow::MainWindow(int w, int h, const GameData &data) :
    SDLWindow(w, h), mapWindow(new MapWindow(data, *this, w, h))
{
}

MainWindow::MainWindow(const MainWindow &win) :
    SDLWindow(win), mapWindow(new MapWindow(*win.mapWindow))
{
}

MainWindow::~MainWindow()
{
    delete mapWindow;
}

SDLWindow & MainWindow::operator=(const SDLWindow &win)
{
    assert(typeid(win) == typeid(*this));
    return this->operator=(dynamic_cast<const MainWindow &>(win));
}

MainWindow & MainWindow::operator=(const MainWindow &win)
{
    if (this != &win)
    {
        mapWindow = mapWindow;
    }
    return *this;
}

void MainWindow::display()
{
    if (getSurface() == NULL)
	{
        setSurface(SDL_SetVideoMode(getWidth(), getHeight(), 0, SDL_ANYFORMAT));

		// if could not create surface, throw exception
		if (getSurface() == NULL)
			throw SDLException();
	}

    // cleanup any artifacts on the surface
    SDL_Rect blackRect = { 0, 0, (Uint16) getWidth(), (Uint16) getHeight() };
    SDL_FillRect(getSurface(), &blackRect, 0);

    // render the map window
    mapWindow->display();

    // blit the map surface to the display surface
    SDL_Rect srcRect = { 0, 0, (Uint16) mapWindow->getWidth(), (Uint16) mapWindow->getHeight() };
    SDL_Rect tgtRect = { (Sint16) mapWindow->getX(), (Sint16) mapWindow->getY(),
        (Uint16) min(mapWindow->getWidth(), mapWindow->getSurface()->w),
        (Uint16) min(mapWindow->getHeight(), mapWindow->getSurface()->h) };
    SDL_BlitSurface(mapWindow->getSurface(), &srcRect, getSurface(), &tgtRect);

    // update the rect
    SDL_UpdateRect(getSurface(), 0, 0, 0, 0);
}
