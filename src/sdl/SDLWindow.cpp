/* SDLWindow.cpp */

#include "SDL/SDL.h"
#include "sdl/SDLWindow.h"      // SDLWindow

using namespace std;
using darkcore::sdl::SDLWindow;

SDLWindow::SDLWindow(int w, int h, int x, int y) :
    parent(NULL), width(w), height(h), x(x), y(y), surface(NULL)
{
}

SDLWindow::SDLWindow(SDLWindow &parent, int w, int h, int x, int y) :
    parent(&parent), width(w), height(h), x(x), y(y), surface(NULL)
{
}

SDLWindow::SDLWindow(const SDLWindow &win) :
    width(win.width), height(win.height), x(win.x), y(win.y), surface(NULL)
{
}

SDLWindow::~SDLWindow()
{
    freeSurface();
}

SDLWindow & SDLWindow::operator=(const SDLWindow &win)
{
    if (this != &win)
    {
        parent = win.parent;
        width = win.width;
        height = win.height;
        x = win.x;
        y = win.y;
        surface = NULL;
    }
    return *this;
}

void SDLWindow::setSurface(SDL_Surface *surface)
{
    freeSurface();
    this->surface = surface;
}

void SDLWindow::freeSurface()
{
    if (surface != NULL)
    {
        SDL_FreeSurface(surface);
        surface = NULL;
    }
}
