/* SDLException.cpp */

#include <string>               // std::string
#include "SDL/SDL.h"            // SDL_GetError
#include "Exception.h"          // Exception
#include "sdl/SDLException.h"   // SDLException

using namespace std;
using darkcore::Exception;
using darkcore::sdl::SDLException;

SDLException::SDLException() :
    Exception(string(SDL_GetError()))
{
}

SDLException::SDLException(const SDLException &ex) :
    Exception(ex)
{
}

SDLException::~SDLException() throw ()
{
}
