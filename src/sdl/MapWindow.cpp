/* MapWindow.cpp */

#include <cassert>                  // assert
#include <iostream>                 // std::cout
#include <typeinfo>                 // typeid
#include <vector>                   // vector
#include "game/Coordinates.h"       // Coordinates
#include "game/GameData.h"          // GameData
#include "image/Color.h"            // Pixel
#include "image/Image.h"            // Image
#include "image/ImageFormat.h"      // ImageFormat
#include "image/ImageSet.h"         // ImageSet
#include "image/ColorMask.h"        // ColorMask
#include "image/converter/CGACompositeConverter.h" // CGACompositeConverter
#include "map/Map.h"                // Map
#include "map/MapImageBuilder.h"    // MapImageBuilder
#include "sdl/MapWindow.h"          // MapWindow
#include "sdl/SDLWindow.h"          // SDLWindow

using namespace std;
using darkcore::game::Coordinates;
using darkcore::game::GameData;
using darkcore::image::ColorMask;
using darkcore::image::Image;
using darkcore::image::ImageFormat;;
using darkcore::image::ImageSet;
using darkcore::image::Pixel;
using darkcore::image::converter::CGACompositeConverter;
using darkcore::map::Map;
using darkcore::map::MapImageBuilder;
using darkcore::sdl::MapWindow;
using darkcore::sdl::SDLWindow;

MapWindow::MapWindow(const GameData &data, SDLWindow &parent, int w, int h) :
    SDLWindow(parent, w, h), data(&data),
    // maybe make this static on MainWindow?
    rgbImgFmt(ColorMask::FULL32), img()
{
}

MapWindow::MapWindow(const MapWindow &win) :
    SDLWindow(win), data(win.data), rgbImgFmt(win.rgbImgFmt), img()
{
}

MapWindow::~MapWindow()
{
}

SDLWindow & MapWindow::operator=(const SDLWindow &win)
{
    assert(typeid(win) == typeid(*this));
    return this->operator=(dynamic_cast<const MapWindow &>(win));
}

MapWindow & MapWindow::operator=(const MapWindow &win)
{
    if (this != &win)
    {
        data = data;
        rgbImgFmt = rgbImgFmt;
    }
    return *this;
}

void MapWindow::display()
{
    Coordinates c = data->getCoordinates();
    MapImageBuilder builder = data->getMap().getMapImageBuilder(data->getTileSet());
    // TODO - need better way to convert from pixel dimensions to tile dimensions
    this->img = builder.createMapImage(c.getX(), c.getY(), getWidth() / 16, getHeight() / 16);

    // convert the image to the specified format
    if (img->getImageFormat().getFormatType() == ImageFormat::INDEXED)
    {
        if (img->getImageFormat().getColorDepth() == 2 && data->isCGAComposite())
            this->img = this->img->convert(CGACompositeConverter(rgbImgFmt));
        else
            this->img = this->img->convert(rgbImgFmt);
    }

    displayRGBImage();
}

void MapWindow::displayRGBImage()
{
    const vector<Pixel> &imgdata = this->img->getImageData();

    // create an image surface from the pixel data
    SDL_Surface *imgSurface = SDL_CreateRGBSurfaceFrom(
            const_cast<Pixel *>(&imgdata[0]), // TODO: total hack
            this->img->getWidth(),
            this->img->getHeight(),
            rgbImgFmt.getColorDepth(),
            this->img->getPitch(),
            rgbImgFmt.getColorMask().getRedMask(),
            rgbImgFmt.getColorMask().getGreenMask(),
            rgbImgFmt.getColorMask().getBlueMask(),
            0);
    setSurface(imgSurface);
}
