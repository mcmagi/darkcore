/* Arguments.cpp */

#include <string>               // std::string
#include <vector>               // std::vector
#include "command/Arguments.h"  // Arguments

using namespace std;
using darkcore::command::Arguments;

Arguments::Arguments(const std::string program, const std::string command,
        const std::string subcommand, const std::vector<std::string> arguments) :
    program(program), command(command), subcommand(subcommand), arguments(arguments)
{
}

Arguments::Arguments(const Arguments &args) :
    program(args.program), command(args.command), subcommand(args.subcommand), arguments(args.arguments)
{
}