/* ViewImage.cpp */

#include <cstdlib>                      // atoi
#include <iostream>                     // std::cout, std::endl
#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include <vector>                       // std::vector
#include "browser/SDLBrowser.h"         // SDLBrowser
#include "browser/TileBrowser.h"        // TileBrowser
#include "command/Arguments.h"          // Arguments
#include "command/Command.h"            // UsageException
#include "command/ViewImage.h"          // ViewImage
#include "config/UltimaGameConfig.h"    // UltimaGameConfig, UltimaGame
#include "config/UltimaImageConfig.h"   // UltimaImageConfig
#include "image/Image.h"                // Image
#include "image/ImageSet.h"             // ImageSet
#include "image/UltimaImageReader.h"    // UltimaImageReader
#include "util/File.h"                  // File

using namespace std;
using darkcore::browser::SDLBrowser;
using darkcore::browser::TileBrowser;
using darkcore::command::Arguments;
using darkcore::command::UsageException;
using darkcore::command::ViewImage;
using darkcore::config::UltimaGame;
using darkcore::config::UltimaGameConfig;
using darkcore::config::UltimaImageConfig;
using darkcore::image::Image;
using darkcore::image::ImageSet;
using darkcore::image::UltimaImageReader;
using darkcore::util::File;

unique_ptr<ViewImage> ViewImage::Factory::create(const Arguments &args) const
{
    vector<string> optargs = args.getArguments();

    if (optargs.size() < 2)
        throw UsageException(args, help());

    UltimaGame game = (UltimaGame) atoi(optargs[0].c_str());
    File file = optargs[1];
    bool showall = optargs.size() == 3 && optargs[2] == string("all");

    return (unique_ptr<ViewImage>) new ViewImage(game, file, showall);
}

string ViewImage::Factory::help() const
{
    return "<game #> <filename> [all]";
}

ViewImage::ViewImage(UltimaGame game, const File file, bool showall) :
    Command(), game(game), file(file), showall(showall)
{
}

void ViewImage::run() const
{
    // get game configuration
    cout << "loading game configuration" << endl;
    UltimaGameConfig gameCfg = UltimaGameConfig::getGameConfig(game, file.getParentName());
    const UltimaImageConfig &imgCfg = gameCfg.getImageConfig(file.getName());

    // read tileset
    cout << "reading tileset from file " << file.getFullName() << endl;
    UltimaImageReader uir(imgCfg);
    unique_ptr<ImageSet> ts = uir.readAll();

    unique_ptr<SDLBrowser> sb;
    unique_ptr<Image> img;
    if (showall)
    {
        // create an image from the tileset (10 tiles across)
        img.reset(new Image(*ts, TileBrowser::calculateMergedImageWidth(imgCfg)));
        sb.reset(new SDLBrowser(*img));
    }
    else
    {
        sb.reset(new SDLBrowser(*ts, imgCfg));
    }

    // begin the browsing experience
    sb->browserMain();
}