/* ImportMap.cpp */

#include <cstdlib>                      // atoi
#include <iostream>                     // std::cout, std::endl
#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include <vector>                       // std::vector
#include "command/Arguments.h"          // Arguments
#include "command/Command.h"            // UsageException
#include "command/ImportMap.h"          // ImportMap
#include "config/UltimaGameConfig.h"    // UltimaGameConfig, UltimaGame
#include "config/UltimaMapConfig.h"     // UltimaMapConfig
#include "map/Map.h"                    // Map
#include "map/TextIndexMapper.h"        // TextIndexMapper
#include "map/TextMapReader.h"          // TextMapReader
#include "map/UltimaMapWriter.h"        // UltimaMapWriter
#include "util/File.h"                  // File

using namespace std;
using darkcore::command::Arguments;
using darkcore::command::UsageException;
using darkcore::command::ImportMap;
using darkcore::config::UltimaGame;
using darkcore::config::UltimaGameConfig;
using darkcore::config::UltimaMapConfig;
using darkcore::map::Map;
using darkcore::map::TextIndexMapper;
using darkcore::map::TextMapReader;
using darkcore::map::UltimaMapWriter;
using darkcore::util::File;

unique_ptr<ImportMap> ImportMap::Factory::create(const Arguments &args) const
{
    vector<string> optargs = args.getArguments();
    if (optargs.size() < 2)
        throw UsageException(args, help());

    UltimaGame game = (UltimaGame) atoi(optargs[0].c_str());
    File file = optargs[1];
    File textFile = optargs[2];
    unique_ptr<File> destFile;
    if (optargs.size() > 3)
        destFile = (unique_ptr<File>) new File(optargs[3]);

    return (unique_ptr<ImportMap>) new ImportMap(game, file, textFile, std::move(destFile));
}

string ImportMap::Factory::help() const
{
    return "<game #> <filename> <text-filename> [<dest-filename>]";
}

void ImportMap::run() const
{
    // get game configuration
    cout << "loading game configuration" << endl;
    UltimaGameConfig gameCfg = UltimaGameConfig::getGameConfig(game, file.getParentName());

    UltimaMapConfig mapCfg = gameCfg.getMapConfig(file.getName());
    // if a different dest file is provided, use it
    if (destFile)
        mapCfg = mapCfg.forFile(destFile->getFullName());

    cout << "reading map from " << textFile.getFullName();
    TextMapReader tmr(textFile, TextIndexMapper::forConfig(mapCfg), mapCfg.getWidth(), mapCfg.getHeight());
    std::unique_ptr<Map> map = tmr.read();

    // write map to 
    cout << "writing map to " << mapCfg.getFilename();
    UltimaMapWriter umw(mapCfg);
    umw.write(*map);
}