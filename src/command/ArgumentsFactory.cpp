/* ArgumentsFactory.cpp */

#include <string>               // std::string
#include <vector>               // std::vector
#include "command/Arguments.h"  // Arguments, ArgumentsFactory

using namespace std;
using darkcore::command::Arguments;
using darkcore::command::ArgumentsFactory;

Arguments ArgumentsFactory::create(int argc, char *argv[]) const
{
    // transform args into vector array
    vector<string> args(argc-1);
    for (int i = 1; i < argc; i++)
        args[i-1] = argv[i];

    int argsIdx = 0;
    string command;
    if (args.size() > 0) {
        command = args[argsIdx++];
    }
    string subcommand;
    if (hasSubCommand(command) && args.size() > 1) {
        subcommand = args[argsIdx++];
    }

    return Arguments(argv[0], command, subcommand, subset(args, argsIdx));
}

void ArgumentsFactory::registerNestedCommand(const std::string command)
{
    nestedCommands.insert(command);
}

bool ArgumentsFactory::hasSubCommand(const std::string &command) const
{
    return nestedCommands.count(command) == 1;
}

vector<string> ArgumentsFactory::subset(vector<string> orig, int start)
{
    return subset(orig, start, orig.size());
}

vector<string> ArgumentsFactory::subset(vector<string> orig, int start, int end)
{
    vector<string> sub(end - start);
    for (unsigned int i = 0; i < sub.size(); i++)
        sub[i] = orig[i+start];
    return sub;
}