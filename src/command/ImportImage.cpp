/* ImportImage.cpp */

#include <cstdlib>                      // atoi
#include <iostream>                     // std::cout, std::endl, std::ifstream
#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include <vector>                       // std::vector
#include "browser/SDLBrowser.h"         // SDLBrowser
#include "browser/TileBrowser.h"        // TileBrowser
#include "command/Arguments.h"          // Arguments
#include "command/Command.h"            // UsageException
#include "command/ImportImage.h"        // ImportImage
#include "config/UltimaGameConfig.h"    // UltimaGameConfig, UltimaGame
#include "config/UltimaImageConfig.h"   // UltimaImageConfig
#include "image/Image.h"                // Image
#include "image/ImageSet.h"             // ImageSet
#include "image/UltimaImageWriter.h"    // UltimaImageWriter
#include "image/png/PNGReader.h"        // PNGReader
#include "util/File.h"                  // File

using namespace std;
using darkcore::browser::SDLBrowser;
using darkcore::browser::TileBrowser;
using darkcore::command::Arguments;
using darkcore::command::UsageException;
using darkcore::command::ImportImage;
using darkcore::config::UltimaGame;
using darkcore::config::UltimaGameConfig;
using darkcore::config::UltimaImageConfig;
using darkcore::image::Image;
using darkcore::image::ImageSet;
using darkcore::image::UltimaImageWriter;
using darkcore::image::png::PNGReader;
using darkcore::util::File;

unique_ptr<ImportImage> ImportImage::Factory::create(const Arguments &args) const
{
    vector<string> optargs = args.getArguments();

    if (optargs.size() < 3)
        throw UsageException(args, help());

    UltimaGame game = (UltimaGame) atoi(optargs[0].c_str());
    File file = optargs[1];
    File pngFile = optargs[2];

    return (unique_ptr<ImportImage>) new ImportImage(game, file, pngFile);
}

string ImportImage::Factory::help() const
{
    return "<game #> <filename> <png file>";
}

ImportImage::ImportImage(UltimaGame game, const File file, const File pngFile) :
    Command(), game(game), file(file), pngFile(pngFile)
{
}

void ImportImage::run() const
{
    // get game configuration
    cout << "loading game configuration" << endl;
    UltimaGameConfig gameCfg = UltimaGameConfig::getGameConfig(game, file.getParentName());
    const UltimaImageConfig &imgCfg = gameCfg.getImageConfig(file.getName());

    // read png image
    cout << "reading image from png file " << pngFile.getFullName() << endl;
    unique_ptr<ifstream> ifs = pngFile.newInputStream();
    PNGReader pngreader(*ifs);
    unique_ptr<Image> img = pngreader.read();

    // break image into tileset
    cout << "breaking image into tileset" << endl;
    unique_ptr<ImageSet> imgset = img->tileImage(imgCfg.getWidth(), imgCfg.getHeight());
    imgset->resize(imgCfg.getNumImages()); // removes any extra tiles at end

    // output tileset
    cout << "writing tileset to " << file.getFullName() << endl;
    UltimaImageWriter uiw(imgCfg);
    uiw.writeAll(*imgset);
}