/* ViewMap.cpp */

#include <cstdlib>                      // atoi
#include <iostream>                     // std::cout, std::endl
#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include <vector>                       // std::vector
#include "command/Arguments.h"          // Arguments
#include "command/Command.h"            // UsageException
#include "command/ViewMap.h"            // ViewMap
#include "config/UltimaGameConfig.h"    // UltimaGameConfig, UltimaGame
#include "game/GameMain.h"              // GameMain
#include "util/File.h"                  // File

using namespace std;
using darkcore::command::Arguments;
using darkcore::command::UsageException;
using darkcore::command::ViewMap;
using darkcore::config::UltimaGame;
using darkcore::config::UltimaGameConfig;
using darkcore::game::GameMain;
using darkcore::util::File;

unique_ptr<ViewMap> ViewMap::Factory::create(const Arguments &args) const
{
    vector<string> optargs = args.getArguments();
    if (optargs.size() < 2)
        throw UsageException(args, help());

    UltimaGame game = (UltimaGame) atoi(optargs[0].c_str());
    File file = optargs[1];
    unique_ptr<File> textFile;
    if (optargs.size() > 2)
        textFile = (unique_ptr<File>) new File(optargs[2]);

    return (unique_ptr<ViewMap>) new ViewMap(game, file, std::move(textFile));
}

string ViewMap::Factory::help() const
{
    return "<game #> <filename> [<text-filename>]";
}

ViewMap::ViewMap(UltimaGame game, const File file, unique_ptr<File> textFile) :
    Command(), game(game), file(file), textFile(std::move(textFile))
{
}

void ViewMap::run() const
{
    // get game configuration
    cout << "loading game configuration" << endl;
    UltimaGameConfig gameCfg = UltimaGameConfig::getGameConfig(game, file.getParentName());

    // start the game
    GameMain main(gameCfg, file.getName(), textFile);
    main.start();
}