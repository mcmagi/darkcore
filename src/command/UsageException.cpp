/* UsageException.cpp */

#include <sstream>              // std::ostringstream
#include <string>               // std::string
#include "Exception.h"          // Exception
#include "command/Arguments.h"  // Arguments
#include "command/Command.h"    // UsageException

using namespace std;
using darkcore::Exception;
using darkcore::command::Arguments;
using darkcore::command::UsageException;

UsageException::UsageException(const string helpMessage) :
    Exception(helpMessage), program(), command(), subcommand()
{
}
UsageException::UsageException(const Arguments &args, const string helpMessage) :
    Exception(helpMessage), program(args.getProgram()), command(args.getCommand()),
    subcommand(args.getSubCommand())
{
}

UsageException::UsageException(const UsageException &ex) :
    Exception(ex), program(ex.program), command(ex.command), subcommand(ex.subcommand)
{
}

UsageException::~UsageException() throw ()
{
}