/* IndexException.cpp */

#include <sstream>              // std::ostringstream
#include <string>               // std::string
#include "Exception.h"          // Exception, IndexException

using namespace std;
using darkcore::Exception;
using darkcore::IndexException;

IndexException::IndexException(const string &errmsg, int lower, int upper, int index) :
    Exception(errmsg), lower_bound(lower), upper_bound(upper),
    passed_index(index)
{
}

IndexException::IndexException(const IndexException &ex) :
    Exception(ex), lower_bound(ex.lower_bound),
    upper_bound(ex.upper_bound), passed_index(ex.passed_index)
{
}

IndexException::~IndexException() throw ()
{
}

string IndexException::getMessage() const throw ()
{
    ostringstream stream;
    stream << "index=" << passed_index << ",bounds{lower=" << lower_bound << ",upper=" << upper_bound << "}: ";
    stream << Exception::getMessage();
    return stream.str().data();
}