/* GameMain.cpp */

#include "SDL/SDL.h"
#include <cstdlib>                          // std::atexit
#include <iostream>                         // std::cout, std::ostream
#include <memory>                           // std::unique_ptr
#include <string>                           // std::string
#include "Exception.h"                      // Exception, UnimplementedException
#include "config/UltimaGameConfig.h"        // UltimaGameConfig
#include "config/UltimaImageConfig.h"       // UltimaImageConfig
#include "config/UltimaMapConfig.h"         // UltimaMapConfig
#include "game/Coordinates.h"               // Coordinates
#include "game/Direction.h"                 // Direction
#include "game/GameData.h"                  // GameData
#include "game/GameMain.h"                  // GameMain
#include "image/ColorMask.h"                // ColorMask
#include "image/Image.h"                    // Image
#include "image/ImageFormat.h"              // ImageFormat, RGBImageFormat
#include "image/ImageSet.h"                 // ImageSet
#include "image/UltimaImageReader.h"        // UltimaImageReader
#include "image/converter/CGACompositeConverter.h" // CGACompositeConverter
#include "image/png/PNGException.h"         // PNGException
#include "image/png/PNGWriter.h"            // PNGWriter
#include "map/Map.h"                        // Map
#include "map/MapImageBuilder.h"            // MapImageBuilder
#include "map/TextIndexMapper.h"            // TextIndexMapper
#include "map/TextMapReader.h"              // TextMapReader
#include "map/UltimaMapReader.h"            // UltimaMapReader
#include "sdl/MainWindow.h"                 // MainWindow
#include "util/File.h"                      // File

using namespace std;
using darkcore::Exception;
using darkcore::FileException;
using darkcore::UnimplementedException;
using darkcore::config::UltimaGameConfig;
using darkcore::config::UltimaImageConfig;
using darkcore::config::UltimaMapConfig;
using darkcore::game::Coordinates;
using darkcore::game::Direction;
using darkcore::game::GameData;
using darkcore::game::GameMain;
using darkcore::image::ColorMask;
using darkcore::image::Image;
using darkcore::image::ImageSet;
using darkcore::image::RGBImageFormat;
using darkcore::image::UltimaImageReader;
using darkcore::image::converter::CGACompositeConverter;
using darkcore::image::png::PNGException;
using darkcore::image::png::PNGWriter;
using darkcore::map::Map;
using darkcore::map::MapImageBuilder;
using darkcore::map::TextIndexMapper;
using darkcore::map::TextMapReader;
using darkcore::map::UltimaMapReader;
using darkcore::sdl::MainWindow;
using darkcore::util::File;

GameMain::GameMain(const UltimaGameConfig &cfg, const string &mapFilename, const unique_ptr<File> &textFile) :
    cfg(cfg), mapCfg(cfg.getMapConfig(mapFilename)), mapFilename(mapFilename),
    tileSet(loadTileSet(mapCfg)), map(loadMap(mapCfg, textFile)),
    gameData(*map, *tileSet, Coordinates()), window(new MainWindow(640, 640, gameData)),
    textFile(textFile)
{
    SDL_Init(SDL_INIT_VIDEO);
    atexit(SDL_Quit);
}

GameMain::GameMain(const GameMain &gm) :
    cfg(gm.cfg), mapCfg(gm.mapCfg), mapFilename(gm.mapFilename), tileSet(), map(),
    gameData(gm.gameData), window(), textFile(gm.textFile)
{
    throw UnimplementedException(__FUNCTION__);
}

GameMain::~GameMain()
{
}

GameMain & GameMain::operator=(const GameMain &gm)
{
    throw UnimplementedException(__FUNCTION__);
}

void GameMain::start()
{
    bool updateDisplay = true;
    SDL_Event event;

    SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL / 4);

    while (true)
    {
        if (updateDisplay)
        {
            window->display();
            updateDisplay = false;
        }

        if (SDL_WaitEvent(&event) != 0)
        {
            if (event.type == SDL_QUIT)
                break;
            else if (event.type == SDL_KEYDOWN)
            {
                Direction dir = Direction::NONE;
                int newBpp = 0;
                bool newCgaComp = gameData.isCGAComposite();

                if (event.key.keysym.sym == SDLK_UP)
                    dir = Direction::NORTH;
                else if (event.key.keysym.sym == SDLK_DOWN)
                    dir = Direction::SOUTH;
                else if (event.key.keysym.sym == SDLK_LEFT)
                    dir = Direction::WEST;
                else if (event.key.keysym.sym == SDLK_RIGHT)
                    dir = Direction::EAST;
                else if (event.key.keysym.sym == SDLK_2)
                {
                    newBpp = 2; // CGA
                    newCgaComp = false;
                }
                else if (event.key.keysym.sym == SDLK_3)
                {
                    newBpp = 2; // CGA
                    newCgaComp = true;
                }
                else if (event.key.keysym.sym == SDLK_4)
                    newBpp = 4; // EGA
                else if (event.key.keysym.sym == SDLK_8)
                    newBpp = 8; // VGA
                else if (event.key.keysym.sym == SDLK_x)
                    exportImage();
                else if (event.key.keysym.sym == SDLK_q)
                    break;

                if (dir != Direction::NONE)
                {
                    Coordinates coords = gameData.getCoordinates() + dir;
                    if (validateCoordinates(coords))
                        gameData.getCoordinates() = coords;
                }

                // if bpp changes
                if ((newBpp != 0 && newBpp != tileSet->getImageFormat().getColorDepth()) ||
                    newCgaComp != gameData.isCGAComposite())
                {
                    if (mapCfg.hasTileSetConfig(newBpp))
                    {
                        // load the tileset for the specified bpp
                        tileSet = loadTileSet(mapCfg, newBpp);
                        gameData.setTileSet(*tileSet);
                        gameData.setCGAComposite(newCgaComp);
                    }
                    else
                        cout << "No tileset for bpp " << newBpp << endl;
                }

                updateDisplay = true;
            }
        }
    }
}

unique_ptr<ImageSet> GameMain::loadTileSet(const UltimaMapConfig &mapCfg, int bpp)
{
    const UltimaImageConfig &imgCfg = mapCfg.getTileSetConfig(bpp);

    cout << "reading tileset '" << imgCfg.getDescription() << "' from file " << imgCfg.getFileConfig().getFile().getFullName() << endl;
    UltimaImageReader uir(imgCfg);
    return uir.readAll();
}

unique_ptr<Map> GameMain::loadMap(const UltimaMapConfig &mapCfg, const unique_ptr<File> &textFile)
{
    if (textFile)
    {
        cout << "reading text map '" << mapCfg.getDescription() << "' from file " << textFile->getName() << endl;
        TextMapReader tmr(*textFile, TextIndexMapper::forConfig(mapCfg), mapCfg.getWidth(), mapCfg.getHeight());
        return tmr.read();
    }
    cout << "reading map '" << mapCfg.getDescription() << "' from file " << mapCfg.getFilename() << endl;
    UltimaMapReader mr(mapCfg);
    return mr.read();
}

bool GameMain::validateCoordinates(Coordinates coords) const
{
    return coords.getX() >= 0 && coords.getX() <= map->getWidth() - window->getWidth() / tileSet->getWidth()
        && coords.getY() >= 0 && coords.getY() <= map->getHeight() - window->getHeight() / tileSet->getHeight();
}


void GameMain::exportImage() const
{
    MapImageBuilder builder = map->getMapImageBuilder(*tileSet);
    unique_ptr<Image> img = builder.createMapImage();

    if (gameData.isCGAComposite())
    {
        RGBImageFormat rgb(ColorMask::FULL32);
        CGACompositeConverter compositeConv(rgb);
        img = img->convert(compositeConv);
    }
    try
    {
		string mode = "";

        // derive filename
        int bpp = gameData.getTileSet().getImageFormat().getColorDepth();
        if (bpp == 2 && gameData.isCGAComposite())
            mode = "cgacomp";
        else if (bpp == 2)
            mode = "cga";
        else if (bpp == 4)
            mode = "ega";
        else if (bpp == 8)
            mode = "vga";
        string filename = mapCfg.getFilename() + string("-") + mode + string(".png");

        cout << "Exporting image to " << filename << endl;

        File file(filename);
        unique_ptr<ostream> os = file.newOutputStream();

        PNGWriter pngw(*os);
        pngw.write(*img);
    }
    catch (PNGException pe)
    {
        cout << pe.what();
    }
    catch (FileException fe)
    {
        cout << fe.what();
    }
}