/* Coordinates.cpp */

#include "game/Coordinates.h"       // Coordinates
#include "game/Direction.h"         // Direction
//#include "game/Vector.h"            // Vector
#include "util/ToStringBuilder.h"   // ToStringBuilder

using namespace std;
using darkcore::game::Coordinates;
using darkcore::game::Direction;
//using darkcore::game::Vector;
using darkcore::util::ToStringBuilder;

Coordinates::Coordinates(int x, int y, int z) :
    x(x), y(y), z(z)
{
}

Coordinates::Coordinates(const Coordinates &coords) :
    x(coords.x), y(coords.y), z(coords.z)
{
}

Coordinates::~Coordinates()
{
}

Coordinates & Coordinates::operator=(const Coordinates &coords)
{
    if (this != &coords)
    {
        x = coords.x;
        y = coords.y;
        z = coords.z;
    }
    return *this;
}

Coordinates Coordinates::operator+(const Coordinates &coords) const
{
    return Coordinates(*this) += coords;
}

Coordinates Coordinates::operator+(const Direction &dir) const
{
    return *this + dir.getOffset();
}

/*Coordinates Coordinates::operator+(const Vector &vect) const
{
    return Coordinates(*this) + vect.getOffset();
}*/

Coordinates & Coordinates::operator+=(const Coordinates &coords)
{
    x += coords.x;
    y += coords.y;
    z += coords.z;
    return *this;
}

Coordinates & Coordinates::operator+=(const Direction &dir)
{
    return *this += dir.getOffset();
}

/*Coordinates & Coordinates::operator+=(const Vector &vect)
{
    return *this += vect.getOffset();
}*/

bool Coordinates::operator==(const Coordinates &coords) const
{
    return x == coords.x && y == coords.y && z == coords.z;
}

bool Coordinates::operator!=(const Coordinates &coords) const
{
    return ! (*this == coords);
}

Coordinates::operator const char * () const
{
    return toString().c_str();
}

string Coordinates::toString() const
{
    return ToStringBuilder("Coordinates")
        .add("x", x)
        .add("y", y)
        .add("z", z)
        .toString();
}
