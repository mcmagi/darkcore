/* GameData.cpp */

#include <iostream>                 // std::cout
#include "game/Coordinates.h"       // Coordinates
#include "game/GameData.h"          // GameData
#include "image/ImageSet.h"         // ImageSet
#include "map/Map.h"                // Map

using namespace std;
using darkcore::game::Coordinates;
using darkcore::game::GameData;
using darkcore::image::ImageSet;
using darkcore::map::Map;

GameData::GameData(const Map &map, const ImageSet &tileSet, const Coordinates &coords) :
    map(&map), tileSet(&tileSet), coords(coords), cgaComposite(false)
{
    //cout << "creating GameData" << endl;
}

GameData::GameData(const GameData &data) :
    map(data.map), tileSet(data.tileSet), coords(data.coords), cgaComposite(data.cgaComposite)
{
}

GameData::~GameData()
{
}

GameData & GameData::operator=(const GameData &data)
{
    if (this != &data)
    {
        map = data.map;
        tileSet = data.tileSet;
        coords = data.coords;
    }
    return *this;
}
