/* Direction.cpp */

#include "game/Coordinates.h"       // Coordinates
#include "game/Direction.h"         // Direction
#include "util/ToStringBuilder.h"   // ToStringBuilder

using namespace std;
using darkcore::game::Coordinates;
using darkcore::game::Direction;
using darkcore::util::ToStringBuilder;

const Direction Direction::NONE = Coordinates();
const Direction Direction::NORTH(Coordinates(0, -1));
const Direction Direction::SOUTH(Coordinates(0, 1));
const Direction Direction::EAST(Coordinates(1, 0));
const Direction Direction::WEST(Coordinates(-1, 0));
const Direction Direction::NORTHEAST(NORTH.getOffset() + EAST.getOffset());
const Direction Direction::NORTHWEST(NORTH.getOffset() + WEST.getOffset());
const Direction Direction::SOUTHEAST(SOUTH.getOffset() + EAST.getOffset());
const Direction Direction::SOUTHWEST(SOUTH.getOffset() + WEST.getOffset());
const Direction Direction::UP(Coordinates(0, 0, 1));
const Direction Direction::DOWN(Coordinates(0, 0, -1));

Direction::Direction(const Coordinates &coords) :
    coords(coords)
{
}

Direction::Direction(const Direction &dir) :
    coords(dir.coords)
{
}

Direction::~Direction()
{
}

Direction & Direction::operator=(const Direction &dir)
{
    if (this != &dir)
        coords = dir.coords;
    return *this;
}

bool Direction::operator==(const Direction &dir) const
{
    return coords == dir.coords;
}

bool Direction::operator!=(const Direction &dir) const
{
    return coords != dir.coords;
}

Direction::operator const char * () const
{
    return toString().c_str();
}

string Direction::toString() const
{
    return ToStringBuilder("Direction")
        .add("coords", coords.toString())
        .toString();
}
