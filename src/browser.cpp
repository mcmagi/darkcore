/* browser.cpp */

#include <cstdlib>                      // exit
#include <exception>                    // std::exception
#include <iostream>                     // std::cout, std::endl
#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include "Exception.h"                  // Exception
#include "browser/AsciiBrowser.h"       // AsciiBrowser
#include "config/UltimaGameConfig.h"    // UltimaGameConfig, UltimaGame
#include "config/UltimaImageConfig.h"   // UltimaImageConfig
#include "image/ImageSet.h"             // ImageSet
#include "image/UltimaImageReader.h"    // UltimaImageReader
#include "util/File.h"                  // File

using namespace std;
using darkcore::Exception;
using darkcore::browser::AsciiBrowser;
using darkcore::config::UltimaGame;
using darkcore::config::UltimaGameConfig;
using darkcore::config::UltimaImageConfig;
using darkcore::image::ImageSet;
using darkcore::image::UltimaImageReader;
using darkcore::util::File;

int main(int argc, char *argv[])
{
    // check arguments
    if (argc < 3)
    {
        // print error
        cout << "usage:" << endl;
        cout << " " << argv[0] << " <game #> <filename>" << endl;
        exit(1);
    }

    UltimaGame game = (UltimaGame) atoi(argv[1]);
    File file = string(argv[2]);

    try
    {
        // get game configuration
        cout << "loading game configuration" << endl;
        UltimaGameConfig gameCfg = UltimaGameConfig::getGameConfig(game, file.getParentName());
        const UltimaImageConfig &imgCfg = gameCfg.getImageConfig(file.getName());

        // read tileset
        cout << "reading tileset from file " << file.getFullName() << endl;
        UltimaImageReader uir(imgCfg);
        unique_ptr<ImageSet> ts = uir.readAll();

        // begin the browsing experience
        cout << "starting ASCII browser" << endl;
        AsciiBrowser ab(*ts, imgCfg);
        ab.browserMain();
    }
    catch (const Exception &ex)
    {
        cout << "dark core exception: " << ex.what() << endl;
    }
    catch (const exception &ex)
    {
        cout << "general exception: " << ex.what() << endl;
    }
    catch (...)
    {
        cout << "unknown exception" << endl;
    }

    return 0;
}
