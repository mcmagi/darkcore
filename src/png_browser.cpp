/* sdl_browser.cpp */

#include <cstdlib>                      // exit
#include <exception>                    // std::exception
#include <iostream>                     // std::cout, std::endl
#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include "Exception.h"                  // Exception
#include "browser/SDLBrowser.h"         // SDLBrowser
#include "image/Image.h"                // Image
#include "image/png/PNGReader.h"        // PNGReader
#include "util/File.h"                  // File

using namespace std;
using darkcore::Exception;
using darkcore::browser::SDLBrowser;
using darkcore::image::Image;
using darkcore::image::png::PNGReader;
using darkcore::util::File;

const int WIDTH = 100;
const int HEIGHT = 100;

int main(int argc, char *argv[])
{
    // check arguments
    if (argc < 2)
    {
        // print error
        cout << "usage:" << endl;
        cout << " " << argv[0] << " <filename>" << endl;
        exit(1);
    }
    
    File file = string(argv[1]);

    try
    {
        // read image
        cout << "reading image from file " << file.getFullName() << endl;
        std::unique_ptr<ifstream> ifs = file.newInputStream();
        PNGReader reader(*ifs);
        unique_ptr<Image> img = reader.read();

        // begin the browsing experience
        cout << "starting SDL browser" << endl;
        SDLBrowser sb(*img);
        sb.browserMain();
    }
    catch (const Exception &ex)
    {
        cout << "dark core exception: " << ex.what() << endl;
    }
    catch (const exception &ex)
    {
        cout << "general exception: " << ex.what() << endl;
    }
    catch (...)
    {
        cout << "unknown exception" << endl;
    }

    return 0;
}
