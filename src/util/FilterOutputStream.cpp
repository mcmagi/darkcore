/* FilterOutputStream.h */

#include <memory>                       // std::unique_ptr, std::move
#include <ostream>                      // std::ostream
#include "util/FilterIOStream.h"        // FilterOutputStream, FilterStreamBuffer

using namespace std;
using darkcore::util::FilterOutputStream;
using darkcore::util::FilterStreamBuffer;

FilterOutputStream::FilterOutputStream(ostream &os) :
    ostream(new FilterStreamBuffer(os)), ownStreamBuf(true)
{
}

FilterOutputStream::FilterOutputStream(unique_ptr<ostream> os) :
    ostream(new FilterStreamBuffer(std::move(os))), ownStreamBuf(true)
{
}

FilterOutputStream::FilterOutputStream(FilterStreamBuffer *buf) :
    ostream(buf), ownStreamBuf(false)
{
}

FilterOutputStream::~FilterOutputStream()
{
    if (ownStreamBuf)
        delete rdbuf();
}
