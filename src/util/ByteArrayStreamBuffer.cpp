/* ByteArrayStreamBuffer.cpp */

#include <algorithm>                    // std::min
#include <cstring>                      // std::memcpy
#include <streambuf>                    // std::streambuf, std::streamsize
#include <vector>                       // std::vector
#include "util/ByteArrayIOStream.h"     // ByteArrayInputStream

using namespace std;
using darkcore::util::ByteArrayStreamBuffer;

ByteArrayStreamBuffer::ByteArrayStreamBuffer() :
    streambuf(), data(), inpos(0), outpos(0)
{
}

ByteArrayStreamBuffer::ByteArrayStreamBuffer(const unsigned char *data, int size) :
    streambuf(), data(data, data + size), inpos(0), outpos(0)
{
}

ByteArrayStreamBuffer::ByteArrayStreamBuffer(const vector<unsigned char> &data) :
    streambuf(), data(data), inpos(0), outpos(0)
{
}

ByteArrayStreamBuffer::~ByteArrayStreamBuffer()
{
}

streamsize ByteArrayStreamBuffer::showmanyc()
{
    return data.size() - inpos;
}

int ByteArrayStreamBuffer::pbackfail(int c)
{
    // return EOF if we're at the beginning
    if (inpos == 0)
        return EOF;

    // decrement array pointer
    --inpos;

    // modify stream only if not EOF
    if (c != EOF)
        data[inpos] = (unsigned char) c;

    return traits_type::not_eof(c);
}

int ByteArrayStreamBuffer::uflow()
{
    return inpos == data.size() ? EOF : data[inpos++];
}

int ByteArrayStreamBuffer::underflow()
{
    return inpos == data.size() ? EOF : data[inpos];
}

int ByteArrayStreamBuffer::overflow(int c)
{
    if (c != EOF)
        data.push_back(c);
    return traits_type::not_eof(c);
}

streamsize ByteArrayStreamBuffer::xsgetn(char *s, streamsize n)
{
    // determine how much to copy
    int copyamt = min(n, showmanyc());

    // do the copy
    memcpy(s, &data[inpos], copyamt);

    // increment position
    inpos += copyamt;

    // return actual amt read
    return copyamt;
}

streamsize ByteArrayStreamBuffer::xsputn(const char *s, streamsize n)
{
    data.insert(data.end(), s, s + n);
    return n;
}

streampos ByteArrayStreamBuffer::seekoff(streamoff off, ios_base::seekdir way, ios_base::openmode which)
{
    streampos rc = -1;

    if (which & ios_base::out)
    {
        // TODO: not implemented (yet)
        /* if (way == ios::cur)
            outpos += off;
        else if (way == ios::beg)
            outpos = off;
        else if (way == ios::end)
            outpos = data.size() - 1 - off; */
    }

    if (which & ios_base::in)
    {
        unsigned int newpos = inpos;
        if (way == ios::cur)
            newpos += off;
        else if (way == ios::beg)
            newpos = off;
        else if (way == ios::end)
            newpos = data.size() + off;

        if (newpos <= data.size())
            rc = inpos = newpos;
    }

    return rc;
}

streampos ByteArrayStreamBuffer::seekpos(streampos sp, ios_base::openmode which)
{
    return seekoff(sp, ios::beg, which);
}
