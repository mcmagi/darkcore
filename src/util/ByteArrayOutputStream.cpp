/* ByteArrayOutputStream.h */

#include <ostream>                      // std::ostream
#include "util/ByteArrayIOStream.h"     // ByteArrayInputStream

using namespace std;
using darkcore::util::ByteArrayOutputStream;

ByteArrayOutputStream::ByteArrayOutputStream() :
    ostream(&buf), buf()
{
}

ByteArrayOutputStream::~ByteArrayOutputStream()
{
}

void ByteArrayOutputStream::writeTo(ostream &stream)
{
    stream.write((char *) toArray(), getSize());
}
