/* FilterStreamBuffer.cpp */

#include <istream>                      // std::istream
#include <memory>                       // std::unique_ptr
#include <ostream>                      // std::ostream
#include <streambuf>                    // std::streambuf, std::streamsize
#include "util/FilterIOStream.h"        // FilterStreamBuffer

using namespace std;
using darkcore::util::FilterStreamBuffer;

FilterStreamBuffer::FilterStreamBuffer(istream &is) :
    streambuf(), is(&is), os(NULL), ownStream(false)
{
}

FilterStreamBuffer::FilterStreamBuffer(unique_ptr<istream> is) :
    streambuf(), is(is.release()), os(NULL), ownStream(true)
{
}

FilterStreamBuffer::FilterStreamBuffer(ostream &os) :
    streambuf(), is(NULL), os(&os), ownStream(false)
{
}

FilterStreamBuffer::FilterStreamBuffer(unique_ptr<ostream> os) :
    streambuf(), is(NULL), os(os.release()), ownStream(true)
{
}

FilterStreamBuffer::~FilterStreamBuffer()
{
    if (ownStream)
    {
        delete is;
        delete os;
    }
}

streamsize FilterStreamBuffer::showmanyc()
{
    return is->rdbuf()->in_avail();
}

int FilterStreamBuffer::pbackfail(int c)
{
    is->putback(c);
    return is->bad() ? traits_type::eof() : traits_type::not_eof(c);
}

int FilterStreamBuffer::underflow()
{
    return is->peek();
}

int FilterStreamBuffer::uflow()
{
    return is->get();
}

int FilterStreamBuffer::overflow(int c)
{
    os->put(c);
    return c;
}

streamsize FilterStreamBuffer::xsgetn(char *s, streamsize n)
{
    is->read(s, n);
    return is->gcount();
}

streamsize FilterStreamBuffer::xsputn(const char *s, streamsize n)
{
    os->write(s, n);
    return n;
}

streampos FilterStreamBuffer::seekoff(streamoff off, ios_base::seekdir way, ios_base::openmode which)
{
    streampos rc = -1;

    if (which & ios_base::out && os != NULL)
    {
        os->seekp(off, way);
        if (! os->fail())
            rc = os->tellp();
    }

    if (which & ios_base::in && is != NULL)
    {
        is->seekg(off, way);
        if (! is->fail())
            rc = is->tellg();
    }

    return rc;
}

streampos FilterStreamBuffer::seekpos(streampos sp, ios_base::openmode which)
{
    return seekoff(sp, ios::beg, which);
}
