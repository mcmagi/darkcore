/* FilterInputStream.cpp */

#include <istream>                      // std::istream
#include <memory>                       // std::unique_ptr, std::move
#include "util/FilterIOStream.h"        // FilterInputStream, FilterStreamBuffer

using namespace std;
using darkcore::util::FilterInputStream;
using darkcore::util::FilterStreamBuffer;

FilterInputStream::FilterInputStream(istream &is) :
    istream(new FilterStreamBuffer(is)), ownStreamBuf(true)
{
}

FilterInputStream::FilterInputStream(unique_ptr<istream> is) :
    istream(new FilterStreamBuffer(std::move(is))), ownStreamBuf(true)
{
}

FilterInputStream::FilterInputStream(FilterStreamBuffer *buf) :
    istream(buf), ownStreamBuf(false)
{
}

FilterInputStream::~FilterInputStream()
{
    if (ownStreamBuf)
        delete rdbuf();
}
