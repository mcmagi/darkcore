/* ToStringBuilder.cpp */

#include <string>                   // std::string
#include <sstream>                  // std::ostringstream
#include <vector>                   // std::vector
#include "Exception.h"              // UnimplementedException
#include "util/ToStringBuilder.h"   // ToStringBuilder

using namespace std;
using darkcore::UnimplementedException;
using darkcore::util::ToStringBuilder;

ToStringBuilder::ToStringBuilder(const string &classname) :
    ss(), firstTime(true)
{
    ss << classname << '{';
}

ToStringBuilder::ToStringBuilder(const ToStringBuilder &builder) :
    ss(), firstTime(true)
{
    throw UnimplementedException(__FUNCTION__);
}

ToStringBuilder::~ToStringBuilder()
{
}

ToStringBuilder & ToStringBuilder::operator=(const ToStringBuilder &builder)
{
    throw UnimplementedException(__FUNCTION__);
}

ToStringBuilder & ToStringBuilder::add(const string &param, const int &value)
{
    commaCheck();
    ss << param << '=' << value;
    return *this;
}

ToStringBuilder & ToStringBuilder::add(const string &param, const string &value)
{
    commaCheck();
    ss << param << '=' << value;
    return *this;
}

void ToStringBuilder::commaCheck()
{
    if (firstTime)
        firstTime = false;
    else
        ss << ',';
}

string ToStringBuilder::toString() const
{
    return ss.str() + '}';
}
