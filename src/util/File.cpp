/* File.cpp */

#define BOOST_FILESYSTEM_VERSION 3

#include <sys/stat.h>                   // stat
#include <fstream>                      // std::ifstream, std::ofstream, std::ios_base
#include <iostream>
#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include <boost/filesystem/path.hpp>    // boost:filesystem::path
#include <locale>
#include "Exception.h"                  // FileException
#include "util/File.h"                  // File

using namespace std;
using boost::filesystem::path;
using darkcore::FileException;
using darkcore::util::File;

File::File(const string &filename) :
    filename(filename)
{
}

File::File(const string &path, const string &filename) :
    filename(path)
{
    this->filename += string("/") += filename;
}

File::File(const File &file) :
    filename(file.filename)
{
}

File::~File()
{
}

File & File::operator=(const File &file)
{
    if (this != &file)
        filename = file.filename;
    return *this;
}

std::string File::getName() const
{
    path file = filename;
    return file.filename().string();
}

std::string File::getParentName() const
{
    path file = filename;
    return file.parent_path().string();
}

File File::getParent() const
{
    return File(getParentName());
}

bool File::exists() const
{
    // stat file
    struct stat filestat;
    return stat(filename.c_str(), &filestat) == 0;
}

unique_ptr<ifstream> File::newInputStream(ios_base::openmode mode) const
{
    cout << "File: creating new input stream for " << filename << endl;
    if (! exists())
        throw FileException("File does not exist for reading", filename);

    unique_ptr<ifstream> ifs(new ifstream(filename.c_str(), mode));

    if (! ifs->is_open())
        throw FileException("Could not open file for reading", filename);

    return ifs;
}

unique_ptr<ofstream> File::newOutputStream(ios_base::openmode mode) const
{
    unique_ptr<ofstream> ofs(new ofstream(filename.c_str(), mode));

    if (! ofs->is_open())
        throw FileException("Could not open file for writing", filename);

    return ofs;
}
