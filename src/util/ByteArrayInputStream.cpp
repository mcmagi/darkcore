/* ByteArrayInputStream.cpp */

#include <istream>                      // std::istream
#include <vector>                       // std::vector
#include "util/ByteArrayIOStream.h"     // ByteArrayInputStream

using std::istream;
using std::vector;
using darkcore::util::ByteArrayInputStream;

ByteArrayInputStream::ByteArrayInputStream(const unsigned char data[], int size)
    : istream(&buf), buf(data, size)
{
}

ByteArrayInputStream::ByteArrayInputStream(const vector<unsigned char> &data)
    : istream(&buf), buf(data)
{
}

ByteArrayInputStream::~ByteArrayInputStream()
{
}
