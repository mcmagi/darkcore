/* darkcore.cpp */

#include <cstdlib>                      // exit, atoi
#include <exception>                    // std::exception
#include <iostream>                     // std::cout, std::endl
#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include <vector>                       // std::vector
#include "Exception.h"                  // Exception
#include "command/Arguments.h"          // Arguments, ArgumentsFactory
#include "command/Command.h"            // Command, CommandFactory, UsageException
#include "command/ImportImage.h"        // ImportImage
#include "command/ImportMap.h"          // ImportMap
#include "command/ViewImage.h"          // ViewImage
#include "command/ViewMap.h"            // ViewMap

using namespace std;
using darkcore::Exception;
using darkcore::UnimplementedException;
using darkcore::command::Arguments;
using darkcore::command::ArgumentsFactory;
using darkcore::command::Command;
using darkcore::command::CommandFactory;
using darkcore::command::UsageException;
using darkcore::command::ImportImage;
using darkcore::command::ImportMap;
using darkcore::command::ViewImage;
using darkcore::command::ViewMap;

// prototypes
unique_ptr<CommandFactory<Command> > getFactory(const Arguments &args);

int main(int argc, char *argv[])
{
    int returnCode = 0;

    // parse command-line arguments
    ArgumentsFactory argFactory;
    argFactory.registerNestedCommand("image");
    argFactory.registerNestedCommand("map");
    Arguments args = argFactory.create(argc, argv);

    try
    {
        // get the command factory instance
        cout << "obtaining the CommandFactory: " << endl;
        unique_ptr<CommandFactory<Command> > factory = getFactory(args);

        // map arguments to the command
        cout << "obtaining the Command: " << endl;
        unique_ptr<Command> cmd = factory->create(args);

        // run the command
        cout << "running the Command: " << endl;
        cmd->run();
    }
    catch (const UsageException &ex)
    {
        cout << "usage:" << endl;
        cout << " " << argv[0] << " " << ex.getMessage() << endl;
        returnCode = 1;
    }
    catch (const Exception &ex)
    {
        cout << "dark core exception: " << ex.what() << endl;
        returnCode = 2;
    }
    catch (const exception &ex)
    {
        cout << "general exception: " << ex.what() << endl;
        returnCode = 3;
    }
    catch (...)
    {
        cout << "unknown exception" << endl;
        returnCode = 4;
    }

    return returnCode;
}

unique_ptr<CommandFactory<Command> > getFactory(const Arguments &args)
{
    unique_ptr<CommandFactory<Command> > factory;
    if (args.getCommand() == string("image"))
    {
        // do image processing stuff
        if (args.getSubCommand() == string("view"))
            factory.reset((CommandFactory<Command> *) new ViewImage::Factory());
        else if (args.getSubCommand() == string("import"))
            factory.reset((CommandFactory<Command> *) new ImportImage::Factory());
        else
            throw UsageException("image view|import <options>");
    }
    else if (args.getCommand() == string("map"))
    {
        if (args.getSubCommand() == string("view"))
            factory.reset((CommandFactory<Command> *) new ViewMap::Factory());
        else if (args.getSubCommand() == string("import"))
            factory.reset((CommandFactory<Command> *) new ImportMap::Factory());
        else
            throw UsageException("map view|import <options>");
    }
    else
        throw UsageException("image|map <options>");

    return factory;
}