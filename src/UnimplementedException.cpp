/* UnimplementedException.cpp */

#include <string>               // std::string
#include "Exception.h"          // Exception, UnimplementedException

using namespace std;
using darkcore::Exception;
using darkcore::UnimplementedException;

UnimplementedException::UnimplementedException(const string &errmsg) :
    Exception(errmsg)
{
}

UnimplementedException::UnimplementedException(const UnimplementedException &ex) :
    Exception(ex)
{
}

UnimplementedException::~UnimplementedException() throw ()
{
}
