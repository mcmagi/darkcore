/* Exception.cpp */

#include <exception>            // std::exception
#include <ios>                  // std::ios_base
#include <string>               // std::string
#include "Exception.h"          // Exception

using namespace std;
using darkcore::Exception;

Exception::Exception() :
    exception(), message(), cause(NULL)
{
}

Exception::Exception(const string &message) :
    exception(), message(message), cause(NULL)
{
}

Exception::Exception(const string &message, exception &cause) :
    exception(), message(message), cause(&cause)
{
}

Exception::Exception(const Exception &ex) :
    exception(ex), message(ex.message.str()), cause(ex.cause)
{
}

Exception & Exception::operator<<(const bool &msg)
{
    message << msg;
    return *this;
}

Exception & Exception::operator<<(const char &msg)
{
    message << msg;
    return *this;
}

Exception & Exception::operator<<(const char *msg)
{
    message << msg;
    return *this;
}

Exception & Exception::operator<<(const int &msg)
{
    message << msg;
    return *this;
}

Exception & Exception::operator<<(const unsigned int &msg)
{
    message << msg;
    return *this;
}

Exception & Exception::operator<<(const double &msg)
{
    message << msg;
    return *this;
}

Exception & Exception::operator<<(const string &msg)
{
    message << msg;
    return *this;
}

Exception & Exception::operator<<(ios_base & (*pf)(ios_base &))
{
    message << pf;
    return *this;
}

Exception::~Exception() throw ()
{
}

string Exception::getMessage() const throw ()
{
    return message.str();
}

const char *Exception::what() const throw()
{
    return getMessage().data();
}
