/* TextMapReader.cpp */

#include <iostream>                     // cout
#include <memory>                       // unique_ptr
#include "map/Map.h"                    // Map
#include "map/MapReader.h"              // MapReader
#include "map/TextIndexMapper.h"        // TextIndexMapper
#include "map/TextMapReader.h"          // TextMapReader
#include "util/File.h"                  // File

using namespace std;
using darkcore::map::Map;
using darkcore::map::MapReader;
using darkcore::map::TextMapReader;
using darkcore::util::File;

TextMapReader::TextMapReader(const File &file, const TextIndexMapper &mapper, int width, int height) :
    MapReader(), inputStream(file.newInputStream()), mapper(mapper), width(width), height(height)
{
    //cout << "created TextMapReader for file " << file.getFullName() << endl;
}

TextMapReader::~TextMapReader()
{
}

unique_ptr<Map> TextMapReader::read()
{
    int size = width * height;
    int *mapdata = new int[size];
    char c;

    // load all index data
    for (int k = 0; k < size && ! inputStream.get()->eof(); )
    {
        c = (char) inputStream.get()->get();
        if (! isWhitespace(c))
        {
            int idx = mapper.getTileIndex(c);
            //cout << "k=" << k << " c=" << c << " idx=" << idx << endl;
            mapdata[k++] = idx;
        }
    }

    // TODO: read directly into Map object
    unique_ptr<Map> m(new Map(width, height, mapdata));

    // cleanup mapdata array
    delete [] mapdata;

    isDone = true;
    //m->dump();
    return m;
}

bool TextMapReader::isWhitespace(char c)
{
    return c == ' ' || c == '\t' || c == '\n' || c == '\r';
}

bool TextMapReader::end() const
{
    return isDone;
}
