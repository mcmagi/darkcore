/* MapImageBuilder.cpp */

#include <algorithm>                // min
#include <memory>                   // std::unique_ptr
#include <vector>                   // std::vector
#include "Exception.h"              // UnimplementedException
#include "image/Image.h"            // Image
#include "image/ImageSet.h"         // ImageSet
#include "map/Map.h"                // Map
#include "map/MapImageBuilder.h"    // MapImageBuilder

using namespace std;
using darkcore::UnimplementedException;
using darkcore::image::Image;
using darkcore::image::ImageSet;
using darkcore::map::Map;
using darkcore::map::MapImageBuilder;

MapImageBuilder::MapImageBuilder(const Map &map, const ImageSet &tileSet) :
    map(map), tileSet(tileSet)
{
}

MapImageBuilder::MapImageBuilder(const MapImageBuilder &mib) :
    map(mib.map), tileSet(mib.tileSet)
{
}

MapImageBuilder::~MapImageBuilder()
{
}

MapImageBuilder & MapImageBuilder::operator=(const MapImageBuilder &mib)
{
    throw UnimplementedException(__FUNCTION__);
}

unique_ptr<Image> MapImageBuilder::createMapImage() const
{
    return createMapImage(0, 0, map.getWidth(), map.getHeight());
}

unique_ptr<Image> MapImageBuilder::createMapImage(int x, int y, int w, int h) const
{
    // constrain returned image by the edges of the map
    w = min(w, map.getWidth()-x);
    h = min(h, map.getHeight()-y);

    int numTiles = w * h;
    vector<const Image *> vect(numTiles);
    for (int i = 0, row = 0; row < h; row++)
    {
        for (int col = 0; col < w; col++)
            vect[i++] = &getImage(x+col, y+row);
    }

    return unique_ptr<Image>(new Image(vect, w));
}
