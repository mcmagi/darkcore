/* UltimaMapReader.cpp */

#include <iostream>                     // cout
#include <memory>                       // unique_ptr
#include <string>                       // string
#include <vector>                       // vector
#include "config/IndexConfig.h"         // IndexConfig
#include "config/IndexEntry.h"          // IndexEntry
#include "config/UltimaMapConfig.h"     // UltimaMapConfig
#include "map/Map.h"                    // Map
#include "map/MapReader.h"              // MapReader
#include "map/UltimaMapReader.h"        // UltimaMapReader
#include "util/Reader.h"                // Reader

using namespace std;
using darkcore::config::IndexConfig;
using darkcore::config::IndexEntry;
using darkcore::config::UltimaMapConfig;
using darkcore::map::Map;
using darkcore::map::MapReader;
using darkcore::map::UltimaMapReader;
using darkcore::util::Reader;

// hard-coding these for U5
const int UltimaMapReader::U5_WATER_TILE = 1;
const int UltimaMapReader::U5_WATER_INDEX = 255;

UltimaMapReader::UltimaMapReader(const UltimaMapConfig &cfg) :
    MapReader(), cfg(cfg), indexReader(cfg.getIndexConfig().newEntryReader()),
    fileChunks(0)
{
    //cout << "created UltimaMapReader for file " << cfg.getFilename() << endl;
}

UltimaMapReader::~UltimaMapReader()
{
    // cleanup
    if (cfg.isChunked())
    {
        for (int k = 0; k < cfg.getChunkConfig()->getNumMaps(); k++)
            delete fileChunks[k];

        if (cfg.isCompressedWaterChunk())
            delete fileChunks[U5_WATER_INDEX];
    }
}

unique_ptr<Map> UltimaMapReader::read()
{
    /* cout << "reading map entry: file=" << cfg.getFilename()
        << ",w=" << cfg.getWidth()
        << ",h=" << cfg.getHeight()
        << ",size=" << cfg.getSize()
        << ",chunked=" << cfg.isChunked()
        << endl; */

    //unsigned char *rawdata = new unsigned char[cfg.getRawSize()];

    // read raw data from file
    //cout << "reading map data of rawSize=" << cfg.getRawSize() << endl;
    //getInputStream().read((char *) rawdata, cfg.getRawSize());

    // unpack rawdata into map object
    unique_ptr<Map> map;
    if (cfg.isChunked())
        map = readChunkedMap();
    else
        map = readMap();

    // clean up
    //delete [] rawdata;

    return map;
}

unique_ptr<Map> UltimaMapReader::readMap()
{
    int *mapdata = new int[cfg.getSize()];

    // load all index data
    for (int k = 0; k < cfg.getSize(); k++)
    {
        IndexEntry entry = indexReader->read();
        mapdata[k] = entry.getOffset();
    }

    // TODO: read directly into Map object
    unique_ptr<Map> m(new Map(cfg.getWidth(), cfg.getHeight(), mapdata));

    // cleanup mapdata array
    delete [] mapdata;

    //m->dump();
    return m;
}

unique_ptr<Map> UltimaMapReader::readChunkedMap()
{
    const IndexConfig &idxCfg = cfg.getIndexConfig();

    // load and cache the file chunks
    if (fileChunks.empty())
        loadFileChunks();

    // arrange chunks into another vector by map index
    vector<Map *> mapChunks(idxCfg.getSize());
    for (int k = 0; k < idxCfg.getSize(); k++)
    {
        IndexEntry entry = indexReader->read();
        mapChunks[k] = fileChunks[entry.getOffset()];
        //cout << "read map index: k=" << k << ",idx=" << entry.getOffset() << endl;
    }

    /* cout << "constructing map from chunks: "
        << "mapW=" << cfg.getWidth()
        << ",chunkW=" << cfg.getChunkConfig()->getWidth()
        << ",numColumns=" << (cfg.getWidth() / cfg.getChunkConfig()->getWidth())
        << ",mapChunks.size=" << mapChunks.size()
        << endl; */

    return unique_ptr<Map>(new Map(mapChunks, cfg.getWidth() / cfg.getChunkConfig()->getWidth()));
}

void UltimaMapReader::loadFileChunks()
{
    const UltimaMapConfig *chunkCfg = cfg.getChunkConfig();

    UltimaMapReader chunkReader(*chunkCfg);

    cout << "reading " << chunkCfg->getNumMaps() << " chunks" << endl;

    // capture all chunks (which are really just sub-maps) into a vector
    fileChunks.resize(chunkCfg->getNumMaps());
    for (int k = 0; k < chunkCfg->getNumMaps(); k++)
        fileChunks[k] = chunkReader.read().release();

    // add water chunk for U5 if needed
    if (cfg.isCompressedWaterChunk())
    {
        int chunkSize = chunkCfg->getSize();
        int *chunkdata = new int[chunkSize];

        cout << "creating water chunk" << endl;
        fileChunks.resize(U5_WATER_INDEX + 1);
        for (int j = 0; j < chunkSize; j++)
            chunkdata[j] = U5_WATER_TILE; // U5's water tile
        fileChunks[U5_WATER_INDEX] = new Map(chunkCfg->getWidth(), chunkCfg->getHeight(), chunkdata);

        delete [] chunkdata;
    }
}

bool UltimaMapReader::end() const
{
    return indexReader->end();
}
