/* Map.cpp */

#include <cstring>                  // memcpy
#include <iostream>                 // std::cout
#include <vector>                   // std::vector
#include "Exception.h"              // Exception, IndexException
#include "image/ImageSet.h"         // ImageSet
#include "map/Map.h"                // Map
#include "map/MapImageBuilder.h"    // MapImageBuilder

using namespace std;
using darkcore::Exception;
using darkcore::IndexException;
using darkcore::image::ImageSet;
using darkcore::map::Map;
using darkcore::map::MapImageBuilder;

Map::Map(int w, int h) :
    width(w), height(h), size(w*h), mapData(new int[size])
{
    //cout << "creating empty map with dimensions w=" << w << ",h=" << h << endl;
}

Map::Map(int w, int h, int *data) :
    width(w), height(h), size(w*h), mapData(new int[size])
{
    //cout << "creating map with data and dimensions w=" << w << ",h=" << h << endl;

    // dumps the mapdata
    //dump();

    memcpy(mapData, data, sizeof(int) * size);
}

Map::Map(const vector<Map *> &chunks, int columns) :
    width(0), height(0), size(0), mapData(NULL)
{
    cout << "creating map from vector of " << chunks.size() << " chunks" << endl;

    if (columns < 1)
        throw Exception("Columns must be greater than 0");

    if (chunks.size() < 1)
        throw Exception("No chunks in vector to constuct a map from");

    // get number of rows (rounding up)
    int rows = chunks.size() / columns;
    if (chunks.size() % columns > 0)
        rows++;
    cout << "# of cols: " << columns << endl;
    cout << "# of rows: " << rows << endl;

    int chunkWidth = chunks[0]->getWidth();
    int chunkHeight = chunks[0]->getHeight();

    width = chunkWidth * columns;
    height = chunkHeight * rows;
    size = width * height;

    cout << "map width: " << width << endl;
    cout << "map height: " << height << endl;
    cout << "map size: " << size << endl;

    // now that we know the size, allocate the memory
    mapData = new int[size];

    // copy in each chunk to the map
    unsigned int k = 0;
    for (int r = 0; r < rows; r++)
    {
        for (int c = 0; c < columns && k < chunks.size(); c++)
        {
            int x = c * chunkWidth;
            int y = r * chunkHeight;
            const Map *m = chunks[k++];

            //cout << "x=" << x << ",y=" << y << ",k=" << (k-1) << endl;
            //m->dump();

            // memcpy each row of the chunk into this map
            for (int i = 0; i < chunkHeight; i++)
                memcpy(mapData + calculateIndex(x, y+i), m->mapData + (i*chunkWidth), chunkWidth * sizeof(int));
        }
    }

    //dump();
}

Map::Map(const Map &map) :
    width(map.width), height(map.height), size(map.size), mapData(new int[map.size])
{
    memcpy(mapData, map.mapData, size * sizeof(int));
}

Map::~Map()
{
    delete [] mapData;
}

Map & Map::operator=(const Map &map)
{
    if (this != &map)
    {
        bool newSize = (map.size == size);

        width = map.width;
        height = map.height;

        // resize array if necessary
        if (newSize)
        {
            size = map.size;
            delete [] mapData;
            mapData = new int[size];
        }

        memcpy(mapData, map.mapData, size * sizeof(int));
    }
    return *this;
}

MapImageBuilder Map::getMapImageBuilder(const ImageSet &tileSet) const
{
    return MapImageBuilder(*this, tileSet);
}

int Map::calculateIndex(int x, int y) const
{
    if (x < 0 || x >= width)
        throw new IndexException("Requested column position exceeds map width", 0, width, x);
    if (y < 0 || y >= height)
        throw new IndexException("Requested row position exceeds map height", 0, height, y);

    return y * width + x;
}

void Map::dump() const
{
    cout << "mapData=[" << endl;
    for (int i = 0, k = 0; i < height; i++)
    {
        cout << "  [";
        for (int j = 0; j < width; j++)
            cout << mapData[k++] << ",";
        cout << "]," << endl;
    }
    cout << "]" << endl;
}
