/* TextIndexMapper.cpp */

#include <exception>                    // exception
#include <map>                          // map
#include <sstream>                      // ostringstream
#include "Exception.h"                  // UnimplementedException
#include "config/UltimaMapConfig.h"     // UltimaMapConfig
#include "map/TextIndexMapper.h"        // TextIndexMapper

using namespace std;
using darkcore::Exception;
using darkcore::UnimplementedException;
using darkcore::config::UltimaMapConfig;
using darkcore::map::TextIndexMapper;

// ultima 2 (overworld)
const TextIndexMapper TextIndexMapper::U2_MAPPER = TextIndexMapper()
    .add('~', 0) // water
    .add('*', 1) // swamp
    .add('.', 2) // grass
    .add('O', 3) // forest
    .add('^', 4) // mountain
    .add('V', 5) // village
    .add('T', 6) // town
    .add('W', 7) // tower
    .add('C', 8) // castle
    .add('D', 9) // dungeon
    .add('S', 10); // sign

// ultima 2 (town)
const TextIndexMapper TextIndexMapper::U2_TOWN_MAPPER = TextIndexMapper()
    .add('~', 0) // water
    .add('*', 1) // swamp
    .add('.', 2) // grass
    .add('O', 3) // forest
    .add('^', 4) // mountain
    .add('h', 17) // horse
    .add('f', 18) // frigate
    .add('p', 19) // plane
    .add('r', 20) // rocket
    .add('$', 21) // shield
    .add('/', 22) // sword
    .add('!', 23) // force
    .add('#', 28) // brick
    .add('-', 30) // wall
    .add('_', 31) // wall-blank
    .add('A', 32)
    .add('B', 33)
    .add('C', 34)
    .add('D', 35)
    .add('E', 36)
    .add('F', 37)
    .add('G', 38)
    .add('H', 39)
    .add('I', 40)
    .add('J', 41)
    .add('K', 42)
    .add('L', 43)
    .add('M', 44)
    .add('N', 45)
    .add('o', 46)
    .add('P', 47)
    .add('R', 49)
    .add('S', 50)
    .add('T', 51)
    .add('U', 52)
    .add('V', 53)
    .add('W', 54)
    .add('X', 55)
    .add('Y', 56)
    .add('Z', 57);

// ultima 3 (overworld)
const TextIndexMapper TextIndexMapper::U3_MAPPER = TextIndexMapper()
    .add('~', 0) // water
    .add('.', 1) // grass
    .add('o', 2) // brush
    .add('O', 3) // forest
    .add('^', 4) // mountain
    .add('D', 5)
    .add('T', 6) // town
    .add('C', 7) // castle
    .add('f', 11) // frigate
    .add('@', 12) // whirlpool
    .add('!', 32) // force
    .add('%', 33) // lava
    .add('{', 58) // snake tail
    .add('}', 59); // snake head


TextIndexMapper::TextIndexMapper() :
    textToIndexMap(), indexToTextMap()
{
}

TextIndexMapper::~TextIndexMapper()
{
}

TextIndexMapper & TextIndexMapper::add(char textValue, int intValue)
{
    textToIndexMap[textValue] = intValue;
    indexToTextMap[intValue] = textValue;
    return *this;
}

int TextIndexMapper::getTileIndex(char textValue) const
{
	try {
    	return textToIndexMap.at(textValue);
	} catch (exception) {
    	ostringstream stream;
    	stream << "No tile mapping for text value = " << textValue;
		throw Exception(stream.str());
	}
}

char TextIndexMapper::getTextValue(int intValue) const
{
	try {
    	return textToIndexMap.at(intValue);
	} catch (exception) {
    	ostringstream stream;
    	stream << "No tile mapping for int value = " << intValue;
		throw Exception(stream.str());
	}
}

const TextIndexMapper & TextIndexMapper::forConfig(const UltimaMapConfig &mapCfg)
{
    int game = mapCfg.getGameConfig().getGame();
    if (game == 2)
        return TextIndexMapper::U2_MAPPER; // TODO: determine if town map
    if (game == 3)
        return TextIndexMapper::U3_MAPPER;
    throw UnimplementedException("no text index mapper for game");
}
