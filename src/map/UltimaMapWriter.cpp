/* UltimaMapWriter.cpp */

#include <iostream>                     // cout
#include <memory>                       // unique_ptr
#include "config/IndexEntry.h"          // IndexEntry
#include "config/UltimaMapConfig.h"     // UltimaMapConfig
#include "map/Map.h"                    // Map
#include "map/MapWriter.h"              // MapWriter
#include "map/UltimaMapWriter.h"        // UltimaMapWriter
#include "util/Writer.h"                // Writer

using namespace std;
using darkcore::config::IndexEntry;
using darkcore::config::UltimaMapConfig;
using darkcore::map::Map;
using darkcore::map::MapWriter;
using darkcore::map::UltimaMapWriter;
using darkcore::util::Writer;

UltimaMapWriter::UltimaMapWriter(const UltimaMapConfig &cfg) :
    MapWriter(), cfg(cfg), indexWriter(cfg.getIndexConfig().newEntryWriter())
{
    //cout << "created UltimaMapWriter for file " << cfg.getFilename() << endl;
}

UltimaMapWriter::~UltimaMapWriter()
{
}

void UltimaMapWriter::write(const Map &map)
{
    /* cout << "writing map entry: file=" << cfg.getFilename()
        << ",w=" << cfg.getWidth()
        << ",h=" << cfg.getHeight()
        << ",size=" << cfg.getSize()
        << ",chunked=" << cfg.isChunked()
        << endl; */

    // unpack rawdata into map object
    if (cfg.isChunked())
        throw Exception("UltimaMapWriter does not yet support chunked maps");

    for (int y = 0; y < map.getHeight(); y++)
    {
        for (int x = 0; x < map.getWidth(); x++)
        {
            int idx = map.getImageId(x, y);
            IndexEntry indexEntry(idx, IndexEntry::MaskType::NORMAL);
            indexWriter.get()->write(indexEntry);
        }
    }

    // TODO: this is a hack and doesn't feel right at this level; find the right place to put this
    if (cfg.getGameConfig().getGame() == 3)
    {
        // write talk data
        IndexEntry empty(0, IndexEntry::MaskType::NORMAL); // hack b/c IndexEntry is wrong level of abstraction
        for (int i = 0; i < 384; i++)
            indexWriter.get()->write(empty);

        // write npc data
        for (int i = 0; i < 160; i++)
            indexWriter.get()->write(empty);

        // write whirlpool coords
        IndexEntry whirlX(0, IndexEntry::MaskType::NORMAL);
        IndexEntry whirlY(0, IndexEntry::MaskType::NORMAL);
        IndexEntry whirlCountX(0x03, IndexEntry::MaskType::NORMAL); // should be 0x0c
        IndexEntry whirlCountY(0x01, IndexEntry::MaskType::NORMAL); // should be 0x04
        indexWriter.get()->write(whirlX);
        indexWriter.get()->write(whirlY);
        indexWriter.get()->write(whirlCountX);
        indexWriter.get()->write(whirlCountY);

        // write moon state
        IndexEntry moon1(0, IndexEntry::MaskType::NORMAL);
        IndexEntry moon2(0, IndexEntry::MaskType::NORMAL);
        IndexEntry moonCount1(0x03, IndexEntry::MaskType::NORMAL); // should be 0x0c
        IndexEntry moonCount2(0x01, IndexEntry::MaskType::NORMAL); // should be 0x04
        indexWriter.get()->write(moon1);
        indexWriter.get()->write(moon2);
        indexWriter.get()->write(moonCount1);
        indexWriter.get()->write(moonCount2);
    }
}