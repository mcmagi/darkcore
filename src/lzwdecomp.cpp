/* lzwdecomp.cpp */

#include <cstdlib>                      // exit, atoi
#include <exception>                    // std::exception
#include <fstream>                      // std::ifstream, std::ofstream
#include <iostream>                     // std::cout, std::endl
#include <memory>                       // std::unique_ptr, std::move
#include <string>                       // std::string
#include <vector>                       // std::vector
#include "Exception.h"                  // Exception
#include "lzw/LZWIOStream.h"            // LZWInputStream, LZWStreamBuffer
#include "util/File.h"                  // File

using namespace std;
using darkcore::Exception;
using darkcore::lzw::LZWInputStream;
using darkcore::lzw::LZWStreamBuffer;
using darkcore::util::File;

int main(int argc, char *argv[])
{
    // check arguments
    if (argc < 3)
    {
        // print error
        cout << "usage:" << endl;
        cout << " " << argv[0] << " <source> <target>" << endl;
        exit(1);
    }

    File sourceFile = string(argv[1]);
    File targetFile = string(argv[2]);

    try
    {
        unique_ptr<ifstream> source = sourceFile.newInputStream(ios::in | ios::binary);
        LZWInputStream lzwsource(static_cast<unique_ptr<istream> >(std::move(source)));

        unique_ptr<ofstream> target = targetFile.newOutputStream(ios::out | ios::binary);

        int outputSize = lzwsource.getDecompressedSize();
        vector<char> data(outputSize);

        // read from source stream
        lzwsource.read(&data[0], outputSize);

        // write to target stream
        target->write(&data[0], outputSize);
    }
    catch (const Exception &ex)
    {
        cout << "dark core exception: " << ex.what() << endl;
    }
    catch (const exception &ex)
    {
        cout << "general exception: " << ex.what() << endl;
    }
    catch (...)
    {
        cout << "unknown exception" << endl;
    }

    return 0;
}
