/* TileBrowser.cpp */

#include <iostream>                     // std::cout, std::cin, std::cerr, std::endl
#include <string>                       // std::string
#include "Exception.h"                  // IndexException, FileException
#include "browser/AbstractTileBrowser.h"   // AbstractTileBrowser
#include "browser/ImageBrowser.h"       // ImageBrowser
#include "browser/ImageDisplay.h"       // ImageDisplay
#include "image/Color.h"                // Pixel
#include "image/Image.h"                // Image
#include "image/png/PNGException.h"     // PNGException
#include "image/png/PNGWriter.h"        // PNGWriter
#include "util/File.h"                  // File

using namespace std;
using darkcore::FileException;
using darkcore::IndexException;
using darkcore::browser::AbstractTileBrowser;
using darkcore::browser::ImageBrowser;
using darkcore::browser::ImageDisplay;
using darkcore::image::Image;
using darkcore::image::Pixel;
using darkcore::image::png::PNGException;
using darkcore::image::png::PNGWriter;
using darkcore::util::File;

ImageBrowser::ImageBrowser(Image &img, const ImageDisplay &id) :
    AbstractTileBrowser(id), img(img)
{
}

void ImageBrowser::saveTileSet(const string &filename)
{
    // TODO: save changes to the image - no need for this yet
}

void ImageBrowser::display() const
{
    id.display(img);
}

void ImageBrowser::exportImage() const
{
    try
    {
        string filename = "img-export.png";
        cout << "Exporting image to " << filename << endl;

        File file(filename);
        unique_ptr<ostream> os = file.newOutputStream();

        PNGWriter pngw(*os);
        pngw.write(img);
    }
    catch (PNGException pe)
    {
        cout << pe.what();
    }
    catch (FileException fe)
    {
        cout << fe.what();
    }
}