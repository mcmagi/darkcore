/* TileBrowser.cpp */

#include <cmath>                        // std::pow
#include <iostream>                     // std::cout, std::cin, std::cerr, std::endl
#include <string>                       // std::string
#include "Exception.h"                  // IndexException, FileException
#include "browser/AbstractTileBrowser.h"   // AbstractTileBrowser
#include "browser/ImageDisplay.h"       // ImageDisplay
#include "browser/TileBrowser.h"        // TileBrowser
#include "config/UltimaGameConfig.h"    // UltimaGame
#include "config/UltimaImageConfig.h"   // UltimaImageConfig
#include "image/Color.h"                // Pixel
#include "image/Image.h"                // Image
#include "image/ImageSet.h"             // ImageSet
#include "image/UltimaImageWriter.h"    // UltimaImageWriter
#include "image/png/PNGException.h"     // PNGException
#include "image/png/PNGWriter.h"        // PNGWriter
#include "util/File.h"                  // File

using namespace std;
using darkcore::FileException;
using darkcore::IndexException;
using darkcore::browser::AbstractTileBrowser;
using darkcore::browser::ImageDisplay;
using darkcore::browser::TileBrowser;
using darkcore::config::UltimaGame;
using darkcore::config::UltimaImageConfig;
using darkcore::image::Image;
using darkcore::image::ImageSet;
using darkcore::image::Pixel;
using darkcore::image::UltimaImageWriter;
using darkcore::image::png::PNGException;
using darkcore::image::png::PNGWriter;
using darkcore::util::File;

TileBrowser::TileBrowser(ImageSet &ts, const ImageDisplay &id, const UltimaImageConfig &imgCfg) :
    AbstractTileBrowser(id), ts(ts), imgCfg(imgCfg), tile(0)
{
}

bool TileBrowser::nextTile()
{
    bool success = true;

    // advance to next tile only if it's in boundary
    if (tile == ts.getNumImages() - 1)
    {
        cout << "Already at end!" << endl;
        success = false;
    }
    else
        tile++;

    return success;
}

bool TileBrowser::prevTile()
{
    bool success = true;

    // previous tile
    if (tile == 0)
    {
        cout << "Already at beginning!" << endl;
        success = false;
    }
    else
        tile--;

    return success;
}

bool TileBrowser::firstTile()
{
    // beginning of tileset
    tile = 0;

    return true;
}

bool TileBrowser::lastTile()
{
    // end of tileset
    tile = ts.getNumImages() - 1;

    return true;
}

bool TileBrowser::gotoTile(int i)
{
    bool success = true;

    if (i >= ts.getNumImages() || i < 0)
    {
        cout << "Invalid tile!  Please specify a number between 0 and "
            << ts.getNumImages() - 1 << endl;
        success = false;
    }
    else
        tile = i;

    return success;
}

bool TileBrowser::setColor(int x, int y, int c)
{
    bool success = true;

    try
    {
        if (c >= (int) pow((float) 2, getColorDepth()) || c < 0)
        {
            cout << "Invalid color!" << endl;
            success = false;
        }
        else
            ts.getImage(tile).setPixel(x, y, c);
    }
    catch (IndexException ie)
    {
        cout << ie.what() << endl;
        success = false;
    }

    return success;
}

bool TileBrowser::changeColor(Pixel old_c, Pixel new_c)
{
    bool success = true;
    int x, y;
    Image &img = ts.getImage(tile);

    if (new_c >= (unsigned int) pow((float) 2, getColorDepth()) || new_c < 0)
    {
        cout << "Invalid color!" << endl;
        success = false;
    }
    else
        // change color
        for (y = 0; y < img.getHeight(); y++)
            for (x = 0; x < img.getWidth(); x++)
                if (img.getPixel(x, y) == old_c)
                    img.setPixel(x, y, new_c);

    return success;
}

void TileBrowser::saveTileSet(const string &filename)
{
    try
    {
        //UltimaImageWriter uiw(filename, imgCfg);
        UltimaImageWriter uiw(imgCfg);
        uiw.writeAll(ts);
    }
    catch (FileException fe)
    {
        cout << fe.what();
    }
}

void TileBrowser::display() const
{
    if (ts.getNumImages() == 0)
        cout << "Empty tileset" << endl;
    else
        // display current image
        id.display(ts.getImage(tile));
}

void TileBrowser::exportImage() const
{
    try
    {
        string filename = imgCfg.getFileConfig().getFile().getFullName() + string(".png");
        cout << "Exporting tileset to " << filename << endl;

        File file(filename);
        unique_ptr<ostream> os = file.newOutputStream();

        PNGWriter pngw(*os);
        Image img(ts, calculateMergedImageWidth(imgCfg));
        pngw.write(img);
    }
    catch (PNGException pe)
    {
        cout << pe.what();
    }
    catch (FileException fe)
    {
        cout << fe.what();
    }
}

int TileBrowser::calculateMergedImageWidth(const UltimaImageConfig &imgCfg)
{
    // determine exported image dimensions
    //  (we div by 100 then mul by 10 to exploit floor() rounding in
    //   integer division; so 200 <= x < 300 is 20 tiles across)
    int tileX = imgCfg.getNumImages() / 100 * 10;

    // less than 100 images is 10 tiles across
    if (tileX == 0)
        tileX = 10;

    // U3's ANIMATE.* always gets 1 tile across
    if (imgCfg.getGameConfig().getGame() == UltimaGame::ULTIMA3 &&
        imgCfg.getWidth() == 92 && imgCfg.getHeight() == 16)
        tileX = 1;

    // limit by number of images in imageset
    return tileX = min(imgCfg.getNumImages(), tileX);
}