/* SDLBrowser.cpp */

#include "SDL/SDL.h"                    // SDL_PollEvent, SDL_Event, SDL_Init, SDL_Quit
#include <cstdlib>                      // std::atexit
#include <iostream>                     // std::cout, std::endl
#include <string>                       // std::string
#include "browser/AbstractTileBrowser.h"    // AbstractTileBrowser
#include "browser/ImageBrowser.h"       // ImageBrowser
#include "browser/SDLBrowser.h"         // SDLBrowser
#include "browser/TileBrowser.h"        // TileBrowser
#include "config/UltimaImageConfig.h"   // UltimaImageConfig
#include "image/Image.h"                // Image
#include "image/ImageSet.h"             // ImageSet
#include "Exception.h"                  // IndexException

using namespace std;
using darkcore::IndexException;
using darkcore::browser::AbstractTileBrowser;
using darkcore::browser::ImageBrowser;
using darkcore::browser::SDLBrowser;
using darkcore::browser::TileBrowser;
using darkcore::config::UltimaImageConfig;
using darkcore::image::Image;
using darkcore::image::ImageSet;

SDLBrowser::SDLBrowser(ImageSet &ts, const UltimaImageConfig &imgCfg) :
    ids(imgCfg.getWidth() * 2, imgCfg.getHeight() * 2), tb(new TileBrowser(ts, ids, imgCfg))
{
    SDL_Init(SDL_INIT_VIDEO);
    atexit(SDL_Quit);
}

SDLBrowser::SDLBrowser(Image &img) :
    ids(img.getWidth() * 2, img.getHeight() * 2), tb(new ImageBrowser(img, ids))
{
    SDL_Init(SDL_INIT_VIDEO);
    atexit(SDL_Quit);
}

void SDLBrowser::browserMain()
{
    //string action;              // next action
    //string tilenum;             // number of tile to go to
    //string row, col;            // row and column entries
    //string oldcolor, newcolor;  // for setting old and new colors
    //string filename;            // name of output file for writing

    SDL_Event event;
    display_tile = true;

    // begin ui loop
    do
    {
        if (display_tile)
        {
            // display tile
            cout << "Tile #" << tb->getTileNum() << endl; // to stdout for debugging
            //ts[i].print_size();
            tb->display();
            display_tile = false;
        }

        try
        {
            if (SDL_WaitEvent(&event) != 0)
            {
                if (event.type == SDL_QUIT)
                    break;
                else if (event.type == SDL_KEYDOWN)
                {
                    if (event.key.keysym.sym == SDLK_n)
                        display_tile = tb->nextTile();
                    else if (event.key.keysym.sym == SDLK_p)
                        display_tile = tb->prevTile();
                    else if (event.key.keysym.sym == SDLK_b)
                        display_tile = tb->firstTile();
                    else if (event.key.keysym.sym == SDLK_e)
                        display_tile = tb->lastTile();
                    else if (event.key.keysym.sym == SDLK_z)
                    {
                        if (ids.getZoom() < ImageDisplaySDL::MAX_ZOOM)
                        {
                            ids.setZoom(ids.getZoom() + 1);
                            display_tile = true;
                        }
                    }
                    else if (event.key.keysym.sym == SDLK_a)
                    {
                        if (ids.getZoom() > ImageDisplaySDL::MIN_ZOOM)
                        {
                            ids.setZoom(ids.getZoom() - 1);
                            display_tile = true;
                        }
                    }
                    else if (event.key.keysym.sym == SDLK_c)
					{
						// toggle cga composite mode (if cga)
						ids.setCgaComposite(! ids.isCgaComposite());
						display_tile = true;
            			cout << "CGA Composite=" << ids.isCgaComposite() << endl;
					}
                    else if (event.key.keysym.sym == SDLK_x)
                    {
                        tb->exportImage();
                    }
                    else if (event.key.keysym.sym == SDLK_q)
                        break;
                }
            }
        }
        catch (const IndexException &ie)
        {
            // do not display tile when exception occurs
            display_tile = false;
        }

    } while (true);
}
