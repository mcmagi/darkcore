/* ImageDisplayAscii.cpp */

#include <iostream>                     // std::cerr, std::cout, std::endl
#include <string>                       // std::string
#include "browser/ImageDisplay.h"       // ImageDisplay
#include "browser/ImageDisplayAscii.h"  // ImageDisplayAscii
#include "image/Color.h"                // Color, Pixel
#include "image/Image.h"                // Image
#include "image/ImageFormat.h"          // ImageFormat, IndexedImageFormat

using namespace std;
using darkcore::browser::ImageDisplay;
using darkcore::browser::ImageDisplayAscii;
using darkcore::image::Color;
using darkcore::image::Image;
using darkcore::image::ImageFormat;
using darkcore::image::IndexedImageFormat;
using darkcore::image::Pixel;


ImageDisplayAscii::ImageDisplayAscii() :
    ImageDisplay()
{
}

ImageDisplayAscii::~ImageDisplayAscii()
{
}

void ImageDisplayAscii::display(const Image &img) const
{
    if (img.getImageFormat().getFormatType() == ImageFormat::RGB)
    {
        cerr << "RGB image formats unsupported in Ascii mode" << endl;
        return;
    }
    else if (img.getImageFormat().getColorDepth() == 8)
    {
        cerr << "VGA graphic unsupported in Ascii mode" << endl;
        return;
    }

    const IndexedImageFormat &iif = dynamic_cast<const IndexedImageFormat &>(img.getImageFormat());

    // loop for each row
    for (int y = 0; y < img.getHeight(); y++)
    {
        // loop for each column
        for (int x = 0; x < img.getWidth(); x++)
        {
            // get color of pixel at x,y
            Pixel pixel_color = img.getPixel(x, y);

            Color c = iif.getColorPalette().getColor((int) pixel_color);

            printAscii(getAsciiColorCode(c), PIXEL_CHAR);
        }

        // reset for newline
        printAscii("00", '\n');
    }
}


/*
 * Private Methods
 */

void ImageDisplayAscii::printAscii(const string &colorCode, char c) const
{
    cout << "\033[" << colorCode << "m" << c;
}

string ImageDisplayAscii::getAsciiColorCode(const Color &c) const
{
    string s;
    if (c == Color::BLACK)
        s = "00;30";
    else if (c == Color::BLUE)
        s = "00;34";
    else if (c == Color::GREEN)
        s = "00;32";
    else if (c == Color::CYAN)
        s = "00;36";
    else if (c == Color::RED)
        s = "00;31";
    else if (c == Color::MAGENTA)
        s = "00;35";
    else if (c == Color::BROWN)
        s = "00;33";
    else if (c == Color::GRAY)
        s = "00;37";
    else if (c == Color::DK_GRAY)
        s = "01;30";
    else if (c == Color::LT_BLUE)
        s = "01;34";
    else if (c == Color::LT_GREEN)
        s = "01;32";
    else if (c == Color::LT_CYAN)
        s = "01;36";
    else if (c == Color::LT_RED)
        s = "01;31";
    else if (c == Color::LT_MAGENTA)
        s = "01;35";
    else if (c == Color::YELLOW)
        s = "01;33";
    else if (c == Color::WHITE)
        s = "01;37";
    else
    {
        cerr << "Warning: color out of bounds: " << c << endl;
        s = getAsciiColorCode(Color::BLACK);
    }

    return s;
}
