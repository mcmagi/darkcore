/* AsciiBrowser.cpp */

#include <cstdlib>                      // std::atoi
#include <iostream>                     // std::cin, std::cout, std::endl
#include <string>                       // std::string
#include "browser/AsciiBrowser.h"       // AsciiBrowser
#include "config/UltimaImageConfig.h"   // UltimaImageConfig
#include "image/ImageSet.h"             // ImageSet
#include "Exception.h"                  // IndexException

using namespace std;
using darkcore::IndexException;
using darkcore::browser::AsciiBrowser;
using darkcore::config::UltimaImageConfig;
using darkcore::image::ImageSet;

AsciiBrowser::AsciiBrowser(ImageSet &ts, const UltimaImageConfig &imgCfg) :
    ida(), tb(ts, ida, imgCfg)
{
}

void AsciiBrowser::browserMain()
{
    string action;              // next action
    string tilenum;             // number of tile to go to
    string row, col;            // row and column entries
    string oldcolor, newcolor;  // for setting old and new colors
    string filename;            // name of output file for writing

    display_tile = true;

    // begin ui loop
    do
    {
        if (display_tile)
        {
            // display tile
            cout << "Tile #" << tb.getTileNum() << "\n";
            //ts[i].print_size();
            tb.display();
        }

        // set display tile to true again
        display_tile = true;

        // read in action
        cout << "Action (? for help): ";
        cin >> action;

        try
        {
            if (action == "?")
                printHelp();
            else if (action == "d")
                ; // do nothing (just display tile)
            else if (action == "n")
                display_tile = tb.nextTile();
            else if (action == "p")
                display_tile = tb.prevTile();
            else if (action == "b")
                display_tile = tb.firstTile();
            else if (action == "e")
                display_tile = tb.lastTile();
            else if (action == "g")
            {
                // goto # in tileset

                // read in tile num
                cout << "Tile to go to: ";
                cin >> tilenum;

                display_tile = tb.gotoTile(atoi(tilenum.c_str()));
            }
            else if (action == "s")
            {
                // set a single pixel color

                // read in pixel coord color
                cout << "Pixel row to change: ";
                cin >> row;

                // read in pixel coord color
                cout << "Pixel col to change: ";
                cin >> col;

                // read in ega color
                if (tb.getColorDepth() == 4)
                    cout << "new EGA Color (0-15): ";
                else if (tb.getColorDepth() == 2)
                    cout << "new CGA Color (0-3): ";
                cin >> newcolor;

                display_tile = tb.setColor(atoi(row.c_str()),
                    atoi(col.c_str()), atoi(newcolor.c_str()));
            }
            /*
            else if (action == "s2")
            {
                // set rectangle
                cout << "Setting Color for Rectangle\n";

                cout << "Starting Row: ";
                cin >> ans;
                start_row = ans;
                cout << "Ending Row: ";
                cin >> ans;
                end_row = ans;

                cout << "Starting Column: ";
                cin >> ans;
                start_col = ans;
                cout << "Ending Column: ";
                cin >> ans;
                end_col = ans;

                // read in ega color
                cout << "new EGA Color (0-15): ";
                cin >> ans;
                newcolor = ans;

                for (int y = atoi(start_row); y <= atoi(end_row); y++)
                    for (int x = atoi(start_col); x <= atoi(end_col); x++)
                        ts->getImage(tile).putColor(y, x, atoi(newcolor));
            }
            */
            else if (action == "c")
            {
                // change color

                // read in old color
                if (tb.getColorDepth() == 4)
                    cout << "Color to change (0-15): ";
                else if (tb.getColorDepth() == 2)
                    cout << "Color to change (0-3): ";
                cin >> oldcolor;

                // read in new color
                if (tb.getColorDepth() == 4)
                    cout << "new EGA Color (0-15): ";
                else if (tb.getColorDepth() == 2)
                    cout << "new CGA Color (0-3): ";
                cin >> newcolor;

                display_tile = tb.changeColor(atoi(oldcolor.c_str()),
                    atoi(newcolor.c_str()));
            }
            /*
            else if (action == "c2")
            {
                // change color in a region
                cout << "Changing Color for Region\n";

                // read in cga color
                cout << "Starting Row: ";
                cin >> ans;
                start_row = ans;
                cout << "Ending Row: ";
                cin >> ans;
                end_row = ans;

                cout << "Starting Column: ";
                cin >> ans;
                start_col = ans;
                cout << "Ending Column: ";
                cin >> ans;
                end_col = ans;

                cout << "Color to change (0-15): ";
                cin >> ans;
                oldcolor = ans;
                cout << "new EGA Color (0-15): ";
                cin >> ans;
                newcolor = ans;

                for (int y = atoi(start_row); y <= atoi(end_row); y++)
                {
                    for (int x = atoi(start_col); x <= atoi(end_col); x++)
                    {
                        if (tile[i].getColor(y, x) == atoi(oldcolor))
                            tile[i].putColor(y, x, atoi(newcolor));
                    }
                }
            }
            */
            else if (action == "w")
            {
                // write file

                do
                {
                    // read in output file
                    cout << "Name of output file: ";
                    cin >> filename;
            
                    // check if non-null string was specified
                } while (filename.length() == 0);

                // write it out
                tb.saveTileSet(filename);

                display_tile = false;
            }
            /*
            else if (action == (String) "x")
            {
                // display binary
                tile[i].displayRawHex();

                display_tile = false;
            }
            else if (action == (String) "z")
            {
                // display hex
                tile[i].displayRawBin();

                display_tile = false;
            }
            */
            else if (action != "q")
            {
                // all else
                cout << "Invalid command!" << endl;
                display_tile = false;
            }
        }
        catch (const IndexException &ie)
        {
            // print exception info
            cout << ie.what() << "  Invalid position " << ie.getPassedIndex()
                << "!  Please enter a number between " << ie.getLowerBound()
                << " and " << ie.getUpperBound() << "." << endl;

            // do not display tile when exception occurs
            display_tile = false;
        }

        cout << endl;

    } while (action != "q");    // quit
}

void AsciiBrowser::printHelp()
{
    cout << "n   - next tile\n"
        << "p   - previous tile\n"
        << "b   - beginning of tile set\n"
        << "e   - end of tile set\n"
        << "g   - goto tile #\n"
        << "d   - display tile\n"
        << "c   - change color (entire color)\n"
//      << "c2  - change color (specify bounds)\n"
        << "s   - set (entire row/col)\n"
//      << "s2  - set color (specify bounds)\n"
        << "q   - quit\n"
        << "w   - write to file\n"
//      << "x   - display raw hex\n"
//      << "z   - display raw bin\n"
        << "?   - this message\n";

    display_tile = false;
}
