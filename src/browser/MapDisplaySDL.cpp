/* ImageDisplaySDL.cpp */

#include <cstring>                      // memcpy
#include <string>                       // std::string
#include "SDL/SDL.h"                    // SDL_Surface, SDL_FreeSurface, SDL_SetVideoMode, SDL_CreateRGBSurfaceFrom
#include "browser/ImageDisplay.h"       // ImageDisplay
#include "browser/ImageDisplaySDL.h"    // ImageDisplaySDL
#include "image/Color.h"                // Pixel
#include "image/ColorMask.h"            // ColorMask
#include "image/Image.h"                // Image
#include "image/ImageFormat.h"          // ImageFormat, RGBImageFormat

using namespace std;
using darkcore::browser::ImageDisplay;
using darkcore::browser::MapDisplaySDL;
using darkcore::image::ColorMask;
using darkcore::image::Image;
using darkcore::image::ImageFormat;
using darkcore::image::Pixel;
using darkcore::image::RGBImageFormat;


/*
 * Public Methods
 */

ImageDisplaySDL::ImageDisplaySDL(int width, int height, int zoom) :
    ImageDisplay(), pDisplaySurface(NULL), 
    displayColorMask(32, 0xFF0000, 0x00FF00, 0x0000FF),
    displayImageFormat(32, displayColorMask)
{
    pDisplaySurface = SDL_SetVideoMode(width, height, 0, SDL_ANYFORMAT);
}

ImageDisplaySDL::~ImageDisplaySDL()
{
    SDL_FreeSurface(pDisplaySurface);
}

void ImageDisplaySDL::display(const Image &img) const
{
    // FIXME - not handling RGB-to-RGB conversions

    if (img.getImageFormat().getFormatType() == ImageFormat::INDEXED)
        // convert the image to the specified format
        displayRGBImage(img.convert(displayImageFormat));
    else
        displayRGBImage(img);
}

void ImageDisplaySDL::displayRGBImage(const Image &img) const
{
    // get the image data and apply a zoom
    Pixel zoomedImageData[img.getNumPixels() * zoom * zoom];
    zoomImage(img, zoomedImageData);

    // create an image surface from the pixel data
    SDL_Surface *imgSurf = SDL_CreateRGBSurfaceFrom(
            zoomedImageData,
            img.getWidth(),
            img.getHeight(),
            displayImageFormat.getColorDepth(),
            img.getPitch() * zoom, // pitch
            displayColorMask.getRedMask(),
            displayColorMask.getGreenMask(),
            displayColorMask.getBlueMask(),
            0);

    //img.operate(DumpImageOperation());

    // cleanup any artifacts on the surface
    SDL_Rect blackRect = { 0, 0, img.getWidth() * MAX_ZOOM, img.getHeight() * MAX_ZOOM };
    SDL_FillRect(pDisplaySurface, &blackRect, 0);

    // blit the image surface to the display surface
    SDL_Rect rect = { 0, 0, img.getWidth() * zoom, img.getHeight() * zoom };
    SDL_BlitSurface(imgSurf, &rect, pDisplaySurface, &rect);
    SDL_UpdateRect(pDisplaySurface, 0, 0, 0, 0);

    // free the image surface
    SDL_FreeSurface(imgSurf);
}

void ImageDisplaySDL::zoomImage(const Image &img, Pixel *target) const
{
    int tgtIdx = 0;
    for (int y = 0; y < img.getHeight(); y++)
    {
        // capture pointer to start index of row
        const Pixel *rowStart = target + tgtIdx;

        // output row once
        for (int x = 0; x < img.getWidth(); x++)
        {
            //printf("tgtIdx=%d, y=%d, x=%d\n", tgtIdx, y, x); // DEBUG
            Pixel p = img.getPixel(x, y);
            for (int z = 0; z < zoom; z++)
                target[tgtIdx++] = p;
        }

        // copy row 'zoom-1' more times
        for (int z = 1; z < zoom; z++, tgtIdx += img.getWidth() * zoom)
            memcpy(target + tgtIdx, rowStart, img.getPitch() * zoom);
    }
}
