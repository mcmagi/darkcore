/* ImageDisplaySDL.cpp */

#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include "SDL/SDL.h"                    // SDL_Surface, SDL_FreeSurface, SDL_SetVideoMode, SDL_CreateRGBSurfaceFrom
#include "browser/ImageDisplay.h"       // ImageDisplay
#include "browser/ImageDisplaySDL.h"    // ImageDisplaySDL
#include "image/Color.h"                // Pixel
#include "image/ColorMask.h"            // ColorMask
#include "image/ColorPalette.h"         // StandardPaletteType
#include "image/Image.h"                // Image
#include "image/ImageFormat.h"          // ImageFormat, RGBImageFormat
#include "image/converter/CGACompositeConverter.h" // CGACompositeConverter
#include "image/converter/ZoomConverter.h" // ZoomConverter

using namespace std;
using darkcore::browser::ImageDisplay;
using darkcore::browser::ImageDisplaySDL;
using darkcore::image::ColorMask;
using darkcore::image::Image;
using darkcore::image::ImageFormat;
using darkcore::image::Pixel;
using darkcore::image::RGBImageFormat;
using darkcore::image::StandardPaletteType;
using darkcore::image::converter::CGACompositeConverter;
using darkcore::image::converter::ZoomConverter;


const int ImageDisplaySDL::MIN_ZOOM = 1;
const int ImageDisplaySDL::MAX_ZOOM = 10;

/*
 * Public Methods
 */

ImageDisplaySDL::ImageDisplaySDL(int width, int height, int zoom) :
    ImageDisplay(), pDisplaySurface(NULL),
    displayImageFormat(ColorMask::FULL32), zoomConv(zoom),
	cga_composite(false)
{
    pDisplaySurface = SDL_SetVideoMode(width, height, 0, SDL_ANYFORMAT);
}

ImageDisplaySDL::~ImageDisplaySDL()
{
    SDL_FreeSurface(pDisplaySurface);
}

void ImageDisplaySDL::display(const Image &img) const
{
    // FIXME - not handling RGB-to-RGB conversions

	if (cga_composite && img.getImageFormat().getColorDepth() == StandardPaletteType::CGA)
	{
        RGBImageFormat rgb(ColorMask::FULL32);
        CGACompositeConverter compositeConv(rgb);
        unique_ptr<Image> convImg = img.convert(compositeConv);
        displayRGBImage(*convImg);
	}
	else if (img.getImageFormat().getFormatType() == ImageFormat::INDEXED)
    {
        // convert the image to the specified format
        unique_ptr<Image> convImg = img.convert(displayImageFormat);
        displayRGBImage(*convImg);
    }
    else
        displayRGBImage(img);
}

void ImageDisplaySDL::displayRGBImage(const Image &img) const
{
    // apply a zoom on the image data
    unique_ptr<Image> zImg = img.convert(zoomConv);
    const vector<Pixel> zoomedImageData = zImg->getImageData();

    // create an image surface from the pixel data
    SDL_Surface *imgSurf = SDL_CreateRGBSurfaceFrom(
            const_cast<Pixel *>(&zoomedImageData[0]),
            zImg->getWidth(),
            zImg->getHeight(),
            displayImageFormat.getColorDepth(),
            zImg->getPitch(),
            displayImageFormat.getColorMask().getRedMask(),
            displayImageFormat.getColorMask().getGreenMask(),
            displayImageFormat.getColorMask().getBlueMask(),
            0);

    //img.operate(DumpOperation());

    // cleanup any artifacts on the surface
    SDL_Rect blackRect = { 0, 0, Uint16(img.getWidth() * MAX_ZOOM), Uint16(img.getHeight() * MAX_ZOOM) };
    SDL_FillRect(pDisplaySurface, &blackRect, 0);

    // blit the image surface to the display surface
    SDL_Rect rect = { 0, 0, (Uint16) zImg->getWidth(), (Uint16) zImg->getHeight() };
    SDL_BlitSurface(imgSurf, &rect, pDisplaySurface, &rect);
    SDL_UpdateRect(pDisplaySurface, 0, 0, 0, 0);

    // free the image surface
    SDL_FreeSurface(imgSurf);
}