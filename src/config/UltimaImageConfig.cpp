/* UltimaImageConfig.cpp */

#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include "config/Flags.h"               // Flags
#include "config/IndexConfig.h"         // IndexConfig
#include "config/UltimaFileConfig.h"    // UltimaFileConfig
#include "config/UltimaGameConfig.h"    // UltimaGameConfig
#include "config/UltimaImageConfig.h"   // UltimaImageConfig
#include "image/ImageFormat.h"          // ImageFormat

using namespace std;
using darkcore::config::Flags;
using darkcore::config::IndexConfig;
using darkcore::config::UltimaFileConfig;
using darkcore::config::UltimaImageConfig;
using darkcore::image::ImageFormat;

const Flags UltimaImageConfig::INTERLACED = 0x01;
const Flags UltimaImageConfig::BITMASKED = 0x02;
const Flags UltimaImageConfig::IBMCGA = 0x04;
const Flags UltimaImageConfig::TYPEMASKED = 0x08;

UltimaImageConfig::UltimaImageConfig(const string &filename,
        const string &description, int numImages, int width, int height,
        const ImageFormat &format, const Flags &flags, const IndexConfig *idx) :
    gameCfg(NULL), fileCfg(filename), description(description),
    numImages(numImages), width(width), height(height), format(format.clone()),
    flags(flags), idxCfg(clone(idx))
{
}

UltimaImageConfig::UltimaImageConfig(const UltimaFileConfig &fileCfg,
        const string &description, int numImages, int width, int height,
        const ImageFormat &format, const Flags &flags, const IndexConfig *idx) :
    gameCfg(NULL), fileCfg(fileCfg), description(description),
    numImages(numImages), width(width), height(height), format(format.clone()),
    flags(flags), idxCfg(clone(idx))
{
}

UltimaImageConfig::UltimaImageConfig(const UltimaImageConfig &cfg) :
    gameCfg(cfg.gameCfg), fileCfg(cfg.fileCfg), description(cfg.description),
    numImages(cfg.numImages), width(cfg.width), height(cfg.height), format(cfg.format->clone()),
    flags(cfg.flags), idxCfg(clone(cfg.idxCfg.get()))
{
}

UltimaImageConfig::~UltimaImageConfig()
{
}

UltimaImageConfig & UltimaImageConfig::operator=(const UltimaImageConfig &cfg)
{
    if (this != &cfg)
    {
        gameCfg = cfg.gameCfg;
        fileCfg = cfg.fileCfg;
        description = cfg.description;
        numImages = cfg.numImages;
        width = cfg.width;
        height = cfg.height;
        format = cfg.format->clone();
        flags = cfg.flags;
        idxCfg = clone(cfg.idxCfg.get());
    }
    return *this;
}

unique_ptr<IndexConfig> UltimaImageConfig::clone(const IndexConfig *idxCfg)
{
    // do a null check before cloning
    return idxCfg == NULL ? unique_ptr<IndexConfig>() : idxCfg->clone();
}