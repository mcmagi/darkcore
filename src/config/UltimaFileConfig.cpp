/* UltimaFileConfig.cpp */

#include <iostream>                     // std::istream, std::ostream
#include <memory>                       // std::unique_ptr, std::move
#include <string>                       // std::string
#include "config/Flags.h"               // Flags
#include "config/UltimaFileConfig.h"    // UltimaFileConfig
#include "lzw/LZWIOStream.h"

using namespace std;
using darkcore::config::Flags;
using darkcore::config::UltimaFileConfig;
using darkcore::lzw::LZWInputStream;
using darkcore::lzw::LZWOutputStream;

const Flags UltimaFileConfig::COMPRESSED = 0x01;

UltimaFileConfig::UltimaFileConfig(const string &filename,
        const Flags &flags, int offset) :
    gameCfg(NULL), filename(filename), flags(flags), offset(offset)
{
}

UltimaFileConfig::UltimaFileConfig(const UltimaFileConfig &cfg) :
    gameCfg(cfg.gameCfg), filename(cfg.filename), flags(cfg.flags), offset(cfg.offset)
{
}

UltimaFileConfig::~UltimaFileConfig()
{
}

UltimaFileConfig & UltimaFileConfig::operator=(const UltimaFileConfig &cfg)
{
    if (this != &cfg)
    {
        gameCfg = cfg.gameCfg;
        filename = cfg.filename;
        flags = cfg.flags;
        offset = cfg.offset;
    }
    return *this;
}

unique_ptr<istream> UltimaFileConfig::newInputStream() const
{
    //cout << "UltimafileConfig: creating new input stream: " << endl;
    unique_ptr<istream> str(getFile().newInputStream(ios::in | ios::binary));

    // wrap stream in LZW streams if the file is compressed
    if (isCompressed())
        str = unique_ptr<istream>(new LZWInputStream(std::move(str)));

    str->seekg(offset);

    //cout << "UltimafileConfig: returning input stream" << endl;
    return str;
}

unique_ptr<ostream> UltimaFileConfig::newOutputStream() const
{
    unique_ptr<ostream> str(getFile().newOutputStream(ios::out | ios::binary));

    // wrap stream in LZW streams if the file is compressed
    if (isCompressed())
        str = unique_ptr<ostream>(new LZWOutputStream(std::move(str)));

    str->seekp(offset);

    return str;
}

unique_ptr<UltimaFileConfig> UltimaFileConfig::forFile(const std::string &newFilename) const
{
    unique_ptr<UltimaFileConfig> fileCfg(new UltimaFileConfig(*this));
    fileCfg.get()->filename = newFilename;
    return fileCfg;
}