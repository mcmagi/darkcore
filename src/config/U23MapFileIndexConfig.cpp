/* U23MapFileIndexConfig.cpp */

#include <cassert>                      // assert
#include <cmath>                        // pow
#include <iostream>
#include <memory>                       // std::unique_ptr, std::move
#include <typeinfo>                     // typeid
#include "config/IndexConfig.h"         // IndexConfig
#include "config/IndexEntry.h"          // IndexEntry
#include "config/MapFileIndexConfig.h"  // MapFileIndexConfig
#include "config/U23MapFileIndexConfig.h"   // U23MapFileIndexConfig
#include "util/Reader.h"                // Reader
#include "util/Writer.h"                // Writer

using namespace std;
using darkcore::config::IndexConfig;
using darkcore::config::IndexEntry;
using darkcore::config::MapFileIndexConfig;
using darkcore::config::U23MapFileIndexConfig;
using darkcore::util::Reader;
using darkcore::util::Writer;

U23MapFileIndexConfig::U23MapFileIndexConfig(const string &filename, int size) :
    MapFileIndexConfig(filename, size), multiplier(4)
{
    //cout << "U23MapFileIndexConfig constructor" << endl;
}

U23MapFileIndexConfig::U23MapFileIndexConfig(const UltimaFileConfig &fileCfg, int size) :
    MapFileIndexConfig(fileCfg, size), multiplier(4)
{
    //cout << "U23MapFileIndexConfig constructor" << endl;
}

U23MapFileIndexConfig::U23MapFileIndexConfig(const U23MapFileIndexConfig &idx) :
    MapFileIndexConfig(idx), multiplier(idx.multiplier)
{
    //cout << "U23MapFileIndexConfig copy constructor" << endl;
}

U23MapFileIndexConfig::~U23MapFileIndexConfig()
{
}

IndexConfig & U23MapFileIndexConfig::operator=(const IndexConfig &idx)
{
    assert(typeid(idx) == typeid(*this));
    return this->operator=(dynamic_cast<const U23MapFileIndexConfig &>(idx));
}

MapFileIndexConfig & U23MapFileIndexConfig::operator=(const MapFileIndexConfig &idx)
{
    assert(typeid(idx) == typeid(*this));
    return this->operator=(dynamic_cast<const U23MapFileIndexConfig &>(idx));
}

U23MapFileIndexConfig & U23MapFileIndexConfig::operator=(const U23MapFileIndexConfig &idx)
{
    if (this != &idx)
    {
        MapFileIndexConfig::operator=(idx);
        multiplier = idx.multiplier;
    }
    return *this;
}

unique_ptr<IndexConfig> U23MapFileIndexConfig::clone() const
{
    return unique_ptr<IndexConfig>(new U23MapFileIndexConfig(*this));
}

unique_ptr<Reader<IndexEntry> > U23MapFileIndexConfig::newEntryReader() const
{
    // wrap the reader with our own
    return unique_ptr<Reader<IndexEntry> >(new IndexEntryReader(*this, MapFileIndexConfig::newEntryReader()));
}

unique_ptr<Writer<IndexEntry> > U23MapFileIndexConfig::newEntryWriter() const
{
    // TODO: Writer not implemented for now
    return unique_ptr<Writer<IndexEntry> >(new IndexEntryWriter(*this, MapFileIndexConfig::newEntryWriter()));
}

// inner class: IndexEntryReader

U23MapFileIndexConfig::IndexEntryReader::IndexEntryReader(
        const U23MapFileIndexConfig &cfg, unique_ptr<Reader<IndexEntry> > reader) :
    Reader<IndexEntry>(), cfg(cfg), reader(std::move(reader))
{
}

U23MapFileIndexConfig::IndexEntryReader::~IndexEntryReader()
{
}

IndexEntry U23MapFileIndexConfig::IndexEntryReader::read()
{
    // read new entry into index data
    IndexEntry entry = reader->read();

    // perform mapping of index before returning it
    return IndexEntry(mapIndex(entry.getOffset()));
}

int U23MapFileIndexConfig::IndexEntryReader::mapIndex(int value) const
{
    return value / cfg.getMultiplier();
}

// inner class: IndexEntryWriter

U23MapFileIndexConfig::IndexEntryWriter::IndexEntryWriter(
        const U23MapFileIndexConfig &cfg, unique_ptr<Writer<IndexEntry> > writer) :
    Writer<IndexEntry>(), cfg(cfg), writer(std::move(writer))
{
}

U23MapFileIndexConfig::IndexEntryWriter::~IndexEntryWriter()
{
}

void U23MapFileIndexConfig::IndexEntryWriter::write(const IndexEntry &entry)
{
    // map and write index entry
    writer->write(IndexEntry(mapIndex(entry.getOffset())));
}

int U23MapFileIndexConfig::IndexEntryWriter::mapIndex(int value) const
{
    return value * cfg.getMultiplier();
}
