/* Flags.cpp */

#include <iostream>
#include "config/Flags.h"               // UltimaGameConfig

using namespace std;
using darkcore::config::Flags;

const Flags Flags::NONE = 0x00;

Flags::Flags(int value) :
    value(value)
{
}

Flags::Flags(const Flags &flags) :
    value(flags.value)
{
}

Flags::~Flags()
{
}

Flags & Flags::operator=(const Flags &flags)
{
    if (this != &flags)
        value = flags.value;
    return *this;
}
