/* U6ChunkIndexConfig.cpp */

#include <cassert>                      // assert
#include <cmath>                        // pow
#include <iostream>
#include <memory>                       // std::unique_ptr
#include <typeinfo>                     // typeid
#include "config/IndexConfig.h"         // IndexConfig
#include "config/IndexEntry.h"          // IndexEntry
#include "config/MapFileIndexConfig.h"  // MapFileIndexConfig
#include "config/U6ChunkIndexConfig.h"   // U6ChunkIndexConfig
#include "util/Reader.h"                // Reader
#include "util/StreamReader.h"          // StreamReader
#include "util/Writer.h"                // Writer

using namespace std;
using darkcore::config::IndexConfig;
using darkcore::config::IndexEntry;
using darkcore::config::MapFileIndexConfig;
using darkcore::config::U6ChunkIndexConfig;
using darkcore::util::Reader;
using darkcore::util::StreamReader;
using darkcore::util::Writer;

U6ChunkIndexConfig::U6ChunkIndexConfig(const string &filename, int size) :
    MapFileIndexConfig(filename, size)
{
}

U6ChunkIndexConfig::U6ChunkIndexConfig(const UltimaFileConfig &fileCfg, int size) :
    MapFileIndexConfig(fileCfg, size)
{
}

U6ChunkIndexConfig::U6ChunkIndexConfig(const U6ChunkIndexConfig &idx) :
    MapFileIndexConfig(idx)
{
}

U6ChunkIndexConfig::~U6ChunkIndexConfig()
{
}

IndexConfig & U6ChunkIndexConfig::operator=(const IndexConfig &idx)
{
    assert(typeid(idx) == typeid(*this));
    return this->operator=(dynamic_cast<const U6ChunkIndexConfig &>(idx));
}

MapFileIndexConfig & U6ChunkIndexConfig::operator=(const MapFileIndexConfig &idx)
{
    assert(typeid(idx) == typeid(*this));
    return this->operator=(dynamic_cast<const U6ChunkIndexConfig &>(idx));
}

U6ChunkIndexConfig & U6ChunkIndexConfig::operator=(const U6ChunkIndexConfig &idx)
{
    if (this != &idx)
        MapFileIndexConfig::operator=(idx);
    return *this;
}

unique_ptr<IndexConfig> U6ChunkIndexConfig::clone() const
{
    return unique_ptr<IndexConfig>(new U6ChunkIndexConfig(*this));
}

unique_ptr<Reader<IndexEntry> > U6ChunkIndexConfig::newEntryReader() const
{
    return unique_ptr<Reader<IndexEntry> >(new IndexEntryReader(*this));
}

unique_ptr<Writer<IndexEntry> > U6ChunkIndexConfig::newEntryWriter() const
{
    // TODO: Writer not implemented for now
    return unique_ptr<Writer<IndexEntry> >();
}

U6ChunkIndexConfig::IndexEntryReader::IndexEntryReader(
        const U6ChunkIndexConfig &cfg) :
    StreamReader<IndexEntry>(cfg.getFileConfig().newInputStream()),
    cfg(cfg), nextData(-1), counter(0)
{
}

U6ChunkIndexConfig::IndexEntryReader::~IndexEntryReader()
{
}

IndexEntry U6ChunkIndexConfig::IndexEntryReader::read()
{
    int data;

    if (nextData == -1)
    {
        // read raw data
        unsigned char bytes[3];
        getInputStream().read((char *) &bytes, 3);

        /* cout << "read data: " << hex << int(bytes[0]) << "-"
                              << hex << int(bytes[1]) << "-"
                              << hex << int(bytes[2]) << endl; */

        // 3-nybble indexes are stored in a 12-05-34 format
        // need to extract to a 012-345 format
        // see http://red5.graf.torun.pl/~rackne/u6like.html

        data = bytes[0];
        data += (bytes[1] & 0x0F) << 8;
        nextData = bytes[1] >> 4;
        nextData += int(bytes[2]) << 4;

        /* cout << "index: " << hex << data << ","
                          << hex << nextData << endl; */
    }
    else
    {
        data = nextData;
        nextData = -1;
    }

    counter++;
    return IndexEntry(data);
}
