/* UltimaGameConfig.cpp */

#include <iostream>                     // std::cout
#include <map>                          // std::map
#include <string>                       // std::string
#include <vector>                       // std::vector
#include "Exception.h"                  // Exception
#include "config/Flags.h"               // Flags
#include "config/MapFileIndexConfig.h"  // MapFileIndexConfig
#include "config/MapSeqIndexConfig.h"   // MapSeqIndexConfig
#include "config/U1MapFileIndexConfig.h"// U1MapFileIndexConfig
#include "config/U23MapFileIndexConfig.h"   // U23MapFileIndexConfig
#include "config/U6ChunkIndexConfig.h"  // U6ChunkIndexConfig
#include "config/U2CGAIndexConfig.h"    // U2CGAIndexConfig
#include "config/U6TileIndexConfig.h"   // U6TileIndexConfig
#include "config/UltimaFileConfig.h"    // UltimaFileConfig
#include "config/UltimaGameConfig.h"    // UltimaGameConfig, UltimaGame
#include "config/UltimaImageConfig.h"   // UltimaImageConfig
#include "config/UltimaMapConfig.h"     // UltimaMapConfig
#include "image/Color.h"         		// Color
#include "image/ColorPalette.h"         // ColorPalette, StandardPaletteType
#include "image/ImageFormat.h"          // ImageFormat, IndexedImageFormat

using namespace std;
using darkcore::Exception;
using darkcore::config::Flags;
using darkcore::config::MapFileIndexConfig;
using darkcore::config::MapSeqIndexConfig;
using darkcore::config::U2CGAIndexConfig;
using darkcore::config::U6ChunkIndexConfig;
using darkcore::config::U6TileIndexConfig;
using darkcore::config::UltimaFileConfig;
using darkcore::config::UltimaGame;
using darkcore::config::UltimaGameConfig;
using darkcore::config::UltimaImageConfig;
using darkcore::config::UltimaMapConfig;
using darkcore::image::Color;
using darkcore::image::ColorPalette;
using darkcore::image::ImageFormat;
using darkcore::image::IndexedImageFormat;
using darkcore::image::StandardPaletteType;

UltimaGameConfig::UltimaGameConfig(UltimaGame game, const string &path) :
    game(game), path(path), images()
{
}

UltimaGameConfig::UltimaGameConfig(const UltimaGameConfig &cfg) :
    game(cfg.game), path(cfg.path), images(cfg.images)
{
}

UltimaGameConfig::~UltimaGameConfig()
{
}

UltimaGameConfig & UltimaGameConfig::operator=(const UltimaGameConfig &cfg)
{
    if (this != &cfg)
    {
        game = cfg.game;
        path = cfg.path;
        images = cfg.images;
        maps = cfg.maps;
    }
    return *this;
}

void UltimaGameConfig::addImageConfig(const UltimaImageConfig &imgCfg)
{
    string key = imgCfg.getFileConfig().getFilename();
    cout << "adding UltimaImageConfig for filename=" << key << endl;

    images.insert(pair<string,UltimaImageConfig>(key, imgCfg));

    images.find(key)->second.setGameConfig(*this);
}

const UltimaImageConfig & UltimaGameConfig::getImageConfig(const string &filename) const
{
    cout << "getting UltimaImageConfig for filename=" << filename << endl;

    const ImageConfigMap::const_iterator it = images.find(filename);
    if (it == images.end())
        throw Exception(string("No image config found for ") += filename);
    return it->second;
}

std::vector<UltimaImageConfig> UltimaGameConfig::getImageConfigs() const
{
    std::vector<UltimaImageConfig> vect;
    for (ImageConfigMap::const_iterator it = images.begin(); it != images.end(); it++)
        vect.push_back(it->second);
    return vect;
}

void UltimaGameConfig::addMapConfig(const UltimaMapConfig &mapCfg)
{
    string key = mapCfg.getFilename();
    cout << "adding UltimaMapConfig for filename=" << key << endl;

    maps.insert(pair<string,UltimaMapConfig>(key, mapCfg));

    maps.find(key)->second.setGameConfig(*this);
}

const UltimaMapConfig & UltimaGameConfig::getMapConfig(const string &filename) const
{
    //cout << "getting UltimaMapConfig for filename=" << filename << endl;

    const MapConfigMap::const_iterator it = maps.find(filename);
    if (it == maps.end())
        throw Exception(string("No map config found for ") += filename);
    return it->second;
}

std::vector<UltimaMapConfig> UltimaGameConfig::getMapConfigs() const
{
    std::vector<UltimaMapConfig> vect;
    for (MapConfigMap::const_iterator it = maps.begin(); it != maps.end(); it++)
        vect.push_back(it->second);
    return vect;
}

const UltimaGameConfig UltimaGameConfig::getGameConfig(UltimaGame game, const string &path)
{
    cout << "creating UltimaGameConfig for game=" << game << ", path=" << path << endl;

    UltimaGameConfig ugc(game, path);

    IndexedImageFormat monoFormat = IndexedImageFormat(darkcore::image::MONO);
    IndexedImageFormat cgaFormat = IndexedImageFormat(darkcore::image::CGA);
    IndexedImageFormat egaFormat = IndexedImageFormat(darkcore::image::EGA);

    switch (game)
    {
        case ULTIMA1: {
			// the u1 EGA castle image has a custom palette
			ColorPalette u1CastlePal = ColorPalette(darkcore::image::EGA);
			u1CastlePal.setColor(0, Color::BLACK);
			u1CastlePal.setColor(1, Color::DK_GRAY);
			u1CastlePal.setColor(2, Color::GRAY);
			u1CastlePal.setColor(3, Color::WHITE);
			u1CastlePal.setColor(4, Color::WHITE);
			u1CastlePal.setColor(5, Color::GREEN);
			u1CastlePal.setColor(6, Color::LT_GREEN);
			u1CastlePal.setColor(7, Color::BROWN);
			u1CastlePal.setColor(8, Color::LT_RED);
			u1CastlePal.setColor(9, Color::GRAY);
			u1CastlePal.setColor(10, Color::LT_MAGENTA);
			u1CastlePal.setColor(11, Color::LT_CYAN);
			u1CastlePal.setColor(12, Color::BLUE);
			u1CastlePal.setColor(13, Color::LT_BLUE);
			u1CastlePal.setColor(14, Color::GRAY);
			u1CastlePal.setColor(15, Color::WHITE);
            IndexedImageFormat u1CastleEgaFormat = IndexedImageFormat(darkcore::image::EGA, u1CastlePal);

            ugc.addImageConfig(UltimaImageConfig("CGATILES.BIN", "CGA Sosarian Tileset", 52, 16, 16, cgaFormat));
            ugc.addImageConfig(UltimaImageConfig("EGATILES.BIN", "EGA Sosarian Tileset", 52, 16, 16, egaFormat, UltimaImageConfig::BITMASKED));
            ugc.addImageConfig(UltimaImageConfig("CGAMOND.BIN", "CGA Mondain Tileset", 19, 16, 16, cgaFormat));
            ugc.addImageConfig(UltimaImageConfig("EGAMOND.BIN", "EGA Mondain Tileset", 19, 16, 16, egaFormat, UltimaImageConfig::BITMASKED));
            ugc.addImageConfig(UltimaImageConfig("CGATOWN.BIN", "CGA Town Tileset", 51, 8, 8, cgaFormat));
            ugc.addImageConfig(UltimaImageConfig("EGATOWN.BIN", "EGA Town Tileset", 51, 8, 8, egaFormat, UltimaImageConfig::BITMASKED));
            ugc.addImageConfig(UltimaImageConfig("CGASPACE.BIN", "CGA Space Tileset", 48, 32, 19, cgaFormat));
            ugc.addImageConfig(UltimaImageConfig("EGASPACE.BIN", "EGA Space Tileset", 96, 32, 19, monoFormat));
            ugc.addImageConfig(UltimaImageConfig("CGAFIGHT.BIN", "CGA Space Fight Tileset", 28, 24, 19, cgaFormat));
            ugc.addImageConfig(UltimaImageConfig("EGAFIGHT.BIN", "EGA Space Fight Tileset", 56, 24, 19, monoFormat));
            ugc.addImageConfig(UltimaImageConfig("CASTLE.4", "CGA Castle Intro", 1, 320, 200, cgaFormat,
                        UltimaImageConfig::INTERLACED | UltimaImageConfig::IBMCGA));
            ugc.addImageConfig(UltimaImageConfig("CASTLE.16", "EGA Castle Intro", 1, 320, 200, u1CastleEgaFormat));

            std::map<int,const UltimaImageConfig *> tileSets;
            tileSets[2] = &ugc.getImageConfig("CGATILES.BIN");
            tileSets[4] = &ugc.getImageConfig("EGATILES.BIN");

            ugc.addMapConfig(UltimaMapConfig(U1MapFileIndexConfig("MAP.BIN"), "Sosaria Map", tileSets, 168, 156));
            break;

        } case ULTIMA2: {
            U2CGAIndexConfig cgaindex(65, 2);

            // U2 CGA actually has a tile index just prior to 0x7c42
            ugc.addImageConfig(UltimaImageConfig(UltimaFileConfig("ULTIMAII.EXE"), "CGA Tileset (Original)", 65, 16, 16, cgaFormat, Flags::NONE, &cgaindex));
            ugc.addImageConfig(UltimaImageConfig("CGATILES", "CGA Tileset", 65, 16, 16, cgaFormat));
            ugc.addImageConfig(UltimaImageConfig("EGATILES", "EGA Tileset", 65, 16, 16, egaFormat));

            // interlaced with 192 bytes of padding between each set of scanlines (UltimaImageConfig::IBMCGA)
            ugc.addImageConfig(UltimaImageConfig("PICDRA", "CGA Dragon Intro", 1, 320, 200, cgaFormat, UltimaImageConfig::INTERLACED | UltimaImageConfig::IBMCGA));
            ugc.addImageConfig(UltimaImageConfig("PICOUT", "CGA Outdoors Demo", 1, 320, 200, cgaFormat, UltimaImageConfig::INTERLACED | UltimaImageConfig::IBMCGA));
            ugc.addImageConfig(UltimaImageConfig("PICTWN", "CGA Town Demo", 1, 320, 200, cgaFormat, UltimaImageConfig::INTERLACED | UltimaImageConfig::IBMCGA));
            ugc.addImageConfig(UltimaImageConfig("PICCAS", "CGA Castle Demo", 1, 320, 200, cgaFormat, UltimaImageConfig::INTERLACED | UltimaImageConfig::IBMCGA));
            ugc.addImageConfig(UltimaImageConfig("PICDNG", "CGA Dungeon Demo", 1, 320, 200, cgaFormat, UltimaImageConfig::INTERLACED | UltimaImageConfig::IBMCGA));
            ugc.addImageConfig(UltimaImageConfig("PICSPA", "CGA Space Demo", 1, 320, 200, cgaFormat, UltimaImageConfig::INTERLACED | UltimaImageConfig::IBMCGA));

            // these pics are actually stored in VGA format with an EGA palette !!!
            IndexedImageFormat u2vgaFormat = IndexedImageFormat(darkcore::image::VGA);
            ugc.addImageConfig(UltimaImageConfig("PICDRA.EGA", "EGA Dragon Intro", 1, 320, 200, u2vgaFormat));
            ugc.addImageConfig(UltimaImageConfig("PICOUT.EGA", "EGA Outdoors Demo", 1, 320, 200, u2vgaFormat));
            ugc.addImageConfig(UltimaImageConfig("PICTWN.EGA", "EGA Town Demo", 1, 320, 200, u2vgaFormat));
            ugc.addImageConfig(UltimaImageConfig("PICCAS.EGA", "EGA Castle Demo", 1, 320, 200, u2vgaFormat));
            ugc.addImageConfig(UltimaImageConfig("PICDNG.EGA", "EGA Dungeon Demo", 1, 320, 200, u2vgaFormat));
            ugc.addImageConfig(UltimaImageConfig("PICSPA.EGA", "EGA Space Demo", 1, 320, 200, u2vgaFormat));
            ugc.addImageConfig(UltimaImageConfig("PICMIN.EGA", "EGA Minax Demo", 1, 320, 200, u2vgaFormat));

            std::map<int,const UltimaImageConfig *> tileSets;
            tileSets[2] = &ugc.getImageConfig("CGATILES");
            tileSets[4] = &ugc.getImageConfig("EGATILES");

            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX00"), "Legends Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX03"), "Legends : Shadowguard Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX10"), "Pangea Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX11"), "Pangea : Baradin's Town Map", tileSets, 64, 64));
            //ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX15"), "Pangea Dungeon Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX20"), "B.C. Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX21"), "B.C. : Le Jester Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX22"), "B.C. : Towne Linda Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX23"), "B.C. : Lord British's Castle Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX30"), "A.D. Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX31"), "A.D. : Port Bonifice Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX32"), "A.D. : New San Antonio Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX33"), "A.D. : Lord British's Castle Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX40"), "Aftermath Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPX41"), "Aftermath : Pirates Harbour Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG10"), "Mercury Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG20"), "Venus Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG30"), "Mars Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG32"), "Mars : Towne Mary Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG40"), "Jupiter Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG41"), "Jupiter : Preppies Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG50"), "Saturn Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG60"), "Uranus Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG61"), "Uranus : New Jester Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG70"), "Neptune Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG71"), "Neptune : Computer Camp Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG80"), "Pluto Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG81"), "Pluto : Tommersville Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG82"), "Pluto : Towne Makler Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG90"), "Planet X Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG92"), "Planet X : Town Basko Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MAPG93"), "Planet X : Castle Barataria Map", tileSets, 64, 64));
            break;

        } case ULTIMA3: {
            IndexedImageFormat u3vgaFormat = IndexedImageFormat(darkcore::image::VGA, ColorPalette::loadVGAPalette(path + "/U3VGA.PAL"));

            ugc.addImageConfig(UltimaImageConfig("shapes.ult", "CGA Tileset", 80, 16, 16, cgaFormat, UltimaImageConfig::INTERLACED));
            ugc.addImageConfig(UltimaImageConfig("shapes.ega", "EGA Tileset", 80, 16, 16, egaFormat));
            ugc.addImageConfig(UltimaImageConfig("shapes.vga", "VGA Tileset", 80, 16, 16, u3vgaFormat));
            ugc.addImageConfig(UltimaImageConfig("charset.ult", "CGA Charset", 128, 8, 8, cgaFormat, UltimaImageConfig::INTERLACED));
            ugc.addImageConfig(UltimaImageConfig("charset.ega", "EGA Charset", 128, 8, 8, egaFormat));
            ugc.addImageConfig(UltimaImageConfig("charset.vga", "EGA Charset", 128, 8, 8, u3vgaFormat));
            ugc.addImageConfig(UltimaImageConfig("MOONS.ULT", "CGA Moons", 8, 8, 8, cgaFormat, UltimaImageConfig::INTERLACED));
            ugc.addImageConfig(UltimaImageConfig("MOONS.EGA", "EGA Moons", 8, 8, 8, egaFormat));
            ugc.addImageConfig(UltimaImageConfig("MOONS.VGA", "VGA Moons", 8, 8, 8, u3vgaFormat));

            // interlaced with 192 bytes of padding between each set of scanlines (UltimaImageConfig::IBMCGA)
            ugc.addImageConfig(UltimaImageConfig("blank.ibm", "CGA Blank Intro", 1, 320, 200, cgaFormat,
                        UltimaImageConfig::INTERLACED | UltimaImageConfig::IBMCGA));
            ugc.addImageConfig(UltimaImageConfig("exod.ibm", "CGA Exodus Intro", 1, 320, 200, cgaFormat,
                        UltimaImageConfig::INTERLACED | UltimaImageConfig::IBMCGA));
            ugc.addImageConfig(UltimaImageConfig("blank.ega", "EGA Blank Intro", 1, 320, 200, egaFormat));
            ugc.addImageConfig(UltimaImageConfig("exod.ega", "EGA Exodus Intro", 1, 320, 200, egaFormat));
            ugc.addImageConfig(UltimaImageConfig("blank.vga", "VGA Blank Intro", 1, 320, 200, u3vgaFormat));
            ugc.addImageConfig(UltimaImageConfig("exod.vga", "VGA Exodus Intro", 1, 320, 200, u3vgaFormat));

            ugc.addImageConfig(UltimaImageConfig("animate.dat", "CGA Intro Animation", 16, 92, 16, cgaFormat));
            ugc.addImageConfig(UltimaImageConfig("animate.ega", "EGA Intro Animation", 16, 92, 16, egaFormat));
            ugc.addImageConfig(UltimaImageConfig("animate.vga", "VGA Intro Animation", 16, 92, 16, u3vgaFormat));

            ugc.addImageConfig(UltimaImageConfig("dngobj.vga", "VGA Dungeon Objects", 34, 176, 176, u3vgaFormat));

            std::map<int,const UltimaImageConfig *> tileSets;
            tileSets[2] = &ugc.getImageConfig("shapes.ult");
            tileSets[4] = &ugc.getImageConfig("shapes.ega");
            tileSets[8] = &ugc.getImageConfig("shapes.vga");

            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("sosaria.ult"), "Sosaria Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("AMBROSIA.ULT"), "Ambrosia Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("BRITISH.ULT"), "Lord British's Castle Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("LCB.ULT"), "Britain Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("YEW.ULT"), "Yew Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MOON.ULT"), "Moon Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("GREY.ULT"), "Grey Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("FAWN.ULT"), "Fawn Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("DEVIL.ULT"), "Devil Guard Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MONTOR_E.ULT"), "Montor East Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("MONTOR_W.ULT"), "Montor East Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("DEATH.ULT"), "Death Gulch Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("DAWN.ULT"), "Dawn Map", tileSets, 64, 64));
            ugc.addMapConfig(UltimaMapConfig(U23MapFileIndexConfig("EXODUS.ULT"), "Castle of Death Map", tileSets, 64, 64));
            break;

        } case ULTIMA4: {
            IndexedImageFormat u4vgaFormat = IndexedImageFormat(darkcore::image::VGA, ColorPalette::loadVGAPalette(path + "/U4VGA.PAL"));

            ugc.addImageConfig(UltimaImageConfig("SHAPES.CGA", "CGA Tileset", 256, 16, 16, cgaFormat, UltimaImageConfig::INTERLACED));
            ugc.addImageConfig(UltimaImageConfig("SHAPES.EGA", "EGA Tileset", 256, 16, 16, egaFormat));
            ugc.addImageConfig(UltimaImageConfig("SHAPES.VGA", "VGA Tileset", 256, 16, 16, u4vgaFormat));
            ugc.addImageConfig(UltimaImageConfig("CHARSET.CGA", "CGA Charset", 128, 8, 8, cgaFormat, UltimaImageConfig::INTERLACED));
            ugc.addImageConfig(UltimaImageConfig("CHARSET.EGA", "EGA Charset", 128, 8, 8, egaFormat)); // TODO: file is dbl the size it should be - why???
            ugc.addImageConfig(UltimaImageConfig("CHARSET.VGA", "VGA Charset", 128, 8, 8, u4vgaFormat));

            std::map<int,const UltimaImageConfig *> tileSets;
            tileSets[2] = &ugc.getImageConfig("SHAPES.CGA");
            tileSets[4] = &ugc.getImageConfig("SHAPES.EGA");
            tileSets[8] = &ugc.getImageConfig("SHAPES.VGA");

            // britannia map is broken into 64 32x32 chunks
            MapSeqIndexConfig seqChunkIndex(64);
            UltimaMapConfig u4BritChunks("WORLD.MAP", "Britannia Map Chunks", tileSets, 32, 32, 64, NULL, UltimaMapConfig::CHUNK);
            ugc.addMapConfig(UltimaMapConfig("WORLD.MAP", seqChunkIndex, "Britannia Map", tileSets, 256, 256, 1, &u4BritChunks));

            ugc.addMapConfig(UltimaMapConfig("LCB_1.ULT", "Lord British's Castle (Level 1) Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("LCB_2.ULT", "Lord British's Castle (Level 2) Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("LYCAEUM.ULT", "Lycaeum Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("EMPATH.ULT", "Empath Abbey Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("SERPENT.ULT", "Serpent's Hold Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("MOONGLOW.ULT", "Moonglow Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("BRITAIN.ULT", "Britain Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("JHELOM.ULT", "Jhelom Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("YEW.ULT", "Yew Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("MINOC.ULT", "Minoc Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("TRINSIC.ULT", "Trinsic Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("SKARA.ULT", "Skara Brae Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("MAGINCIA.ULT", "Magincia Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("PAWS.ULT", "Paws Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("DEN.ULT", "Buccaneer's Den Map", tileSets, 32, 32));
            ugc.addMapConfig(UltimaMapConfig("VESPER.ULT", "Vesper Map", tileSets, 32, 32));
            break;

        } case ULTIMA5: {
            ugc.addImageConfig(UltimaImageConfig(
                        UltimaFileConfig("TILES.4", UltimaFileConfig::COMPRESSED),
                        "CGA Tileset", 512, 16, 16, cgaFormat));
            ugc.addImageConfig(UltimaImageConfig(
                        UltimaFileConfig("TILES.16", UltimaFileConfig::COMPRESSED),
                        "EGA Tileset", 512, 16, 16, egaFormat));

            std::map<int,const UltimaImageConfig *> tileSets;
            tileSets[2] = &ugc.getImageConfig("TILES.4");
            tileSets[4] = &ugc.getImageConfig("TILES.16");

            // Overworld chunk index is in DATA.OVL at 0x3886.
            // Each byte in index represents a Chunk Number
            // or 0xFF if all "deep water" (tile 0x01).
            // There are 51 compressed water chunks and 205 normal chunks (256 total)
            MapFileIndexConfig britanniaChunkIndex(UltimaFileConfig("DATA.OVL", Flags::NONE, 0x3886), 256);

            UltimaMapConfig u5BritChunks("BRIT.DAT", "Britannia Map Chunks", tileSets, 16, 16, 205, NULL, UltimaMapConfig::CHUNK);
            ugc.addMapConfig(UltimaMapConfig("BRIT.DAT", britanniaChunkIndex, "Britannia Map", tileSets, 256, 256, 1, &u5BritChunks,
                        UltimaMapConfig::COMPRESSED_WATER_CHUNK));

            // for reference - these are the compressed water chunks
            /* int u5emptyChunks[51] = {
                0x00, 0x01, 0x0D, 0x10, 0x1F, 0x2F, 0x6B, 0x77, 0x78, 0x79,
                0x7C, 0x80, 0x87, 0x88, 0x89, 0x8C, 0x97, 0x99, 0x9A, 0x9B,
                0x9C, 0x9F, 0xA7, 0xA9, 0xAA, 0xAD, 0xAE, 0xAF, 0xB0, 0xB1,
                0xB8, 0xB9, 0xBA, 0xBB, 0xBC, 0xBD, 0xC0, 0xC3, 0xC8, 0xC9,
                0xCC, 0xD0, 0xD3, 0xD8, 0xD9, 0xDC, 0xE0, 0xF0, 0xF1, 0xF2,
                0xF5 }; */

            // Underworld chunk index is sequential
            MapSeqIndexConfig underworldChunkIndex(256);

            UltimaMapConfig u5UnderChunks("UNDER.DAT", "Underworld Map Chunks", tileSets, 16, 16, 256, NULL, UltimaMapConfig::CHUNK);
            ugc.addMapConfig(UltimaMapConfig("UNDER.DAT", underworldChunkIndex, "Underworld Map", tileSets, 256, 256, 1, &u5UnderChunks));

            break;

        } case ULTIMA6: {
            IndexedImageFormat u6vgaFormat = IndexedImageFormat(darkcore::image::VGA, ColorPalette::loadVGAPalette(path + "/U6PAL"));

            // MCM - 8/26/2010
            // After several attempts to understand how the U6 EGA data is stored
            // I've given up on it for now so I can move onto other things.  The indices
            // don't make sense in the index file, and the mask types differ from those
            // of all other graphics types.  I may come back to it at some point, and
            // then I'll likely need to reverse engineer both U6EGA.DRV and GAME.EXE.
            // But for now it's out of scope for the initial coding sprint.

            // TODO - need to specify OBJTILES.* (uncompressed)

            int numImgs = 512;
            U6TileIndexConfig cgaIndex(numImgs, 2, UltimaFileConfig("TILEINDX.CGA"), UltimaFileConfig("MASKTYPE.CGA", UltimaFileConfig::COMPRESSED));
            //U6TileIndexConfig egaIndex(numImgs, 4, UltimaFileConfig("TILEINDX.EGA"), UltimaFileConfig("MASKTYPE.EGA", UltimaFileConfig::COMPRESSED));
            U6TileIndexConfig vgaIndex(numImgs, 8, UltimaFileConfig("TILEINDX.VGA"), UltimaFileConfig("MASKTYPE.VGA", UltimaFileConfig::COMPRESSED));

            ugc.addImageConfig(UltimaImageConfig(UltimaFileConfig("MAPTILES.CGA", UltimaFileConfig::COMPRESSED),
                        "CGA Tileset", numImgs, 16, 16, cgaFormat,
                        UltimaImageConfig::TYPEMASKED, &cgaIndex));
            /* ugc.addImageConfig(UltimaImageConfig("MAPTILES.EGA",
                        "EGA Tileset", numImgs, 16, 16, egaFormat,
                        UltimaImageConfig::BITMASKED | UltimaImageConfig::TYPEMASKED, &egaIndex)); */
            ugc.addImageConfig(UltimaImageConfig(UltimaFileConfig("MAPTILES.VGA", UltimaFileConfig::COMPRESSED),
                        "VGA Tileset", numImgs, 16, 16, u6vgaFormat,
                        UltimaImageConfig::TYPEMASKED, &vgaIndex));

            std::map<int,const UltimaImageConfig *> tileSets;
            tileSets[2] = &ugc.getImageConfig("MAPTILES.CGA");
            //tileSets[4] = &ugc.getImageConfig("MAPTILES.EGA");
            tileSets[8] = &ugc.getImageConfig("MAPTILES.VGA");

            // tiles are arranged into 8x8 chunks
            UltimaMapConfig u6BritChunks("CHUNKS", "Britannia Map Chunks", tileSets, 8, 8, 1024, NULL, UltimaMapConfig::CHUNK);
            // chunks are indexed within 16x16 superchunks
            UltimaMapConfig u6BritSuperChunks(U6ChunkIndexConfig("MAP", 256), "Britannia Map Superchunks", tileSets, 128, 128, 64,
                    &u6BritChunks, UltimaMapConfig::CHUNK);
            // superchunks are organized sequentially into an 8x8 map
            ugc.addMapConfig(UltimaMapConfig("MAP", MapSeqIndexConfig(64), "Britannia Map", tileSets, 1024, 1024, 1, &u6BritSuperChunks));

            break;
        }
    }

    return ugc;
}
