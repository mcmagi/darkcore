/* MapFileIndexConfig.cpp */

#include <cassert>                      // assert
#include <iostream>
#include <memory>                       // std::unique_ptr
#include <typeinfo>                     // typeid
#include "config/IndexConfig.h"         // IndexConfig
#include "config/IndexEntry.h"          // IndexEntry
#include "config/MapFileIndexConfig.h"  // MapFileIndexConfig
#include "util/Reader.h"                // Reader
#include "util/StreamReader.h"          // StreamReader
#include "util/Writer.h"                // Writer

using namespace std;
using darkcore::config::IndexConfig;
using darkcore::config::IndexEntry;
using darkcore::config::MapFileIndexConfig;
using darkcore::util::Reader;
using darkcore::util::StreamReader;
using darkcore::util::Writer;

MapFileIndexConfig::MapFileIndexConfig(const string &filename, int size) :
    IndexConfig(size), fileCfg(filename)
{
}

MapFileIndexConfig::MapFileIndexConfig(const UltimaFileConfig &fileCfg, int size) :
    IndexConfig(size), fileCfg(fileCfg)
{
}

MapFileIndexConfig::MapFileIndexConfig(const MapFileIndexConfig &idx) :
    IndexConfig(idx), fileCfg(idx.fileCfg)
{
}

MapFileIndexConfig::~MapFileIndexConfig()
{
}

IndexConfig & MapFileIndexConfig::operator=(const IndexConfig &idx)
{
    assert(typeid(idx) == typeid(*this));
    return this->operator=(dynamic_cast<const MapFileIndexConfig &>(idx));
}

MapFileIndexConfig & MapFileIndexConfig::operator=(const MapFileIndexConfig &idx)
{
    if (this != &idx)
    {
        IndexConfig::operator=(idx);
        fileCfg = idx.fileCfg;
    }
    return *this;
}

unique_ptr<IndexConfig> MapFileIndexConfig::clone() const
{
    return unique_ptr<IndexConfig>(new MapFileIndexConfig(*this));
}

unique_ptr<Reader<IndexEntry> > MapFileIndexConfig::newEntryReader() const
{
    return unique_ptr<Reader<IndexEntry> >(new IndexEntryReader(*this));
}

unique_ptr<Writer<IndexEntry> > MapFileIndexConfig::newEntryWriter() const
{
    // TODO: Writer not implemented for now
    return unique_ptr<Writer<IndexEntry> >(new IndexEntryWriter(*this));
}

unique_ptr<IndexConfig> MapFileIndexConfig::forFile(const string &newFilename) const
{
    unique_ptr<IndexConfig> idxCfg = clone();
    MapFileIndexConfig* mapFileIdxCfg = dynamic_cast<MapFileIndexConfig *>(idxCfg.get());
    mapFileIdxCfg->fileCfg = *std::move(fileCfg.forFile(newFilename));
    return idxCfg;
}

// inner class: IndexEntryReader

MapFileIndexConfig::IndexEntryReader::IndexEntryReader(const MapFileIndexConfig &cfg) :
    StreamReader<IndexEntry>(cfg.getFileConfig().newInputStream()), cfg(cfg), counter(0)
{
}

MapFileIndexConfig::IndexEntryReader::~IndexEntryReader()
{
}

IndexEntry MapFileIndexConfig::IndexEntryReader::read()
{
    unsigned char c = 0;
    getInputStream().read((char *) &c, 1);
    counter++;
    return IndexEntry(c);
}

bool MapFileIndexConfig::IndexEntryReader::end() const
{
    return cfg.getSize() >= 0 && counter >= cfg.getSize();
}

// inner class: IndexEntryWriter

MapFileIndexConfig::IndexEntryWriter::IndexEntryWriter(const MapFileIndexConfig &cfg) :
    StreamWriter<IndexEntry>(cfg.getFileConfig().newOutputStream()), cfg(cfg)
{
}

MapFileIndexConfig::IndexEntryWriter::~IndexEntryWriter()
{
}

void MapFileIndexConfig::IndexEntryWriter::write(const IndexEntry &entry)
{
    unsigned char c = (unsigned char) entry.getOffset();
    getOutputStream().write((char *) &c, 1);
}