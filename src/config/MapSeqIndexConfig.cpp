/* MapSeqIndexConfig.cpp */

#include <cassert>                      // assert
#include <iostream>
#include <memory>                       // std::unique_ptr
#include <typeinfo>                     // typeid
#include "config/IndexConfig.h"         // IndexConfig
#include "config/IndexEntry.h"          // IndexEntry
#include "config/MapSeqIndexConfig.h"   // MapSeqIndexConfig
#include "util/Reader.h"                // Reader
#include "util/Writer.h"                // Writer

using namespace std;
using darkcore::config::IndexConfig;
using darkcore::config::IndexEntry;
using darkcore::config::MapSeqIndexConfig;
using darkcore::util::Reader;
using darkcore::util::Writer;

MapSeqIndexConfig::MapSeqIndexConfig(int size) :
    IndexConfig(size)
{
}

MapSeqIndexConfig::MapSeqIndexConfig(const MapSeqIndexConfig &idx) :
    IndexConfig(idx)
{
}

MapSeqIndexConfig::~MapSeqIndexConfig()
{
}

IndexConfig & MapSeqIndexConfig::operator=(const IndexConfig &idx)
{
    assert(typeid(idx) == typeid(*this));
    return this->operator=(dynamic_cast<const MapSeqIndexConfig &>(idx));
}

MapSeqIndexConfig & MapSeqIndexConfig::operator=(const MapSeqIndexConfig &idx)
{
    if (this != &idx)
    {
        IndexConfig::operator=(idx);
    }
    return *this;
}

unique_ptr<IndexConfig> MapSeqIndexConfig::clone() const
{
    return unique_ptr<IndexConfig>(new MapSeqIndexConfig(*this));
}

unique_ptr<Reader<IndexEntry> > MapSeqIndexConfig::newEntryReader() const
{
    return unique_ptr<Reader<IndexEntry> >(new IndexEntryReader(*this));
}

unique_ptr<Writer<IndexEntry> > MapSeqIndexConfig::newEntryWriter() const
{
    // it's a virtual index, so Writer not implemented
    return unique_ptr<Writer<IndexEntry> >();
}

MapSeqIndexConfig::IndexEntryReader::IndexEntryReader(const MapSeqIndexConfig &cfg) :
    Reader<IndexEntry>(), cfg(cfg), counter(0)
{
}

MapSeqIndexConfig::IndexEntryReader::~IndexEntryReader()
{
}

IndexEntry MapSeqIndexConfig::IndexEntryReader::read()
{
    return IndexEntry(counter++);
}

bool MapSeqIndexConfig::IndexEntryReader::end() const
{
    return cfg.getSize() >= 0 && counter >= cfg.getSize();
}
