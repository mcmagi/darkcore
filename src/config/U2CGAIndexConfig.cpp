/* U2CGAIndexConfig.cpp */

#include <cassert>                      // assert
#include <memory>                       // std::unique_ptr
#include <typeinfo>                     // typeid
#include "config/IndexConfig.h"         // IndexConfig
#include "config/IndexEntry.h"          // IndexEntry
#include "config/U2CGAIndexConfig.h"    // U2CGAIndexConfig
#include "util/Reader.h"                // Reader
#include "util/Writer.h"                // Writer

using namespace std;
using darkcore::config::IndexConfig;
using darkcore::config::IndexEntry;
using darkcore::config::U2CGAIndexConfig;
using darkcore::config::U2CGAIndexConfig;
using darkcore::util::Reader;
using darkcore::util::Writer;

U2CGAIndexConfig::U2CGAIndexConfig(int size, int padding) :
    IndexConfig(size), padding(padding), imgSize(16 * 16 / 4)
{
}

U2CGAIndexConfig::U2CGAIndexConfig(const U2CGAIndexConfig &idx) :
    IndexConfig(idx), padding(idx.padding), imgSize(idx.imgSize)
{
}

U2CGAIndexConfig::~U2CGAIndexConfig()
{
}

IndexConfig & U2CGAIndexConfig::operator=(const IndexConfig &idx)
{
    assert(typeid(idx) == typeid(*this));
    return this->operator=(dynamic_cast<const U2CGAIndexConfig &>(idx));
}

U2CGAIndexConfig & U2CGAIndexConfig::operator=(const U2CGAIndexConfig &idx)
{
    if (this != &idx)
    {
        IndexConfig::operator=(idx);
        padding = idx.padding;
        imgSize = idx.imgSize;
    }
    return *this;
}

unique_ptr<IndexConfig> U2CGAIndexConfig::clone() const
{
    return unique_ptr<IndexConfig>(new U2CGAIndexConfig(*this));
}

unique_ptr<Reader<IndexEntry> > U2CGAIndexConfig::newEntryReader() const
{
    return unique_ptr<Reader<IndexEntry> >(new IndexEntryReader(*this));
}

unique_ptr<Writer<IndexEntry> > U2CGAIndexConfig::newEntryWriter() const
{
    // it's a virtual index, so Writer not implemented for U2
    return unique_ptr<Writer<IndexEntry> >();
}

IndexEntry U2CGAIndexConfig::IndexEntryReader::read()
{
    IndexEntry entry(offset);
    counter++;

    if (counter < 64)
    	offset += cfg.getPadding() + cfg.getImageSize();
    else
		offset = extraOffset + cfg.getPadding();
    return entry;
}
