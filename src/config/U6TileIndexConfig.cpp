/* U6TileIndexConfig.cpp */

#include <cassert>                      // assert
#include <fstream>                      // ifstream, ofstream, ios
#include <iostream>
#include <memory>                       // std::unique_ptr
#include <typeinfo>                     // typeid
#include "config/IndexConfig.h"         // IndexConfig
#include "config/IndexEntry.h"          // IndexEntry
#include "config/UltimaFileConfig.h"    // UltimaFileConfig
#include "config/U6TileIndexConfig.h"   // U6TileIndexConfig
#include "util/File.h"                  // File
#include "util/Reader.h"                // Reader
#include "util/Writer.h"                // Writer

using namespace std;
using darkcore::config::IndexConfig;
using darkcore::config::IndexEntry;
using darkcore::config::U6TileIndexConfig;
using darkcore::util::Reader;
using darkcore::util::Writer;

U6TileIndexConfig::U6TileIndexConfig(int size, int depth,
        const UltimaFileConfig &indexFileCfg,
        const UltimaFileConfig &maskTypeFileCfg) :
    IndexConfig(size), depth(depth),
    indexFileCfg(indexFileCfg), maskTypeFileCfg(maskTypeFileCfg)
{
}

U6TileIndexConfig::U6TileIndexConfig(const U6TileIndexConfig &idx) :
    IndexConfig(idx), depth(idx.depth),
    indexFileCfg(idx.indexFileCfg), maskTypeFileCfg(idx.maskTypeFileCfg)
{
}

U6TileIndexConfig::~U6TileIndexConfig()
{
}

IndexConfig & U6TileIndexConfig::operator=(const IndexConfig &idx)
{
    assert(typeid(idx) == typeid(*this));
    return this->operator=(dynamic_cast<const U6TileIndexConfig &>(idx));
}

U6TileIndexConfig & U6TileIndexConfig::operator=(const U6TileIndexConfig &idx)
{
    if (this != &idx)
    {
        IndexConfig::operator=(idx);
        depth = idx.depth;
        indexFileCfg = idx.indexFileCfg;
        maskTypeFileCfg = idx.maskTypeFileCfg;
    }
    return *this;
}

unique_ptr<IndexConfig> U6TileIndexConfig::clone() const
{
    return unique_ptr<IndexConfig>(new U6TileIndexConfig(*this));
}

unique_ptr<Reader<IndexEntry> > U6TileIndexConfig::newEntryReader() const
{
    return unique_ptr<Reader<IndexEntry> >(new IndexEntryReader(*this));
}

unique_ptr<Writer<IndexEntry> > U6TileIndexConfig::newEntryWriter() const
{
    return unique_ptr<Writer<IndexEntry> >(new IndexEntryWriter(*this));
}

U6TileIndexConfig::IndexEntryReader::IndexEntryReader(const U6TileIndexConfig &cfg) :
    Reader<IndexEntry>(), cfg(cfg),
    indexStream(cfg.getIndexFileConfig().newInputStream()),
    maskTypeStream(cfg.getMaskTypeFileConfig().newInputStream())
{
}

U6TileIndexConfig::IndexEntryReader::~IndexEntryReader()
{
}

IndexEntry U6TileIndexConfig::IndexEntryReader::read()
{
    int offset = 0;
    indexStream->read((char *) &offset, 2); // XXX: a hack (but it works!)

    char type = 0;
    maskTypeStream->read(&type, 1);

    IndexEntry::MaskType maskType;
    if (cfg.getDepth() == 2)
    {
        CGAMaskType cgaType = (CGAMaskType) type;
        if (cgaType == CGA_NORMAL)
            maskType = IndexEntry::NORMAL;
        else if (cgaType == CGA_PIXEL_BLOCK)
            maskType = IndexEntry::U6CGA_PIXEL_BLOCK;
    }
    else if (cfg.getDepth() == 8)
    {
        VGAMaskType vgaType = (VGAMaskType) type;
        if (vgaType == VGA_NORMAL)
            maskType = IndexEntry::NORMAL;
        else if (vgaType == VGA_TRANSPARENT)
            maskType = IndexEntry::TRANSPARENT;
        else if (vgaType == VGA_PIXEL_BLOCK)
            maskType = IndexEntry::U6VGA_PIXEL_BLOCK;
    }

    return IndexEntry(offset * 16, maskType);
}

bool U6TileIndexConfig::IndexEntryReader::end() const
{
    // technically, these should hit EOF at the same time,
    // but I don't know a better way with two streams, so... yeah.
    return indexStream->eof() || maskTypeStream->eof();
}

U6TileIndexConfig::IndexEntryWriter::IndexEntryWriter(const U6TileIndexConfig &cfg) :
    Writer<IndexEntry>(), cfg(cfg),
    indexStream(cfg.getIndexFileConfig().newOutputStream()),
    maskTypeStream(cfg.getMaskTypeFileConfig().newOutputStream())
{
}

U6TileIndexConfig::IndexEntryWriter::~IndexEntryWriter()
{
}

void U6TileIndexConfig::IndexEntryWriter::write(const IndexEntry &entry)
{
    int offset = entry.getOffset() / 16;
    indexStream->write((char *) &offset, 2); // XXX: inspired by the hack a few lines above

    char type;
    if (cfg.getDepth() == 2)
    {
        CGAMaskType cgaType;
        if (type == IndexEntry::NORMAL)
            cgaType = CGA_NORMAL;
        else if (type == IndexEntry::U6CGA_PIXEL_BLOCK)
            cgaType = CGA_PIXEL_BLOCK;

        type = (char) cgaType;
    }
    else if (cfg.getDepth() == 8)
    {
        VGAMaskType vgaType;
        if (type == IndexEntry::NORMAL)
            vgaType = VGA_NORMAL;
        else if (type == IndexEntry::TRANSPARENT)
            vgaType = VGA_TRANSPARENT;
        else if (type == IndexEntry::U6VGA_PIXEL_BLOCK)
            vgaType = VGA_PIXEL_BLOCK;

        type = (char) vgaType;
    }

    maskTypeStream->write(&type, 1);
}
