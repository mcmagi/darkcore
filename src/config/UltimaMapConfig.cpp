/* UltimaMapConfig.cpp */

#include <iostream>
#include <map>                          // std::map
#include <memory>                       // std::unique_ptr
#include <sstream>                      // std::stringstream
#include <string>                       // std::string
#include <typeinfo>                     // std::bad_cast
#include "Exception.h"                  // Exception
#include "config/Flags.h"               // Flags
#include "config/IndexConfig.h"         // IndexConfig
#include "config/MapFileIndexConfig.h"  // MapFileIndexConfig
#include "config/UltimaFileConfig.h"    // UltimaFileConfig
#include "config/UltimaGameConfig.h"    // UltimaGameConfig
#include "config/UltimaImageConfig.h"   // UltimaImageConfig
#include "config/UltimaMapConfig.h"     // UltimaMapConfig

using namespace std;
using darkcore::Exception;
using darkcore::config::Flags;
using darkcore::config::IndexConfig;
using darkcore::config::MapFileIndexConfig;
using darkcore::config::UltimaFileConfig;
using darkcore::config::UltimaImageConfig;
using darkcore::config::UltimaMapConfig;

const Flags UltimaMapConfig::CHUNK = 0x01;
const Flags UltimaMapConfig::COMPRESSED_WATER_CHUNK = 0x02;

UltimaMapConfig::UltimaMapConfig(
        const string &filename, const string &description,
        const map<int,const UltimaImageConfig *> &tileSetCfgs,
        int width, int height, int numMaps, const UltimaMapConfig *chunkCfg,
        const Flags &flags) :
    gameCfg(NULL), idxCfg(new MapFileIndexConfig(filename)), description(description),
    tileSetCfgs(tileSetCfgs), width(width), height(height), numMaps(numMaps),
    flags(flags), chunkCfg(chunkCfg == NULL ? NULL : new UltimaMapConfig(*chunkCfg)),
    filename(filename)
{
    if (idxCfg->getSize() == -1)
        idxCfg->setSize(getDefaultIndexSize());

    /* cout << "filename=" << fileCfg.getFilename()
        << ",flags=" << bool(flags)
        << ",this->flags=" << bool(flags)
        << ",flags&CHUNKED=" << bool(flags & CHUNKED)
        << ",isChunked=" << isChunked() << endl; */
}

UltimaMapConfig::UltimaMapConfig(
        const UltimaFileConfig &fileCfg, const string &description,
        const map<int,const UltimaImageConfig *> &tileSetCfgs,
        int width, int height, int numMaps, const UltimaMapConfig *chunkCfg,
        const Flags &flags) :
    gameCfg(NULL), idxCfg(new MapFileIndexConfig(fileCfg)), description(description),
    tileSetCfgs(tileSetCfgs), width(width), height(height), numMaps(numMaps),
    flags(flags), chunkCfg(chunkCfg == NULL ? NULL : new UltimaMapConfig(*chunkCfg)),
    filename(fileCfg.getFilename())
{
    if (idxCfg->getSize() == -1)
        idxCfg->setSize(getDefaultIndexSize());

    /* cout << "filename=" << fileCfg.getFilename()
        << ",flags=" << bool(flags)
        << ",this->flags=" << bool(flags)
        << ",flags&CHUNKED=" << bool(flags & CHUNKED)
        << ",isChunked=" << isChunked() << endl; */
}

UltimaMapConfig::UltimaMapConfig(
        const MapFileIndexConfig &idxCfg, const string &description,
        const map<int,const UltimaImageConfig *> &tileSetCfgs,
        int width, int height, int numMaps, const UltimaMapConfig *chunkCfg,
        const Flags &flags) :
    gameCfg(NULL), idxCfg(idxCfg.clone()), description(description),
    tileSetCfgs(tileSetCfgs), width(width), height(height), numMaps(numMaps),
    flags(flags), chunkCfg(chunkCfg == NULL ? NULL : new UltimaMapConfig(*chunkCfg)),
    filename(idxCfg.getFileConfig().getFilename())
{
    // constrain size of index if it has no constraint
    if (this->idxCfg->getSize() == -1)
        this->idxCfg->setSize(getDefaultIndexSize());

    /* cout << "filename=" << fileCfg.getFilename()
        << ",flags=" << bool(flags)
        << ",this->flags=" << bool(flags)
        << ",flags&CHUNKED=" << bool(flags & CHUNKED)
        << ",isChunked=" << isChunked() << endl; */
}

UltimaMapConfig::UltimaMapConfig(
        const string &filename, const IndexConfig &idxCfg, const string &description,
        const map<int,const UltimaImageConfig *> &tileSetCfgs,
        int width, int height, int numMaps, const UltimaMapConfig *chunkCfg,
        const Flags &flags) :
    gameCfg(NULL), idxCfg(idxCfg.clone()), description(description),
    tileSetCfgs(tileSetCfgs), width(width), height(height), numMaps(numMaps),
    flags(flags), chunkCfg(chunkCfg == NULL ? NULL : new UltimaMapConfig(*chunkCfg)),
    filename(filename)
{
    // constrain size of index if it has no constraint
    if (this->idxCfg->getSize() == -1)
        this->idxCfg->setSize(getDefaultIndexSize());

    /* cout << "filename=" << fileCfg.getFilename()
        << ",flags=" << bool(flags)
        << ",this->flags=" << bool(flags)
        << ",flags&CHUNKED=" << bool(flags & CHUNKED)
        << ",isChunked=" << isChunked() << endl; */
}

UltimaMapConfig::UltimaMapConfig(const UltimaMapConfig &cfg) :
    gameCfg(cfg.gameCfg), idxCfg(cfg.idxCfg->clone()), description(cfg.description),
    tileSetCfgs(cfg.tileSetCfgs), width(cfg.width), height(cfg.height), numMaps(cfg.numMaps),
    flags(cfg.flags), chunkCfg(cfg.chunkCfg.get() == NULL ? NULL : new UltimaMapConfig(*cfg.chunkCfg)),
    filename(cfg.filename)
{
}

UltimaMapConfig::~UltimaMapConfig()
{
}

UltimaMapConfig & UltimaMapConfig::operator=(const UltimaMapConfig &cfg)
{
    if (this != &cfg)
    {
        gameCfg = cfg.gameCfg;
        idxCfg = cfg.idxCfg->clone();
        description = cfg.description;
        tileSetCfgs = cfg.tileSetCfgs;
        numMaps = cfg.numMaps;
        width = cfg.width;
        height = cfg.height;
        flags = cfg.flags;
        chunkCfg = unique_ptr<UltimaMapConfig>(cfg.chunkCfg.get() == NULL ? NULL : new UltimaMapConfig(*cfg.chunkCfg));
        filename = cfg.filename;
    }
    return *this;
}

bool UltimaMapConfig::hasTileSetConfig(int bpp) const
{
    TileSetMap::const_iterator it = tileSetCfgs.find(bpp);
    return it != tileSetCfgs.end();
}

const UltimaImageConfig & UltimaMapConfig::getTileSetConfig(int bpp) const
{
    if (bpp == 0)
        return getBestTileSetConfig();
    else if (! hasTileSetConfig(bpp))
    {
        stringstream ss;
        ss << "No tileset config with bpp" << bpp << " for " << description;
        throw Exception(ss.str());
    }

    //return *(tileSetCfgs[bpp]);
    return *(tileSetCfgs.find(bpp)->second);
}

const UltimaImageConfig & UltimaMapConfig::getBestTileSetConfig() const
{
    if (tileSetCfgs.empty())
        throw Exception(string("No tilesets configured for ") += description);

    return *(tileSetCfgs.rbegin()->second);
}

string UltimaMapConfig::getDefaultFilename() const
{
    string filename;

    try
    {
        // if indexConfig represents a file, use that as the default
        MapFileIndexConfig* mapFileIdxCfg = dynamic_cast<MapFileIndexConfig *>(idxCfg.get());
        filename = mapFileIdxCfg->getFileConfig().getFilename();
    }
    catch (const bad_cast &e)
    {
        if (chunkCfg.get() != NULL)
            filename = chunkCfg->getFilename();
    }

    return filename;
}

UltimaMapConfig UltimaMapConfig::forFile(const string &newFilename) const
{
    // clone and override the filename
    UltimaMapConfig newCfg(*this);
    newCfg.filename = filename;
    try
    {
        // if indexConfig represents a file, change its filename
        MapFileIndexConfig* mapFileIdxCfg = dynamic_cast<MapFileIndexConfig *>(idxCfg.get());
        newCfg.idxCfg = mapFileIdxCfg->forFile(newFilename);
    }
    catch (const bad_cast &e)
    {
        // does not represent a file and is therefore not writeable
        throw Exception("Cannot write to map that's not backed by a MapFileIndexConfig");
    }

    return newCfg;
}

int UltimaMapConfig::getDefaultIndexSize() const
{
    // rules to determine the default size of the index:
    return chunkCfg.get() == NULL ?
        // if we're not a chunked map, index should be
        // the area of the map times the number of maps
        width * height * numMaps :
        // otherwise, it's simply the number of chunks
        chunkCfg->getNumMaps();
}
