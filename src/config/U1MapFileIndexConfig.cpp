/* U1MapFileIndexConfig.cpp */

#include <cassert>                      // assert
#include <cmath>                        // pow
#include <iostream>
#include <memory>                       // std::unique_ptr, std::move
#include <typeinfo>                     // typeid
#include "config/IndexConfig.h"         // IndexConfig
#include "config/IndexEntry.h"          // IndexEntry
#include "config/MapFileIndexConfig.h"  // MapFileIndexConfig
#include "config/U1MapFileIndexConfig.h"// U1MapFileIndexConfig
#include "util/Reader.h"                // Reader
#include "util/Writer.h"                // Writer

using namespace std;
using darkcore::config::IndexConfig;
using darkcore::config::IndexEntry;
using darkcore::config::MapFileIndexConfig;
using darkcore::config::U1MapFileIndexConfig;
using darkcore::util::Reader;
using darkcore::util::Writer;

U1MapFileIndexConfig::U1MapFileIndexConfig(const string &filename, int size) :
    MapFileIndexConfig(filename, size), bitSize(4), // hard-coded
    bitMask((int) pow((double) 2, bitSize) - 1),
    indexPerByte(8 / bitSize)
{
}

U1MapFileIndexConfig::U1MapFileIndexConfig(const UltimaFileConfig &fileCfg, int size) :
    MapFileIndexConfig(fileCfg, size), bitSize(4),
    bitMask((int) pow((double) 2, bitSize) - 1),
    indexPerByte(8 / bitSize)
{
}

U1MapFileIndexConfig::U1MapFileIndexConfig(const U1MapFileIndexConfig &idx) :
    MapFileIndexConfig(idx), bitSize(idx.bitSize), bitMask(idx.bitMask),
    indexPerByte(idx.indexPerByte)
{
}

U1MapFileIndexConfig::~U1MapFileIndexConfig()
{
}

IndexConfig & U1MapFileIndexConfig::operator=(const IndexConfig &idx)
{
    assert(typeid(idx) == typeid(*this));
    return this->operator=(dynamic_cast<const U1MapFileIndexConfig &>(idx));
}

MapFileIndexConfig & U1MapFileIndexConfig::operator=(const MapFileIndexConfig &idx)
{
    assert(typeid(idx) == typeid(*this));
    return this->operator=(dynamic_cast<const U1MapFileIndexConfig &>(idx));
}

U1MapFileIndexConfig & U1MapFileIndexConfig::operator=(const U1MapFileIndexConfig &idx)
{
    if (this != &idx)
    {
        MapFileIndexConfig::operator=(idx);
        bitSize = idx.bitSize;
        bitMask = idx.bitMask;
        indexPerByte = idx.indexPerByte;
    }
    return *this;
}

unique_ptr<IndexConfig> U1MapFileIndexConfig::clone() const
{
    return unique_ptr<IndexConfig>(new U1MapFileIndexConfig(*this));
}

unique_ptr<Reader<IndexEntry> > U1MapFileIndexConfig::newEntryReader() const
{
    // wrap the reader with our own
    return unique_ptr<Reader<IndexEntry> >(
            new IndexEntryReader(*this, MapFileIndexConfig::newEntryReader()));
}

unique_ptr<Writer<IndexEntry> > U1MapFileIndexConfig::newEntryWriter() const
{
    // TODO: Writer not implemented for now
    return unique_ptr<Writer<IndexEntry> >();
}

U1MapFileIndexConfig::IndexEntryReader::IndexEntryReader(
        const U1MapFileIndexConfig &cfg, unique_ptr<Reader<IndexEntry> > reader) :
    Reader<IndexEntry>(), cfg(cfg), reader(std::move(reader)),
    counter(0), idxCtr(0), data(0)
{
}

U1MapFileIndexConfig::IndexEntryReader::~IndexEntryReader()
{
}

IndexEntry U1MapFileIndexConfig::IndexEntryReader::read()
{
    if (idxCtr == 0)
    {
        // start index counter at most HO index
        idxCtr = cfg.getIndexPerByte();

        // read new entry into index data
        IndexEntry entry = reader->read();
        data = entry.getOffset();
    }

    // extract index from data
    int index = cfg.getBitMask() & (data >> (--idxCtr * cfg.getBitSize()));

    // perform mapping of index before returning it
    counter++;
    return IndexEntry(mapIndex(index));
}

int U1MapFileIndexConfig::IndexEntryReader::mapIndex(int value) const
{
    // byte -> index
    // 0 -> 0       (water)
    // 1 -> 1       (grass)
    // 2 -> 2       (forest)
    // 3 -> 3       (mountain)
    // 4 -> 4/5     (towne)
    // 5 -> 6       (sign)
    // 6 -> 7/8     (castle)
    // 7 -> 9       (dungeon)

    if (value >= 7) // dungeon
        value++;
    if (value >= 5) // sign, castle, & dungeon
        value++;
    return value;
}
