/* FileException.cpp */

#include <sstream>              // std::ostringstream
#include <string>               // std::string
#include "Exception.h"          // Exception, FileException

using namespace std;
using darkcore::Exception;
using darkcore::FileException;

FileException::FileException(const string &errmsg, const string &file) :
    Exception(errmsg), filename(file)
{
}

FileException::FileException(const FileException &ex) :
    Exception(ex), filename(ex.filename)
{
}

FileException::~FileException() throw ()
{
}

string FileException::getMessage() const throw ()
{
    ostringstream stream;
    stream << "filename=" << filename << ": ";
    stream << Exception::getMessage();
    return stream.str().data();
}