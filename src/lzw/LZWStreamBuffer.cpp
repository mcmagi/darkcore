/* LZWStreamBuffer.cpp */

#include <cstdio>                       // BUFSIZ
#include <iostream>
#include <istream>                      // std::istream
#include <memory>                       // std::unique_ptr, std::move
#include <ostream>                      // std::ostream
#include <streambuf>                    // std::streamsize
#include <vector>                       // std::vector
#include "lzw/LZW.h"                    // Compress, Decompress
#include "lzw/LZWIOStream.h"            // LZWStreamBuffer
#include "util/ByteArrayIOStream.h"     // ByteArrayInputStream, ByteArrayOutputStream
#include "util/FilterIOStream.h"        // FilterStreamBuffer

using namespace std;
using darkcore::lzw::Compress;
using darkcore::lzw::Decompress;
using darkcore::lzw::LZWStreamBuffer;
using darkcore::util::ByteArrayInputStream;
using darkcore::util::ByteArrayOutputStream;
using darkcore::util::FilterStreamBuffer;

LZWStreamBuffer::LZWStreamBuffer(istream &is) :
    FilterStreamBuffer(is), bais(NULL), baos(NULL),
    inDecompData(NULL), outStart(0)
{
}

LZWStreamBuffer::LZWStreamBuffer(unique_ptr<istream> is) :
    FilterStreamBuffer(std::move(is)), bais(NULL), baos(NULL),
    inDecompData(NULL), outStart(0)
{
}

LZWStreamBuffer::LZWStreamBuffer(ostream &os) :
    FilterStreamBuffer(os), bais(NULL), baos(new ByteArrayOutputStream()),
    inDecompData(NULL), outStart(os.tellp())
{
}

LZWStreamBuffer::LZWStreamBuffer(unique_ptr<ostream> os) :
    FilterStreamBuffer(std::move(os)), bais(NULL), baos(new ByteArrayOutputStream()),
    inDecompData(NULL), outStart(os->tellp())
{
}

LZWStreamBuffer::~LZWStreamBuffer()
{
    if (bais != NULL)
    {
        delete bais;
        delete [] inDecompData;
    }

    if (baos != NULL)
        delete baos;
}

std::streamsize LZWStreamBuffer::showmanyc()
{
    if (bais == NULL)
        underflow();

    return bais->rdbuf()->in_avail();
}

int LZWStreamBuffer::pbackfail(int c)
{
    if (bais == NULL)
        underflow();

    bais->putback(c);
    return bais->bad() ? traits_type::eof() : traits_type::not_eof(c);
}

int LZWStreamBuffer::uflow()
{
    if (bais == NULL)
        underflow();

    return bais->get();
}

int LZWStreamBuffer::underflow()
{
    if (bais == NULL)
        decompressData();

    return bais->peek();
}

int LZWStreamBuffer::overflow(int c)
{
    baos->put(c);
    return c;
}

int LZWStreamBuffer::sync()
{
    // if we're not in the starting position, attempt to seek to it
    if (outStart != getOutputStream()->tellp())
        getOutputStream()->seekp(outStart);

    // compress the data
    compressData();

    return getOutputStream()->bad();
}

streamsize LZWStreamBuffer::xsgetn(char *s, streamsize n)
{
    if (bais == NULL)
        decompressData();

    bais->read(s, n);
    return bais->gcount();
}

streamsize LZWStreamBuffer::xsputn(const char *s, streamsize n)
{
    baos->write(s, n);
    return n;
}

streampos LZWStreamBuffer::seekoff(streamoff off, ios_base::seekdir way, ios_base::openmode which)
{
    streampos rc = -1;

    if (which & ios_base::out && baos != NULL)
    {
        baos->seekp(off, way);
        if (! baos->fail())
            rc = baos->tellp();
    }
    else if (which & ios_base::in && bais == NULL)
        decompressData();

    if (which & ios_base::in && bais != NULL)
    {
        bais->seekg(off, way);
        if (! bais->fail())
            rc = bais->tellg();
    }

    return rc;
}

void LZWStreamBuffer::compressData()
{
    // compress the data, copying to a temp buffer
    Compress comp((unsigned char *) baos->toArray(), baos->getSize());
    int datasize = comp.getCompressedSize();
    char outCompData[datasize];
    comp.getCompressedData((unsigned char *) outCompData);

    // output the compressed data to the wrapped output stream
    getOutputStream()->write(outCompData, datasize);
}

void LZWStreamBuffer::decompressData()
{
    // read in data from underlying stream into a vector
    vector<unsigned char> vbuf;
    vector<unsigned char> tmpbuf(BUFSIZ);
    while (! getInputStream()->eof())
    {
        // read the data from the in
        getInputStream()->read((char *) &tmpbuf[0], BUFSIZ);

        // reserve space for next BUFSIZ elements in the vector
        vbuf.reserve(vbuf.size() + BUFSIZ);

        // append them to the vector
        vbuf.insert(vbuf.end(), &tmpbuf[0], &tmpbuf[getInputStream()->gcount()]);
    }

    std::cout << "decompressing file of size: " << vbuf.size() << std::endl;

    // decompress the data in the vector
    Decompress decomp(&vbuf[0], vbuf.size());

    std::cout << "decompressed data size: " << decomp.getUncompressedSize() << std::endl;

    // create input stream to the uncompressed data
    int datasize = decomp.getUncompressedSize();
    inDecompData = new unsigned char [datasize];
    decomp.getUncompressedData(inDecompData);

    // create input stream to the uncompressed data
    bais = new ByteArrayInputStream(inDecompData, datasize);
}
