/* LZWInputStream.cpp */

#include <istream>                      // std::istream
#include <memory>                       // std::unique_ptr, std::move
#include "lzw/LZWIOStream.h"            // LZWInputStream, LZWStreamBuffer
#include "util/FilterIOStream.h"        // FilterInputStream

using namespace std;
using darkcore::lzw::LZWInputStream;
using darkcore::lzw::LZWStreamBuffer;
using darkcore::util::FilterInputStream;

LZWInputStream::LZWInputStream(istream &is) :
    FilterInputStream(new LZWStreamBuffer(is))
{
}

LZWInputStream::LZWInputStream(unique_ptr<istream> is) :
    FilterInputStream(new LZWStreamBuffer(std::move(is)))
{
}

LZWInputStream::~LZWInputStream()
{
    delete rdbuf();
}
