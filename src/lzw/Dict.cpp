/* Dict.cpp */

#include <iostream>                 // NULL
#include "lzw/Dict.h"               // Dict
#include "lzw/LZW.h"                // CompressionException

using darkcore::lzw::CompressionException;
using darkcore::lzw::Dict;


Dict::Dict(int size)
    : dict(NULL), contains(0x102), size(size)
{
    if (size > 0)
        dict = new struct dict_entry [size];
}

Dict::~Dict()
{
    if (dict != NULL)
        delete [] dict;
}

void Dict::add(unsigned char root, int codeword)
{
    if (contains == size)
        throw CompressionException("Dictionary full!");

    dict[contains].root = root;
    dict[contains++].codeword = codeword;
}
