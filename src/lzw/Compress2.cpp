/* Compress.cpp */

#include <iostream>                 // NULL
#include <string.h>                 // memcpy
#include "lzw/LZW.h"                // Compress

using darkcore::lzw::Compress;


// constants
int Compress::BITS = 9;
int Compress::HASHING_SHIFT = BITS - 8;
int Compress::MAX_VALUE = (1 << BITS) - 1;
int Compress::MAX_CODE = MAX_VALUE - 1;
int Compress::TABLE_SIZE = 5021;


Compress::Compress(const unsigned char *data, long length)
    : source(NULL), destination(NULL), source_length(length),
        destination_length(length), destination_pos(0), code_value(NULL),
        prefix_code(NULL), appended_char(NULL)
{
    if (length > 0)
    {
        // allocate supporting arrays
        code_value = new int [TABLE_SIZE];
        prefix_code = new unsigned int [TABLE_SIZE];
        appended_char = new unsigned char [TABLE_SIZE];

        // allocate data buffers
        source = new unsigned char [length];
        destination = new unsigned char [length];

        // load data into source buffer
        memcpy(source, data, source_length);

        // begin compression routine
        compress();
    }
}

Compress::~Compress()
{
    if (destination != NULL)
        delete [] destination;

    if (source != NULL)
        delete [] source;

    if (appended_char != NULL)
        delete [] appended_char;

    if (prefix_code != NULL)
        delete [] prefix_code;

    if (code_value != NULL)
        delete [] code_value;
}

void Compress::getCompressedData(unsigned char *data) const
{
    data[0] = (unsigned char) (destination_pos & 0xFF);
    data[1] = (unsigned char) ((destination_pos >> 8) & 0xFF);
    data[2] = (unsigned char) ((destination_pos >> 16) & 0xFF);
    data[3] = (unsigned char) ((destination_pos >> 24) & 0xFF);

    memcpy(&data[4], destination, destination_pos);
}


void Compress::outputCode(int code)
{
    static int output_bit_count = 0;
    static unsigned long output_bit_buffer = 0L;

    output_bit_buffer |=
        (unsigned long) code << (32 - BITS - output_bit_count);
    output_bit_count += BITS;

    while (output_bit_count >= 8)
    {
        // do bounds check on destination
        if (destination_pos == destination_length)
            // if this happens, the destination will be larger than the
            // source, which is possible and thus should be accounted for
            reallocDestination(destination_length + 4096);

        destination[destination_pos++] =
            (unsigned char) (output_bit_buffer >> 24);

        output_bit_buffer <<= 8;
        output_bit_count -= 8;
    }
}

void Compress::compress()
{
    unsigned int next_code = 0x100;
    //unsigned int character;
    unsigned int string_code;
    unsigned int index;
    int source_pos = 0;

    // clear string table
    for (int i = 0; i < TABLE_SIZE; i++)
        code_value[i] = -1;

    // read first character
    string_code = (unsigned int) source[source_pos++];

    // loop through rest of input
    for ( ; source_pos < source_length; source_pos++)
    {
        // find index of string code
        index = find_match(string_code, (unsigned int) source[source_pos]);

        // does code exist in table?
        if (code_value[index] != -1)
            // get code from table
            string_code = code_value[index];
        else
        {
            // add code to table
            if (next_code <= MAX_CODE)
            {
                code_value[index] = next_code++;
                prefix_code[index] = string_code;
                appended_char[index] = (unsigned int) source[source_pos];
            }
        }
        outputCode(string_code);
        string_code = (unsigned int) source[source_pos];
    }

    outputCode(string_code);
    outputCode(MAX_VALUE);
    outputCode(0);
}

int Compress::find_match(int hash_prefix, unsigned int hash_char)
{
    int index;
    int offset;

    index = (hash_char << HASHING_SHIFT) ^ hash_prefix;

    if (index == 0)
        offset = 1;
    else
        offset = TABLE_SIZE - index;

    while (true)
    {
        if (code_value[index] == -1)
            return index;

        if (prefix_code[index] == hash_prefix
            && appended_char[index] == hash_char)
            return index;

        index -= offset;

        if (index < 0)
            index += TABLE_SIZE;
    }
}

void Compress::reallocDestination(int newsize)
{
    unsigned char *new_destination = NULL;      // ptr to new space
    
    // allocate new space
    new_destination = new unsigned char [newsize];

    if (destination != NULL)
    {
        // copy existing data into new destination array
        memcpy(new_destination, destination, destination_length);

        // delete old destination array
        delete [] destination;
    }

    // set as destination pointer
    destination = new_destination;

    // resize buffer length
    destination_length = newsize;
}
