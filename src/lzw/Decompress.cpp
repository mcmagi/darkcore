/* Decompress.cpp */

#include <string.h>                 // memcpy
#include <iostream>                 // std::cerr, NULL
#include <stack>                    // std::stack
#include "lzw/LZW.h"                // Decompress, CompressionException
#include "lzw/Dict.h"               // Dict

using namespace std;
using darkcore::lzw::CompressionException;
using darkcore::lzw::Decompress;
using darkcore::lzw::Dict;


Decompress::Decompress(const unsigned char *data, long length) :
    source(NULL), destination(NULL), source_length(length - 4),
    destination_length(0), stack(), dict(NULL)
{
    if (isValidLZWData(data, length))
    {
        // determine the buffer sizes
        destination_length = readUncompressedSize(data);

        // get local copy of source data
        source = new unsigned char [source_length];
        memcpy(source, &data[4], source_length);

        // allocate space for destination data
        destination = new unsigned char [destination_length];

        // decompress source data
        decompress();
    }
    else
        throw CompressionException("not valid LZW compressed data");
}

Decompress::~Decompress()
{
    if (dict != NULL)
        delete dict;

    if (destination != NULL)
        delete [] destination;

    if (source != NULL)
        delete [] source;
}

// this function only checks a few *necessary* conditions
// returns "false" if the file doesn't satisfy these conditions
// return "true" otherwise
bool Decompress::isValidLZWData(const unsigned char *data, int length)
{
    // file must contain 4-byte size header and space for the 9-bit value 0x100
    if (length < 6) { return(false); }
    // the last byte of the size header must be 0
    //  (U6's files aren't *that* big)
    if (data[3] != 0) { return(false); }
    // the 9 bits after the size header must be 0x100
    if ((data[4] != 0) || ((data[5] & 1) != 1)) { return(false); }

    return(true);
}

long Decompress::readUncompressedSize(const unsigned char *data)
{
    return data[0] + (data[1]<<8) + (data[2]<<16) + (data[3]<<24);
}

// ----------------------------------------------
// Read the next code word from the source buffer
// ----------------------------------------------
int Decompress::getNextCodeword(long &bits_read, int codeword_size) const
{
    unsigned char b0, b1, b2;
    int codeword;
 
    int idx = bits_read / 8;
    b0 = readSource(idx);
    b1 = readSource(idx+1);
    b2 = readSource(idx+2);

    codeword = ((b2 << 16) + (b1 << 8) + b0);
    codeword = codeword >> (bits_read % 8);
    switch (codeword_size)
    {
      case 0x9:
        codeword = codeword & 0x1ff;
        break;
      case 0xa:
        codeword = codeword & 0x3ff;
        break;
      case 0xb:
        codeword = codeword & 0x7ff;
        break;
      case 0xc:
        codeword = codeword & 0xfff;
        break;
      default:
        cerr << "LZW Warning: weird codeword size!" << endl;
        break;
    }
    bits_read += codeword_size;

    return (codeword);
}

int Decompress::readSource(int index) const
{
    return index < source_length ? source[index] : 0;
}

void Decompress::outputRoot(unsigned char root, long &position)
{
    if (position == destination_length)
        throw CompressionException("Data length exceeds size in header");

    destination[position++] = root;
}

void Decompress::getString(int codeword)
{
    unsigned char root;
    int current_codeword;
 
    current_codeword = codeword;
    if (stack.empty())
        stack = std::stack<unsigned char>();
    while (current_codeword > 0xff)
    {
        root = dict->getRoot(current_codeword);
        current_codeword = dict->getCodeword(current_codeword);
        stack.push(root);
    }

    // push the root at the leaf
    stack.push((unsigned char)current_codeword);
}

// -----------------------------------------------------------------------------
// LZW-decompress from buffer to buffer.
// The parameters "source_length" and "destination_length" are currently unused.
// They might be used to prevent reading/writing outside the buffers.
// -----------------------------------------------------------------------------
void Decompress::decompress()
{
    const int max_codeword_length = 12;

    bool end_marker_reached = false;
    int codeword_size = 9;
    long bits_read = 0; 
    int next_free_codeword = 0x102;
    int dictionary_size = 0x200;

    long bytes_written = 0;

    int cW;
    int pW;
    unsigned char C;

    while (! end_marker_reached)
    {
        cW = getNextCodeword(bits_read, codeword_size);
        switch (cW)
        {
          // re-init the dictionary
          case 0x100:
            codeword_size = 9;
            next_free_codeword = 0x102;
            dictionary_size = 0x200;
            initDictionary();
            cW = getNextCodeword(bits_read, codeword_size);
            outputRoot((unsigned char)cW, bytes_written);
            break;
          // end of compressed file has been reached
          case 0x101:
            end_marker_reached = true;
            break;
          // (cW <> 0x100) && (cW <> 0x101)
          default:
            if (cW < next_free_codeword)
            {
                // codeword is already in the dictionary

                // create the string associated with cW (on the stack)
                getString(cW);
                C = stack.top();
                // output the string represented by cW
                while (! stack.empty())
                {
                    outputRoot(stack.top(), bytes_written);
                    stack.pop();
                }

                // add pW+C to the dictionary
                dict->add(C,pW);

                next_free_codeword++;
                if (next_free_codeword >= dictionary_size)
                {
                    if (codeword_size < max_codeword_length)
                    {
                        codeword_size += 1;
                        dictionary_size *= 2;
                    }
                }
            }
            else  // codeword is not yet defined
            {
                // create the string associated with pW (on the stack)
                getString(pW);
                C = stack.top();
                // output the string represented by pW
                while (! stack.empty())
                {
                    outputRoot(stack.top(), bytes_written);
                    stack.pop();
                }

                // output the char C
                outputRoot(C, bytes_written);
                // the new dictionary entry must correspond to cW
                // if it doesn't, something is wrong with the
                // lzw-compressed data.
                if (cW != next_free_codeword)
                    throw CompressionException("invalid LZW codeword");

                // add pW+C to the dictionary
                dict->add(C,pW);

                next_free_codeword++;
                if (next_free_codeword >= dictionary_size)
                {
                    if (codeword_size < max_codeword_length)
                    {                   
                        codeword_size += 1;
                        dictionary_size *= 2;
                    } 
                }
            };
            break;
        }
        // shift roles - the current cW becomes the new pW
        pW = cW;
    }
}

void Decompress::initDictionary()
{
    if (dict != NULL)
        delete dict;

    dict = new Dict(10000);
}
