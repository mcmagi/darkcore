/* Compress.cpp */

#include <iostream>                 // NULL
#include <string.h>                 // memcpy
#include "lzw/LZW.h"                // Compress

using namespace std;
using darkcore::lzw::Compress;


// static members
int Compress::BLOCK_SIZE = 64;


Compress::Compress(const unsigned char *data, long length) :
    source(NULL), destination(NULL), source_length(length),
    destination_length(0), source_pos(0), destination_pos(0),
    bits_written(0)
{
    int number_of_blocks;

    if (length > 0)
    {
        // allocate source buffer
        source = new unsigned char [length];

        // load data into source buffer
        memcpy(source, data, source_length);

        // allocate destination buffer
        number_of_blocks =
            (source_length + (BLOCK_SIZE-1)) / BLOCK_SIZE;
        destination_length =
            4 + (9 * (source_length + number_of_blocks + 1) + 7) / 8;
        destination = new unsigned char [destination_length];

        // begin compression routine
        compress();
    }
}

Compress::~Compress()
{
    if (destination != NULL)
        delete [] destination;

    if (source != NULL)
        delete [] source;
}

void Compress::write9(int nbyte)
{
    int ignore_bytes = bits_written / 8;
    int ignore_bits = bits_written % 8;
    int shifted_byte = nbyte << ignore_bits;
    unsigned char lo_byte = (unsigned char) (shifted_byte & 0xFF);
    unsigned char hi_byte = (unsigned char) ((shifted_byte >> 8) & 0xFF);
    unsigned char bit_mask_lo = ~((0xFF << ignore_bits) & 0xFF);
    unsigned char bit_mask_hi = (0xFF << (1+ignore_bits)) & 0xFF;

    // first byte
    destination[ignore_bytes+4]
        = (destination[ignore_bytes+4] & bit_mask_lo) | lo_byte;

    // second byte
    destination[ignore_bytes+5]
        = (destination[ignore_bytes+5] & bit_mask_hi) | hi_byte;

    bits_written += 9;
}

void Compress::write4(long outval)
{
    destination[0] = (unsigned char) (outval & 0xFF);
    destination[1] = (unsigned char) ((outval >> 8) & 0xFF);
    destination[2] = (unsigned char) ((outval >> 16) & 0xFF);
    destination[3] = (unsigned char) ((outval >> 24) & 0xFF);
}

void Compress::compress()
{
    int bytes_processed;

    // write size header to output buffer
    write4(source_length);

    // perform "compression" algorithm
    while (source_pos < source_length)
    {
        write9(0x100);
        bytes_processed = 0;

        while ((bytes_processed < BLOCK_SIZE) && (source_pos < source_length))
        {
            write9(source[source_pos++]);
            bytes_processed++;
        }
    }

    write9(0x101);
}
