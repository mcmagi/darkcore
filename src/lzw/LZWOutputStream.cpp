/* LZWOutputStream.h */

#include <memory>                       // std::unique_ptr, std::move
#include <ostream>                      // std::ostream
#include "lzw/LZWIOStream.h"            // LZWOutputStream, LZWStreamBuffer
#include "util/FilterIOStream.h"        // FilterInputStream

using namespace std;
using darkcore::lzw::LZWOutputStream;
using darkcore::lzw::LZWStreamBuffer;
using darkcore::util::FilterOutputStream;

LZWOutputStream::LZWOutputStream(ostream &os) :
    FilterOutputStream(new LZWStreamBuffer(os)) 
{
}

LZWOutputStream::LZWOutputStream(unique_ptr<ostream> os) :
    FilterOutputStream(new LZWStreamBuffer(std::move(os)))
{
}

LZWOutputStream::~LZWOutputStream()
{
    delete rdbuf();
}
