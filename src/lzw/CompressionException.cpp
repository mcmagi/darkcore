/* CompressionException.cpp */

#include <string>                   // std::string
#include "Exception.h"              // Exception
#include "lzw/LZW.h"                // Compress

using std::string;
using darkcore::Exception;
using darkcore::lzw::CompressionException;


CompressionException::CompressionException(const string &errmsg)
    : Exception(errmsg)
{
}

CompressionException::CompressionException(const CompressionException &ex)
    : Exception(ex)
{
}

CompressionException::~CompressionException() throw ()
{
}
