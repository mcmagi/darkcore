/* ColorMask.cpp */

#include <cstdint>                  // uint32_t (c++0x)
#include <string>                   // std::string
#include "image/Color.h"            // Color, Component
#include "image/ColorMask.h"        // ColorMask
#include "util/ToStringBuilder.h"   // ToStringBuilder

using namespace std;
using darkcore::image::Color;
using darkcore::image::ColorMask;
using darkcore::util::ToStringBuilder;

const ColorMask ColorMask::FULL32(32, 0xFF0000, 0x00FF00, 0x0000FF);

ColorMask::ColorMask(int bpp, uint32_t rmask, uint32_t gmask, uint32_t bmask) :
    bpp(bpp), rmask(rmask), gmask(gmask), bmask(bmask),
    rshift(calculateShift(rmask)), gshift(calculateShift(gmask)), bshift(calculateShift(bmask))
{
}

ColorMask::ColorMask(const ColorMask &mask) :
    bpp(mask.bpp), rmask(mask.rmask), gmask(mask.gmask), bmask(mask.bmask),
    rshift(mask.rshift), gshift(mask.gshift), bshift(mask.bshift)
{
}

ColorMask::~ColorMask()
{
}

uint32_t ColorMask::getRGBValue(const Color &c) const
{
    uint32_t p = (c.getRed() << rshift) +
            (c.getGreen() << gshift) +
            (c.getBlue() << bshift) ;
    return p;
}

Color ColorMask::getColor(const uint32_t &p) const
{
    Component r = (p & rmask) >> rshift;
    Component g = (p & gmask) >> gshift;
    Component b = (p & bmask) >> bshift;

    return Color(r, g, b);;
}

int ColorMask::calculateShift(uint32_t mask)
{
    int shift = 0;
    for ( ; mask % 2 == 0; shift++)
        mask /= 2; 
    return shift;
}

ColorMask & ColorMask::operator=(const ColorMask &mask)
{
    if (this != &mask)
    {
        rmask = mask.rmask;
        gmask = mask.gmask;
        bmask = mask.bmask;
        rshift = mask.rshift;
        gshift = mask.gshift;
        bshift = mask.bshift;
    }
    return *this;
}

bool ColorMask::operator==(const ColorMask &mask) const
{
    return bpp == mask.bpp && rmask == mask.rmask && gmask == mask.gmask && bmask == mask.bmask;
    // not necessary to compare [rgb]shifts since they are derived from the [rgb]masks
}

bool ColorMask::operator!=(const ColorMask &mask) const
{
    return ! (*this == mask);
}

string ColorMask::toString() const
{
    return ToStringBuilder("ColorMask")
        .add("rmask", rmask)
        .add("gmask", gmask)
        .add("bmask", bmask)
        .add("rshift", rshift)
        .add("gshift", gshift)
        .add("bshift", bshift)
        .toString();
}
// 111100000000 > 256 : 0xF00(3824)
