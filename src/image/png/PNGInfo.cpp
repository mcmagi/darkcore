/* PNGInfo.cpp */

#include <png.h>
#include <memory>                   // std::unique_ptr
#include "image/png/PNGContext.h"   // PNGContext
#include "image/png/PNGException.h" // PNGException
#include "image/png/PNGInfo.h"      // PNGInfo
#include "image/png/PNGPalette.h"   // PNGPalette

using namespace std;
using darkcore::image::png::PNGContext;
using darkcore::image::png::PNGException;
using darkcore::image::png::PNGInfo;
using darkcore::image::png::PNGPalette;

PNGInfo::PNGInfo(const PNGContext &pngCtx) :
    pngCtx(pngCtx), info_ptr(createInfoStruct(pngCtx))
{
    if (! info_ptr)
        throw PNGException("Could not allocate png_info");
}

PNGInfo::~PNGInfo()
{
    png_destroy_info_struct(pngCtx, &info_ptr);
}

png_uint_32 PNGInfo::getWidth() const
{
    return png_get_image_width(pngCtx, info_ptr);
}

png_uint_32 PNGInfo::getHeight() const
{
    return png_get_image_height(pngCtx, info_ptr);
}

png_byte PNGInfo::getColorType() const
{
    return png_get_color_type(pngCtx, info_ptr);
}

png_byte PNGInfo::getBitDepth() const
{
    return png_get_bit_depth(pngCtx, info_ptr);
}

png_byte PNGInfo::getChannels() const
{
    return png_get_channels(pngCtx, info_ptr);
}

png_byte PNGInfo::getFilterType() const
{
    return png_get_filter_type(pngCtx, info_ptr);
}

png_byte PNGInfo::getCompressionType() const
{
    return png_get_compression_type(pngCtx, info_ptr);
}

png_byte PNGInfo::getInterlaceType() const
{
    return png_get_interlace_type(pngCtx, info_ptr);
}

png_uint_32 PNGInfo::getRowBytes() const
{
    return png_get_rowbytes(pngCtx, info_ptr);
}

unique_ptr<PNGPalette> PNGInfo::getPalette() const
{
    return unique_ptr<PNGPalette>(new PNGPalette(pngCtx, *this));
}

void PNGInfo::setPalette(const PNGPalette &pal)
{
    png_set_PLTE(pngCtx, *this, pal, pal.getNumColors());
}

png_infop PNGInfo::createInfoStruct(png_structp png_ptr)
{
    return png_create_info_struct(png_ptr);
}

void PNGInfo::configure(png_uint_32 width, png_uint_32 height, png_byte depth, png_byte colorType)
{
    png_set_IHDR(pngCtx, *this, width, height, depth, colorType,
                 PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_COMPRESSION_TYPE_DEFAULT);
}