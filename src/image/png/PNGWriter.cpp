/* PNGWriter.cpp */

#include <png.h>
#include <csetjmp>                      // setjmp
#include <iostream>                     // std::cout, std::endl
#include <ostream>                      // std::ostream
#include "image/Color.h"                // Color, Pixel
#include "image/ColorMask.h"            // ColorMask
#include "image/ColorPalette.h"         // ColorPalette
#include "image/Image.h"                // Image
#include "image/ImageIO.h"              // ImageWriter
#include "image/ImageFormat.h"          // ImageFormat, IndexedImageFormat, RGBImageFormat
#include "image/png/PNGContext.h"       // PNGContext
#include "image/png/PNGException.h"     // PNGException
#include "image/png/PNGImage.h"         // PNGImage
#include "image/png/PNGPalette.h"       // PNGPalette
#include "image/png/PNGWriter.h"        // PNGWriter

using namespace std;
using darkcore::image::Color;
using darkcore::image::ColorMask;
using darkcore::image::ColorPalette;
using darkcore::image::Image;
using darkcore::image::ImageFormat;
using darkcore::image::ImageWriter;
using darkcore::image::IndexedImageFormat;
using darkcore::image::Pixel;
using darkcore::image::RGBImageFormat;
using darkcore::image::png::PNGContext;
using darkcore::image::png::PNGException;
using darkcore::image::png::PNGImage;
using darkcore::image::png::PNGPalette;
using darkcore::image::png::PNGWriter;

PNGWriter::PNGWriter(ostream &os) :
    ImageWriter(os), pngCtx(PNGContext::WRITE), pngInfo(pngCtx),
    imgWritten(false)
{
    if (setjmp(png_jmpbuf((png_structp) pngCtx)))
        throw PNGException("Error during PNG I/O initialization");

    png_set_write_fn(pngCtx, (png_voidp) &os, userWriteData, userFlushData);
}

PNGWriter::~PNGWriter()
{
}

void PNGWriter::write(const Image &img)
{
    const Image *imgptr = &img;
    unique_ptr<Image> convertedImg; // for ownership of possibly converted image

    if (imgWritten)
        throw PNGException("No more PNG image data can be written");

    const ImageFormat &fmt = img.getImageFormat();
    bool indexed = fmt.getFormatType() == ImageFormat::INDEXED;

    setImageFormat(fmt); // save image format for future fcn calls

    cout << "before pngInfo.configure()" << endl;

    // set PNG image dimensions
    pngInfo.configure(img.getWidth(), img.getHeight(),
                      indexed ? fmt.getColorDepth() : 8, // 24 or 32 bit RGB formats have 8 bits per channel
                      indexed ? PNG_COLOR_TYPE_PALETTE : PNG_COLOR_TYPE_RGB);

    cout << "before determining format" << endl;

    if (indexed)
    {
        // set palette on indexed images
        const IndexedImageFormat &idxfmt = dynamic_cast<const IndexedImageFormat &>(fmt);
        const ColorPalette &pal = idxfmt.getColorPalette();

        // map color palette to PNG palette
        PNGPalette pngpal(pngCtx, pal.getNumColors());
        for (int i = 0; i < pal.getNumColors(); i++)
            pngpal.setColor(i, mapColor(pal.getColor(i)));

        pngInfo.setPalette(pngpal);
    }
    else // RGB
    {
        const RGBImageFormat &rgbfmt = dynamic_cast<const RGBImageFormat &>(fmt);

        if (rgbfmt.getColorMask() != ColorMask::FULL32)
        {
            // up-convert to standard 32-bit color depth before writing
            unique_ptr<ImageFormat> newfmt(new RGBImageFormat(ColorMask::FULL32));
            convertedImg = img.convert(*newfmt);
            imgptr = &*convertedImg;
        }
    }

    if (setjmp(png_jmpbuf((png_structp) pngCtx)))
        throw PNGException("Error writing PNG meta data");

    cout << "before png_write_info()" << endl;

    png_write_info(pngCtx, pngInfo);

    cout << "before creating PNGImage" << endl;

    // copy image data to pngimg
    PNGImage pngimg(pngInfo);
    cout << "before buildImageData()" << endl;
    buildImageData(imgptr->getImageData(), pngimg.getData());

    if (setjmp(png_jmpbuf((png_structp) pngCtx)))
        throw PNGException("Error writing PNG image data");

    cout << "before png_write_image()" << endl;

    // write out the image
    png_write_image(pngCtx, pngimg);

    cout << "done" << endl;

    imgWritten = true;
}

void PNGWriter::userWriteData(png_structp png_ptr, png_bytep data, png_size_t length)
{
    png_voidp io_ptr = png_get_io_ptr(png_ptr);
    ostream *os = static_cast<ostream *>(io_ptr);
    os->write((char *) data, length);
}

void PNGWriter::userFlushData(png_structp png_ptr)
{
    png_voidp io_ptr = png_get_io_ptr(png_ptr);
    ostream *os = static_cast<ostream *>(io_ptr);
    os->flush();
}

void PNGWriter::buildImageData(const vector<Pixel> &imgdata, vector<vector<png_byte> > &pngdata) const
{
    int width = pngInfo.getWidth();
    int height = pngInfo.getHeight();
    int channels = pngInfo.getChannels();
    int rowbytes = 0; // depends on image format
    int depth = getImageFormat().getColorDepth();
    const ColorMask *cmask = NULL;

    if (getImageFormat().getFormatType() == ImageFormat::RGB)
    {
        cmask = &dynamic_cast<const RGBImageFormat &>(getImageFormat()).getColorMask();
        rowbytes = width * channels;
    }
    else
        rowbytes = width * depth / 8;

    int i = 0;

    for (int r = 0; r < height; r++)
    {
        for (int c = 0; c < rowbytes; c += channels)
        {
            // use color information to transform pixel
            if (getImageFormat().getFormatType() == ImageFormat::RGB)
            {
                Color color = cmask->getColor(imgdata[i++]);

                pngdata[r][c] = color.getRed();
                pngdata[r][c+1] = color.getGreen();
                pngdata[r][c+2] = color.getBlue();

                // if target contains alpha, skip it (for now)
                if (pngInfo.getColorType() & PNG_COLOR_MASK_ALPHA)
                    pngdata[r][c+3] = 0;
            }
            else // ImageFormat::INDEXED
            {
                // determine number of pixels in one byte
                int count = 8 / depth;

                // pack each pixel into the byte
                for (int j = 0; j < count; j++)
                {
                    int shift = (count - j - 1) * depth;
                    //cout << "r=" << r << ",c=" << c << ",j=" << j << ",i=" << i << ",imgdata[i]=" << imgdata[i] << ",val=" << (imgdata[i] << shift) << endl;
                    pngdata[r][c] |= imgdata[i++] << shift;
                }

                // e.g. depth=2, count=4
                // j=0, shift = (4-0-1)*2 = 6
                // j=1, shift = (4-1-1)*2 = 4
                // j=2, shift = (4-2-1)*2 = 2
                // j=3, shift = (4-3-1)*2 = 0
            }
        }
    }
}

png_color PNGWriter::mapColor(Color color)
{
    png_color pngc;
    pngc.red = color.getRed();
    pngc.green = color.getGreen();
    pngc.blue = color.getBlue();
    return pngc;
}