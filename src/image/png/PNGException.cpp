/* PNGException.cpp */

#include <string>                   // std::string
#include "Exception.h"              // Exception
#include "image/png/PNGException.h" // PNGException

using namespace std;
using darkcore::Exception;
using darkcore::image::png::PNGException;

PNGException::PNGException(const string &errmsg) :
    Exception(errmsg)
{
}

PNGException::PNGException(const PNGException &ex) :
    Exception(ex)
{
}

PNGException::~PNGException() throw ()
{
}