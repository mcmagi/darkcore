/* PNGContext.cpp */

#include <png.h>
#include <memory>                   // std::unique_ptr
#include "image/png/PNGContext.h"   // PNGContext
#include "image/png/PNGException.h" // PNGException

using namespace std;
using darkcore::image::png::PNGContext;
using darkcore::image::png::PNGException;

PNGContext::PNGContext(AccessType type) :
    type(type), png_ptr(createPNGStruct(type))
{
    if (! png_ptr)
        throw PNGException("Could not allocate png_struct");
}

PNGContext::~PNGContext()
{
    if (type == READ)
        png_destroy_read_struct(&png_ptr, NULL, NULL);
    else
        png_destroy_write_struct(&png_ptr, NULL);
}

png_structp PNGContext::createPNGStruct(AccessType type)
{
    if (type == READ)
        return png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    else // WRITE
        return png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
}