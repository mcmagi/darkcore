/* PNGImage.cpp */

#include <png.h>
#include <vector>                       // std::vector
#include "image/png/PNGImage.h"         // PNGImage
#include "image/png/PNGInfo.h"          // PNGInfo

using namespace std;
using darkcore::image::png::PNGImage;
using darkcore::image::png::PNGInfo;

PNGImage::PNGImage(const PNGInfo &info) :
    data(info.getHeight(), vector<png_byte>(info.getRowBytes())),
    rowptrs(data.size())
{
    // rowptrs is simply a set of pointers to the rows in data.
    // This allows us to return it as a double pointer which the png_*
    // functions will need.
    for (int i = 0; i < (int) data.size(); i++)
        rowptrs[i] = &data[i][0];
}

PNGImage::~PNGImage()
{
}