/* PNGReader.cpp */

#include <png.h>
#include <csetjmp>                      // setjmp
#include <istream>                      // std::istream
#include <memory>                       // std::unique_ptr, std::shared_ptr, std::move
#include <iostream>                     // std::cout, std::endl
#include <vector>                       // std::vector
#include "image/Color.h"                // Color, Pixel
#include "image/ColorMask.h"            // ColorMask
#include "image/ColorPalette.h"         // ColorPalette
#include "image/Image.h"                // Image
#include "image/ImageIO.h"              // ImageReader
#include "image/ImageFormat.h"          // ImageFormat, IndexedImageformat, RGBImageFormat
#include "image/png/PNGContext.h"       // PNGContext
#include "image/png/PNGException.h"     // PNGException
#include "image/png/PNGImage.h"         // PNGImage
#include "image/png/PNGPalette.h"       // PNGPalette
#include "image/png/PNGReader.h"        // PNGReader

using namespace std;
using darkcore::image::Color;
using darkcore::image::ColorMask;
using darkcore::image::ColorPalette;
using darkcore::image::Image;
using darkcore::image::ImageFormat;
using darkcore::image::ImageReader;
using darkcore::image::IndexedImageFormat;
using darkcore::image::RGBImageFormat;
using darkcore::image::Pixel;
using darkcore::image::png::PNGContext;
using darkcore::image::png::PNGException;
using darkcore::image::png::PNGImage;
using darkcore::image::png::PNGPalette;
using darkcore::image::png::PNGReader;

const int PNGReader::PNG_HDR_SZ = 8;

PNGReader::PNGReader(istream &is) :
    ImageReader(is), pngCtx(PNGContext::READ), pngInfo(pngCtx),
    imgRead(false)
{
    // check that it's a PNG file
    vector<png_byte> header(PNG_HDR_SZ);
    getInputStream().read((char *) &header[0], PNG_HDR_SZ);

    if (png_sig_cmp(&header[0], 0, PNG_HDR_SZ))
        throw PNGException("PNG header not found");

    // notify PNG that we read in the header
    png_set_sig_bytes(pngCtx, PNG_HDR_SZ);

    if (setjmp(png_jmpbuf((png_structp) pngCtx)))
        throw PNGException("Error during PNG I/O initialization");

    // set a static method as the custom read function
    png_set_read_fn(pngCtx, (png_voidp) &is, userReadData);

    if (setjmp(png_jmpbuf((png_structp) pngCtx)))
        throw PNGException("Error reading PNG meta data");

    // read the info header
    png_read_info(pngCtx, pngInfo);

    // set dimensions
    setWidth(pngInfo.getWidth());
    setHeight(pngInfo.getHeight());
    setNumImages(1);

    cout << "preparing to read PNG image with w,h: " << getWidth() << "," << getHeight() << "; "
        << "bit depth: " << int(pngInfo.getBitDepth()) << "; channels: " << int(pngInfo.getChannels()) << endl;

    // derive the image format
    shared_ptr<ImageFormat> fmt;
    if (pngInfo.getColorType() == PNG_COLOR_TYPE_PALETTE)
    {
        // populate color palette
        unique_ptr<ColorPalette> pal(new ColorPalette(pngInfo.getBitDepth()));
        unique_ptr<PNGPalette> pngpal = pngInfo.getPalette();
        for (int i = 0; i < pngpal->getNumColors(); i++)
        {
            //cout << "mapping color i=" << i << ",color=" << mapColor(pngpal->getColor(i)) << endl;
            pal->setColor(i, mapColor(pngpal->getColor(i)));
        }

        fmt = shared_ptr<ImageFormat>(new IndexedImageFormat(pngInfo.getBitDepth(), std::move(pal)));
    }
    else
    {
        // convert grayscale to RGB
        if (pngInfo.getColorType() == PNG_COLOR_TYPE_GRAY || pngInfo.getColorType() == PNG_COLOR_TYPE_GRAY_ALPHA)
            png_set_gray_to_rgb(pngCtx);

        // strip 16 bit PNG data to 8
        if (pngInfo.getBitDepth() == 16)
            png_set_strip_16(pngCtx);

        fmt = shared_ptr<ImageFormat>(new RGBImageFormat(ColorMask::FULL32));
    }

    cout << "PNG image format = " << fmt->toString() << endl;

    setImageFormatPtr(fmt);
}

PNGReader::~PNGReader()
{
}

unique_ptr<Image> PNGReader::read()
{
    if (imgRead)
        throw PNGException("No more PNG image data available to read");

    PNGImage pngimg(pngInfo);

    if (setjmp(png_jmpbuf((png_structp) pngCtx)))
        throw PNGException("Error reading PNG image data");

    // read in the image data
    png_read_image(pngCtx, pngimg);

    unique_ptr<Image> img(new Image(getWidth(), getHeight(), getImageFormatPtr()));
    buildImageData(pngimg.getData(), img->getImageData());

    imgRead = true;

    return img;
}

void PNGReader::userReadData(png_structp png_ptr, png_bytep data,
        png_size_t length)
{
    png_voidp io_ptr = png_get_io_ptr(png_ptr);
    istream *is = static_cast<istream *>(io_ptr);
    is->read((char *) data, length);
}

void PNGReader::buildImageData(const vector<vector<png_byte> > &pngdata, vector<Pixel> &imgdata) const
{
    int width = pngInfo.getWidth();
    int height = pngInfo.getHeight();
    int channels = pngInfo.getChannels();
    int rowbytes = 0; // depends on image format
    int depth = getImageFormat().getColorDepth();
    const ColorMask *cmask = NULL;

    if (getImageFormat().getFormatType() == ImageFormat::RGB)
    {
        cmask = &dynamic_cast<const RGBImageFormat &>(getImageFormat()).getColorMask();
        rowbytes = width * channels;
    }
    else
        rowbytes = width * depth / 8;

    int i = 0;

    for (int r = 0; r < height; r++)
    {
        for (int c = 0; c < rowbytes; c += channels)
        {
            // use color information to transform pixel
            if (getImageFormat().getFormatType() == ImageFormat::RGB)
            {
                Color color(pngdata[r][c],      // red
                            pngdata[r][c+1],    // green
                            pngdata[r][c+2]);   // blue

                // if source info contains alpha, skip it (for now)
                if (pngInfo.getColorType() & PNG_COLOR_MASK_ALPHA)
                    pngdata[r][c+3];

                imgdata[i++] = cmask->getRGBValue(color);
            }
            else // ImageFormat::INDEXED
            {
                png_byte src = pngdata[r][c];

                // mask - depth (numcolors-1) xcount <<shift
                // 11111111 - 8 (255) x1 <<0
                // 00001111 - 4 (15)  x2 <<4
                // 00000011 - 2 (3)   x4 <<6
                // 00000001 - 1 (1)   x8 <<7

                // determine mask from number of colors
                int mask = (int) (getImageFormat().getNumColors() - 1);

                // determine number of pixels in one byte
                int count = 8 / depth;

                // shift mask to the left
                mask <<= 8 - depth;

                // extract each pixel from byte with the mask,
                // shifting the mask to the right by the bpp (depth) after each iteration
                for (int j = 0; j < count; j++, mask >>= depth)
                {
                    int shift = (count - j - 1) * depth;
                    imgdata[i++] = (src & mask) >> shift;
                }

                // e.g. depth=2, count=4
                // j=0, shift = (4-0-1)*2 = 6
                // j=1, shift = (4-1-1)*2 = 4
                // j=2, shift = (4-2-1)*2 = 2
                // j=3, shift = (4-3-1)*2 = 0
            }
        }
    }
}

Color PNGReader::mapColor(png_color color)
{
    return Color(color.red, color.blue, color.green);
}