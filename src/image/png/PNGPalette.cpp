/* PNGPalette.cpp */

#include <png.h>
#include <memory>                   // std::unique_ptr
#include "image/png/PNGContext.h"   // PNGContext
#include "image/png/PNGInfo.h"      // PNGInfo
#include "image/png/PNGException.h" // PNGException
#include "image/png/PNGPalette.h"   // PNGPalette

using namespace std;
using darkcore::image::png::PNGContext;
using darkcore::image::png::PNGException;
using darkcore::image::png::PNGInfo;
using darkcore::image::png::PNGPalette;

PNGPalette::PNGPalette(const PNGContext &pngCtx, const PNGInfo &pngInfo) :
    pngCtx(pngCtx), palette(NULL), num_colors(0)
{
    if (pngCtx.getAccessType() != PNGContext::READ)
        throw PNGException("this constructor can only be called in PNG read mode");

    png_get_PLTE(pngCtx, pngInfo, &palette, &num_colors);
}

PNGPalette::PNGPalette(const PNGContext &pngCtx, int size) :
    pngCtx(pngCtx), palette(NULL), num_colors(size)
{
    if (pngCtx.getAccessType() != PNGContext::WRITE)
        throw PNGException("this constructor can only be called in PNG write mode");

    palette = new png_color[size];
}

PNGPalette::~PNGPalette()
{
    // free palette
    if (pngCtx.getAccessType() == PNGContext::WRITE)
        delete [] palette;

    // palette returned by png_get_PLTE will be freed by libpng when the info_ptr is destroyed
}

png_color PNGPalette::getColor(int idx) const
{
    if (idx < 0 || idx >= num_colors)
        throw PNGException("Palette index out of bounds");

    return palette[idx];
}

void PNGPalette::setColor(int idx, png_color color)
{
    if (idx < 0 || idx >= num_colors)
        throw PNGException("Palette index out of bounds");

    palette[idx] = color;
}