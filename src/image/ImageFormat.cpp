/* ImageFormat.cpp */

#include <iostream>                         // std::cout
#include <string>                           // std::string
#include "image/ImageFormat.h"              // ImageFormat
#include "util/ToStringBuilder.h"           // ToStringBuilder

using namespace std;
using darkcore::image::ImageFormat;
using darkcore::util::ToStringBuilder;

ImageFormat::ImageFormat(FormatType type, int bpp) :
    type(type), depth(bpp)
{
}

ImageFormat::ImageFormat(const ImageFormat &format) :
    type(format.type), depth(format.depth)
{
}

ImageFormat::~ImageFormat()
{
}

ImageFormat & ImageFormat::operator=(const ImageFormat &format)
{
    if (this != &format)
    {
        type = format.type;
        depth = format.depth;
    }
    return *this;
}

bool ImageFormat::operator==(const ImageFormat &format) const
{
    //cout << "ImageFormat::operator==(" << format.toString() << ") " << toString() << endl;
    return type == format.type && depth == format.depth;
}

bool ImageFormat::operator!=(const ImageFormat &format) const
{
    return ! (*this == format);
}

string ImageFormat::toString() const
{
    return ToStringBuilder("ImageFormat")
        .add("type", type)
        .add("depth", depth)
        .toString();
}
