/* ImageFormat.cpp */

#include <cassert>                          // assert
#include <memory>                           // std::unique_ptr
#include <string>                           // std::string
#include <typeinfo>                         // bad_cast
#include "image/ColorMask.h"                // ColorMask
#include "image/ImageFormat.h"              // ImageFormat, RGBImageFormat
#include "util/ToStringBuilder.h"           // ToStringBuilder

using namespace std;
using darkcore::image::ColorMask;
using darkcore::image::ImageFormat;
using darkcore::image::RGBImageFormat;
using darkcore::util::ToStringBuilder;

RGBImageFormat::RGBImageFormat(const ColorMask &mask) :
    ImageFormat(ImageFormat::RGB, mask.getColorDepth()), mask(mask)
{
}

RGBImageFormat::RGBImageFormat(const RGBImageFormat &format) :
    ImageFormat(format), mask(format.mask)
{
}

RGBImageFormat::~RGBImageFormat()
{
}

ImageFormat & RGBImageFormat::operator=(const ImageFormat &format)
{
    assert(typeid(format) == typeid(*this));
    return this->operator=(dynamic_cast<const RGBImageFormat &>(format));
}

RGBImageFormat & RGBImageFormat::operator=(const RGBImageFormat &format)
{
    if (this != &format)
    {
        ImageFormat::operator=(format);
        mask = format.mask;
    }
    return *this;
}

bool RGBImageFormat::operator==(const ImageFormat &format) const
{
    try {
        const RGBImageFormat &rif = dynamic_cast<const RGBImageFormat &>(format);
        return mask == rif.mask && ImageFormat::operator==(format);
    } catch (const bad_cast &ex) {
    }
    return false;
}

unique_ptr<ImageFormat> RGBImageFormat::clone() const
{
    return unique_ptr<ImageFormat>(new RGBImageFormat(*this));
}

string RGBImageFormat::toString() const
{
    return ToStringBuilder("RGBImageFormat")
        .add("ImageFormat", ImageFormat::toString())
        .add("mask", mask.toString())
        .toString();
}
