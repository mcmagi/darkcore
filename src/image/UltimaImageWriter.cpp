/* UltimaImageWriter.cpp */

#include <iostream>
#include <ostream>                      // std::ios, std::ostream
#include <string>                       // std::string
#include <vector>                       // std::vector
#include "Exception.h"                  // Exception
#include "config/IndexConfig.h"         // IndexConfig
#include "config/IndexEntry.h"          // IndexEntry
#include "config/UltimaFileConfig.h"    // UltimaFileConfig
#include "config/UltimaImageConfig.h"   // UltimaImageConfig
#include "image/Color.h"                // Pixel
#include "image/Image.h"                // Image
#include "image/ImageIO.h"              // ImageWriter
#include "image/ImageFormat.h"          // ImageFormat
#include "image/UltimaImageWriter.h"    // UltimaImageWriter
#include "util/Writer.h"                // Writer

using namespace std;
using darkcore::Exception;
using darkcore::config::IndexEntry;
using darkcore::config::UltimaImageConfig;
using darkcore::image::Image;
using darkcore::image::ImageFormat;
using darkcore::image::ImageWriter;
using darkcore::image::UltimaImageWriter;
using darkcore::util::Writer;

UltimaImageWriter::UltimaImageWriter(const UltimaImageConfig &imgCfg) :
    ImageWriter(imgCfg.getFileConfig().newOutputStream(), imgCfg.getWidth(), imgCfg.getHeight(),
                imgCfg.getNumImages(), imgCfg.getImageFormat().clone()),
    imgCfg(imgCfg),
    indexWriter(imgCfg.getIndexConfig() != NULL ? imgCfg.getIndexConfig()->newEntryWriter() : unique_ptr<Writer<IndexEntry> >())
{
    // advance the stream to the offset
    if (imgCfg.getFileConfig().getOffset() > 0)
        getOutputStream().seekp(imgCfg.getFileConfig().getOffset(), ios::beg);
}

UltimaImageWriter::~UltimaImageWriter()
{
}

void UltimaImageWriter::write(const Image &img)
{
    // TODO: edits - ensure passed-in image matches width/height & so-on?

    int rawdata_size = img.getNumPixels() * imgCfg.getImageFormat().getColorDepth() / 8;

    // add 384 bytes if in Interlaced IBMCGA foramt
    if (imgCfg.isInterlaced() && imgCfg.isIBMCGA())
        rawdata_size += 384;

    // skip to next offset if images are indexed
    // (TODO: is skip sufficient, or do we need to write X zero-bytes?)
    if (indexWriter.get() != NULL)
    {
        throw Exception("Cannot write image files if they have an index.");

        // TODO

        // do we seek (U2)?  or do we write (U6)?
        //IndexEntry entry(getOutputStream().tellg());
        //getOutputStream().seekp(imgCfg.getFileConfig().getOffset() + index->getNextOffset(), ios::beg);
        //cout << "reading img @ offset " << entry.getOffset() << ", type=" << entry.getMaskType() << endl;
    }

    vector<unsigned char> rawdata(rawdata_size);

    // get image data
    const vector<Pixel> &imgdata = img.getImageData();

    // transform image data into raw data
    buildRawData(&imgdata[0], &rawdata[0], rawdata_size);

    // write raw data to file
    getOutputStream().write((char *) &rawdata[0], rawdata_size);
}

void UltimaImageWriter::buildRawData(const Pixel *imgdata, unsigned char *rawdata, int rawdata_size) const
{
    int pix_idx = 0;                // index of pixel in image data
    int byte_idx = 0;               // index of byte in raw data
    int row_idx;                    // index row in image

    int rawPitch = getWidth() * imgCfg.getImageFormat().getColorDepth() / 8;

    // loop through all bytes in data
    for (row_idx = 0; row_idx < getHeight(); row_idx++)
    {
        packRow(&imgdata[pix_idx], &rawdata[byte_idx]);

        // advance to next row in rawdata
        byte_idx += rawPitch;

        // TODO: some duplicated checks b/w this and getPixelFromRow()
        // Interlaced IBMCGA: 192 bytes of padding between each set of scanlines
        if (imgCfg.isInterlaced() && imgCfg.isIBMCGA() && row_idx == getHeight() / 2 - 1)
            byte_idx += 192;

        // calculate pixel offset of next row in raw data
        pix_idx = getPixelFromRow(pix_idx, row_idx);
    }
}

void UltimaImageWriter::packRow(const Pixel *row, unsigned char *rawdata) const
{
    // check if this is a bitmasked image
    if (imgCfg.isBitMasked())
        packBitMaskedRow(row, rawdata, getWidth());
    else
        packSequentialRow(row, rawdata, getWidth());
}

void UltimaImageWriter::packSequentialRow(const Pixel *row, unsigned char *rawdata, int width) const
{
    int col_idx = 0;                // index of column (pixel) in row
    int bit_idx = 0;                // index of bit in byte
    int byte_idx = 0;               // index of byte in raw data
    int firstpix_idx;               // index of first pixel in byte

    int depth = imgCfg.getImageFormat().getColorDepth();

    // find the bit number of the first pixel in the byte
    firstpix_idx = 8 - depth;

    // loop for all columns (pixels) in row
    for (byte_idx = 0; col_idx < width; byte_idx++)
    {
        // initialize byte to zero
        rawdata[byte_idx] = 0;

        // loop through all pixels in the byte
        for (bit_idx = firstpix_idx; bit_idx >= 0 && col_idx < width; bit_idx -= depth)
            // leftshift pixel as necessary and or into byte
            rawdata[byte_idx] |= row[col_idx++] << bit_idx;
    }
}

void UltimaImageWriter::packBitMaskedRow(const Pixel *row, unsigned char *rawdata, int width) const
{
    unsigned char colormask = 1;    // used to add color to pixel
    int master_bit_idx = 0;         // index of bit in raw data
    int byte_idx = 0;               // index of byte in raw data
    int bit_idx;                    // index of bit within byte
    int col_idx;                    // index of column (pixel) in row
    int color;                      // current color number

    int depth = imgCfg.getImageFormat().getColorDepth();

    // initialize raw data to all zero
    for (byte_idx = 0; byte_idx < width * depth / 8; byte_idx++)
        rawdata[byte_idx] = 0;

    // loop for each color
    for (color = 0; color < depth; color++)
    {
        // loop through all pixels (columns) in row
        for (col_idx = 0; col_idx < width; master_bit_idx++, col_idx++)
        {
            // find the byte to pull the bit out of
            byte_idx = master_bit_idx / 8;

            // find the offset of the bit within the byte
            bit_idx = 7 - master_bit_idx % 8;

            // check if the color bit is set in pixel
            if (row[col_idx] & colormask)
                // shift bit into position and or into byte
                rawdata[byte_idx] |= 1 << bit_idx;
        }

        // leftshift mask to next color
        colormask <<= 1;
    }
}

int UltimaImageWriter::getPixelFromRow(int pix_idx, int row_idx) const
{
    if (imgCfg.isInterlaced())
    {
        // interlaced: even rows first, then odd rows

        // check if row number is at midpoint
        if (row_idx == imgCfg.getHeight() / 2 - 1)
            // midpoint: swap to odd rows
            pix_idx = imgCfg.getWidth();
        else
            // advance row by two
            pix_idx += imgCfg.getWidth() * 2;
    }
    else
    {
        // non-interlaced: advance to next row, sequentially
        pix_idx += imgCfg.getWidth();
    }

    return pix_idx;
}
