/* ImageSet.cpp */

#include <iostream>                 // std::cout, std::endl
#include <memory>                   // std::unique_ptr, std::shared_ptr
#include <vector>                   // std::vector
#include "Exception.h"              // Exception, IndexException
#include "image/Image.h"            // Image
#include "image/ImageSet.h"         // ImageSet
#include "image/ImageFormat.h"      // ImageFormat

using namespace std;
using darkcore::Exception;
using darkcore::IndexException;
using darkcore::image::Image;
using darkcore::image::ImageSet;
using darkcore::image::ImageFormat;

ImageSet::ImageSet(int width, int height, const ImageFormat &format, int size) :
    images(size), width(width), height(height), format(format.clone())
{
    validateSize(size);
}

ImageSet::ImageSet(int width, int height, shared_ptr<ImageFormat> format, int size) :
    images(size), width(width), height(height), format(format)
{
    validateSize(size);
}

ImageSet::ImageSet(const ImageSet &imgset) :
    images(imgset.images.size()), width(imgset.width), height(imgset.height), format(imgset.format)
{
    // copy each Image individually so we create a copy of the entire Image, not just its pointer
    for (unsigned int i = 0; i < images.size(); i++)
        setImage(i, imgset.getImage(i));
}

ImageSet::~ImageSet()
{
    for (unsigned int i = 0; i < images.size(); i++)
        delete images[i];
}

const Image & ImageSet::getImage(int num) const
{
    validateImageNum(num);

    return *images[num];
}

Image & ImageSet::getImage(int num)
{
    validateImageNum(num);

    return *images[num];
}

void ImageSet::setImage(int num, unique_ptr<Image> img)
{
    setImage(num, img.release());
}

void ImageSet::setImage(int num, const Image &img)
{
    setImage(num, new Image(img));
}

void ImageSet::setImage(int num, Image *img)
{
    //cout << "setting image: " << num << endl;

    validateImageNum(num);
    if (img != NULL)
        validateImage(*img);

    // delete any previous image at that index
    delete images[num];

    // store the image
    images[num] = img;
}

void ImageSet::addImage(unique_ptr<Image> img)
{
    addImage(img.release());
}

void ImageSet::addImage(const Image &img)
{
    addImage(new Image(img));
}

void ImageSet::addImage(Image *img)
{
    //cout << "adding image: " << images.size() << endl;

    if (img != NULL)
        validateImage(*img);

    // store the image
    images.push_back(img);
}

void ImageSet::resize(int size)
{
    validateSize(size);

    // if downsizing, make sure we clean up
    for (unsigned int i = size; i < images.size(); i++)
        delete images[i];

    images.resize(size);
}

void ImageSet::validateSize(int size) const
{
    if (size < 0)
        throw Exception(string("Cannot create an ImageSet with size=") += size);
}

void ImageSet::validateImageNum(int num) const
{
    if (num < 0 || num >= getNumImages())
        throw IndexException("invalid Image index", 0, getNumImages() - 1, num);
}

void ImageSet::validateImage(const Image &img) const
{
    if (width != img.getWidth() || height != img.getHeight() || *format != img.getImageFormat())
        throw Exception("supplied Image does not meet ImageSet attributes");
}
