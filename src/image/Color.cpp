/* Color.cpp */

#include <cstdint>                  // unit8_t
#include <cstdio>                   // sprintf
#include <string>                   // std::string
#include "image/Color.h"            // Color

using namespace std;
using darkcore::image::Color;

const Color Color::BLACK        (0x00, 0x00, 0x00);
const Color Color::BLUE         (0x00, 0x00, 0xAA);
const Color Color::GREEN        (0x00, 0xAA, 0x00);
const Color Color::CYAN         (0x00, 0xAA, 0xAA);
const Color Color::RED          (0xAA, 0x00, 0x00);
const Color Color::MAGENTA      (0xAA, 0x00, 0xAA);
const Color Color::BROWN        (0xAA, 0x55, 0x00);
const Color Color::GRAY         (0xAA, 0xAA, 0xAA);
const Color Color::DK_GRAY      (0x55, 0x55, 0x55);
const Color Color::LT_BLUE      (0x55, 0x55, 0xFF);
const Color Color::LT_GREEN     (0x55, 0xFF, 0x55);
const Color Color::LT_CYAN      (0x55, 0xFF, 0xFF);
const Color Color::LT_RED       (0xFF, 0x55, 0x55);
const Color Color::LT_MAGENTA   (0xFF, 0x55, 0xFF);
const Color Color::YELLOW       (0xFF, 0xFF, 0x55);
const Color Color::WHITE        (0xFF, 0xFF, 0xFF);

Color::Color(uint8_t red, uint8_t green, uint8_t blue) :
    red(red), green(green), blue(blue)
{
}

Color::Color(const Color &c) :
    red(c.red), green(c.green), blue(c.blue)
{
}

Color::~Color()
{
}

Color & Color::operator=(const Color &c)
{
    if (this != &c)
    {
        red = c.red;
        green = c.green;
        blue = c.blue;
    }
    return *this;
}

bool Color::operator==(const Color &c) const
{
    return red == c.red && green == c.green && blue == c.blue;
}

bool Color::operator!=(const Color &c) const
{
    return ! (*this == c);
}

bool Color::operator<(const Color &c) const
{
    // sort by reds, greens, then blues (this is pretty arbitrary)
    return red < c.red ? true :
        green < c.green ? true :
        blue < c.blue;
}

Color::operator const char *() const
{
    return toString().c_str();
}

string Color::toString() const
{
    char str[100];
    sprintf(str, "Color{red=%02x,green=%02x,blue=%02x}", red, green, blue);
    return string(str);
}
