/* ImageWriter.cpp */

#include <string>                       // std::string
#include <memory>                       // std::unique_ptr, std::shared_ptr, std::move
#include <ostream>                      // std::ostream
#include "image/ImageFormat.h"          // ImageFormat
#include "image/ImageIO.h"              // ImageWriter
#include "image/ImageSet.h"             // ImageSet
#include "util/FileWriter.h"            // FileWriter

using namespace std;
using darkcore::image::ImageFormat;
using darkcore::image::ImageSet;
using darkcore::image::ImageWriter;
using darkcore::util::FileWriter;

ImageWriter::ImageWriter(const string &filename, int width, int height, int numImages, std::shared_ptr<ImageFormat> format) :
    FileWriter<Image>(filename), ImageIO(width, height, numImages, format)
{
}

ImageWriter::ImageWriter(ostream &os, int width, int height, int numImages, std::shared_ptr<ImageFormat> format) :
    FileWriter<Image>(os), ImageIO(width, height, numImages, format)
{
}

ImageWriter::ImageWriter(unique_ptr<ostream> os, int width, int height, int numImages, std::shared_ptr<ImageFormat> format) :
    FileWriter<Image>(std::move(os)), ImageIO(width, height, numImages, format)
{
}

ImageWriter::~ImageWriter()
{
}

void ImageWriter::writeAll(const ImageSet &set)
{
    // validate # of images in set
    if (getNumImages() > 0 && set.getNumImages() > getNumImages())
        throw Exception("More images in ImageSet than allowed for this ImageWriter");

    for (int i = 0; i < set.getNumImages(); i++)
        write(set.getImage(i));
}