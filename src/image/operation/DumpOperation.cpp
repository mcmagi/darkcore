/* DumpOperation.cpp */

#include <cstdio>                           // printf
#include <vector>                           // vector
#include "image/Color.h"                    // Pixel
#include "image/Image.h"                    // Image
#include "image/operation/DumpOperation.h"  // DumpOperation
#include "image/operation/ImageOperation.h" // ImageOperation

using namespace std;
using darkcore::image::Image;
using darkcore::image::Pixel;
using darkcore::image::operation::DumpOperation;
using darkcore::image::operation::ImageOperation;


DumpOperation::DumpOperation() :
    ImageOperation()
{
}

DumpOperation::DumpOperation(const DumpOperation &op) :
    ImageOperation(op)
{
}

DumpOperation::~DumpOperation()
{
}

void DumpOperation::operate(Image &img, vector<Pixel> &data) const
{
    // DEBUG
    for (int y = 0; y < img.getHeight(); y++) {
        for (int x = 0; x < img.getWidth(); x++) {
            printf("%02x ", img.getPixel(x, y));
        }
        printf("\n");
    }
}