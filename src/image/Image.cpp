/* Image.cpp */

#include <algorithm>                                // std::min
#include <cstring>                                  // std::memcpy
#include <iostream>                                 // std::cout, std::endl
#include <memory>                                   // std::unique_ptr, std::shared_ptr
#include <vector>                                   // std::vector
#include "Exception.h"                              // Exception, IndexException
#include "image/Color.h"                            // Pixel
#include "image/Image.h"                            // Image
#include "image/ImageFormat.h"                      // ImageFormat
#include "image/ImageSet.h"                         // ImageSet
#include "image/converter/ImageConverter.h"         // ImageConverter
#include "image/converter/ImageFormatConverter.h"   // ImageFormatConverter
#include "image/operation/DumpOperation.h"          // DumpOperation
#include "image/operation/ImageOperation.h"         // ImageOperation

using namespace std;
using darkcore::Exception;
using darkcore::IndexException;
using darkcore::image::Image;
using darkcore::image::ImageFormat;
using darkcore::image::ImageSet;
using darkcore::image::Pixel;
using darkcore::image::converter::ImageConverter;
using darkcore::image::converter::ImageFormatConverter;
using darkcore::image::operation::ImageOperation;
using darkcore::image::operation::DumpOperation;

Image::Image(int w, int h, const ImageFormat &format) :
    data(w*h), width(w), height(h), format(format.clone())
{
    //static int ctr = 0;
    //cout << "image constructor w/o data:" << ctr++ << endl;
}

Image::Image(int w, int h, shared_ptr<ImageFormat> format) :
    data(w*h), width(w), height(h), format(format)
{
    //static int ctr = 0;
    //cout << "image constructor w/o data:" << ctr++ << endl;
}

Image::Image(int w, int h, const ImageFormat &format, const vector<Pixel> &imgdata) :
    data(imgdata), width(w), height(h), format(format.clone())
{
    //static int ctr = 0;
    //cout << "image constructor w/ data:" << ctr++ << endl;

    validateSize();
}

Image::Image(int w, int h, shared_ptr<ImageFormat> format, const vector<Pixel> &imgdata) :
    data(imgdata), width(w), height(h), format(format)
{
    //static int ctr = 0;
    //cout << "image constructor w/ data:" << ctr++ << endl;

    validateSize();
}

Image::Image(const ImageSet &imgSet, int columns) :
    data(0), width(0), height(0), format()
{
    //static int ctr = 0;
    //cout << "image constructor w/ ImageSet:" << ctr++ << endl;

    // create a vector of image pointers
    vector<const Image *> images(imgSet.getNumImages());
    for (int i = 0; i < imgSet.getNumImages(); i++)
        images[i] = &imgSet.getImage(i);

    constructFromVector(images, columns);
}

Image::Image(const vector<const Image *> &images, int columns) :
    data(0), width(0), height(0), format()
{
    //static int ctr = 0;
    //cout << "image constructor w/ vector:" << ctr++ << endl;

    constructFromVector(images, columns);
}

Image::Image(const Image &img) :
    data(img.data), width(img.width), height(img.height), format(img.format)
{
    //static int ctr = 0;
    //cout << "image copy constructor:" << ctr++ << endl;
}

Image::~Image()
{
    //static int ctr = 0;
    //cout << "image destructor:" << ctr++ << endl;
}

Image & Image::operator=(const Image &img)
{
    //static int ctr = 0;
    //cout << "image assignment operator:" << ctr++ << endl;

    // copy image data if it is not the same image object
    if (this != &img)
    {
        // copy members
        width = img.width;
        height = img.height;
        format = img.format;
        data = img.data;
    }

    return *this;
}

void Image::setPixel(int x, int y, Pixel c)
{
    // TODO: check if color fits in bpp constraints

    validatePosition(x, y);

    // set pixel color
    data[calculatePosition(x, y)] = c;
}

Pixel Image::getPixel(int x, int y) const
{
    // return pixel color
    validatePosition(x, y);

    return data[calculatePosition(x, y)];
}

const vector<Pixel> & Image::getImageData() const
{
    return data;
}

vector<Pixel> & Image::getImageData()
{
    return data;
}

unique_ptr<Image> Image::resize(int w, int h) const
{
    // create new image
    unique_ptr<Image> newimg(new Image(w, h, *format));

    // paste this image data into the new (resized) image
    newimg->paste(*this, 0, 0);

    // return the resized image, calling fcn should own object
    return newimg;
}

unique_ptr<Image> Image::convert(const ImageFormat &format) const
{
    // create a converter
    unique_ptr<ImageFormatConverter> converter = ImageFormatConverter::getDefaultConverter(*this->format, format);

    // do the conversion
    return convert(*converter);
}

unique_ptr<Image> Image::convert(const ImageConverter &converter) const
{
    // do the conversion and save the image data
    return converter.convert(*this, data);
}

void Image::operate(const ImageOperation &op)
{
    op.operate(*this, data);
}

unique_ptr<Image> Image::copy(int x, int y, int w, int h) const
{
    unique_ptr<Image> img(new Image(w, h, getImageFormat()));

    // copy image data from this(x,y) to img(0,0)
    copyImageData(*this, x, y, *img, 0, 0);

    // calling function should own Image
    return img;
}

void Image::paste(const Image &img, int x, int y)
{
    // copy image data from img(0,0) to this(x,y)
    copyImageData(img, 0, 0, *this, x, y);
}

unique_ptr<ImageSet> Image::tileImage(int tileW, int tileH) const
{
    int columns = width / tileW;
    if (width % tileW > 0)
        columns++;

    int rows = height / tileH;
    if (height % tileH > 0)
        rows++;

    cout << "tiling image size={w=" << getWidth() << ",h=" << getHeight() << "}"
         << " into tiles size={w=" << tileW << ",h=" << tileH << "};"
         << " count=" << (rows * columns) << ",dim={r=" << rows << ",c=" << columns << "}"
         << endl;

    unique_ptr<ImageSet> imgSet(new ImageSet(tileW, tileH, format, columns * rows));

    // create each tile
    for (int i = 0, r = 0; r < rows; r++)
    {
        for (int c = 0; c < columns; c++)
        {
            int x = c * imgSet->getWidth();
            int y = r * imgSet->getHeight();
            // note - ownership of the copy is transferred to the ImageSet
            //cout << "setting tile #" << i << endl;
            imgSet->setImage(i++, copy(x, y, tileW, tileH));
        }
    }

    // calling fcn should own imgset
    return imgSet;
}

void Image::dump()
{
    operate(DumpOperation());
}


/*
 * Private Methods
 */

void Image::setImageData(const Pixel *imgdata)
{
    //cout << "setting image data, size=" << data.size() << ", height=" << getHeight() << ", pitch=" << getPitch() << endl;

    memcpy(&data[0], imgdata, getHeight() * getPitch());
}

void Image::copyImageData(const Image &src, int srcX, int srcY,
        Image &tgt, int tgtX, int tgtY)
{
    //cout << "copying image data" << endl;

    // get height & pitch to copy (min of two images)
    int copyHeight = min(src.height - srcY, tgt.height - tgtY);
    int copyWidth = min(src.width - srcX, tgt.width - tgtX);
    int copyPitch = copyWidth * sizeof(Pixel);

    // if we are copying the full width of the image
    // and they are the same on source and target
    // we can do it in one memcpy
    if (srcX == 0 && tgtX == 0 && src.width == tgt.width)
    {
        const Pixel *srcRow = &src.data[src.calculatePosition(0, srcY)];
        Pixel *tgtRow = &tgt.data[tgt.calculatePosition(0, tgtY)];
        memcpy(tgtRow, srcRow, copyPitch * copyHeight);
    }
    else
    {
        // otherwise, do one memcpy per row
        for (int i = 0; i < copyHeight; i++)
        {
            const Pixel *srcRow = &src.data[src.calculatePosition(srcX, srcY + i)];
            Pixel *tgtRow = &tgt.data[tgt.calculatePosition(tgtX, tgtY + i)];
            memcpy(tgtRow, srcRow, copyPitch);
        }
    }
}

int Image::calculatePosition(int x, int y) const
{
    // return index into image data array
    return (y * width + x);
}

void Image::validatePosition(int x, int y) const
{
    // check that we are not out of bounds
    if (x < 0 || x >= width)
        throw IndexException("invalid column position", 0, width, x);
    else if (y < 0 || y >= height)
        throw IndexException("invalid row position", 0, height, y);
}

void Image::constructFromVector(const vector<const Image *> &images, int columns)
{
    if (columns < 1)
        throw Exception("Columns must be greater than 0");

    if (images.size() < 1)
        throw Exception("No images in vector to constuct an image from");

    // get number of rows (rounding up)
    int rows = images.size() / columns;
    if (images.size() % columns > 0)
        rows++;
    //cout << "# of cols: " << columns << endl;
    //cout << "# of rows: " << rows << endl;

    format = images[0]->format;
    int tileWidth = images[0]->width;
    int tileHeight = images[0]->height;

    width = tileWidth * columns;
    height = tileHeight * rows;

    //cout << "img width: " << width << endl;
    //cout << "img height: " << height << endl;

    // now that we know the dimensions, allocate the memory
    data.resize(width * height);

    // and paste in each tile
    unsigned int i = 0;
    for (int r = 0; r < rows; r++)
    {
        for (int c = 0; c < columns && i < images.size(); c++, i++)
        {
            int x = c * tileWidth;
            int y = r * tileHeight;
            paste(*images[i], x, y);
        }
    }
}

void Image::validateSize()
{
    unsigned int expectedSize = width * height;
    if (data.size() != expectedSize)
        throw Exception() << "size mismatch: w*h=" << expectedSize << ", data.size()=" << data.size();
}