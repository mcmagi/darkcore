/* ImageFormat.cpp */

#include <cassert>                          // assert
#include <iostream>                         // std::cout, std::endl
#include <memory>                           // std::unique_ptr, std::move
#include <string>                           // std::string
#include <typeinfo>                         // bad_cast
#include "image/ColorPalette.h"             // ColorPalette, StandardPaletteType
#include "image/ImageFormat.h"              // ImageFormat, IndexedImageFormat
#include "util/ToStringBuilder.h"           // ToStringBuilder

using namespace std;
using darkcore::image::ColorPalette;
using darkcore::image::ImageFormat;
using darkcore::image::IndexedImageFormat;
using darkcore::image::StandardPaletteType;
using darkcore::util::ToStringBuilder;

IndexedImageFormat::IndexedImageFormat(StandardPaletteType type) :
    ImageFormat(ImageFormat::INDEXED, type),
    palette(ColorPalette::getStandardPalette(type).clone())
{
    //cout << "--> constructing IndexedImageForamat with cloned standard palette: " << toString() << endl;
}

IndexedImageFormat::IndexedImageFormat(int bpp, const ColorPalette &pal) :
    ImageFormat(ImageFormat::INDEXED, bpp), palette(pal.clone())
{
    //cout << "--> constructing IndexedImageForamat with cloned palette: " << toString() << endl;
}

IndexedImageFormat::IndexedImageFormat(int bpp, unique_ptr<ColorPalette> pal) :
    ImageFormat(ImageFormat::INDEXED, bpp), palette(std::move(pal))
{
    //cout << "--> constructing IndexedImageForamat with moved palette: " << toString() << endl;
}

IndexedImageFormat::IndexedImageFormat(const IndexedImageFormat &format) :
    ImageFormat(format), palette(format.palette->clone())
{
    //cout << "-c> copy-constructing IndexedImageForamat with cloned palette: " << toString() << endl;
}

IndexedImageFormat::~IndexedImageFormat()
{
    //cout << "!!! destructing IndexedImageForamat, bpp=" << getColorDepth() << endl;
}

ImageFormat & IndexedImageFormat::operator=(const ImageFormat &format)
{
    assert(typeid(format) == typeid(*this));
    return this->operator=(dynamic_cast<const IndexedImageFormat &>(format));
}

IndexedImageFormat & IndexedImageFormat::operator=(const IndexedImageFormat &format)
{
    if (this != &format)
    {
        ImageFormat::operator=(format);
        palette = format.palette->clone();
    }
    return *this;
}

bool IndexedImageFormat::operator==(const ImageFormat &format) const
{
    try {
        const IndexedImageFormat &iif = dynamic_cast<const IndexedImageFormat &>(format);
        return *palette == *iif.palette && ImageFormat::operator==(format);
    } catch (const bad_cast &ex) {
    }
    return false;
}

unique_ptr<ImageFormat> IndexedImageFormat::clone() const
{
    return unique_ptr<ImageFormat>(new IndexedImageFormat(*this));
}

string IndexedImageFormat::toString() const
{
    return ToStringBuilder("IndexedImageFormat")
        .add("ImageFormat", ImageFormat::toString())
        .add("palette", palette->toString())
        .toString();
}
