/* ColorPalette.cpp */

#include <stdint.h>                 // uint8_t
#include <sys/stat.h>               // stat
#include <cmath>                    // pow
#include <fstream>                  // ios, ifstream
#include <iostream>                 // cout, endl
#include <map>                      // map
#include <memory>                   // unique_ptr
#include <string>                   // string
#include "Exception.h"              // IndexException, FileException
#include "image/Color.h"            // Color
#include "image/ColorPalette.h"     // ColorPalette, StandardPaletteType
#include "util/ToStringBuilder.h"   // ToStringBuilder

using namespace std;
using darkcore::Exception;
using darkcore::IndexException;
using darkcore::image::Color;
using darkcore::image::ColorPalette;
using darkcore::image::StandardPaletteType;
using darkcore::util::ToStringBuilder;

map<StandardPaletteType,ColorPalette> ColorPalette::STD_PALETTE_MAP = map<StandardPaletteType,ColorPalette>();

ColorPalette::ColorPalette(int bpp, int transparent) :
    colorData(0, Color::BLACK), transparent(transparent)
{
    if (bpp > 8)
        throw IndexException("Indexed color palettes cannot exceed 8 bpp", 0, 8, bpp);
    else if (bpp < 0)
        throw IndexException("Indexed color palettes cannot have negative bpp ", 0, 8, bpp);

    int numColors = (int) pow((float) 2, bpp);
    //cout << "creating ColorPalette with " << numColors << " entries" << endl;
    colorData.resize(numColors, Color::BLACK);
}

ColorPalette::ColorPalette(const ColorPalette &pal) :
    colorData(pal.colorData), transparent(pal.transparent)
{
}

ColorPalette::~ColorPalette()
{
}

ColorPalette ColorPalette::loadVGAPalette(const string &filename)
{
    ColorPalette vgapal(8);
    int palDataSize = vgapal.getNumColors() * 3; // 1 byte each for RGB for each color

    // stat file
    struct stat filestat;
    if (stat(filename.c_str(), &filestat) != 0)
        throw FileException("could not stat file", filename);

    if (filestat.st_size < palDataSize)
        throw FileException("VGA PAL file does not meet expected VGA PAL file size", filename);

    ifstream file;
    file.open(filename.c_str(), ios::in | ios::binary);

    if (! file)
        throw FileException("could not open file for reading", filename);

    // create storage space for data
    char rawdata[palDataSize];

    // read data from file
    file.read(rawdata, palDataSize);

    // close file
    file.close();

    for (int i = 0, j = 0; j < vgapal.getNumColors(); j++)
    {
        char r = convertVgaPalColor(rawdata[i++]);
        char g = convertVgaPalColor(rawdata[i++]);
        char b = convertVgaPalColor(rawdata[i++]);
        vgapal.setColor(j, Color(r, g, b));
    }

    return vgapal;
}

uint8_t ColorPalette::convertVgaPalColor(char color)
{
    bool shift1 = false;

    // check if last bit is a 1
    if (color % 2 > 0)
        shift1 = true;

    // shift
    color = color << 2;

    // add 3 to simulate shifting in 1 bits 
    if (shift1)
        color += 3;

    return color;
}

/* ColorPalette ColorPalette::getCGACompositePalette()
{
    ColorPalette pal(4);

    // This is the palette I used for the U3 CGA composite driver.
    // It's not perfect (they're all standard EGA colors and some are duplicated),
    // but it's a start.
    // 00,03,01,09,06,02,08,0b,04,07,05,0b,0c,0e,0e,0f

    pal.setColor(0, Color::BLACK);
    pal.setColor(1, Color::CYAN);
    pal.setColor(2, Color::BLUE);
    pal.setColor(3, Color::LT_BLUE);
    pal.setColor(4, Color::BROWN);
    pal.setColor(5, Color::GREEN);
    pal.setColor(6, Color::DK_GRAY);
    pal.setColor(7, Color::LT_CYAN);
    pal.setColor(8, Color::RED);
    pal.setColor(9, Color::GRAY);
    pal.setColor(10, Color::MAGENTA);
    pal.setColor(11, Color::LT_CYAN);
    pal.setColor(12, Color::LT_RED);
    pal.setColor(13, Color::YELLOW);
    pal.setColor(14, Color::YELLOW);
    pal.setColor(15, Color::WHITE);

    return pal;
} */

ColorPalette & ColorPalette::getStandardPalette(StandardPaletteType type)
{
    if (STD_PALETTE_MAP.empty())
        initStdPaletteMap();

    map<StandardPaletteType,ColorPalette>::iterator it = STD_PALETTE_MAP.find(type);
    if (it == STD_PALETTE_MAP.end())
        throw Exception("No standard palette available for requested type");
    return it->second;
}

void ColorPalette::initStdPaletteMap()
{
    StandardPaletteType types[] = { MONO, CGA, EGA, VGA };
    for (int i = 0; i < 4; i++)
    {
        StandardPaletteType type = types[i];

        ColorPalette pal(type);

        int vgaIdx = 16;
        switch (type)
        {
            case VGA:
                // the first 16 colors are from the EGA standard,
                // so don't break when done

                // 16-31 is greyscale
                for (int x = 0; x < 256; x += (x == 0 ? 15 : 16))
                    pal.setColor(vgaIdx++, Color(x, x, x));

                // 32-247 are 9 sets of 24-color wheels
                buildVGAColorWheel(pal, vgaIdx, 0, 255, 51);
                buildVGAColorWheel(pal, vgaIdx, 127, 255, 32);
                buildVGAColorWheel(pal, vgaIdx, 183, 255, 18);
                buildVGAColorWheel(pal, vgaIdx, 0, 112, 28);
                buildVGAColorWheel(pal, vgaIdx, 56, 112, 14);
                buildVGAColorWheel(pal, vgaIdx, 80, 112, 8);
                buildVGAColorWheel(pal, vgaIdx, 0, 64, 16);
                buildVGAColorWheel(pal, vgaIdx, 32, 64, 8);
                buildVGAColorWheel(pal, vgaIdx, 48, 64, 4); // or 44, 64, 5 ?

                // no break - intentional

            case EGA:
                pal.setColor(0, Color::BLACK);
                pal.setColor(1, Color::BLUE);
                pal.setColor(2, Color::GREEN);
                pal.setColor(3, Color::CYAN);
                pal.setColor(4, Color::RED);
                pal.setColor(5, Color::MAGENTA);
                pal.setColor(6, Color::BROWN);
                pal.setColor(7, Color::GRAY);
                pal.setColor(8, Color::DK_GRAY);
                pal.setColor(9, Color::LT_BLUE);
                pal.setColor(10, Color::LT_GREEN);
                pal.setColor(11, Color::LT_CYAN);
                pal.setColor(12, Color::LT_RED);
                pal.setColor(13, Color::LT_MAGENTA);
                pal.setColor(14, Color::YELLOW);
                pal.setColor(15, Color::WHITE);
                break;

            case CGA:
                pal.setColor(0, Color::BLACK);
                pal.setColor(1, Color::CYAN);
                pal.setColor(2, Color::MAGENTA);
                pal.setColor(3, Color::GRAY);
                break;

            case MONO:
                pal.setColor(0, Color::BLACK);
                pal.setColor(1, Color::WHITE);
                break;
        }

        STD_PALETTE_MAP[type] = pal;
    }
}

void ColorPalette::buildVGAColorWheel(ColorPalette &pal, int &idx, int start, int end, int interval)
{
    int r = start, g = start, b = end;  // start at blue
    for ( ; r < end; r += interval)     // increment red (ends at magenta)
        pal.setColor(idx++, Color(r, g, b));
    for ( ; b > start; b -= interval)   // decrement blue (ends at red)
        pal.setColor(idx++, Color(r, g, b));
    for ( ; g < end; g += interval)     // increment green (ends at yellow)
        pal.setColor(idx++, Color(r, g, b));
    for ( ; r > start; r -= interval)   // decrement red (ends at green)
        pal.setColor(idx++, Color(r, g, b));
    for ( ; b < end; b += interval)     // increment blue (ends at cyan)
        pal.setColor(idx++, Color(r, g, b));
    for ( ; g > start; g -= interval)   // decrement green (ends at blue)
        pal.setColor(idx++, Color(r, g, b));
}

ColorPalette & ColorPalette::operator=(const ColorPalette &pal)
{
    if (this != &pal)
    {
        colorData = pal.colorData;
        transparent = pal.transparent;
    }
    return *this;
}

bool ColorPalette::operator==(const ColorPalette &pal) const
{
    return colorData == pal.colorData && transparent == pal.transparent;
}

bool ColorPalette::operator!=(const ColorPalette &pal) const
{
    return ! (*this == pal);
}

string ColorPalette::toString() const
{
    return ToStringBuilder("ColorPalette")
        .add("colorData", colorData)
        .add("transparent", transparent)
        .toString();
}

unique_ptr<ColorPalette> ColorPalette::clone() const
{
    return unique_ptr<ColorPalette>(new ColorPalette(*this));
}