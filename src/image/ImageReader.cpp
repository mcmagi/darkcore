/* ImageReader.cpp */

#include <istream>                      // std::istream
#include <memory>                       // std::unique_ptr, std::shared_ptr, std::move
#include <string>                       // std::string
#include "image/Image.h"                // Image (needed for compilation)
#include "image/ImageFormat.h"          // ImageFormat
#include "image/ImageIO.h"              // ImageReader
#include "image/ImageSet.h"             // ImageSet
#include "util/FileReader.h"            // FileReader

using namespace std;
using darkcore::image::ImageFormat;
using darkcore::image::ImageReader;
using darkcore::image::ImageSet;
using darkcore::util::FileReader;

ImageReader::ImageReader(const string &filename, int width, int height, int numImages, shared_ptr<ImageFormat> format) :
    FileReader<unique_ptr<Image> >(filename), ImageIO(width, height, numImages, format)
{
}

ImageReader::ImageReader(istream &is, int width, int height, int numImages, shared_ptr<ImageFormat> format) :
    FileReader<unique_ptr<Image> >(is), ImageIO(width, height, numImages, format)
{
}

ImageReader::ImageReader(unique_ptr<istream> is, int width, int height, int numImages, shared_ptr<ImageFormat> format) :
    FileReader<unique_ptr<Image> >(std::move(is)), ImageIO(width, height, numImages, format)
{
}

ImageReader::~ImageReader()
{
}

unique_ptr<ImageSet> ImageReader::readAll()
{
    unique_ptr<ImageSet> images(new ImageSet(getWidth(), getHeight(), getImageFormatPtr(), getNumImages()));

    //cout << "reading " << getNumImages() << " into imageset" << endl;
    for (int i = 0; i < getNumImages(); i++)
        images->setImage(i, read());

    // calling fcn will own ImageSet
    return images;
}