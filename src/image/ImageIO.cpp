/* ImageIO.cpp */

#include <memory>                       // std::shared_ptr
#include "image/ImageFormat.h"          // ImageFormat
#include "image/ImageIO.h"              // ImageIO

using namespace std;
using darkcore::image::ImageFormat;
using darkcore::image::ImageIO;

ImageIO::ImageIO() :
    width(0), height(0), num_images(0), format()
{
}

ImageIO::ImageIO(int width, int height, int numImages, shared_ptr<ImageFormat> format) :
    width(width), height(height), num_images(numImages), format(format)
{
}

ImageIO::ImageIO(const ImageIO &io) :
    width(io.width), height(io.height), num_images(io.num_images), format(io.format)
{
}

ImageIO::~ImageIO()
{
}

void ImageIO::setImageFormat(const ImageFormat &format)
{
    this->format = shared_ptr<ImageFormat>(format.clone());
}

void ImageIO::setImageFormatPtr(shared_ptr<ImageFormat> format)
{
    this->format = format;
}