/* UltimaImageReader.cpp */

#include <iostream>
#include <istream>                      // std::ios, std::istream
#include <memory>                       // std::unique_ptr
#include <string>                       // std::string
#include <vector>                       // std::vector
#include "Exception.h"                  // Exception
#include "config/IndexConfig.h"         // IndexConfig
#include "config/IndexEntry.h"          // IndexEntry
#include "config/UltimaFileConfig.h"    // UltimaFileConfig
#include "config/UltimaImageConfig.h"   // UltimaImageConfig
#include "image/Color.h"                // Pixel
#include "image/Image.h"                // Image
#include "image/ImageIO.h"              // ImageReader
#include "image/ImageFormat.h"          // ImageFormat
#include "image/UltimaImageReader.h"    // UltimaImageReader
#include "util/Reader.h"                // Reader

using namespace std;
using darkcore::Exception;
using darkcore::config::IndexEntry;
using darkcore::config::UltimaImageConfig;
using darkcore::image::Image;
using darkcore::image::ImageFormat;
using darkcore::image::ImageReader;
using darkcore::image::UltimaImageReader;
using darkcore::util::Reader;

UltimaImageReader::UltimaImageReader(const UltimaImageConfig &imgCfg) :
    ImageReader(imgCfg.getFileConfig().newInputStream(), imgCfg.getWidth(), imgCfg.getHeight(),
                imgCfg.getNumImages(), imgCfg.getImageFormat().clone()),
    imgCfg(imgCfg),
    indexReader(imgCfg.getIndexConfig() != NULL ? imgCfg.getIndexConfig()->newEntryReader() : unique_ptr<Reader<IndexEntry> >())
{
    cout << "opening file=" << imgCfg.getFileConfig().getFile().getFullName() << ", isCompressed=" << imgCfg.getFileConfig().isCompressed() << endl;

    // advance the stream to the offset
    if (imgCfg.getFileConfig().getOffset() > 0)
        getInputStream().seekg(imgCfg.getFileConfig().getOffset(), ios::beg);
}

UltimaImageReader::~UltimaImageReader()
{
}

unique_ptr<Image> UltimaImageReader::read()
{
    // calculate default data size and mask type
    int rawdata_size = getNumPixels() * getImageFormat().getColorDepth() / 8;
    IndexEntry::MaskType type = IndexEntry::NORMAL;

    // add 384 bytes if in Interlaced IBMCGA foramt
    if (imgCfg.isInterlaced() && imgCfg.isIBMCGA())
        rawdata_size += 384;

    // skip to next offset if images are indexed
    if (indexReader.get() != NULL)
    {
        //cout << "reading index" << endl;
        IndexEntry entry = indexReader->read();
        getInputStream().seekg(imgCfg.getFileConfig().getOffset() + entry.getOffset(), ios::beg);
        //cout << "reading img @ offset " << entry.getOffset() << ", type=" << entry.getMaskType() << endl;

        type = entry.getMaskType();
    }

    // if type is pixel block, get the actual size
    if (type == IndexEntry::U6VGA_PIXEL_BLOCK || type == IndexEntry::U6CGA_PIXEL_BLOCK)
    {
        rawdata_size = 0;
        getInputStream().read((char *) &rawdata_size, 1); // hack
        rawdata_size *= 16; // size was in paragraphs, so get # of bytes
    }

    vector<unsigned char> rawdata(rawdata_size);

    // read raw data from file
    //cout << "reading image data size = " << rawdata_size << endl;
    getInputStream().read((char *) &rawdata[0], rawdata_size);

    if (getInputStream().gcount() != rawdata_size)
    {
        throw Exception() << "Insufficient data in input stream to load an image; expected="
            << rawdata_size << ", got=" << getInputStream().gcount();
    }

    // transform raw data into image data
    unique_ptr<Image> img(new Image(getWidth(), getHeight(), getImageFormatPtr()));

    if (type == IndexEntry::U6VGA_PIXEL_BLOCK)
        buildPixelBlockImageData(img->getImageData(), rawdata);
    else if (type == IndexEntry::U6CGA_PIXEL_BLOCK)
        buildCGAPixelBlockImageData(img->getImageData(), rawdata);
    else
        buildImageData(img->getImageData(), rawdata);

    //img->dump(); // DEBUGGING
    return img;
}

void UltimaImageReader::buildImageData(vector<Pixel> &imgdata, const vector<unsigned char> &rawdata) const
{
    int pix_idx = 0;                // index of pixel in image data
    int byte_idx = 0;               // index of byte in raw data
    int row_idx;                    // index row in image

    int rawPitch = getWidth() * getImageFormat().getColorDepth() / 8;

    // loop through all bytes in data
    for (row_idx = 0; row_idx < getHeight(); row_idx++)
    {
        unpackRow(&imgdata[pix_idx], &rawdata[byte_idx], getWidth());

        // advance to next row in rawdata
        byte_idx += rawPitch;

        // TODO: some duplicated checks b/w this and getPixelFromRow()
        // Interlaced IBMCGA: 192 bytes of padding between each set of scanlines
        if (imgCfg.isInterlaced() && imgCfg.isIBMCGA() && row_idx == getHeight() / 2 - 1)
            byte_idx += 192;

        // calculate pixel offset of next row in raw data
        pix_idx = getPixelFromRow(pix_idx, row_idx);
    }

    //cout << "byte idx=" << byte_idx << endl;
}

void UltimaImageReader::buildCGAPixelBlockImageData(vector<Pixel> &imgdata, const vector<unsigned char> &rawdata) const
{
    //cout << "in buildCGAPixelBlockImageData(): rawdata_size=" << rawdata.size() << ",size=" << getNumPixels() << endl;

    int pix_idx = 0; // index into pixel data
    int byte_idx = 0; // index into raw data
    int type2 = 0, type3 = 0, type4 = 0;
    int blocklength = 0;
    int displacement = 0;

    do
    {
        // reset everything to 0
        displacement = blocklength = type2 = type3 = type4 = 0;

        // read in descriptor data (displacement, block length)
        while (true)
        {
            int byte = rawdata[byte_idx++];

            // HO nybble specifies the "type" of descriptor data
            int type = byte / 16;

            //cout << "type=" << type << ",byte=" << byte << endl;

            // Types 2 and 3 specify a displacement into the image.
            // I'm not 100% sure of the difference, but in most cases
            // type 3 appears to be a displacement on the same row,
            // while type 2 appears to be a displacement that carries
            // into the next row.  Type 2 is always followed by a type 4
            // which indicates a vertical displacement.
            // For example, tile #301 (fountain/spring) @ offset 0x4d50.
            if (type == 2 || type == 3)
            {
                displacement = byte & 0x0F;

                // this is only for debugging
                if (type == 2)
                    type2 = byte & 0x0F;
                else if (type == 3)
                    type3 = byte & 0x0F;
            }
            else if (type == 4)
            {
                // type 4 is a kind of vertical offset,
                // but there are cases where we have to ignore it
                int yOff = type4 = byte & 0x0F;

                // ignore 1 count of type4 if:
                //  * we have a type2 displacement
                //  * or if we're already at the beginning of the next row
                //    * (unless we're at the very beginning of the image)
                if (type2 > 0 || (pix_idx % getWidth() == 0 && pix_idx > 0))
                    yOff--;

                displacement += getWidth() * yOff;
            }
            else
            {
                // "types" 0 & 1 are not really types at all
                // but actually the block length in pixels
                blocklength = byte;

                // once we get the length, the data will begin
                // so break out of the loop to process the data
                break;
            }
        }

        //cout << "type2=" << type2 << ",type3=" << type3 << ",type4=" << type4 << ",pix_idx=" << pix_idx << ",displacement=" << displacement << ",blocklength=" << blocklength << endl;

        // write the transparent pixel over the length of the displacement
        for (int i = 0; i < displacement; i++)
            imgdata[pix_idx++] = 0; // black for now

        if (blocklength > 0)
        {
            unpackRow(&imgdata[pix_idx], &rawdata[byte_idx], blocklength);

            // advance counters
            int bitOffset = blocklength * getImageFormat().getColorDepth();
            byte_idx += bitOffset / 8;
            if (bitOffset % 8 > 0)
                byte_idx++;
            pix_idx += blocklength;
        }
    }
    while (blocklength > 0);

    // write transparent pixel until the end of the image
    int size = getNumPixels();
    while (pix_idx < size)
        imgdata[pix_idx++] = 0; // black for now

    //cout << "byte_idx=" << byte_idx << ",pix_idx=" << pix_idx << endl;
}

void UltimaImageReader::buildPixelBlockImageData(vector<Pixel> &imgdata, const vector<unsigned char> &rawdata) const
{
    //cout << "in buildPixelBlockImageData(): rawdata_size=" << rawdata.size() << ",size=" << getNumPixels() << endl;

    int pix_idx = 0; // index into pixel data
    int byte_idx = 0; // index into raw data
    int displacement = 0;
    int blocklength = 0;

    do
    {
        // get displacement (LO byte then HO byte)
        displacement = rawdata[byte_idx++];
        displacement += rawdata[byte_idx++] * 256;

        // get block length
        blocklength = rawdata[byte_idx++];

        // calculate actual displacement into image data (176 is the width of U6's buffer)
        // see http://nodling.nullneuron.net/ultima/text/ultima6/u6tech.txt
        int x = displacement % 176;
        int y = displacement / 176;
        if (x > imgCfg.getWidth()) // wrap x properly to the next line
            x = x - 176 + imgCfg.getWidth();
        int imgOffset = y * imgCfg.getWidth() + x;

        //cout << "displacement=" << displacement << ",blocklength=" << blocklength << ",x=" << x << ",y=" << y << ",imgOffset=" << imgOffset << endl;

        // write the transparent pixel over the length of the displacement
        for (int i = 0; i < imgOffset; i++)
            imgdata[pix_idx++] = 0xFF;

        if (blocklength > 0)
        {
            unpackRow(&imgdata[pix_idx], &rawdata[byte_idx], blocklength);

            // advance counters
            byte_idx += blocklength;
            pix_idx += blocklength * 8 / getImageFormat().getColorDepth();
        }
    }
    while (blocklength > 0);

    // write transparent pixel until the end of the image
    int size = getNumPixels();
    while (pix_idx < size)
        imgdata[pix_idx++] = 0xFF;

    //cout << "byte_idx=" << byte_idx << ",pix_idx=" << pix_idx << endl;
}

void UltimaImageReader::unpackRow(Pixel *row, const unsigned char *rawdata, int width) const
{
    // check if this is a bitmasked image
    if (imgCfg.isBitMasked())
        unpackBitMaskedRow(row, rawdata, width);
    else
        unpackSequentialRow(row, rawdata, width);
}

void UltimaImageReader::unpackSequentialRow(Pixel *row, const unsigned char *rawdata, int width) const
{
    unsigned char orig_mask, mask;  // bitmask
    int col_idx = 0;                // index of column (pixel) in row
    int bit_idx = 0;                // index of bit in byte
    int byte_idx = 0;               // index of byte in raw data
    int firstpix_idx;               // index of first pixel in byte

    int depth = getImageFormat().getColorDepth();

    // find the bit number of the first pixel in the byte
    firstpix_idx = 8 - depth;

    // calculate bitmask (bpp^2 - 1)
    orig_mask = (1 << depth) - 1;

    // shift mask to leftmost pixel
    orig_mask <<= firstpix_idx;

    // loop for all columns (pixels) in row
    for (byte_idx = 0; col_idx < width; byte_idx++)
    {
        // reset mask
        mask = orig_mask;

        // loop through all pixels in the byte
        for (bit_idx = firstpix_idx; bit_idx >= 0 && col_idx < width; bit_idx -= depth)
        {
            // mask out pixel from rawdata
            unsigned char datum = rawdata[byte_idx] & mask;

            // adjust pixel data to lowest order bits, increment column
            row[col_idx++] = datum >> bit_idx;

            // leftshift mask to next pixel in row
            mask >>= depth;
        }
    }
}

void UltimaImageReader::unpackBitMaskedRow(Pixel *row, const unsigned char *rawdata, int width) const
{
    unsigned char mask;             // used to mask out bit in data
    unsigned char colormask = 1;    // used to add color to pixel
    int master_bit_idx = 0;         // index of bit in raw data
    int byte_idx = 0;               // index of byte in raw data
    int bit_idx;                    // index of bit within byte
    int col_idx;                    // index of column (pixel) in row
    int color;                      // current color number

    // loop for each color
    for (color = 0; color < getImageFormat().getColorDepth(); color++)
    {
        // loop through all pixels (columns) in row
        for (col_idx = 0; col_idx < width; col_idx++, master_bit_idx++)
        {
            // initialize pixel to 0
            if (color == 0)
                row[col_idx] = 0;

            // find the byte to pull the bit out of
            byte_idx = master_bit_idx / 8;

            // find the offset of the bit within the byte
            bit_idx = 7 - master_bit_idx % 8;

            // and transform it into a mask (2 ^^ bit_idx)
            mask = 1 << bit_idx;

            // check if the bit is set
            if (rawdata[byte_idx] & mask)
                // or color into pixel
                row[col_idx] |= colormask;
        }

        // leftshift mask to next color
        colormask <<= 1;
    }
}

int UltimaImageReader::getPixelFromRow(int pix_idx, int row_idx) const
{
    if (imgCfg.isInterlaced())
    {
        // interlaced: even rows first, then odd rows

        // check if row number is at midpoint
        if (row_idx == imgCfg.getHeight() / 2 - 1)
            // midpoint: swap to odd rows
            pix_idx = imgCfg.getWidth();
        else
            // advance row by two
            pix_idx += imgCfg.getWidth() * 2;
    }
    else
    {
        // non-interlaced: advance to next row, sequentially
        pix_idx += imgCfg.getWidth();
    }

    return pix_idx;
}
