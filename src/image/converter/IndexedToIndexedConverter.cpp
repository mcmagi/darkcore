/* IndexedToIndexedConverter.cpp */

#include <map>                                      // map, pair
#include <memory>                                   // unique_ptr
#include <vector>                                   // vector
#include "image/Color.h"                            // Pixel
#include "image/ColorPalette.h"                     // ColorPalette
#include "image/Image.h"                            // Image
#include "image/ImageFormat.h"                      // IndexedImageFormat
#include "image/converter/ImageConverter.h"         // ImageConverter
#include "image/converter/ImageFormatConverter.h"   // ImageFormatConverter, IndexedToIndexedConverter

using namespace std;
using darkcore::image::Color;
using darkcore::image::ColorPalette;
using darkcore::image::Image;
using darkcore::image::IndexedImageFormat;
using darkcore::image::Pixel;
using darkcore::image::converter::ImageFormatConverter;
using darkcore::image::converter::IndexedToIndexedConverter;

IndexedToIndexedConverter::IndexedToIndexedConverter(
        const IndexedImageFormat &srcFormat, const IndexedImageFormat &tgtFormat) :
    ImageFormatConverter(srcFormat, tgtFormat)
{
}

IndexedToIndexedConverter::IndexedToIndexedConverter(
        const IndexedToIndexedConverter &conv) :
    ImageFormatConverter(conv)
{
}

IndexedToIndexedConverter::~IndexedToIndexedConverter()
{
}

unique_ptr<Image> IndexedToIndexedConverter::convert(const Image &img, const vector<Pixel> &data) const
{
    const IndexedImageFormat &fmt1 = static_cast<const IndexedImageFormat &>(getSourceImageFormat());
    const IndexedImageFormat &fmt2 = static_cast<const IndexedImageFormat &>(getTargetImageFormat());
    const ColorPalette &pal1 = fmt1.getColorPalette();
    const ColorPalette &pal2 = fmt2.getColorPalette();

    // we're not smart enough to approximate colors,
    // so just map by exact colors.  everything else will go black!

    // build a reverse lookup index for the target color palette
    map<Color,int> tgtColorMap = map<Color,int>();
    for (int i = 0; i < pal2.getNumColors(); i++)
    {
        Color c = pal2.getColor(i);
        tgtColorMap[c] = i;
        //tgtColorMap.insert(pair<Color,int>(c, i));
    }

    int length = data.size();
    vector<Pixel> target(length);
    for (int i = 0; i < length; i++)
    {
        // get color for source index
        Color c = pal1.getColor(data[i]);

        // get target index for color
        target[i] = tgtColorMap[c];
    }

    return unique_ptr<Image>(new Image(img.getWidth(), img.getHeight(), fmt2, target));
}