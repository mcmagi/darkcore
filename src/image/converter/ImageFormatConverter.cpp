/* ImageFormatConverter.cpp */

#include <memory>                                   // unique_ptr
#include "Exception.h"                              // Exception
#include "image/ImageFormat.h"                      // ImageFormat, IndexedImageFormat, RGBImageFormat
#include "image/converter/ImageConverter.h"         // ImageConverter
#include "image/converter/ImageFormatConverter.h"   // ImageFormatConverter, IndexedToIndexedConverter, IndexedToRGBConverter, RGBToRGBConverter

using namespace std;
using darkcore::Exception;
using darkcore::image::ImageFormat;
using darkcore::image::IndexedImageFormat;
using darkcore::image::RGBImageFormat;
using darkcore::image::converter::ImageConverter;
using darkcore::image::converter::ImageFormatConverter;
using darkcore::image::converter::IndexedToIndexedConverter;
using darkcore::image::converter::IndexedToRGBConverter;
using darkcore::image::converter::RGBToRGBConverter;

ImageFormatConverter::ImageFormatConverter(
        const ImageFormat &srcFormat, const ImageFormat &tgtFormat) :
    ImageConverter(), srcFormat(&srcFormat), tgtFormat(&tgtFormat)
{
}

ImageFormatConverter::ImageFormatConverter(
        const ImageFormatConverter &conv) :
    ImageConverter(conv), srcFormat(conv.srcFormat), tgtFormat(conv.tgtFormat)
{
}

ImageFormatConverter::~ImageFormatConverter()
{
}

unique_ptr<ImageFormatConverter> ImageFormatConverter::getDefaultConverter(
        const ImageFormat &srcFormat, const ImageFormat &tgtFormat)
{
    ImageFormatConverter *converter = NULL;

    if (srcFormat.getFormatType() == ImageFormat::INDEXED)
    {
        if (tgtFormat.getFormatType() == ImageFormat::INDEXED)
        {
            converter = new IndexedToIndexedConverter(
                static_cast<const IndexedImageFormat &>(srcFormat),
                static_cast<const IndexedImageFormat &>(tgtFormat));
        }
        else
        {
            converter = new IndexedToRGBConverter(
                static_cast<const IndexedImageFormat &>(srcFormat),
                static_cast<const RGBImageFormat &>(tgtFormat));
        }
    }
    else // RGB
    {
        if (tgtFormat.getFormatType() == ImageFormat::INDEXED)
            throw Exception("No default converter for RGB to Indexed image formats");
            //converter = new RGBToIndexedConverter((RGBImageFormat &) srcFormat, (IndexedImageFormat &) tgtFormat)
        else
        {
            converter = new RGBToRGBConverter(
                static_cast<const RGBImageFormat &>(srcFormat),
                static_cast<const RGBImageFormat &>(tgtFormat));
        }
    }

    return unique_ptr<ImageFormatConverter>(converter);
}