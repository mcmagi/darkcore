/* ZoomConverter.cpp */

#include <cstring>                                  // memcpy
#include <memory>                                   // unique_ptr
#include <vector>                                   // vector
#include "image/Color.h"                            // Pixel
#include "image/Image.h"                            // Image
#include "image/converter/ImageConverter.h"         // ImageConverter
#include "image/converter/ZoomConverter.h"          // ZoomConverter

using namespace std;
using darkcore::image::Image;
using darkcore::image::Pixel;
using darkcore::image::converter::ImageConverter;
using darkcore::image::converter::ZoomConverter;

ZoomConverter::ZoomConverter(int zoomLevel) :
    ImageConverter(), zoomLevel(zoomLevel)
{
}

ZoomConverter::ZoomConverter(const ZoomConverter &conv) :
    ImageConverter(conv), zoomLevel(conv.zoomLevel)
{
}

ZoomConverter::~ZoomConverter()
{
}

unique_ptr<Image> ZoomConverter::convert(const Image &img, const vector<Pixel> &data) const
{
    int newSize = data.size() * zoomLevel * zoomLevel;
    vector<Pixel> target(newSize);

    int tgtIdx = 0;
    for (int y = 0; y < img.getHeight(); y++)
    {
        // capture pointer to start index of row
        const Pixel *rowStart = &target[tgtIdx];

        // output row once
        for (int x = 0; x < img.getWidth(); x++)
        {
            //printf("tgtIdx=%d, y=%d, x=%d\n", tgtIdx, y, x); // DEBUG
            Pixel p = img.getPixel(x, y);
            for (int z = 0; z < zoomLevel; z++)
                target[tgtIdx++] = p;
        }

        // copy row 'zoom-1' more times
        for (int z = 1; z < zoomLevel; z++, tgtIdx += img.getWidth() * zoomLevel)
            memcpy(&target[tgtIdx], rowStart, img.getPitch() * zoomLevel);
    }

    return unique_ptr<Image>(new Image(img.getWidth() * zoomLevel, img.getHeight() * zoomLevel, img.getImageFormat(), target));
}