/* RGBToRGBConverter.cpp */

#include <memory>                                   // unique_ptr
#include <vector>                                   // vector
#include "image/Color.h"                            // Color, Pixel
#include "image/ColorMask.h"                        // ColorMask
#include "image/Image.h"                            // Image
#include "image/ImageFormat.h"                      // RGBImageFormat
#include "image/converter/ImageConverter.h"         // ImageConverter
#include "image/converter/ImageFormatConverter.h"   // ImageFormatConverter, RGBToRGBConverter

using namespace std;
using darkcore::image::Color;
using darkcore::image::ColorMask;
using darkcore::image::Image;
using darkcore::image::Pixel;
using darkcore::image::RGBImageFormat;
using darkcore::image::converter::ImageFormatConverter;
using darkcore::image::converter::RGBToRGBConverter;

RGBToRGBConverter::RGBToRGBConverter(
        const RGBImageFormat &srcFormat, const RGBImageFormat &tgtFormat) :
    ImageFormatConverter(srcFormat, tgtFormat)
{
}

RGBToRGBConverter::RGBToRGBConverter(
        const RGBToRGBConverter &conv) :
    ImageFormatConverter(conv)
{
}

RGBToRGBConverter::~RGBToRGBConverter()
{
}

unique_ptr<Image> RGBToRGBConverter::convert(const Image &img, const vector<Pixel> &data) const
{
    const RGBImageFormat &fmt1 = static_cast<const RGBImageFormat &>(getSourceImageFormat());
    const RGBImageFormat &fmt2 = static_cast<const RGBImageFormat &>(getTargetImageFormat());
    const ColorMask &mask1 = fmt1.getColorMask();
    const ColorMask &mask2 = fmt2.getColorMask();

    int length = data.size();
    vector<Pixel> target(length);
    for (int i = 0; i < length; i++)
    {
        // get color for rgb pixel value
        Color c = mask1.getColor(data[i]);

        // get rgb pixel value for color
        target[i] = mask2.getRGBValue(c);
    }

    return unique_ptr<Image>(new Image(img.getWidth(), img.getHeight(), fmt2, target));
}