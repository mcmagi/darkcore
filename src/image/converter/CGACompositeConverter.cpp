/* CGACompositeConverter.cpp */

#include <memory>                                   // unique_ptr
#include <vector>                                   // vector
#include "image/Color.h"                            // Color, Pixel
#include "image/Image.h"                            // Image
#include "image/ImageFormat.h"                      // RGBImageFormat
#include "image/converter/CGACompositeConverter.h"  // CGACompositeConverter
#include "image/converter/ImageConverter.h"         // ImageConverter

using namespace std;
using darkcore::image::Color;
using darkcore::image::Image;
using darkcore::image::Pixel;
using darkcore::image::RGBImageFormat;
using darkcore::image::converter::CGACompositeConverter;
using darkcore::image::converter::ImageConverter;

CGACompositeConverter::CGACompositeConverter(const RGBImageFormat &imgFmt) :
    ImageConverter(), imageFormat(imgFmt), compColors(16, Color::BLACK)
{
    // construct color mapping (would prefer to do this statically, but alas)
    // TODO: correct colors
    compColors[0] = Color::BLACK;           // black
    compColors[1] = Color(0, 99, 0);        // dark green
    compColors[2] = Color(0, 66, 226);      // dark blue
    compColors[3] = Color(0, 159, 253);     // medium blue
    compColors[4] = Color(77, 64, 0);       // brown
    compColors[5] = Color(0, 185, 0);       // light green
    compColors[6] = Color(119, 115, 122);   // gray 1
    compColors[7] = Color(0, 235, 145);     // aqua
    compColors[8] = Color(166, 0, 94);      // red
    compColors[9] = Color(119, 115, 122);   // gray 2
    compColors[10] = Color(209, 77, 255);   // purple
    compColors[11] = Color(153, 172, 255);  // light blue
    compColors[12] = Color(255, 68, 0);     // orange
    compColors[13] = Color(223, 196, 0);    // yellow
    compColors[14] = Color(255, 133, 240);  // pink
    compColors[15] = Color::WHITE;          // white
}

CGACompositeConverter::CGACompositeConverter(const CGACompositeConverter &conv) :
    ImageConverter(conv), imageFormat(conv.imageFormat), compColors(conv.compColors)
{
}

CGACompositeConverter::~CGACompositeConverter()
{
}

unique_ptr<Image> CGACompositeConverter::convert(const Image &img, const vector<Pixel> &data) const
{
    vector<Pixel> target(data.size());

    int srcIdx = 0;
    int tgtIdx = 0;
    for (int r = 0; r < img.getHeight(); r++)
    {
        for (int c = 0; c < img.getWidth(); c += 2)
        {
            // the strategy is:
            // * read two pixels and use as index into color map
            // * read pixels immediately before and after to determine
            //   if the pixel needs to be "stretched"

            // get pixel before the first one
            Pixel prePix = c > 0 ? data[srcIdx-1] : 0;

            // get left, right pixels
            Pixel lPix = data[srcIdx++];
            Pixel rPix = (c + 1 < img.getWidth()) ? data[srcIdx++] : 0;

            // get pixel following the last one
            Pixel postPix = (c + 2 < img.getWidth()) ? data[srcIdx] : 0;

            // get composite oolor index using (lPix,rPix) as index
            int compColorIdx = lPix << 2 | rPix;

            // lookup color and get RGB value
            Color color = compColors[compColorIdx];
            Pixel tgtPix = imageFormat.getColorMask().getRGBValue(color);

            // write out left & right pixels
            target[tgtIdx++] = (prePix > 0 || lPix > 0) ? tgtPix : 0;
            target[tgtIdx++] = (postPix > 0 || rPix > 0) ? tgtPix : 0;
        }
    }

    return unique_ptr<Image>(new Image(img.getWidth(), img.getHeight(), imageFormat, target));
}