/* IndexedToRGBConverter.cpp */

#include <cstdio>                                   // printf
#include <memory>                                   // unique_ptr
#include <vector>                                   // vector
#include "image/Color.h"                            // Color
#include "image/ColorMask.h"                        // ColorMask
#include "image/ColorPalette.h"                     // ColorPalette
#include "image/Image.h"                            // Image
#include "image/ImageFormat.h"                      // IndexedImageFormat, RGBImageFormat
#include "image/converter/ImageConverter.h"         // ImageConverter
#include "image/converter/ImageFormatConverter.h"   // ImageFormatConverter, IndexedToRGBConverter

using namespace std;
using darkcore::image::Color;
using darkcore::image::ColorMask;
using darkcore::image::ColorPalette;
using darkcore::image::Image;
using darkcore::image::IndexedImageFormat;
using darkcore::image::Pixel;
using darkcore::image::RGBImageFormat;
using darkcore::image::converter::ImageFormatConverter;
using darkcore::image::converter::IndexedToRGBConverter;

IndexedToRGBConverter::IndexedToRGBConverter(
        const IndexedImageFormat &srcFormat, const RGBImageFormat &tgtFormat) :
    ImageFormatConverter(srcFormat, tgtFormat)
{
}

IndexedToRGBConverter::IndexedToRGBConverter(
        const IndexedToRGBConverter &conv) :
    ImageFormatConverter(conv)
{
}

IndexedToRGBConverter::~IndexedToRGBConverter()
{
}

unique_ptr<Image> IndexedToRGBConverter::convert(const Image &img, const vector<Pixel> &data) const
{
    const IndexedImageFormat &fmt1 = static_cast<const IndexedImageFormat &>(getSourceImageFormat());
    const RGBImageFormat &fmt2 = static_cast<const RGBImageFormat &>(getTargetImageFormat());
    const ColorPalette &pal = fmt1.getColorPalette();
    const ColorMask &mask = fmt2.getColorMask();

    int length = data.size();
    vector<Pixel> target(length);
    for (int i = 0; i < length; i++)
    {
        // get color for index
        Color c = pal.getColor(data[i]);

        // get rgb pixel value for color
        target[i] = mask.getRGBValue(c);

        // DEBUG
        /* printf("converting [%d] to [%d]: color={r=%d,g=%d,b=%d}, mask={r=%x:%d,g=%x:%d,b=%x:%d}\n", source[i], target[i], c.getRed(), c.getGreen(), c.getBlue(),
                mask.getRedMask(), mask.getRedShift(), mask.getGreenMask(), mask.getGreenShift(), mask.getBlueMask(), mask.getBlueShift()); */
    }

    return unique_ptr<Image>(new Image(img.getWidth(), img.getHeight(), fmt2, target));
}