# Dark Core #

Dark Core is an Ultima resource and map viewer for Ultima 1-6.  It's used primarily with the [Exodus Project](https://bitbucket.org/mcmagi/ultima-exodus) to import/export graphic tiles from/to png.  My hope was that I could one day use this as a foundation to build an Ultima-inspired game engine.

### How do I get set up? ###

TBD - I don't have great instructions on this at the moment, but it compiles with GNU C.

### Who do I talk to? ###

* [Michael C. Maggio](mailto:mcmaggio@mcmagi.com) (aka Voyager Dragon)